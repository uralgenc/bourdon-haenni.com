<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

/**
 * Class ElasticsearchConnectionServiceTest
 */
class ElasticsearchConnectionServiceTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Typovision\Baumer\Service\ElasticsearchConnectionService
	 */
	protected $subject;

	/**
	 * @return void
	 */
	public function setUp() {

		$mockConfigurationManager = $this->getAccessibleMock('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager', array('getConfiguration'));
		$mockConfigurationManager->expects($this->any())->method('getConfiguration')->will($this->returnValue(array(
			'elasticsearch' => array(
				'connection' => array(
					'host' => '1.1.1.99',
					'port' => 9200,
					'transport' => 'http',
					'index' => 'es-de'
				)
			)
		)));

		$this->subject = $this->getAccessibleMock('Typovision\\Baumer\\Service\\ElasticsearchConnectionService', array('dummy'));
		$this->subject->injectConfigurationManager($mockConfigurationManager);
		$this->subject->_set('configurationManager', $mockConfigurationManager);
	}

	/**
	 * @test
	 * @return void
	 */
	public function getEsIndexWithConfigurationManager() {
		$index = $this->subject->getEsIndex();
		$this->assertInstanceOf('Elastica\Index', $index, 'Returned object is of wrong type');
	}

	/**
	 * @test
	 * @return void
	 */
	public function getEsIndexWithParameter() {
		$index = $this->subject->getEsIndex('1.1.1.99');
		$this->assertInstanceOf('Elastica\Index', $index, 'Returned object is of wrong type');
	}

	/**
	 * @test
	 * @return void
	 */
	public function checkIndexSelection() {
		$index = $this->subject->getEsIndex();
		$this->assertEquals('es-de', $index->getName(), 'Wrong index selected');
	}

	/**
	 * @test
	 * @return void
	 */
	public function getIndexStatsWithParameter() {
		$index = $this->subject->getEsIndex('1.1.1.99');
		$statsData = $index->getStats()->getData();
		$this->assertNotEmpty($statsData, 'Stats from index');
	}

}
