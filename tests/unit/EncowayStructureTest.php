<?php


class EncowayStructureTest extends \Codeception\TestCase\Test
{
	use Codeception\Specify;

	/**
	 * @var \UnitTester
	 */
	protected $tester;
	/**
	 * @var \JsonSchema\Validator
	 */
	protected $validator;
	/**
	 * @var object
	 */
	protected $schema;

	protected function _before()
	{
		$this->validator = new JsonSchema\Validator();
		$retriever = new JsonSchema\Uri\UriRetriever;
		$this->schema = $retriever->retrieve('file://' . codecept_data_dir() . 'encoway/schema.json');
		\JsonSchema\RefResolver::$maxDepth = 20;
		$refResolver = new JsonSchema\RefResolver($retriever);
		$refResolver->resolve($this->schema);
		$this->afterSpecify(function() {
			$this->validator->reset();
		});
	}

	protected function _after()
	{
	}


	public function testExample()
	{
		$this->specify('Example response matches schema', function () {
			$data = json_decode(file_get_contents(codecept_data_dir() . 'encoway/example.json'));
			$this->validator->check($data, $this->schema);
			verify_that($this->validator->isValid());
		});
	}
}
