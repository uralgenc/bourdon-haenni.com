<?php

class ContentServConnectionServiceTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	protected $defaultSettings = array(
		'extConf' => array(
			'baseUrl' => 'http://123.456.789.0',
			'username' => 'admin',
			'password' => 'foo'
		)
	);

	/**
	 * @var \Typovision\Baumer\Service\ContentServConnectionService|\PHPUnit_Framework_MockObject_MockObject
	 */
	protected $subject;

	public function setUp() {

		$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['baumer'] = $this->defaultSettings;

		$mockConfigurationManager = $this->getAccessibleMock(
			'TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager',
			array('getConfiguration')
		);
		$mockConfigurationManager->expects($this->any())->method('getConfiguration')->will($this->returnValue(array()));

		$this->subject = $this->getAccessibleMock('Typovision\\Baumer\\Service\\ContentServConnectionService', array('dummy'));
		$this->subject->_set('configurationManager', $mockConfigurationManager);
		$this->subject->initializeObject();
		$this->subject->_set('settings', $this->defaultSettings);
	}

	/**
	 * @test
	 */
	public function readingSettingsProvidesExtConfSettingsTest() {
		$this->assertArrayHasKey('extConf', $this->subject->getSettings());
	}

	/**
	 * @test
	 * @expectedException \Typovision\Baumer\Exception\UnsupportedCommandException
	 */
	public function authenticateThrowsExceptionForUnsupportedCommandTest() {
		$this->subject->authenticate('FOO');
	}

	/**
	 * @return array
	 */
	public function buildCommandValues() {
		return array(
			'command without parameters' => array(
				'foo/bar',
				array(),
				'http://123.456.789.0/foo/bar',
			),
			'command with one parameter' => array(
				'foo/bar',
				array('ctsUser' => 'admin'),
				'http://123.456.789.0/foo/bar?ctsUser=admin',
			),
			'multidimensional arrays' => array(
				'test/product/foo',
				array(
					'auth' => array(
						'test' => 'bar',
						'bla' => array(
							'x' => 'y',
						),
					),
				),
				'http://123.456.789.0/test/product/foo?auth%5Btest%5D=bar&auth%5Bbla%5D%5Bx%5D=y',
			)
		);
	}

	/**
	 * @test
	 * @dataProvider buildCommandValues
	 *
	 * @param string $command
	 * @param array $parameter
	 * @param string $expectedResult
	 */
	public function buildUrlReturnsCorrectCommandTest($command, $parameter, $expectedResult) {
		$this->assertEquals($expectedResult, $this->subject->buildUrl('http://123.456.789.0/', $command, $parameter));
	}

	/**
	 * @test
	 */
	public function runCommandReturnsArrayForValidResult() {

		$expectedResult = array(
			'Meta' => 'some values',
			'Files' => array(
				'1' => 'file 1',
				'2' => 'file 2',
			)
		);

		$this->subject = $this->getAccessibleMock('Typovision\\Baumer\\Service\\ContentServConnectionService', array('fetchJson'));
		$this->subject->expects($this->any())->method('fetchJson')->will($this->returnValue($expectedResult));

		$this->assertEquals($expectedResult, $this->subject->runCommand('testCommand', 'testAuth'));
	}

	/**
	 * @test
	 */
	public function runCommandReturnsStringForInvalidResult() {
		$expectedResult = 'An error has occured';

		$this->subject = $this->getAccessibleMock('Typovision\\Baumer\\Service\\ContentServConnectionService', array('fetchJson'));
		$this->subject->expects($this->once())->method('fetchJson')->will($this->returnValue($expectedResult));

		$this->assertEquals($expectedResult, $this->subject->runCommand('testCommand', 'testAuth'));
	}

	/**
	 * @test
	 *
	 * @expectedException \Typovision\Baumer\Exception\InvalidUrlException
	 */
	public function fetchJsonThrowsExceptionForInvalidUrl() {
		$this->subject->fetchJson('apsjdpaosjd');
	}
}
