<?php

class FileImportServiceTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Typovision\Baumer\Service\FileImportService
	 */
	protected $subject;

	public function setUp() {
		$this->subject = $this->getAccessibleMock('Typovision\\Baumer\\Service\\FileImportService', array('retrieveStorage'));

	}

	/**
	 * @test
	 * @expectedException \RuntimeException
	 */
	public function importFolderWillThrowExceptionUponMissingParameter() {
		$this->subject->importFolder();
	}

	/**
	 * @test
	 * @expectedException \RuntimeException
	 */
	public function importFolderWillThrowExceptionUponWrongParameterType() {
		$this->subject->importFolder('foo');
	}

	/**
	 * @test
	 */
	public function defaultFolderIsSetIfNoFolderIsGiven(){
		$mockResourceStorage = $this->getMock('TYPO3\\CMS\\Core\\Resource\\ResourceStorage', array(), array(), '', FALSE);
		$mockResourceStorage->expects($this->once())->method('getDefaultFolder')->will($this->returnValue($this->getMock('TYPO3\\CMS\\Core\\Resource\\Folder', array(), array(), '', FALSE)));
		$this->subject->_set('storage', $mockResourceStorage);
		$this->subject->_call('checkParentFolder');
	}

	/**
	 * @test
	 */
	public function parentFolderIsReturnedIfGiven(){
		$mockFolder = $this->getMock('TYPO3\\CMS\Core\\Resource\\Folder', array(), array(), '', FALSE);
		$this->assertEquals($mockFolder,$this->subject->_call('checkParentFolder', $mockFolder));
	}

}
