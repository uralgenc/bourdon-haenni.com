<?php

/**
 * Class EncowayServiceTest
 * @author Hans Höchtl <extern.hans.hoechtl@typovision.de>
 */
class EncowayServiceTest extends \Codeception\TestCase\Test {

	protected $settings;
	/**
	 * @var \Baumer\Baumer\Service\EncowayService
	 */
	protected $service;

	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * Initialize service for testing
	 *
	 * @throws Exception
	 */
	protected function _before() {
		$this->settings = array(
			'restEndpoint' => 'http://10.8.12.60/baumer/api/configurations/',
			'lang' => 'en',
			'connectiontimeout' => 5
		);
		$this->service = new \Baumer\Baumer\Service\EncowayService($this->settings);
		$this->service->initializeConfiguration('000000000096002532');
	}

	/**
	 * @test
	 * @throws Exception
	 */
	public function initializeConfigurationWithFullArticleName() {
		$service = new \Baumer\Baumer\Service\EncowayService( $this->settings );
		$service->initializeConfiguration( '000000000096002532' );
		$this->assertNotEmpty( $service->getConfigurationId(), 'Configuration not initialized' );
	}

	/**
	 * @test
	 * @throws Exception
	 */
	public function cancelConfiguration() {
		$service = new \Baumer\Baumer\Service\EncowayService( $this->settings );
		$service->initializeConfiguration( '000000000096002532' );
		$response = $service->cancelConfiguration();
		$this->assertEquals(200, $response);
	}

	/**
	 * @test
	 * @throws Exception
	 */
	public function initializeConfigurationWithShortArticleName() {
		$service = new \Baumer\Baumer\Service\EncowayService( $this->settings );
		$service->initializeConfiguration( '96002532' );
		$this->assertNotEmpty( $service->getConfigurationId(), 'Configuration not initialized' );
	}

	/**
	 * @test
	 * @throws Exception
	 */
	public function initializeConfigurationWithIncorrectArticleName() {
		$service = new \Baumer\Baumer\Service\EncowayService( $this->settings );
		try {
			$service->initializeConfiguration( 'FooBar' );
		} catch (Exception $e) {
			$this->assertEquals('catalogue article \'000000000000FooBar\' not found!', $e->getMessage(), 'Article not found');
		}
	}

	/**
	 * @test
	 */
	public function getParameters() {
		$response = $this->service->getParameters();
		$this->assertNotEmpty($response);
	}

	/**
	 * @test
	 */
	public function getContainers() {
		$response = $this->service->getContainers();
		$this->assertTrue(is_array($response));
	}

	/**
	 * @test
	 */
	public function getContainer() {
		$containers = $this->service->getContainers();
		$container = $this->service->getContainer($containers[0]->id);
		$this->assertInstanceOf('\stdClass', $container);
	}

	/**
	 * @test
	 */
	public function getParametersForInvalidSession() {
		$oldConfigId = $this->service->getConfigurationId();
		$this->service->setConfigurationId('FOOBAR');
		$response = $this->service->getParameters();
		$this->assertEquals(404, $response);
		$this->service->setConfigurationId($oldConfigId);
	}

	/**
	 * @test
	 */
	public function getParameter() {
		$containers = $this->service->getParameters();
		$productTab = $containers[0];
		$productParameters = $productTab->parameter;
		$randomParameter = $productParameters[rand(0,count($productParameters) - 1)];

		$response = $this->service->getParameter($randomParameter->id);
		$this->assertInstanceOf('\stdClass', $response);
	}

	/**
	 * @test
	 */
	public function getInvalidParameter() {
		$response = $this->service->getParameter('FOOBAR');
		$this->assertEquals(404, $response);
	}

	/**
	 * @test
	 */
	public function setParameterValue() {
		$containers = $this->service->getParameters();
		$productTab = $containers[0];
		$productParameters = $productTab->parameter;
		$randomParameter = $productParameters[rand(0,count($productParameters) - 1)];
		$randomValue = $randomParameter->value[rand(0,count($randomParameter->value) - 1)];

		$response = $this->service->setParameter($randomParameter->id, $randomValue->value);

		$this->assertEquals('SUCCESS', $response['status']);
	}

	/**
	 * @test
	 */
	public function unsetParameterValue() {
		$containers = $this->service->getParameters();
		$productTab = $containers[0];
		$productParameters = $productTab->parameter;
		$randomParameter = $productParameters[rand(0,count($productParameters) - 1)];
		$randomValue = $randomParameter->value[rand(0,count($randomParameter->value) - 1)];

		$this->service->setParameter($randomParameter->id, $randomValue->value);
		$response = $this->service->unsetParameter($randomParameter->id, $randomValue->value);
		$this->assertEquals(200);
	}

	/**
	 * @test
	 */
	public function resetConfiguration() {
		$oldConfigurationId = $this->service->getConfigurationId();
		$this->service->reset();
		$newConfigurationId = $this->service->getConfigurationId();
		$this->assertNotEquals($oldConfigurationId, $newConfigurationId);
	}


	/**
	 * @test
	 */
	public function heartBeat() {
		$response = $this->service->getStatus();
		$this->assertEquals(200, $response);
	}
}
