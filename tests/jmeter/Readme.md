# Performance tests for encoway API

Products under test:

* 96002532 (MEX5)
* 96002636 (MEX3)
* 96002664 (MCX7)
* 96002635 (MAN7)
* 96002820 (RRPN3)
* 96002728 (RDY5)
* 96002700 (MTA3)
* 96002683 (M31)

## Test plan

10 workers repeat the following procedure:

1. Start configuration session for a randomly selected product (from list above)
2. Get parameters for configuration
3. Select a random parameter that is flagged as not terminal and editable
4. Select a random value from the selected parameter from (3)
5. Repeat Steps 2 - 4 until no terminal parameter is left
6. Get typekey
7. Get variantkey

## Running the test

As we want the results on our local machine, we will use RMI to control the execution on the remote server.
To avoid configuration hell, we will use SSH portforwarding to solve this easily.

### Setting up the server

Java must be installed in a supported version >= 1.7.

    wget http://www.apache.org/dist/jmeter/binaries/apache-jmeter-2.6.tgz
    tar -xf apache-jmeter-2.6.tgz
    cd apache-jmeter-2.6

Edit `bin/jmeter.properties` file and add the following lines:

    server_port=55501
    server.rmi.localhostname=127.0.0.1
    server.rmi.localport=55511

Start the server to listen for incoming control sessions from our local machine.

    bin/jmeter-server -Djava.rmi.server.hostname=127.0.0.1


### Setting up the client

On your computer you should also have Java installed in a supported version >= 1.7.

Open a SSH connection to the server on which you started the `jmeter-server` above with the necessary port forwardings:

    ssh -L 55501:127.0.0.1:55501 -L 55511:127.0.0.1:55511 -R 55512:127.0.0.1:55512 baumer-app4.netways.de

Keep this terminal window open.

Now open Jmeter on your local computer with the following parameters:

    bin/jmeter -Dremote_hosts=127.0.0.1:55501 -Dclient.rmi.localport=55512 -Dmode=Batch -Dnum_sample_threshold=250 -Djava.rmi.server.hostname=127.0.0.1

Now open the testplan and you can have the option "Start on remote host 127.0.0.1:55501" available.

**Notice:** As your internet connection would limit the test if the results are transfered live, we specified a parameter,
 that only every 250 results should be transferred in batch. Therefore you will only see results after 250 samples have been
  generated or the test has been stopped / is finished.
