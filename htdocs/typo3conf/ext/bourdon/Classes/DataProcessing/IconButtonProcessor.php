<?php
namespace Baumer\Bourdon\DataProcessing;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\ContentObject\Exception\ContentRenderingException;

/**
 * Class IconButtonProcessor
 *
 * @package Baumer\Bourdon\DataProcessing
 */
class IconButtonProcessor implements DataProcessorInterface
{
    /**
     * Process data for the CType "icon_button"
     *
     * @param ContentObjectRenderer $cObj The content object renderer, which contains data of the content element
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     * @throws ContentRenderingException
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        // This will be available in fluid with {custom}
        $processedData['custom'] = $this->getOptionsFromFlexFormData($processedData['data']);
        $processedData['custom']['linkIsUrl'] = true;
        if (strpos($processedData['custom']['link'], 'data-') === 0) {
            $processedData['custom']['linkIsUrl'] = false;
        }
        return $processedData;
    }

    /**
     * @param array $row
     * @return array
     */
    protected function getOptionsFromFlexFormData(array $row)
    {
        $options = [];
        $flexFormAsArray = GeneralUtility::xml2array($row['pi_flexform']);
        if (!empty($flexFormAsArray['data']['sDEF']['lDEF']) && is_array($flexFormAsArray['data']['sDEF']['lDEF'])) {
            foreach ($flexFormAsArray['data']['sDEF']['lDEF'] as $optionKey => $optionValue) {
                $optionParts = explode('.', $optionKey);
                $options[array_pop($optionParts)] = $optionValue['vDEF'] === '1' ? true : $optionValue['vDEF'];
            }
        }

        return $options;
    }
}
