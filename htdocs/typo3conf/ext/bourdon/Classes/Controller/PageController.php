<?php
namespace Baumer\Bourdon\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Florian Schlittenbauer <fschlittenbauer@1drop.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Baumer\Baumer\Domain\Model\CsView;
use FluidTYPO3\Fluidpages\Controller\PageController as AbstractController;

/**
 * Page Controller
 *
 * @route off
 */
class PageController extends AbstractController
{

    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Connection
     * @inject
     */
    protected $elasticConnection;

    public function searchResultsAction()
    {
        $searchConfig = $this->elasticConnection->getElasticConfig(CsView::$CLASS_TYPE_MAPPING[CsView::CLASS_SELECTOR]);
        $searchConfig['ES_TYPES'] = [
            'PRODUCTS' => ['type' => $searchConfig['ES_TYPE'], 'currentPage' => 1, 'itemsPerPage' => 12, 'results' => [], 'totalItems' => 0, 'hidden' => false],
            'PAGES' => ['type' => 'PAGE', 'currentPage' => 1, 'itemsPerPage' => 12, 'results' => [], 'totalItems' => 0, 'hidden' => false],
            'FILES' => ['type' => 'FILE', 'currentPage' => 1, 'itemsPerPage' => 12, 'results' => [], 'totalItems' => 0, 'hidden' => false]
        ];
        unset($searchConfig['ES_TYPE']);

        $this->view->assign('searchConfig', json_encode($searchConfig));
        $this->view->assign('csAttributes', json_encode($this->settings['productfinder']['attributes']));
        $this->view->assign('fileAttributes', json_encode($this->settings['downloadcenter']['attributes']));
    }
}
