<INCLUDE_TYPOSCRIPT: source="DIR:EXT:bourdon/Configuration/TypoScript/Constants/" extensions="txt">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:bourdon/Configuration/TypoScript/Constants/global/" extensions="txt">

plugin.tx_bourdon.view {
  templateRootPaths.default = EXT:bourdon/Resources/Private/Templates/
  partialRootPaths.default = EXT:bourdon/Resources/Private/Partials/
  layoutRootPaths.default = EXT:bourdon/Resources/Private/Layouts/
}

plugin.tx_bourdon.settings {
  # cat=BOURDON/settings; type=int+; label=Contentserv Storage PID: Folder for imported Contentserv-Views and -Records
  pimImport.storages.productStorage = 477
  # cat=BOURDON.Contentserv/Configurator/010; type=int+; label=Online views folder: ID of the Contentserv Attribute that holds the Attribute that should be used as product image
  pimImport.CsViewsFolder = 146
  # cat=BOURDON/contentServ; type=int+; label=The Contentserv Attribute ID that contains the SAPID (KMAT)
  pimImport.kmatAttributeId = 361

  productListPage = 449

  productfinder {
    attributes {
      # cat=BOURDON.Contentserv/Pfinder/110; type=int+; label=PFinder - Filters: ID of the Contentserv Attribute that holds the Attributes to be used in filters of the pfinder
      filter = 1898
      # cat=BOURDON.Contentserv/Pfinder/120; type=int+; label=PFinder - Table columns: ID of the Contentserv Attribute that holds the Attributes to be displayed as table columns of the pfinder
      list = 1921
      # cat=BOURDON.Contentserv/Pfinder/130; type=int+; label=PFinder - Gallery attributes: ID of the Contentserv Attribute that holds the Attributes to be displayed in the gallery view of the pfinder
      gallery = 1922
      # cat=BOURDON.Contentserv/Pfinder/140; type=int+; label=PFinder - Product image: ID of the Contentserv Attribute that holds the Attribute that should be used as product image
      image = 1923
      # cat=BOURDON.Contentserv/Pfinder/150; type=int+; label=PFinder - Datasheet: ID of the Contentserv Attribute that holds the Attribute that contains the File reference to the the products datasheet
      datasheet = 1924
      # cat=BOURDON.Contentserv/Pfinder/160; type=int+; label=PFinder - Sorting: ID of the Contentserv Attribute that holds the Attribute that contains the manual product sort order
      sorting = 2119
    }
  }

  evalanche {
    # cat=BOURDON/settings; type=int+; label=Evalanche Pool-ID: The pool-ID where every forms requests should be transmitted to
    poolId = 14380779
  }

  # cat=BOURDON/settings; type=int+; label=Default contact UID: UID of tt_address record that is the primary recipient for all mails (if no other mail can be determined)
  defaultContactAddress = 120

  configurator {
    attributes {
      # cat=BOURDON.Contentserv/Configurator/005; type=int+; label=Configurator - Techdata: ID of the Contentserv Attribute that holds the Attributes that should be displayed as technical data
      techdata = 1929
      # cat=BOURDON.Contentserv/Configurator/010; type=int+; label=Configurator - Product Image: ID of the Contentserv Attribute that holds the Attribute that should be used as product image
      productImage = 1932
      # cat=BOURDON.Contentserv/Configurator/020; type=int+; label=Configurator - Downloads: ID of the Contentserv Attribute that holds the Attributes to be displayed as downloads
      downloads = 1930
      # cat=BOURDON.Contentserv/Configurator/030; type=int+; label=Configurator - Accessories: ID of the Contentserv Attribute that holds the Attributes to be displayed as accessories
      accessories = 1931
      # cat=BOURDON.Contentserv/Configurator/040; type=int+; label=Configurator - Datasheet: ID of the Contentserv Attribute that holds the Attribute that contains the File reference to the datasheet
      datasheet = 1935
      # cat=BOURDON.Contentserv/Configurator/050; type=int+; label=Configurator - Main Group: ID of the Contentserv Attribute that holds the Attribute that contains the main group
      maingroup = 1933
      # cat=BOURDON.Contentserv/Configurator/060; type=int+; label=Configurator - User Benefits: ID of the Contentserv Attribute that holds the Attribute that contains the benefits
      benefits = 1936
      # cat=BOURDON.Contentserv/Configurator/070; type=int+; label=Configurator - Product highlights: ID of the Contentserv Attribute that holds the Attribute that contains the product highlights
      highlights = 1937
      # cat=BOURDON.Contentserv/Configurator/080; type=int+; label=Configurator - Pictograms: ID of the Contentserv Attribute that holds the Attribute that contains the pictograms
      pictograms = 1978
    }
  }

  downloadcenter {
    attributes {
      # cat=BOURDON.Contentserv/Downloadcenter/210; type=int+; label=MAM - Maingroup: ID of the Contentserv MAM-Attribute containing the maingroup
      maingroup = 53
      # cat=BOURDON.Contentserv/Downloadcenter/220; type=int+; label=MAM - Filetype: ID of the Contentserv MAM-Attribute containing the filetype
      filetype = 54
      # cat=BOURDON.Contentserv/Downloadcenter/230; type=int+; label=MAM - Mediatype: ID of the Contentserv MAM-Attribute containing the mediatype
      mediatype = 51
      # cat=BOURDON.Contentserv/Downloadcenter/240; type=int+; label=MAM - Title: ID of the Contentserv MAM-Attribute containing the title
      title = 57
    }
  }
}

forms {
  emailconfig {
    admin {
      subject = Product inquiry to Bourdon-Instruments.com
      sender_email = donotreply@bourdon-instruments.com
      sender_name = Bourdon-Instruments.com
    }

    user {
      subject = Your product inquiry to Bourdon-Instruments.com
      sender_email = donotreply@bourdon-instruments.com
      sender_name = Bourdon-Instruments.com
    }
  }
}
