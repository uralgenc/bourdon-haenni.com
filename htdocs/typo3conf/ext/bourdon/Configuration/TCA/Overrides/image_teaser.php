<?php
defined('TYPO3_MODE') or die();
call_user_func(function () {
    $languageFilePrefix = 'LLL:EXT:fluid_styled_content/Resources/Private/Language/Database.xlf:';
    $customLanguageFilePrefix = 'LLL:EXT:bourdon/Resources/Private/Language/locallang.xlf:';
    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';
    // Add the CType "touch_slider"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem('tt_content', 'CType', [
        $customLanguageFilePrefix . 'image_teaser.title',
        'image_teaser',
        'content-image' //BE icon
    ]);
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['image_teaser'] = $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['textmedia'];
    // Define what fields to display
    $GLOBALS['TCA']['tt_content']['types']['image_teaser'] = [
        'showitem' => '
                --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                pi_flexform, assets
        ',
        'columnsOverrides' => [
            'media' => [
                'label' => $languageFilePrefix . 'tt_content.media_references',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('media', [
                    'minItems' => 1,
                    'size' => 1,
                    'maxItems' => 1,
                    'appearance' => [
                        'createNewRelationLinkTitle' => $languageFilePrefix .
                            'tt_content.media_references.addFileReference'
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // behaves the same as the image field.
                    'foreign_types' => $GLOBALS['TCA']['tt_content']['columns']['image']['config']['foreign_types']
                ], $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext'])
            ]
        ]
    ];
});
