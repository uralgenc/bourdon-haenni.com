(function ($) {

    $(document).ready(function () {
        /**
         * Toggle stuff with data attributes
         */
        $('[data-open-target]').click(function (e) {
            e.preventDefault();
            closeEverything();
            var target = $(e.target).closest('[data-open-target]').attr('data-open-target');
            $(target).toggleClass('open');
			if(target == '#search-overlay') {
				setTimeout(function() {
					$('#searchform').focus();
				}, 300);
			}
        });
        $('[data-close-target]').click(function (e) {
            e.preventDefault();
            var target = $(e.target).closest('[data-close-target]').attr('data-close-target');
            $(target).removeClass('open');
        });
        function closeEverything() {
            $('[data-open-target]').each(function () {
                var target = $(this).closest('[data-open-target]').attr('data-open-target');
                $(target).removeClass('open');
            });
        }

        /**
         * Toggle menu
         */
        $('.menu-trigger').click(function (e) {
            e.preventDefault();
            $('#main-menu').toggleClass('open');
            $('body').toggleClass('overlay-open');
            $('body').append('<div class="overlay-backdrop"></div>');
            setTimeout(function () {
                $('.overlay-backdrop').click(function (e) {
                    e.preventDefault();
                    closeMenu();
                });
            }, 300);
        });
        $('.close-menu').click(function (e) {
            e.preventDefault();
            closeMenu();
        });
        function closeMenu() {
            $('#main-menu').removeClass('open');
            $('body').removeClass('overlay-open');
            $('.overlay-backdrop').remove();
        }

        /**
         * Toggle menu items
         */
        $('#main-menu li.sub > a.sub').click(function (e) {
            e.preventDefault();
            var target = $(e.target).parent().find('ul.lvl-1');
            target.slideToggle();
            target.closest('li').toggleClass('open');
        });

        /**
         * header-teaser-menu script
         */
        $('.header-teaser-menu-toggle').click(function (e) {
            e.preventDefault();
            var target = $(e.target).parent().find('.layover-menu');
            target.closest('.header-teaser').toggleClass('overlay-menu-triggered');
            setTimeout(function () {
                target.closest('.header-teaser').toggleClass('overlay-menu-open');
                target.toggleClass('open');
            }, 300);
        });
        $('.layover-menu-header-close').click(function (e) {
            e.preventDefault();
            var target = $(e.target).parent().closest('.layover-menu');
            target.closest('.header-teaser').addClass('overlay-menu-closed');
            setTimeout(function () {
                target.removeClass('open');
                target.closest('.header-teaser').removeClass('overlay-menu-open');
                target.closest('.header-teaser').removeClass('overlay-menu-triggered');
                target.closest('.header-teaser').removeClass('overlay-menu-closed');
            }, 300);
        });

        /**
         * expandable-box script
         */
        $('.expandable-toggle').click(function (e) {
            e.preventDefault();
            var lower = $(e.target).parent().find('.expandable-lower');
            lower.slideToggle();
        });


        /**
         * open desktop-menu on click
         */
        $('ul.menu-desktop:not([class*=" lvl-"]) > li.sub > a').click(function (e) {
            e.preventDefault();
            var wasOpenBefore = $(this).parent().hasClass('open');
            $('ul.menu-desktop > li.sub.open').removeClass('open');
            if (!wasOpenBefore) {
                $(this).parent().addClass('open');
            }
        });
    });

})(jQuery);
