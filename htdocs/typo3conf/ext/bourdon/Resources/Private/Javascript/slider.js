(function ($) {
	$(document).ready(function () {
		$('.swiper-container').each(function(){
			new Swiper(this, $(this).data('options'));
		});
	});
})(jQuery);
