(function ($) {

	baumerApp.controller('HeaderCtrl', function($scope, $timeout, UserProductList, ContactFormProductList){
		$scope.productListCountChanged = false;
		$scope.inquiryListCountChanged = false;
		$scope.$watch(function(){
			return UserProductList.getLineItemCount();
		}, function(lineItemCount){
			$scope.productListCount = lineItemCount;
			$scope.productListCountChanged = true;
			$timeout(function(){
				$scope.productListCountChanged = false;
			}, 750);
		});
		$scope.$watch(function(){
			return ContactFormProductList.getLineItemCount();
		}, function(lineItemCount){
			$scope.inquiryListCount = lineItemCount;
			$scope.inquiryListCountChanged = true;
			$timeout(function(){
				$scope.inquiryListCountChanged = false;
			}, 750);
		});
	});

	$(document).ready(function () {

		/**
		 * shrinky stiky header
		 */

		var navBar = $('#header');
		var navHeight = navBar.outerHeight();
		var lastScrollTop = 0, delta = 15;

		// dynamicly show & hide navbar
		$(window).on('scroll', function () {
			var scrollTop = $(this).scrollTop();
			checkScrollPosition(scrollTop);
		});

		function checkScrollPosition(scrollTop) {
			if (scrollTop > navHeight) {
				navBar.removeClass('forced-open');
				navBar.addClass('hideable');
				$('body').addClass('nav-sticky');
			} else {
				navBar.addClass('forced-open');
				navBar.removeClass('hideable');
				$('body').removeClass('nav-sticky');
			}
			if (Math.abs(scrollTop - lastScrollTop) <= delta) return false;
			if (scrollTop > lastScrollTop) {
				navBar.removeClass('open');
			} else {
				navBar.addClass('open');
			}
			lastScrollTop = scrollTop;
		}

		function initClippedNav() {
			navBar.addClass('hideable');
			$('body').css('padding-top', navHeight);
			checkScrollPosition($(window).scrollTop());
		}

		// init
		initClippedNav();
		$(window).resize(function () {
			initClippedNav();
		});
	});

})(jQuery);
