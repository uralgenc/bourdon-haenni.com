<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "Baumer.Bourdon".
 *
 * Auto generated 08-10-2015 11:21
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Bourdon',
    'description' => 'Bourdon template',
    'category' => 'misc',
    'shy' => 0,
    'version' => '1.1.5',
    'dependencies' => 'cms,extbase,fluid,flux,fluidpages,fluidcontent,vhs',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author' => 'Florian Schlittenbauer',
    'author_email' => 'fschlittenbauer@1drop.de',
    'author_company' => '',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-7.6.99',
            'cms' => '',
            'extbase' => '',
            'fluid' => '',
            'flux' => '',
            'baumer' => '',
            'fluidpages' => '',
            'fluidcontent' => '',
            'vhs' => '',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    '_md5_values_when_last_written' => 'a:0:{}',
    'suggests' => [
    ],
];
