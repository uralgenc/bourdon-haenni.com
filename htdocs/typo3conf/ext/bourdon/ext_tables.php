<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Bourdon');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Baumer.Bourdon', 'Page');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Baumer.Bourdon', 'Content');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/tsconfig.txt">');


// Add a flexform to the fs_slider CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:bourdon/Configuration/FlexForms/touch_slider_flexform.xml',
    'touch_slider'
);
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['touch_slider']    = \Baumer\Bourdon\Hook\TouchSliderPreviewRenderer::class;


// Add a flexform to the header_image_teaser CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:bourdon/Configuration/FlexForms/header_image_teaser_flexform.xml',
    'header_image_teaser'
);
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['header_image_teaser']    = \Baumer\Bourdon\Hook\ImageTeaserPreviewRenderer::class;


// Add a flexform to the icon_button CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:bourdon/Configuration/FlexForms/icon_button_flexform.xml',
    'icon_button'
);
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['icon_button']    = \Baumer\Bourdon\Hook\ImageTeaserPreviewRenderer::class;


// Add a flexform to the image_teaser CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:bourdon/Configuration/FlexForms/image_teaser_flexform.xml',
    'image_teaser'
);
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['image_teaser']    = \Baumer\Bourdon\Hook\ImageTeaserPreviewRenderer::class;


// Add a flexform to the contact_box CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:bourdon/Configuration/FlexForms/contact_box_flexform.xml',
    'contact_box'
);
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['contact_box']    = \Baumer\Bourdon\Hook\ContactBoxPreviewRenderer::class;


// Add a flexform to the divider CType
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '',
    'FILE:EXT:bourdon/Configuration/FlexForms/divider_flexform.xml',
    'divider'
);
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['divider']    = \Baumer\Bourdon\Hook\ContactBoxPreviewRenderer::class;

$GLOBALS['TCA']['pages']['columns']['urltype']['config']['items'][] = ['data-attribute', 5];
