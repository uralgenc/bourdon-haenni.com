/*
 * Baumer Angular App
 *
 * @author: Martin R. Krause <martin.r.krause@gmx.de>
 */
var baumerApp = angular.module(
	'Baumer',
	[
		'angularLocalStorage',
		'elasticsearch',
		'ui.bootstrap.pagination',
		'ui.bootstrap.transition',
		'ui.bootstrap.tabs',
		'ui.bootstrap.modal'
	]
);

baumerApp.run(['$templateCache', '$http', function($templateCache, $http) {
	$templateCache.put('template/pagination/pagination.html',
		'<ul class="pagination" max-size="5" rotate="false">\n\t<li class="first" data-ng-if="boundaryLinks" data-ng-class="{disabled: noPrevious()}">\n\t\t<a href data-ng-click="selectPage(1)"><span class="fa fa-angle-double-left"></span></a>\n\t</li>\n\t<li class="prev" data-ng-if="directionLinks" data-ng-class="{disabled: noPrevious()}">\n\t\t<a href data-ng-click="selectPage(page - 1)"><span class="fa fa-angle-left"></span></a>\n\t</li>\n\t<li data-ng-repeat="page in pages track by $index" data-ng-class="{active: page.active}">\n\t\t<a href data-ng-click="selectPage(page.number)">{{page.text}}</a>\n\t</li>\n\t<li class="next" data-ng-if="directionLinks" data-ng-class="{disabled: noNext()}">\n\t\t<a href data-ng-click="selectPage(page + 1)"><span class="fa fa-angle-right"></span></a>\n\t</li>\n\t<li class="last" data-ng-if="boundaryLinks" data-ng-class="{disabled: noNext()}">\n\t\t<a href data-ng-click="selectPage(totalPages)"><span class="fa fa-angle-double-right"></span></a>\n\t</li>\n</ul>\n'
	);
	$templateCache.put('template/tabs/tab.html',
		'<li data-ng-class="{active: active, disabled: disabled}">\n\t<a href data-ng-click="select()" data-tab-heading-transclude>{{heading}}</a>\n</li>'
	);
	$templateCache.put('template/tabs/tabset.html',
		'<div>\n\t<ul class="nav nav-{{type || \'tabs\'}}" data-ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" data-ng-transclude></ul>\n\t<div class="tab-content">\n\t\t<div class="tab-pane"\n\t\t\t data-ng-repeat="tab in tabs"\n\t\t\t data-ng-class="{active: tab.active}"\n\t\t\t data-tab-content-transclude="tab">\n\t\t</div>\n\t</div>\n</div>\n'
	);
    $http.defaults.headers.common['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    $http.defaults.headers.common['Cache-Control'] = 'no-cache';
    $http.defaults.headers.common['Pragma'] = 'no-cache';
    $http.defaults.cache = false;
}]);

var INTEGER_REGEXP = /^\-?\d+$/;

baumerApp.filter('debug', function() {
	return function(input) {
		console.log(input);
	};
});

baumerApp.filter('bytes', function() {
	return function(bytes, precision) {
		if (bytes === 0) { return '0 bytes' }
		if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
		if (typeof precision === 'undefined') precision = 1;

		var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
			number = Math.floor(Math.log(bytes) / Math.log(1024)),
			val = (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision);

		return  (val.match(/\.0*$/) ? val.substr(0, val.indexOf('.')) : val) +  ' ' + units[number];
	}
});

baumerApp.directive('equalHeightsAfterRepeat', ['$timeout', '$window', function($timeout, $window) {
	return function(scope, element, attrs) {
		attrs.equalHeightsAfterRepeat = attrs.equalHeightsAfterRepeat || '';

		function applyHeight() {
			$timeout(function(){
				angular.element(attrs.equalHeightsAfterRepeat).css('height', 'auto');
				angular.element(attrs.equalHeightsAfterRepeat).equalHeights();
			},0);
		}

		if (scope.$last && attrs.equalHeightsAfterRepeat.length > 0) {
			applyHeight();
		}
		angular.element($window).bind('resize', function() {
			applyHeight();
		});
		angular.element('[data-tab]').bind('click', function(){
			applyHeight();
		});
	};
}]);

baumerApp.filter('ensureArray', function() {
	return function(input) {
		var outArray = [];
		if (!_.isArray(input)) {
			outArray.push(input);
		} else {
			outArray = input;
		}
		return outArray;
	};
});

baumerApp.filter('removeBlankItems', function() {
	return function(inputArray) {
		var outArray = [];
		for (var i = 0; i < inputArray.length; i++) {
			if(inputArray[i].length != 0){
				outArray.push(inputArray[i]);
			}
		}
		return outArray;
	};
});

baumerApp.filter('productCodeWithoutOptions', function() {
	return function(productCode) {
        if (!productCode) {
            return '';
        }
		var parts = productCode.split('/');
		var output = parts[0];
		if (parts[1]) {
			output = output + '/*';
		}
		return output
	}
});

/**
 * Reformat an attribute value from encoway from
 *
 * "[ID] - Value"
 *
 * to
 *
 * Value Value [ID]
 */
baumerApp.filter('reformatAttributeValue', function() {
	return function(value) {
		var reformatedValueArray = [];
		var multivalueParts = value.split('--&$&--');
		_.each(multivalueParts, function(value) {
			var valueParts = value.split(" - ");
			var indexPart = valueParts.shift();
			reformatedValueArray.push(valueParts.join(" - ") + " " + indexPart);
		});

		return reformatedValueArray.join(", ");
	};
});

baumerApp.factory('sharedProduct', ['$rootScope', function($rootScope) {
	var storedCode = false;
	var variantCode = false;
	var price = false;
	var shipping = false;
	var storedContainer = {};
	var storedEncowayconfig = {};

	return {
		setCode: function(productCode) {
			storedCode = productCode;
			$rootScope.$broadcast('updateProductCode');
		},
		getCode: function() {
			return storedCode;
		},
		setVariantCode: function(variant) {
			variantCode = variant;
		},
		getVariantCode: function() {
			return variantCode;
		},
		setPrice: function(val) {
			price = val;
			$rootScope.$broadcast('updateProductPrice');
		},
		getPrice: function() {
			return price;
		},
		setShipping: function(val) {
			shipping = val;
			$rootScope.$broadcast('updateProductShipping');
		},
		getShipping: function() {
			return shipping;
		},
		setContainer: function(container) {
			storedContainer = container;
		},
		getContainer: function() {
			return storedContainer;
		},
		setEncowayConfig: function(encowayConfig) {
			storedEncowayconfig = encowayConfig;
		},
		getEncowayConfig: function() {
			return storedEncowayconfig;
		}
	}
}]);

/**
 * @ngdoc service
 * @name Baumer.UserProductList
 * @requires storage
 * @description The UserProductList provides the localStorage based "cart" functionality for products
 */
baumerApp.service('UserProductList', ['storage', '$rootScope', function(storage, $rootScope) {

	var self = this;
	var _storageKey = 'currentProductList';
	self.currentProductList = window.currentProductList ? window.currentProductList : {};
	var productListDefault = {
		uid: 0,
		name: '',
		code: null,
		crdate: Date.now(),
		lineItems: [],
		feUser: {uid: 0}
	};

	var storedProductList = storage.get(_storageKey);
	if (storedProductList) {
		self.currentProductList = storedProductList;
	} else if (window.currentProductList) {
		self.currentProductList = window.currentProductList;
	} else {
		self.currentProductList = productListDefault;
	}

	function updateProductList() {
		storage.set(_storageKey, self.currentProductList);
		$rootScope.$broadcast('updateProductList');
	}

	this.addLineItem = function (lineItem) {
		if(typeof lineItem !== 'undefined') {
			// unset unneccessary values, that the line item might have picked up
			delete lineItem.familyOptions;
			var existingLineItem = _.find(
					self.currentProductList.lineItems,
					function(existingLineItem) {
						// As lineItems can have different identity properties, we check them in prioritized order
						if (lineItem.configuration) {
							return existingLineItem.configuration == lineItem.configuration;
						} else if (lineItem.csViewRecord) {
							return existingLineItem.csViewRecord == lineItem.csViewRecord;
						} else if (lineItem.family) {
							return existingLineItem.family == lineItem.family;
						} else if (lineItem.groupTitle) {
							return existingLineItem.groupTitle == lineItem.groupTitle && existingLineItem.label == lineItem.label;
						}
						return false;
					});
			if(existingLineItem) {
				// increase amount
				existingLineItem.amount++;
			} else {
				// add new line item
				self.currentProductList.lineItems.push(lineItem);
			}
			// every change to the product list invalidates the code, so we unset the code
			self.currentProductList.code = null;
			updateProductList();
		}

		return self.currentProductList;
	};
	this.getProductList = function() {
		return self.currentProductList;
	};
	this.getLineItems = function() {
		return self.currentProductList.lineItems;
	};
	this.setProductList = function(newProductList) {
		self.currentProductList = angular.extend(productListDefault, newProductList);
		updateProductList();
	};
	this.removeLineItemByIdx = function(idx) {
		if (self.currentProductList.lineItems.length >= idx) {
			self.currentProductList.lineItems.splice(idx, 1);
			updateProductList();
		}
		return self.currentProductList;
	};
	this.clear = function() {
		self.currentProductList = productListDefault;
		updateProductList();
		return self.currentProductList;
	};
	this.getLineItemCount = function() {
		return self.currentProductList.lineItems.length;
	};
}]);

/**
 * @ngdoc service
 * @name Baumer.ContactFormProductList
 * @requires UserProductList
 * @description The ContactFormProductList provides the localStorage based "cart" functionality for products
 */
baumerApp.service('ContactFormProductList', ['storage', '$rootScope', function(storage, $rootScope) {
	var self = this;
	var _storageKey = 'currentContactFormProductList';
	self.lineItems = [];
	var lineItemTemplate = {
		note: '',
		amount: 1,
		price: 0.0,
		shipping: 0
	};

	var storedLineItems = storage.get(_storageKey);
	if (storedLineItems) {
		self.lineItems = storedLineItems;
	}

	function updateLineItems() {
		storage.set(_storageKey, self.lineItems);
		$rootScope.$broadcast('updateLineItems');
	}

	this.addLineItem = function (lineItem) {
		var lineItemConfiguration = lineItem.configuration;
		var lineItemProductMainGroup = lineItem.productMainGroup;
		lineItem = angular.copy(lineItemTemplate, lineItem);
		lineItem.configuration = lineItemConfiguration;
		lineItem.productMainGroup = lineItemProductMainGroup;
		if(typeof lineItem !== 'undefined') {
			var existingLineItem = _.find(
				self.lineItems,
				function(existingLineItem) {
					// As lineItems can have different identity properties, we check them in prioritized order
					if (lineItem.configuration) {
						return existingLineItem.configuration === lineItem.configuration;
					} else if (!lineItem.configuration && lineItem.csViewRecord) {
						return existingLineItem.csViewRecord === lineItem.csViewRecord;
					} else if (!lineItem.configuration && lineItem.family) {
						return existingLineItem.family === lineItem.family;
					}
					return false;
				});
			if(existingLineItem) {
				// increase amount
				existingLineItem.amount++;
			} else {
				// add new line item
				console.log(lineItem);
				self.lineItems.push(lineItem);
			}
			updateLineItems();
		}

		return self.lineItems;
	};
	this.getLineItems = function() {
		return self.lineItems;
	};
	this.removeLineItemByIdx = function(idx) {
		if (self.lineItems.length >= idx) {
			self.lineItems.splice(idx, 1);
			updateLineItems();
		}
		return self.currentProductList;
	};
	this.clear = function() {
		self.lineItems = [];
		updateLineItems();
		return self.lineItems;
	};
	this.getLineItemCount = function() {
		return self.lineItems.length;
	};
}]);


var spinner = '<span class="spinner"></span>';

/**
 * Spinner span as directive
 *
 * Usage
 *
 * <spinner></spinner>
 *
 * or
 *
 * <ANY spinner />
 */
baumerApp.directive('spinner', function() {
	return {
		template: window.spinner,
		restrict: 'AE'
	}
});

/**
 * Activate Bootstrap select picker on all select Elements
 * And watch click on dropdown to refresh select options that
 * might have been changed by JS
 *
 * Copyright 2014 nyasoft
 * Licensed under MIT license
 */
baumerApp.directive('selectpicker', ['$parse', function ($parse) {
	'use strict';
	// NG_OPTIONS_REGEXP copy from angular.js select directive
	var NG_OPTIONS_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/;
	return {
		restrict: 'C',
		scope: false,
		require: ['^ngModel', 'select'],

		link: function(scope, element, attrs, ctrls) {
			element = $(element);
			var optionsExp = attrs.ngOptions;
			var valuesFn, match, track, groupBy;
			if(optionsExp && (match = optionsExp.match(NG_OPTIONS_REGEXP))) {
				groupBy = match[3];
				valuesFn = $parse(match[7]);
				track = match[8];
			}
			var ngCtrl = ctrls[0];
			var selectCtrl = ctrls[1];
			// prevent selectDirective render an unknownOption.
			selectCtrl.renderUnknownOption = angular.noop;
			var optionArray = [];

			// store data- attribute options of select
			var selectorOptions = {};
			var BS_ATTR = ['container', 'countSelectedText', 'dropupAuto', 'header', 'hideDisabled', 'selectedTextFormat', 'size', 'showSubtext', 'showIcon', 'showContent', 'style', 'title', 'width', 'disabled'];

			var checkSelectorOptionsEquality = function() {
				var isEqual = true;
				angular.forEach(BS_ATTR, function(attr) {
					isEqual = isEqual && attrs[attr] === selectorOptions[attr];
				});
			};

			var updateSelectorOptions = function() {
				angular.forEach(BS_ATTR, function(attr) {
					selectorOptions[attr] = attrs[attr];
				});

				return selectorOptions;
			};

			/**
			 * Check option data attributes, text and value equality.
			 * @param opt the option dom element
			 * @param index the index of the option
			 * @returns {boolean}
			 */
			var checkOptionEquality = function(opt, index) {
				var isEqual = opt.value === optionArray[index].value && opt.text === optionArray[index].text;
				if(isEqual) {
					for(var i = 0; i< opt.attributes.length; i++){
						if(opt.attributes[i].nodeName.indexOf('data-')!==-1) {
							if(optionArray[index].attributes[opt.attributes[i].nodeName] !== opt.attributes[i].nodeValue) {
								isEqual = false;
								break;
							}
						}
					}
				}
				return isEqual;
			};

			var resetDataProperties = function(opt) {
				var attributes = opt.attributes;
				for(var i = 0; i < attributes.length; i++) {
					if(attributes[i].nodeName.indexOf('data-')!==-1) {
						angular.element(opt).data(attributes[i].nodeName.substring(5, attributes[i].nodeName.length), attributes[i].value);
					}
				}
			};

			function optionDOMWatch(){
				// check every option if has changed.
				var optionElements = element.find('option');

				//if the first option has no value and label or value an value of ?, this must be generated by ngOptions directive. Remove it.
				if(!optionElements.eq(0).html() && (optionElements.eq(0).attr('value') ==='?' || !optionElements.eq(0).attr('value'))) {
					// angular seams incorrectly remove the first element of the options. so we have to keep the ? element in the list
					// only remove this ? element when group by is provided.
					if(!!groupBy) {
						optionElements.eq(0).remove();
					}
				}

				if(optionElements.length !== optionArray.length) {
					optionArray = makeOptionArray(optionElements);
					buildSelector();
				} else {
					var hasChanged = false;
					optionElements.each(function(index, value){
						if(!checkOptionEquality(value, index)) {
							// if check fails. reset all data properties.
							resetDataProperties(value);
							hasChanged = true;
						}
					});
					if(hasChanged) {
						buildSelector();
					}
					if(!checkSelectorOptionsEquality()) {
						updateSelectorOptions();
						element.selectpicker('refresh');
					}
					optionArray = makeOptionArray(optionElements);
				}
			}

			scope.$watch(function(){
				// Create an object to deep inspect if anything has changed.
				// This is slow, but not as slow as calling optionDOMWatch every $digest
				return {
					ngModel: ngCtrl.$viewValue,
					options: makeOptionArray( element.find('option') ),
					selectors: updateSelectorOptions()
				};
				// If any of the above properties change, call optionDOMWatch.
			}, optionDOMWatch, true);

			var setValue = function(modelValue) {
				var collection = valuesFn(scope);
				if(angular.isArray(collection) && !angular.isArray(modelValue)) {
					// collection is array and single select mode
					var index = indexInArray(modelValue, collection);
					if(index > -1) {
						element.val(index).selectpicker('render');
					}
				} else if(angular.isArray(collection) && angular.isArray(modelValue)) {
					// collection is array and multiple select mode.
					var indexArray = [];
					for(var i = 0; i < modelValue.length; i++) {
						var indexOfOptions = indexInArray(modelValue[i], collection);
						if(indexOfOptions > -1) {
							indexArray.push(indexOfOptions);
						}
					}
					element.val(indexArray).selectpicker('render');
				} else if(!angular.isArray(collection) && !angular.isArray(modelValue)) {
					// collection is object and single select mode.
					var key = keyOfObject(modelValue, collection);
					if(key) {
						element.val(key).selectpicker('render');
					}
				} else if(!angular.isArray(collection) && angular.isArray(modelValue)) {
					// collection is object and multiple select mode.
					var keyArray = [];
					for(var j = 0; j < modelValue.length; j++) {
						var k = keyOfObject(modelValue[j], collection);
						if(k) {
							keyArray.push(k);
						}
					}
					element.val(keyArray).selectpicker('render');
				}
			};

			ngCtrl.$render = function() {
				// model -> view
				var data = element.data('selectpicker');
				if(data) {
					if(!!valuesFn && !track) {
						// transform value to index of options
						setValue(ngCtrl.$viewValue);
					} else {
						element.val(ngCtrl.$viewValue).selectpicker('render');
					}
				}
			};

			function indexInArray(value, array) {
				for(var i = 0; i < array.length; i++) {
					if(angular.equals(value, array[i])) {
						return i;
					}
				}
				return -1;
			}

			function keyOfObject(value, object) {
				var key = null;
				angular.forEach(object, function(v, k) {
					if(angular.equals(v, value)) {
						key = k;
					}
				});
				return key;
			}

			/**
			 * Copy option value and text and data attributes to an array for future comparison.
			 * @param optionElements the source option elements. a jquery objects' array.
			 * @returns {Array} the copied array.
			 */
			function makeOptionArray(optionElements) {
				var optionArray = [];
				optionElements.each(function(index, childNode){
					var attributes = {};
					for(var i = 0; i < childNode.attributes.length; i++) {
						if(childNode.attributes[i].nodeName.indexOf('data-')!==-1) {
							attributes[childNode.attributes[i].nodeName] = childNode.attributes[i].nodeValue;
						}
					}
					optionArray.push({
						value: childNode.value,
						text: childNode.text,
						attributes: attributes
					});
				});
				return optionArray;
			}

			function buildSelector() {
				// build new selector. if previous select exists. remove previous data and DOM
				var oldSelectPicker = element.data('selectpicker');
				if(oldSelectPicker) {
					oldSelectPicker.$menu.parent().remove();
					oldSelectPicker.$newElement.remove();
					element.removeData('selectpicker');
				}
				element.selectpicker();
				if(!!valuesFn && !track) {
					setValue(ngCtrl.$modelValue);
				} else {
					element.val(ngCtrl.$modelValue).selectpicker('render');
				}

			}

		}
	};
}]);
