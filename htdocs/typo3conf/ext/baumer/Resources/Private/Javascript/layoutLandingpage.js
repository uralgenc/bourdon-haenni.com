/*
 * General javascript
 */
(function($){
	$(document).ready(function() {

		/*
		 * Main Menu
		 * Menu should open on click, overlay for rest of content
		 * click on overlay should close menu and hide overlay
		 */
		var objMenu = $('#nav_main');
		var objSubmenu = $('li.hassub');
		if(objMenu.length) {
			objMenu.on('click', '> li.hassub > a', function(event) {
				event.preventDefault();
				var hasSubmenu = $(this).parents("li").hasClass('active');
				objSubmenu.removeClass('active');
				if(!hasSubmenu) {
					$(this).parents('li').addClass('active');
				}
			});
			$('.nav-wrapper').click(function(event) {
				event.stopPropagation();
			});
			$('body').click(function() {
				objSubmenu.removeClass('active');
			});
		}

		/*
		 * Select design. This function uses selectpincker plugin.
		 * http://silviomoreto.github.io/bootstrap-select/
		 */
		if(!$('html').hasClass('lt-ie9')) {
			$('select').selectpicker();
		}


		/*
		 * Navigation toggle
		 */
		var menuRunning = false;
		var objBanner = $('#banner');
		var objMenu = $('#header .nav-cont');
		$('.menu-toggle').click(function() {
			if(menuRunning == false) {
				objBanner.animate({
					'padding-top': 140
				}, 300, function() {
					objMenu.fadeIn();
					menuRunning = true;
				});
			} else {
				objMenu.fadeOut(function() {
					objBanner.animate({ 'padding-top': 90 }, 300);
					menuRunning = false;
				});
			}
		});


		/*
		 * Waypoint animated. This effects uses waypoint plugin.
		 * http://imakewebthings.com/waypoints/
		 */
		$('#header').waypoint(function (direction) {
			if (direction == 'down') {
				$('#banner .col-md-7', this).addClass('fadeInLeft animated');
				$('#banner .actions', this).addClass('fadeInRight animated');
				$('#banner figure', this).addClass('bounceIn animated');
			}
		}, {
			offset: '80%'
		});

		$('.section-features article').waypoint(function (direction) {
			if (direction == 'down') {
				$('.the-title', this).addClass('fadeInLeft animated');
				$('.the-subtitle', this).addClass('fadeInRight animated');
				$('.the-content', this).addClass('fadeInUp animated');
				$('.link', this).delay(500).queue(function () {
					$(this).addClass('fadeIn animated');
				});
			}
		}, {
			offset: '80%'
		});

		$('.jumbotron-one').waypoint(function (direction) {
			if (direction == 'down') {
				$(this).addClass('fadeIn animated');
				$('.the-title', this).delay(400).queue(function () {
					$(this).addClass('fadeInLeft animated');
				});
				$('.the-subtitle', this).delay(400).queue(function () {
					$(this).addClass('fadeInRight animated');
				});
				$('.the-content', this).delay(400).queue(function () {
					$(this).addClass('fadeInUp animated');
				});
				$('.btn', this).delay(600).queue(function () {
					$(this).addClass('fadeInUp animated');
				});
				$('.i-mark', this).delay(600).queue(function () {
					$(this).addClass('fadeIn animated');
				});

			}
		}, {
			offset: '80%'
		});

		$('.section-applications').waypoint(function (direction) {
			if (direction == 'down') {
				$('.the-title', this).addClass('fadeInLeft animated');
				$('article', this).addClass('zoomIn animated');
			}
		}, {
			offset: '80%'
		});

		$('.section-sensor').waypoint(function (direction) {
			if (direction == 'down') {
				$('.the-title', this).addClass('fadeInLeft animated');
				$('.the-subtitle', this).addClass('fadeInRight animated');
				$('.box-sensor').addClass('bounceIn animated');
				$('.box-info-top-left', this).delay(200).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.box-info-top-right', this).delay(400).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.box-info-right-top', this).delay(600).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.box-info-right-bottom', this).delay(800).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.box-info-bottom', this).delay(1000).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.box-info-left-bottom', this).delay(1200).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.box-info-left-top', this).delay(1400).queue(function () {
					$(this).addClass('fadeIn animated');
				});
			}
		}, {
			offset: '80%'
		});

		$('.section-download').waypoint(function (direction) {
			if (direction == 'down') {
				$(this).addClass('fadeIn animated');
				$('.the-title', this).delay(400).queue(function () {
					$(this).addClass('fadeInLeft animated');
				});
				$('.the-subtitle', this).delay(600).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.animate-left', this).delay(800).queue(function () {
					$(this).addClass('fadeInLeftBig animated');
				});
				$('.animate-right', this).delay(800).queue(function () {
					$(this).addClass('fadeInRightBig animated');
				});
				$('.i-mark', this).delay(1000).queue(function () {
					$(this).addClass('fadeIn animated');
				});
			}
		}, {
			offset: '80%'
		});

		$('.section-form').waypoint(function (direction) {
			if (direction == 'down') {
				$(this).addClass('fadeIn animated');
				$('.col-md-7', this).delay(400).queue(function () {
					$(this).addClass('fadeInLeft animated');
				});
				$('figure', this).delay(800).queue(function () {
					$(this).addClass('fadeIn animated');
				});
				$('.animate-left', this).delay(800).queue(function () {
					$(this).addClass('fadeInLeftBig animated');
				});
				$('.animate-right', this).delay(800).queue(function () {
					$(this).addClass('fadeInRightBig animated');
				});
				$('.animate-bottom', this).delay(1000).queue(function () {
					$(this).addClass('fadeInUp animated');
				});
				$('.action', this).delay(1200).queue(function () {
					$(this).addClass('fadeInUpBig animated');
				});
				$('.i-mark', this).delay(1000).queue(function () {
					$(this).addClass('fadeIn animated');
				});
			}
		}, {
			offset: '80%'
		});


	});
})(jQuery);
