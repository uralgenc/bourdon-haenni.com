baumerApp.controller('ProductboxCtrl',
	['$scope', '$rootScope', '$timeout', 'sharedProduct', 'UserProductList', 'ContactFormProductList',
		function($scope, $rootScope, $timeout, sharedProduct, UserProductList, ContactFormProductList) {

    $scope.encowayConfigurationId = 0;

	if (CONTACT_ADDRESS && typeof CONTACT_ADDRESS === 'object') {
		var contactAddresses = CONTACT_ADDRESS;
		if(contactAddresses.length ==  1) {
			$scope.contactAddressData = contactAddresses[0];
		}
	}

	$scope.adding = false;
	$scope.productPrice = false;
	$scope.shipping = false;
    $scope.sharing = false;
    $scope.shareLink = '';
	$scope.$on('updateProductCode', function() {
		$scope.productCode = sharedProduct.getCode();
	});
	$scope.$on('updateProductPrice', function() {
		$scope.productPrice = sharedProduct.getPrice();
	});
	$scope.$on('updateProductShipping', function(){
		$scope.shipping = sharedProduct.getShipping();
	});

	var mainGroups = [];
	if (typeof PRODUCT_MAIN_GROUPS == 'object') mainGroups = PRODUCT_MAIN_GROUPS;

	$scope.datasheets = [];
	if (typeof CONFIGURATOR_RECORD == 'object' && CONFIGURATOR_RECORD.datasheets) $scope.datasheets = CONFIGURATOR_RECORD.datasheets;

	/**
	 * Add line item to currentProductList
	 * with timeout for user feedback
	 */
	$scope.addToPlWidget = function() {

		var lineItem = {
			amount: 1,
			label: CONFIGURATOR_RECORD.label,
			price: $scope.productPrice,
			shipping: $scope.shipping,
			configuration: sharedProduct.getCode(),
			variant: sharedProduct.getVariantCode(),
			groupTitle: CONFIGURATOR_RECORD.mainGroup,
			attributes: $scope.buildParameterArray(),
			imageUrl: CONFIGURATOR_RECORD.image,
			csViewRecord: CONFIGURATOR_RECORD.uid
		};
		// Ensure that the shrinky header is visible
		angular.element('#header').addClass('open');
		if(CONFIGURATOR_RECORD.mainGroupTitle) lineItem.groupTitle = CONFIGURATOR_RECORD.mainGroupTitle;
		// save configuration and add to current product list
		$rootScope.$broadcast('saveConfiguration');
		if(typeof lineItem.configuration !== 'undefined') {
			var encowayConfig = sharedProduct.getEncowayConfig();
			$scope.adding = true;
			// listen to the save promise
			encowayConfig.then(function(encowayConfigurationShortCode) {
				lineItem.encowayConfiguration = encowayConfigurationShortCode;
				UserProductList.addLineItem(lineItem);
				$scope.adding = false;
			}, function(reason) {
				alert(reason);
				$scope.adding = false;
			}, null);
		} else {
			UserProductList.addLineItem(lineItem);
		}
	};

    /**
     * Save the current configuration state using the TYPO3 Encoway API
     *
     * @returns {deferred.promise|{then, catch, finally}}
     */
    $scope.shareProduct = function () {
        $scope.sharing = true;
        $rootScope.$broadcast('saveConfiguration');
    };

    $scope.$on('savedConfiguration', function(event, data){
        if ($scope.sharing) {
            $scope.sharing = false;
            $scope.shareLink = data.link;
            angular.element('#modal-link').modal();
        }
    });

	$scope.addToFormWidget = function() {
		var lineItem = {
			amount: 1,
			label: CONFIGURATOR_RECORD.label,
			configuration: sharedProduct.getCode(),
			variant: sharedProduct.getVariantCode(),
			groupTitle: CONFIGURATOR_RECORD.mainGroup,
			attributes: $scope.buildParameterArray(),
			imageUrl: CONFIGURATOR_RECORD.image,
			csViewRecord: CONFIGURATOR_RECORD.uid,
			price: $scope.productPrice,
			shipping: $scope.shipping
		};
		// Ensure that the shrinky header is visible
		angular.element('#header').addClass('open');

		angular.forEach(mainGroups, function(mainGroup) {
			if (mainGroup.contentServId == CONFIGURATOR_RECORD.mainGroup) {
				lineItem.productMainGroup = mainGroup;
				lineItem.productMainGroup.uid = parseInt(mainGroup.uid);
			} else if (mainGroup.contentServId.indexOf('/' + CONFIGURATOR_RECORD.mainGroup) > -1) {
				lineItem.productMainGroup = mainGroup;
				lineItem.productMainGroup.uid = parseInt(mainGroup.uid);
			}
		});
		if (!lineItem.productMainGroup) {
			lineItem.productMainGroup = {
				title: 'n/a',
				contactAddress: null
			};
		}

		// save configuration and add to current product list
		$rootScope.$broadcast('saveConfiguration');
		if(typeof lineItem.configuration !== 'undefined') {
			var encowayConfig = sharedProduct.getEncowayConfig();
			$scope.addingInquiry = true;
			// listen to the save promise
			encowayConfig.then(function(encowayConfigurationShortCode) {
				lineItem.encowayConfiguration = encowayConfigurationShortCode;
				ContactFormProductList.addLineItem(lineItem);
				$scope.addingInquiry = false;
			}, function(reason) {
				alert(reason);
				$scope.addingInquiry = false;
			}, null);
		} else {
			ContactFormProductList.addLineItem(lineItem);
		}
		angular.element('#contact-overlay').addClass('open');
	};

	/**
	 * Build array with {key, value} of all set parameters in configuration
	 *
	 * @returns {*}
	 */
	$scope.buildParameterArray = function() {
		var containers = sharedProduct.getContainer();
		return _.map(containers, function (container) {
			var parameters = _.flatten(container.parameters, true);
			parameters = _.map(parameters, function (param) {
				var selectedValues = _.filter(param.values, function(value) { return value.selected; });
				if (selectedValues.length > 0) {
					return {
						key: param.name,
						label: param.translatedName,
						// we join multivalue values with a strange seperator "--&$&--", so we can filter it later
						value: _.pluck(selectedValues, 'translatedValue').join('--&$&--')
					};
				}
			});
			return {
				id: container.id,
				translatedName: container.translatedName,
				parameters: _.compact(parameters)
			};
		});
	};
}]);
