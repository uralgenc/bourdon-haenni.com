/**
 * @ngdoc controller
 * @name Baumer:DownloadCtrl
 * @requires esFactory
 * @requires $timeout
 * @requires $log
 */
baumerApp.controller('DownloadCtrl', function($scope, esFactory, $timeout, $log) {

	$scope.selectorIsAvailable = true;

	var ejs = null;
	var elasticsearchIndex;
	var elasticsearchType;
	var activeFilters = {};

	// This global variables are assigned inside the template
	if (SELECTOR_CONFIG) {
		ejs = esFactory(_.clone(SELECTOR_CONFIG.ES_HOST));
		elasticsearchIndex = SELECTOR_CONFIG.ES_INDEX;
		elasticsearchType = SELECTOR_CONFIG.ES_TYPE;
	} else {
		$scope.selectorIsAvailable = false;
	}

	$scope.currentPage = 1;
	$scope.totalItems = 0;
	$scope.itemsPerPage = 6;
	$scope.sortingAttributeId = false;
	$scope.sortDirection = 'asc';
	$scope.displayView = 'list';
	$scope.filtersExpanded = false;
	$scope.q = '';
	$scope.showThumbnails = SHOW_THUMBNAILS;
	$scope.selectAttributes = CS_ATTRIBUTES.attributes;
	$scope.filterAttributes = CS_ATTRIBUTES.filters || {};

	/**
	 * Pagination click
	 * @param pageNo
	 */
	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
	};

	$scope.$watch('itemsPerPage', function(){
		$scope.currentPage = 1;
		$scope.search();
	});

	/**
	 * Expand all filters
	 */
	$scope.toggleFiltersExpanded = function() {
		$scope.filtersExpanded = !$scope.filtersExpanded;
		if ($scope.filtersExpanded) {
			$timeout(function(){
				$('.filterCatFlyout').equalHeights();
			});
		} else {
			$timeout(function(){
				$('.filterCatFlyout').css('height', '');
			});
		}
	};

	/**
	 * Reset all filters of this selector
	 */
	$scope.resetFilters = function() {
		activeFilters = {};
		$scope.sortingAttributeId = false;
		$scope.sortDirection = 'asc';
		$scope.search();
	};

	/**
	 * Sort values of facet by rawValue if values are of type measureunit
	 *
	 * @param facetValues
	 * @returns {*}
	 */
	$scope.sortFacetValues = function(facetValues) {
		if (facetValues.length > 0 && facetValues[0].valueType == 'measureunit') {
			return _.sortBy(facetValues, function(facetValue) { return parseFloat(facetValue.rawValue); });
		} else {
			return facetValues;
		}

	};

	/**
	 * Checks if the search is currently filtered after attributeKey and valueKey
	 * @param attributeKey
	 * @param valueKey
	 * @returns {boolean}
	 */
	$scope.isFiltered = function(attributeKey, valueKey) {
		attributeKey = searchAttributeIdByLabelInAvailableFacets(attributeKey);
		return !!(activeFilters[attributeKey] && activeFilters[attributeKey].indexOf(valueKey) > -1);
	};

	/**
	 * Checks if there are any filtered values for the attribute
	 * @param attributeKey
	 * @returns {boolean}
	 */
	$scope.attributeIsFiltered = function(attributeKey) {
		attributeKey = _.find($scope.availableFacets, {key: attributeKey}).attributeId;
		return !!(activeFilters[attributeKey] && activeFilters[attributeKey].length > 0);
	};

	function searchAttributeIdByLabelInAvailableFacets(facetLabel){
		var attributeId = 0;
		angular.forEach($scope.availableFacets, function(facet){
			if (facet.key === facetLabel) {
				attributeId = facet.attributeId;
			}
		});
		return attributeId;
	}

	/**
	 * Set an attribute and it's value to be filtered
	 * @param attributeKey
	 * @param valueKey
	 */
	$scope.toggleFilter = function(attributeKey, valueKey) {
		attributeKey = searchAttributeIdByLabelInAvailableFacets(attributeKey);

		if (!activeFilters[attributeKey]) {
			activeFilters[attributeKey] = [];
		}
		var valIdx = activeFilters[attributeKey].indexOf(valueKey);
		if (valIdx > -1) {
			activeFilters[attributeKey].splice(valIdx, 1);
		} else {
			activeFilters[attributeKey].push(valueKey);
		}
		$scope.currentPage = 1;
		this.search();
	};

	/**
	 * Constructs the filter for a filtered elasticsearch query
	 * according to the currently set filters.
	 * @returns {*}
	 */
	function buildFilters() {
		var and = [];

		angular.forEach(activeFilters, function(attributeValues, attributeId){
			angular.forEach(attributeValues, function(attributeValue){
				var valueTermFilter = {term:{}};
				valueTermFilter.term['data.FileAttributes.' + attributeId + '.FormattedValue'] = attributeValue;
				and.push(valueTermFilter);
			});
		});

		// Predefined category filters from plugin/CE configuration
		if (CATEGORIES && angular.isArray(CATEGORIES)) {
			var categoryOr = [];
			angular.forEach(CATEGORIES, function(categoryId) {
				categoryOr.push({term:{categories: categoryId}});
			});
			if (categoryOr.length > 0) {
				and.push({or: categoryOr});
			}
		}

		if (and.length > 0) {
			return {and: and};
		}
		return {};
	}

	/**
	 * Build aggregation query part according to given configuration
	 * @returns {{}}
     */
	function buildAggregations() {
		var aggs = {};
		angular.forEach($scope.filterAttributes, function(attributeId){
			aggs[attributeId] = {
				terms: {
					size: 100,
					field: 'data.FileAttributes.' + attributeId + '.FormattedValue'
				}
			};
		});
		return aggs;
	}

	/**
	 * Build the list of fields that should be returned by elasticsearch
	 * @returns {string[]}
     */
	function buildIncludeFields() {
		var fields = [
			'extension',
			'downloads.*',
			'size',
			'title',
			'thumbnail'
		];

		angular.forEach($scope.selectAttributes, function (attributeId) {
			fields.push('data.FileAttributes.' + attributeId + '.TranslatedName');
			fields.push('data.FileAttributes.' + attributeId + '.FormattedValue');
		});

		return fields;
	}

	/**
	 * Execute the actual search
	 */
	$scope.search = function() {
		document.body.style.cursor = 'wait';

		var queryFilter = buildFilters();

		var query = {
			filtered: {
				filter: queryFilter
			}
		};

		if ($scope.q.length > 0) {
			query = {
				bool: {
					must: [
						{
							bool: {
								should: [
									{
										match: {
											"content" : $scope.q
										}
									},
									{
										match: {
											"title" : {
												query: $scope.q,
												fuzziness: 'auto'
											}
										}
									}
								]
							}
						},
						{
							filtered: {
								filter: queryFilter
							}
						}
					]
				}
			}
		}

		var aggs = buildAggregations();
		var includeFields = buildIncludeFields();

		ejs.search({
			index: elasticsearchIndex,
			type: elasticsearchType,
			body: {
				query: query,
				aggs: aggs,
				size: $scope.itemsPerPage,
				from: ($scope.currentPage-1)*$scope.itemsPerPage,
				_source: {
					include: includeFields
				}
			}
		}).then(function (resp) {
			$scope.results = resp.hits.hits;
			$scope.totalItems = resp.hits.total;
			$scope.availableFacets = [];
			if (resp.hits.hits && resp.hits.hits.length > 0) {
				var availableFacets = [];
				angular.forEach($scope.filterAttributes, function(attributeId){
					availableFacets.push({
						key: resp.hits.hits[0]._source.data.FileAttributes[attributeId].TranslatedName,
						attributeId: attributeId,
						value: {
							buckets: resp.aggregations[attributeId].buckets
						}
					});
				});
				$scope.availableFacets = availableFacets;
			}
			document.body.style.cursor = 'default';
		}, function (error) {
			$log.error(error);
		});
	};

	/**
	 * Given a elasticsearch hit and a fieldname, return the value
	 * @param fieldName
	 * @param hit
	 * @returns {*}
     */
	$scope.getFieldValue = function(fieldName, hit) {
		if (!$scope.selectAttributes[fieldName]) {
			return '';
		}
		return hit._source.data.FileAttributes[$scope.selectAttributes[fieldName]].FormattedValue;
	};

	ejs.ping({
		requestTimeout: 5000
	}, function (error) {
		$scope.selectorIsAvailable = !error;
	});
	// initial data from elastic
	$scope.search();

});
