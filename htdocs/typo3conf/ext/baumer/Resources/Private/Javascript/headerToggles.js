/**
 * Toggles for mobile header
 */
//Mobile Toggles
var mobileToggles = [$('#products-toggle'), $('#search-toggle'), $('#menu-toggle')];
var mobileTargets = [$('.my-products'), $('.searchform'), $('.nav-container')];
mobileToggles[0].click(function () {
	mobileTargets[0].slideToggle(300);
});
mobileToggles[1].click(function () {
	mobileTargets[1].slideToggle(300);
});
mobileToggles[2].click(function () {
	mobileTargets[2].slideToggle(300);
});
