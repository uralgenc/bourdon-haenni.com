/**
 * @ngdoc controller
 * @name Baumer:SelectorCtrl
 * @requires esFactory
 * @requires $timeout
 * @requires $log
 * @requires $window
 * @requires UserProductList
 */
baumerApp.controller('SelectorCtrl',
	function($scope, esFactory, $timeout, $log, $window, UserProductList) {

	$scope.selectorIsAvailable = true;

	var ejs = null;
	var elasticsearchIndex;
	var elasticsearchType;
	var csViewId;
	var csViewLabel;
	var filterAttributeId;
	var listAttributeId;
	var gridAttributeId;
	var imageAttributeId;
	var datasheetAttributeId;
	var sortingAttributeId;
	var kmatAttributeId;
	var activeFilters = {};
	var productsContentServIds = [];

	// This global variables are assigned inside the template
	if (SELECTOR_CONFIG && CS_ATTRIBUTES && CS_SELECTION.length > 0) {
		ejs = esFactory(_.clone(SELECTOR_CONFIG.ES_HOST));
		elasticsearchIndex = SELECTOR_CONFIG.ES_INDEX;
		elasticsearchType = SELECTOR_CONFIG.ES_TYPE;
		csViewId = CS_VIEW_ID;
		csViewLabel = CS_VIEW_LABEL;
		filterAttributeId = CS_ATTRIBUTES.filter;
		listAttributeId = CS_ATTRIBUTES.list;
		gridAttributeId = CS_ATTRIBUTES.gallery;
		imageAttributeId = CS_ATTRIBUTES.image;
		datasheetAttributeId = CS_ATTRIBUTES.datasheet;
		sortingAttributeId = CS_ATTRIBUTES.sorting;
		kmatAttributeId = CS_ATTRIBUTES.kmat;
		productsContentServIds = CS_SELECTION;
	} else {
		$scope.selectorIsAvailable = false;
	}

	$scope.currentPage = 1;
	$scope.totalItems = 0;
	$scope.itemsPerPage = 6;
	$scope.sortingAttributeId = sortingAttributeId;
	$scope.sortDirection = 'asc';
	$scope.displayView = 'list';
	$scope.filtersExpanded = false;
	$scope.listAttributeId = listAttributeId;
	$scope.gridAttributeId = gridAttributeId;
	$scope.datasheetAttributeId = datasheetAttributeId;
	$scope.imageAttributeId = imageAttributeId;
	$scope.isSmallDevice = false;
	if ($window.innerWidth < 768) {
		$scope.isSmallDevice = true;
		$scope.displayView = 'grid';
	}

	/**
	 * When the window size changes (device orientation)
	 * recheck if the list view should be allowed
	 */
	angular.element($window).bind('resize', function() {
		$scope.isSmallDevice = $window.innerWidth < 768;
		if ($scope.isSmallDevice) {
			$scope.displayView = 'grid';
		}
		return $scope.$apply();
	});

	$scope.$watch('itemsPerPage', function(){
		$scope.currentPage = 1;
		$scope.search();
	});

	/**
	 * Pagination click
	 * @param pageNo
	 */
	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
	};

	/**
	 * Expand all filters
	 */
	$scope.toggleFiltersExpanded = function() {
		$scope.filtersExpanded = !$scope.filtersExpanded;
		if ($scope.filtersExpanded) {
			$timeout(function(){
				$('.filterCatFlyout').equalHeights();
			});
		} else {
			$timeout(function(){
				$('.filterCatFlyout').css('height', '');
			});
		}
	};

	/**
	 * Change current view type
	 * @param viewType
	 */
	$scope.switchView = function(viewType) {
		$scope.displayView = viewType;
	};

	/**
	 * Handles the click on a sortable table column
	 * @param tableAttribute
	 */
	$scope.sortBy = function(tableAttribute){
		if($scope.sortingAttributeId === tableAttribute.ID) {
			if($scope.sortDirection == 'asc'){
				$scope.sortDirection = 'desc';
			} else {
				$scope.sortDirection = 'asc';
				$scope.sortingAttributeId = false;
			}
		} else {
			$scope.sortDirection = 'asc';
			$scope.sortingAttributeId = tableAttribute.ID;
		}
		$scope.currentPage = 1;
		this.search();
	};

	/**
	 * Reset all filters of this selector
	 */
	$scope.resetFilters = function() {
		activeFilters = {};
		$scope.sortingAttributeId = false;
		$scope.sortDirection = 'asc';
		$scope.search();
	};

	/**
	 * Checks if the search is currently filtered after attributeKey and valueKey
	 * @param attributeKey
	 * @param valueKey
	 * @returns {boolean}
	 */
	$scope.isFiltered = function(attributeKey, valueKey) {
		return !!(activeFilters[attributeKey] && activeFilters[attributeKey].indexOf(valueKey) > -1);
	};

	/**
	 * Checks if there are any filtered values for the attribute
	 * @param attributeKey
	 * @returns {boolean}
	 */
	$scope.attributeIsFiltered = function(attributeKey) {
		return !!(activeFilters[attributeKey] && activeFilters[attributeKey].length > 0);
	};

	/**
	 * Set an attribute and it's value to be filtered
	 * @param attributeKey
	 * @param valueKey
	 */
	$scope.toggleFilter = function(attributeKey, valueKey) {
		if (!activeFilters[attributeKey]) {
			activeFilters[attributeKey] = [];
		}
		var valIdx = activeFilters[attributeKey].indexOf(valueKey);
		if (valIdx > -1) {
			activeFilters[attributeKey].splice(valIdx, 1);
		} else {
			activeFilters[attributeKey].push(valueKey);
		}
		$scope.currentPage = 1;
		this.search();
	};

	/**
	 * Constructs the filter for a filtered elasticsearch query
	 * according to the currently set filters.
	 * @returns {*}
	 */
	function buildFilters() {
		var and = [];
		var aggregationKey = 'data.GroupedAttributes.'+filterAttributeId+'.TranslatedName';
		var aggregationValue = 'data.GroupedAttributes.'+filterAttributeId+'.FormattedValue';

		angular.forEach(activeFilters, function(attributeValues, attributeLabel){
			var filter = {
				nested: {
					path: 'data.GroupedAttributes.'+filterAttributeId,
					filter: {
						and: []
					}
				}
			};
			// We only want to filter the nested attribute that has the provided label
			var keyTermFilter = {term:{}};
			keyTermFilter.term[aggregationKey] = attributeLabel;
			filter.nested.filter.and.push(keyTermFilter);

			// We filter all selected values of the attribute with AND logic
			angular.forEach(attributeValues, function(attributeValue){
				var valueTermFilter = {term:{}};
				valueTermFilter.term[aggregationValue] = attributeValue;
				filter.nested.filter.and.push(valueTermFilter);
			});

			and.push(filter);
		});
		if (and.length > 0) {
			return {and: and};
		}
		return {};
	}

	/**
	 * Constructs the sorting configuration for the elasticsearch query
	 * according to the selected table columns' attributeId (CS)
	 * @returns {*}
	 */
	function buildSorting() {
		if ($scope.sortingAttributeId) {
			var sorting = {};
			sorting['data.GroupedAttributes.'+sortingAttributeId+'.NumberValue'] = {
				mode: 'avg',
				order: $scope.sortDirection
			};
			return [sorting];
		}
		return [{
			'sorting': 'asc'
		}];
	}

	/**
	 * Execute the actual search
	 */
	$scope.search = function() {
		document.body.style.cursor = 'wait';

		var queryFilter = buildFilters();
		var querySorting = buildSorting();

		ejs.search({
			index: elasticsearchIndex,
			type: elasticsearchType,
			body: {
				query: {
					filtered: {
						query: {
							terms: {
								contentServId: productsContentServIds
							}
						},
						filter: queryFilter
					}
				},
				sort: querySorting,
				aggs: {
					all: {
						global: {},
						aggs: {
							currentView: {
								filter: {
									term: {
										contentServViewId: csViewId
									}
								},
								aggs: {
									attributes: {
										nested: {
											path: 'data.GroupedAttributes.'+filterAttributeId
										},
										aggs: {
											key: {
												terms: {
													size: 100,
													field: 'data.GroupedAttributes.'+filterAttributeId+'.TranslatedName'
												},
												aggs: {
													value: {
														terms: {
															size: 100,
															field: 'data.GroupedAttributes.'+filterAttributeId+'.FormattedValue',
															order: {
																'_term': 'asc'
															}

														},
														aggs: {
															rawValueBucket: {
																terms: {
																	size: 1,
																	field: 'data.GroupedAttributes.' + filterAttributeId + '.Value'
																},
																aggs: {
																	valueTypeBucket: {
																		terms: {
																			size: 1,
																			field: 'data.GroupedAttributes.' + filterAttributeId + '.Type'
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					},
					attributes: {
						nested: {
							path: 'data.GroupedAttributes.'+filterAttributeId
						},
						aggs: {
							key: {
								terms: {
									size: 100,
									field: 'data.GroupedAttributes.'+filterAttributeId+'.TranslatedName'
								},
								aggs: {
									value: {
										terms: {
											size: 100,
											field: 'data.GroupedAttributes.'+filterAttributeId+'.FormattedValue',
											order: {
												'_term': 'asc'
											}
										},
										aggs: {
											rawValueBucket: {
												terms: {
													size: 1,
													field: 'data.GroupedAttributes.' + filterAttributeId + '.Value'
												},
												aggs: {
													valueTypeBucket: {
														terms: {
															size: 1,
															field: 'data.GroupedAttributes.' + filterAttributeId + '.Type'
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				},
				size: $scope.itemsPerPage,
				from: ($scope.currentPage-1)*$scope.itemsPerPage
			}
		}).then(function (resp) {
			$scope.results = resp.hits.hits;
			$scope.totalItems = resp.hits.total;
			if(typeof resp.aggregations.attributes.key !== 'undefined') {

				// We compare the aggregations for the available options with all options and mark the possible values
				$scope.availableFacets = _.map(resp.aggregations.all.currentView.attributes.key.buckets, function(property){
					var filteredProperty = _.find(resp.aggregations.attributes.key.buckets, {key: property.key});
					property.value.buckets = _.map(property.value.buckets, function(value){
						var filteredValue = _.find(filteredProperty.value.buckets, {key: value.key});
						if (filteredValue && filteredValue.rawValueBucket.buckets.length > 0) {
							value.rawValue = filteredValue.rawValueBucket.buckets[0].key;
							value.valueType = filteredValue.rawValueBucket.buckets[0].valueTypeBucket.buckets[0].key;
						} else {
							value.rawValue = '';
						}
						if (filteredValue && filteredValue.doc_count > 0) {
							value.selectable = true;
							value.doc_count = filteredValue.doc_count;
						} else {
							value.selectable = false;
							value.doc_count = 0;
						}
						return value;
					});
					return property;
				});

			}
			if(typeof resp.hits.hits[0] !== 'undefined') {
				$scope.tableAttributes = resp.hits.hits[0]._source.data.GroupedAttributes[listAttributeId];
				$scope.gridAttributes = resp.hits.hits[0]._source.data.GroupedAttributes[gridAttributeId];

				// As elasticsearch can't sort the aggregations by an external key, we do this
				var filterAttributeOrder = _.pluck(resp.hits.hits[0]._source.data.GroupedAttributes[filterAttributeId], 'TranslatedName');
				$scope.availableFacets = _.sortBy($scope.availableFacets, function(item) {
					return filterAttributeOrder.indexOf(item.key);
				});
			}
			angular.forEach($scope.results, function(attributeValues, attributeLabel){
				var links = attributeValues._source.data.GroupedAttributes[CS_ATTRIBUTES.datasheet][0].Links;
				var url = document.location.pathname;
				var datasheetPath = '';
				for(var i = 0; i < links.length; i++){
					if(url.indexOf('/de/') != -1){
						if(links[i].FormattedValue.indexOf('_DE_') != -1){
							datasheetPath =links[i].FormattedValue;
						}
					}
					if(url.indexOf('/en/') != -1){
						if(links[i].FormattedValue.indexOf('_EN_') != -1){
							datasheetPath =links[i].FormattedValue;
						}
					}
					if(url.indexOf('/fr/') != -1){
						if(links[i].FormattedValue.indexOf('_FR_') != -1){
							datasheetPath =links[i].FormattedValue;
						}
					}
				}
				if(datasheetPath == ''){
					datasheetPath = $scope.results[attributeLabel]._source.data.GroupedAttributes[CS_ATTRIBUTES.datasheet][0].Links[0].FormattedValue;
				}
				$scope.results[attributeLabel]._source.data.GroupedAttributes[CS_ATTRIBUTES.datasheet][0].mainDatasheet = datasheetPath;
			});
			document.body.style.cursor = 'default';
		}, function (error) {
			$log.error(error);
		});

	};

	/**
	 * Sort values of facet by rawValue if values are of type measureunit
	 *
	 * @param facetValues
	 * @returns {*}
	 */
	$scope.sortFacetValues = function(facetValues) {
		if (facetValues.length > 0 && facetValues[0].valueType == 'measureunit') {
			return _.sortBy(facetValues, function(facetValue) { return parseFloat(facetValue.rawValue); });
		} else {
			return facetValues;
		}

	};

	/**
	 * Checks if a given product has a kmat number.
	 *
	 * @param product
	 * @returns boolean
	 */
	$scope.hasKmat = function(product) {
		var kmatAttribute = _.findWhere(product.data.Attributes, {ID:kmatAttributeId});
		return kmatAttribute.Value && kmatAttribute.Value.length > 0;
	};

	ejs.ping({
		requestTimeout: 5000
	}, function (error) {
		$scope.selectorIsAvailable = !error;
	});

	// initial data from elastic
	$scope.search();

	/**
	 * Add line item to currentProductList
	 * with timeout for user feedback
	 *
	 * @param {object} product
	 */
	$scope.addToPlWidget = function(product) {
		var imageUrl = '/typo3conf/ext/baumer/Resources/Public/Images/Dummy83x56.png';
		try {
			imageUrl = product._source.data.GroupedAttributes[$scope.imageAttributeId][0].Links[0].FormattedValue;
		} catch (e) {}

		var lineItem = {
			amount: 1,
			label: product._source.label,
			groupTitle: csViewLabel,
			imageUrl: imageUrl,
			csViewRecord: product._source.uid
		};
		// add with spinner and 1 sec. timeout
		product.adding = true;
		$timeout(function () {
			UserProductList.addLineItem(lineItem);
			product.adding = false;
		}, 500);
	};

});
