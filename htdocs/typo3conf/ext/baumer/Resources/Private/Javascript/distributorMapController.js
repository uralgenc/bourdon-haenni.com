/*
 * Form Angular JS Controller
 */
baumerApp.controller('DistributorMapCtrl', ['$scope', '$filter', function($scope, $filter) {
	$scope.init = function(uuid) {
		$scope.uuid = uuid;
		$scope.mapData = mapsData[uuid];
		$scope.map = {};
		$scope.markerCluster = {};
		$scope.markerData = {};
		$scope.availableData = {
			countries: [],
			continents: [],
			types: [],
			productsegments: []
		};
		$scope.activeMarkers = $scope.mapData.markers;
		$scope.totalItems = $scope.activeMarkers.length;
		$scope.currentPage = 1;
		$scope.itemsPerPage = 10;
		$scope.itemsPerPages = [10, 25, 50];

		generateAvailableData($scope.mapData.markers);
		generateMap(uuid);
	};

	$scope.zoomToMarker = function(marker) {
		jumpTo($scope.uuid);
		$scope.map.setView([marker.lat + 0.15, marker.lon], 10, {animate: false});
		$scope.markerData[marker.uuid].openPopup();
	};

	$scope.$watch('markerSearch', function() {
		filterMarkers();
	});

	$scope.$watch('countryFilter', function() {
		filterMarkers();
	});

	$scope.$watch('continentFilter', function() {
		filterMarkers();
	});

	$scope.$watch('typeFilter', function() {
		filterMarkers();
	});

	$scope.$watch('productsegmentFilter', function() {
		filterMarkers();
	});

	$scope.resetFilter = function(filter) {
		if(filter == 'search') {
			$scope.markerSearch = '';
		}
		if(filter == 'country') {
			$scope.countryFilter = null;
		}
		if(filter == 'continent') {
			$scope.continentFilter = null;
		}
		if(filter == 'type') {
			$scope.typeFilter = null;
		}
		if(filter == 'productsegment') {
			$scope.productsegmentFilter = null;
		}
	};

	$scope.resetFilters = function() {
		$scope.markerSearch = null;
		$scope.countryFilter = null;
		$scope.continentFilter = null;
		$scope.typeFilter = null;
		$scope.productsegmentFilter = null;
		filterMarkers();
	};

	function filterMarkers() {
		$scope.activeMarkers = $filter('filter')($scope.mapData.markers, $scope.markerSearch);
		if($scope.countryFilter) { $scope.activeMarkers = $filter('filter')($scope.activeMarkers, {country: $scope.countryFilter}); }
		if($scope.continentFilter) { $scope.activeMarkers = $filter('filter')($scope.activeMarkers, {continent: $scope.continentFilter}); }
		if($scope.typeFilter) { $scope.activeMarkers = $filter('filter')($scope.activeMarkers, {type: $scope.typeFilter}); }
		if($scope.productsegmentFilter) { $scope.activeMarkers = $filter('filter')($scope.activeMarkers, {productsegment: $scope.productsegmentFilter}); }
		$scope.totalItems = $scope.activeMarkers.length;
		generateAvailableData($scope.activeMarkers);
		reDrawMarkers();
	}

	function generateMap(uuid) {
		L.Icon.Default.imagePath = '/typo3conf/ext/baumer/Resources/Public/Images/leaflet';
        var Icon = L.Icon.extend({options: {}});
        var blueIcon = new Icon({iconUrl: L.Icon.Default.imagePath + '/marker-icon.png' }),
            greenIcon = new Icon({ iconUrl: L.Icon.Default.imagePath + '/marker-icon-green.png' }),
            redIcon = new Icon({iconUrl: L.Icon.Default.imagePath + '/marker-icon-red.png' }),
			yellowIcon = new Icon({iconUrl: L.Icon.Default.imagePath + '/marker-icon-yellow.png' }),
            icon;

		$scope.map = L.map(uuid).setView([20, 0], 2);
		var mapboxLayer = L.tileLayer('https://a.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
			maxZoom: 13,
			minZoom: 2,
			id: mapsData[uuid].mapId,
			accessToken: mapsData[uuid].accessToken
		});

		$scope.map.addLayer(mapboxLayer);

		$scope.markerCluster = new L.MarkerClusterGroup({showCoverageOnHover: false});
		$.each($scope.mapData.markers, function(key, marker) {
            if(Object.keys(marker.type)[0] == '709') {
                icon = redIcon;
            } else if(Object.keys(marker.type)[0] == '713') {
                icon = greenIcon;
			} else if(Object.keys(marker.type)[0] == '1129') {
				icon = yellowIcon;
            } else {
                icon = blueIcon;
            }
			$scope.markerData[marker.uuid] = L.marker([marker.lat, marker.lon], { title: marker.company, zIndexOffset: 500, icon: icon });
			$scope.markerData[marker.uuid].bindPopup(getPopupContent(marker), {minWidth: 320});
			$scope.markerCluster.addLayer($scope.markerData[marker.uuid]);
		});
		$scope.map.addLayer($scope.markerCluster);
	}

	function reDrawMarkers() {
		$scope.markerCluster.clearLayers($scope.markerData);
		$.each($scope.activeMarkers, function(key, marker) {
			$scope.markerCluster.addLayer($scope.markerData[marker.uuid]);
		});
	}

	function generateAvailableData(markers) {
		$scope.availableData.continents = [];
		$scope.availableData.countries = [];
		$scope.availableData.types = [];
		$scope.availableData.productsegments = [];

		$.each(markers, function(key1, marker) {
			if(marker.country && $scope.availableData.countries.indexOf(marker.country) < 0) {
				$scope.availableData.countries.push(marker.country);
			}
			if(marker.continent && $scope.availableData.continents.indexOf(marker.continent) < 0) {
				$scope.availableData.continents.push(marker.continent);
			}
			$.each(marker.type, function(key2, type) {
				if(type && $scope.availableData.types.indexOf(type) < 0) {
					$scope.availableData.types.push(type);
				}
			});
			$.each(marker.productsegment, function(key2, productsegment) {
				if(productsegment && $scope.availableData.productsegments.indexOf(productsegment) < 0) {
					$scope.availableData.productsegments.push(productsegment);
				}
			});
		});

		$scope.availableData.countries = $scope.availableData.countries.sort();
		$scope.availableData.continents = $scope.availableData.continents.sort();
		$scope.availableData.types = $scope.availableData.types.sort();
		$scope.availableData.productsegments = $scope.availableData.productsegments.sort();
	}

	function getPopupContent(marker) {
		var htmlContent = '';
		htmlContent += '<h4>' + marker.company + '</h4>';

		htmlContent += '<div class="row-content">';
			htmlContent += '<div class="left">';
				htmlContent += marker.street + '<br />';
				htmlContent += marker.zip + ' ' + marker.city + '<br />';
				htmlContent += marker.country;
			htmlContent += '</div>';

			htmlContent += '<div class="right">';
				if(marker.email) {
					htmlContent += '<span class="fa fa-envelope-o"></span> <a href="mailto:' + marker.email + '">' + marker.email + '</a><br />';
				}
				if(marker.website) {
					htmlContent += '<span class="fa fa-globe"></span> <a href="' + marker.website + '" target="_blank">' + marker.website + '</a><br />';
				}
				if(marker.phone) {
					htmlContent += '<span class="fa fa-phone"></span> <a href="tel:' + marker.clearedPhone + '">' + marker.phone + '</a><br />';
				}
				if(marker.fax) {
					htmlContent += '<span class="fa fa-fax"></span> ' + marker.fax;
				}
			htmlContent += '</div>';
		htmlContent += '</div>';

        htmlContent += '<div class="clearfix"></div>';

		return htmlContent;
	}

	function jumpTo(uuid) {
		var url = location.href;
		location.hash = uuid;
		history.replaceState(null, null, url);
	}
}]);

baumerApp.filter('startFrom', function() {
	return function(input, start) {
		start = +start; //parse to int
		return input.slice(start);
	}
});
