/*
 * GLOSSARY
 */
(function ($) {
	$(document).ready(function () {
		// We can skip everything if glossary is not present
		if ($('#glossary').length == 1) {

			$('#glossary .btn-toolbar label').on('click', function () {
				$('#glossary div[id^="glossary-body-"]').collapse('hide');
				$('#glossary div[id^="glossary-panel-"]').hide();
				$($(this).data('target')).show();
				$($(this).data('target') + ' > div.panel-collapse').collapse('show');
			});

		}
	});
})(jQuery);
