/*
 * Form Angular JS Controller
 */
baumerApp.controller('MediaLibraryController', ['$scope', function($scope) {
	$scope.youtubeVideos = youtubeVideos;
	$scope.currentVideo = {};
	$scope.filtersExpanded = false;

	$scope.changeVideo = function (video) {
		$scope.currentVideo = video;
		var iFrame = document.getElementById('videoplayer');
		iFrame.src = 'http://www.youtube.com/embed/' + video.videoId + '?rel=0&amp;fs=1&amp;showinfo=0&amp;autohide=1&amp;hd=1';
		$('html, body').animate({scrollTop: $('#videoplayer').offset().top - 100}, 300);
	};
	$scope.changeVideo(youtubeVideos[0]);

	$scope.filterLanguages = [];
	$scope.availableLanguages = _.unique(_.pluck(youtubeVideos, 'language'));

	$scope.filterCategories = [];
	$scope.availableCategories = _.unique(_.map(youtubeVideos, function(e){return e.categories[0]}));

	$scope.updateLanguageFilter = function(language) {
		if(_.contains($scope.filterLanguages, language)) {
			$scope.filterLanguages = _.filter($scope.filterLanguages, function(lang){ return lang != language; });
		} else {
			$scope.filterLanguages.push(language);
		}
	};

	$scope.updateCategoryFilter = function(category) {
		if(_.contains($scope.filterCategories, category)) {
			$scope.filterCategories = _.filter($scope.filterCategories, function(cat){ return cat != category; });
		} else {
			$scope.filterCategories.push(category);
		}
	};

	$scope.isFilteredByLanguage = function (language) {
		return _.contains($scope.filterLanguages, language);
	};
	$scope.isFilteredByCategory = function (category) {
		return _.contains($scope.filterCategories, category);
	};

	$scope.filterVideos = function(video) {
		var languageMatches = _.contains($scope.filterLanguages, video.language);
		if($scope.filterLanguages.length == 0){
			languageMatches = true;
		}
		var categoryMatches = _.contains($scope.filterCategories, video.categories[0]);
		if($scope.filterCategories.length == 0){
			categoryMatches = true;
		}
		return languageMatches && categoryMatches;
	};

	$scope.toggleFilterExpansion = function() {
		$scope.filtersExpanded = !$scope.filtersExpanded;
		if ($scope.filtersExpanded) {
			$timeout(function(){
				$('.filterCatFlyout').equalHeights();
			});
		} else {
			$timeout(function(){
				$('.filterCatFlyout').css('height', '');
			});
		}
	};

	$scope.resetFilters = function() {
		$scope.filterLanguages = [];
		$scope.filterCategories = [];
	};
}]);
