/*
 * FLYOUT
 */
(function($){
	$(document).ready(function(){

		var flyoutRequest = null;

		//$("#searchfield").on('input', function() {
		//	var tx_baumer_flyout = {'q' : $(this).val()};
		//	flyoutRequest = $.ajax({
		//		type: "GET",
		//		url: '/',
		//		data: {
		//			type: '1337',
		//			tx_baumer_flyout : tx_baumer_flyout
		//		},
		//		dataType : 'html'
		//	});
		//
		//	flyoutRequest.done(function(result) {
		//		$('#flyout-container').html(result);
		//		if($('#flyout-container').not(':visible') && $('#flyout-container .thumbnail').length > 0) {
		//			$('#flyout-container').slideDown({
		//				duration: 1000,
		//				easing: 'easeInOutSine'
		//			});
		//		} else if($('#flyout-container').is(':visible') && $('#flyout-container .thumbnail').length == 0) {
		//			$('#flyout-container').slideUp({
		//				duration: 500,
		//				easing: 'easeInOutSine'
		//			});
		//		}
		//	});
		//
		//});


		$(document).on('click', '#flyout-container button.close', function(){
			if($('#flyout-container').is(':visible')){
				$('#flyout-container').slideUp({
					duration: 500,
					easing: 'easeInOutSine'
				});
			}
		});

		$(window).resize(function() {
			if($('#flyout-container').is(':visible')) {
				$('#flyout-container').hide();
			}
		});
	});
})(jQuery);
