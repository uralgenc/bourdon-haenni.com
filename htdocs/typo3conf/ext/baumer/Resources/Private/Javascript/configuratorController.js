'use strict';

baumerApp.filter('wrapProductCode', ['$sce', function($sce){
	return function (text) {
		if (!text) {
			return '';
		}
		text = text.replace(/#/g, '<span class="productcode-nonterminal">#</span>');
		return $sce.trustAsHtml(text);
	};
}]);

baumerApp.filter('removeValueIdentifier', function(){
	return function (text) {
		return text.replace(/\[.*\] - /g, '');
	};
});

baumerApp.controller('ConfiguratorCtrl',
['$http', '$scope', '$rootScope', '$timeout', '$interval', '$q', 'sharedProduct',
function ($http, $scope, $rootScope, $timeout, $interval, $q, sharedProduct) {

	// We must set this explicitly or IE w
	$http.defaults.headers.common['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
	$http.defaults.headers.common['Cache-Control'] = 'no-cache';
	$http.defaults.headers.common['Pragma'] = 'no-cache';
	$http.defaults.cache = false;

	$scope.loading = true;
	$scope.conflictMode = false;
	$scope.notAvailable = false;
	$scope.lastSelectionFailed = false;
	$scope.filtersExpanded = false;
	$scope.variant = false;
	$scope.showConfigurationReset = false;
	$scope.encowayConfigurationId = 0;
	$scope.selectedParameterValues = [];
	$scope.affectedParameters = [];
	$scope.containers = [];
	$scope.lastChangedParameter = {
		parameter : { id: false },
		value: false
	};

	/**
	 * Returns true if a parameter contains a value that is marked as selected
	 * @param {object} parameter
	 * @returns {boolean}
	 */
	$scope.parameterContainsSelectedValues = function (parameter) {
		var selectedValue = _.find(parameter.values, {selected: true});
		return typeof(selectedValue) != 'undefined';
	};

	/**
	 * Toggle the expansion of the filters
	 */
	$scope.toggleFiltersExpanded = function () {
		$scope.filtersExpanded = !$scope.filtersExpanded;
		if ($scope.filtersExpanded) {
			$timeout(function () {
				angular.element('.filterCatFlyout').each(function (index, el) {
					angular.element(el).parents('.row').find('.filterCatFlyout').equalHeights();
				});
			});
		} else {
			$timeout(function () {
				angular.element('.filterCatFlyout').css('height', '');
			});
		}
	};

	/**
	 * Takes an encoway parameter container and enriches every parameter by
	 * extracting the selected value to the top level.
	 * Afterwards the parameters are chunked into pieces for responsive layout columns.
	 *
	 * @param {object} container
	 * @returns {object}
	 */
	function enrichSelectedParameterValues(container) {
		container.parameters = _.map(container.parameters, function (parameter) {
			var selectedValues = _.filter(parameter.values, function (paramValue) {
				return paramValue.selected && typeof(paramValue.value) != "undefined";
			});
			if (selectedValues.length > 0) {
				//noinspection JSUnresolvedVariable
				parameter.selectedValues = selectedValues;
				parameter.selected = true;
				$scope.selectedParameterValues.push({
					parameterId: parameter.id,
					parameterName: parameter.translatedName,
					parameterUndoable: parameter.undoable,
					selectedValues: selectedValues
				});
			} else {
				parameter.selected = false;
			}

			return parameter;
		});

		container.parameters = _.chain(container.parameters).groupBy(function (el, idx) {
			return Math.floor(idx / 4);
		}).toArray().value();

		return container;
	}

	/**
	 * Handles a successfull API response from the TYPO3 Encoway API
	 * @param {object} data
	 */
	function handleParameterResponse(data) {
		if (data.success) {
			if (!data.container) {
				$scope.notAvailable = true;
				return;
			}
			$scope.selectedParameterValues = [];
			var containers = [];
			angular.forEach(data.container, function(container) {
				var processedContainer = enrichSelectedParameterValues(container);
				containers.push(processedContainer);
			});
			$scope.containers = containers;
			$scope.loading = false;
			$scope.conflictMode = false;
			$scope.productCode = data.typekey;
			sharedProduct.setCode($scope.productCode);
			sharedProduct.setContainer($scope.containers);
			$scope.encodedProductCode = encodeURIComponent($scope.productCode);
			// If the configuration is finished, we look for a variant code
			if (data.typekey.length > 0 && data.typekey.indexOf('#') == -1) {
				// Get variant code
				$http.get(encowayConfiguratorUrls.variant).then(function(response){
					// todo: Variant Code should never be shown (s. B4PB-345)
					// $scope.variant = response.data.variant;
					//if ($scope.variant.length > 0) {
						// sharedProduct.setVariantCode($scope.variant);
					//}
				});
				// Get shipping information
				$http.get(encowayConfiguratorUrls.priceShipping + '&tx_baumer_configurator%5BtypeKey%5D=' + data.typekey).then(function(response){
					if (response.data.success) {
						// todo: Price should never be shown (s. B4PB-345)
						// sharedProduct.setPrice(response.data.priceShipping.price);
						sharedProduct.setPrice(false);
						sharedProduct.setShipping(response.data.priceShipping.leadTime);
					} else {
						sharedProduct.setPrice(false);
						sharedProduct.setShipping(false);
					}
				});
			} else {
				$scope.variant = false;
				sharedProduct.setVariantCode($scope.variant);
				sharedProduct.setPrice(false);
				sharedProduct.setShipping(false);
			}
		} else if (data.status === 'UNCONFIRMED') {
			$scope.loading = false;
			$scope.conflictMode = true;
			$scope.affectedParameters = data.affectedParameters;
		} else if (data.status === 'CONFLICTED'){
			$scope.loading = false;
			alert('setting this parameter would yield an unresolvable conflict.')
		}
		else {
			$scope.loading = false;
			$scope.lastSelectionFailed = true;
		}
	}

	/**
	 * Update the value of a parameter (either set or unset).
	 * Also takes an override parameter to ignore the action,
	 * e.g. if the parameter is not undoable.
	 *
	 * @param {object} parameter
	 * @param {object} value
	 * @param {boolean} selected
	 * @param {boolean} actionPossible
	 */
	$scope.update = function (parameter, value, selected, actionPossible) {
		if (actionPossible) {
			if (typeof value !== 'object' || value === null) {
				value = {
					value: value,
					translatedValue: value
				};
			}
			$scope.lastChangedParameter = {
				parameter: parameter,
				value: value.value,
				translatedValue: value.translatedValue
			};
			$scope.loading = true;
			if (selected) {
				var uri = encowayConfiguratorUrls.setParameter + '&tx_baumer_configurator%5BparameterId%5D=' + parameter.id;

				$http.get(encowayConfiguratorUrls.setParameter +
					'&tx_baumer_configurator%5BparameterId%5D=' + parameter.id +
					'&tx_baumer_configurator%5BparameterValue%5D=' + value.value
				).success(handleParameterResponse);
			} else {
				//@todo finish error handling
				$http.get(encowayConfiguratorUrls.unsetParameter +
					'&tx_baumer_configurator%5BparameterId%5D=' + parameter.id
				).success(handleParameterResponse);
			}
		}
	};

	// If a signal "saveConfiguration" is emitted, we do so
	$scope.$on('saveConfiguration', function () {
		$scope.save()
	});

	/**
	 * Save the current configuration state using the TYPO3 Encoway API
	 *
	 * @returns {deferred.promise|{then, catch, finally}}
	 */
	$scope.save = function () {
		var deferred = $q.defer();
		$http.get(encowayConfiguratorUrls.saveConfiguration +
			'&tx_baumer_configurator%5BproductCode%5D=' + encodeURIComponent($scope.productCode)).success(function (data) {
			$scope.encowayConfigurationId = data.encowayConfiguration;
			//noinspection JSUnresolvedVariable
			deferred.resolve(data.encowayConfigurationShortCode);
            $rootScope.$broadcast('savedConfiguration', data);
		}).error(function () {
			$scope.encowayConfigurationId = null;
			deferred.reject('saving of configuration failed');
		});
		sharedProduct.setEncowayConfig(deferred.promise);
		return deferred.promise;
	};

	/**
	 * Save the current configuration to the user's profile (if he's logged in)
	 */
	$scope.saveToProfile = function () {
		var saveNamedConfigurationUrl = encowayConfiguratorUrls.saveNamedConfiguration;
		saveNamedConfigurationUrl += '&tx_baumer_configurator%5BencowayConfiguration%5D=' + $scope.encowayConfigurationId;
		saveNamedConfigurationUrl += '&tx_baumer_configurator%5Btitle%5D=' + encodeURIComponent($scope.configurationTitle);
		if ($scope.productList) {
			saveNamedConfigurationUrl += '&tx_baumer_configurator%5BproductList%5D=' + $scope.productList;
		}
		$http.get(saveNamedConfigurationUrl).success(function () {
			$scope.configurationTitle = '';
		});
	};

	/**
	 * Reset the current configuration state
	 */
	$scope.resetConfiguration = function() {
		$scope.loading = true;
		$scope.showConfigurationReset = false;
		$http.get(encowayConfiguratorUrls.reset)
			.success(handleParameterResponse)
			.error(
				function () {
					$scope.loading = false;
					$scope.notAvailable = true;
				}
			);
	};

	/**
	 * Load the initial state of the configuratore
	 */
	$scope.initializeConfiguration = function() {
		$scope.loading = true;
		$http.get(encowayConfiguratorUrls.getParameters)
			.success(handleParameterResponse)
			.error(
				function () {
					$scope.loading = false;
					$scope.notAvailable = true;
				}
			);
	};

	/**
	 * Get all parameters from containers as one array
	 *
	 * @returns {Array}
	 */
	var getAllParameters = function() {
		if ($scope.containers) {
			// all parameter from containers
			var parametersFromContainers = _.flatten(_.pluck($scope.containers, 'parameters'));
			// in case of a conflict we can have new values for a parameter
			if ($scope.affectedParameters.length > 0) {
				var parametersFromAffectedParameters = $scope.affectedParameters;
				_.each(parametersFromAffectedParameters, function(newParameter) {
					var affectedExistingParameter = _.find(parametersFromContainers, function(parameterFromContainer) {
						return parameterFromContainer.id === newParameter.id;
					});
					affectedExistingParameter.values = _.union(affectedExistingParameter.values, newParameter.values);
				});
			}
			return parametersFromContainers;
		} else {
			return [];
		}
	};

	/**
	 * Cancel setting of a parameter with conflict
	 * Call API to decline operation and end conflict mode
	 */
	$scope.cancelConflictMode = function() {
		$scope.loading = true;
		$http.get(encowayConfiguratorUrls.confirmOperation +
		          '&tx_baumer_configurator%5Bconfirm%5D=0'
		).success(function(data) {
			$scope.loading = false;
			$scope.conflictMode = false;
		});
	};

	// Initialize the configuration by loading the configuration
	$scope.initializeConfiguration();

	$interval(function () {
		//@todo implement error handling for heartbeat
		$http.get(encowayConfiguratorUrls.heartBeat)
			.success(function (data, status, header, config) {
				//console.log('HeartBeat successful');
			});
	}, 60000);

	/**
	 * Checks if a given parameter has selected values other then [&]
	 */
	$scope.parameterIsSelected = function(parameter) {
		return (   parameter.selectedValues
		        && parameter.selectedValues
		        && parameter.selectedValues.length > 0
				&& parameter.selectedValues[0].value !== "&");
	};
}]);
