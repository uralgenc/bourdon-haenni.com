
(function() {

	/*
	 * Product List Angular JS Controller
	 *
	 * @author: Martin R. Krause <martin.r.krause@gmx.de>
	 */
	baumerApp.controller(
		'ProductListCtrl',
		['$http', '$scope', '$rootScope', '$modal', 'UserProductList',
			function ($http, $scope, $rootScope, $modal, UserProductList) {

				var spinner = '<spinner></spinner>';
				var url = '/index.php?eID=ajax';

				// init static scope variables
				if(typeof feUser !== 'undefined') {
					$scope.feUser = feUser;
				}
				$scope.widgetInTransition = false;
				$scope.productListCodeRegex = /\S{4}-\S{4}$/;
				$scope.currentProductList = UserProductList.getProductList();

				// initialize values from localStorage or window
				if (typeof PRODUCT_MAIN_GROUPS == 'object') $scope.productMainGroups = PRODUCT_MAIN_GROUPS;
				if (typeof window.productFamilies == 'object') $scope.productFamilies = window.productFamilies;
				if (typeof window.productLists == 'object') $scope.productLists = window.productLists;

				$scope.addToPlWidget = function(lineItem) {
					$scope.currentProductList = UserProductList.addLineItem(lineItem);
					$scope.addNewLineItemMode = false;
					$scope.newLineItem = {amount: 1};
				};

				/**
				 * Close the new line item form
				 */
				$scope.trashNewLineItem = function () {
					$scope.addNewLineItemMode = false;
					$scope.newLineItem = {amount: 1};
					$scope.addProduct.$setPristine();
				};

				$scope.$on('updateProductList', function() {
					$scope.currentProductList = UserProductList.getProductList();
				});

				/**
				 * Remove line item from current product list
				 */
				$scope.removeFromCurrentProductList = function (idx) {
					$scope.currentProductList = UserProductList.removeLineItemByIdx(idx);
				};

				/**
				 * Reset the current product list
				 */
				$scope.clearCurrentProductList = function () {
					$scope.currentProductList = UserProductList.clear();
				};

				$scope.saveProductList = function(){

					UserProductList.setProductList(UserProductList.currentProductList);

				};

				/**
				 * Delete a product list (ajax)
				 */
				$scope.deleteProductList = function (idx) {
					var productList = $scope.productLists[idx];
					var arguments = {
						plugin: 'ProductList',
						controller: 'ProductList',
						action: 'delete',
						arguments: {
							productListId: productList.uid
						}
					};

					productList.deleting = true;
					$http.get(url + '&' + $.param(arguments)).success(function () {
						productList.deleting = false;
						$scope.productLists.splice(idx, 1);
					}).error(function (data) {
						alert('Something went wrong. The product list could not be deleted (1421244645)\n\r' + data);
					});
				};

				$scope.loadProductListFromWidget = function () {
					var arguments = {
						plugin: 'ProductList',
						controller: 'ProductList',
						action: 'findProductListByCode',
						arguments: {
							code: $scope.code.trim().toUpperCase()
						}
					};

					$scope.isLoading = true;
					$http.get(url + '&' + $.param(arguments)).success(function (data) {
						$scope.isLoading = false;
						$scope.currentProductList = data.productList;
					}).error(function (data) {
						alert('Something went wrong. The product list could not be found (1421850452)\n\r' + data);
						$scope.isLoading = true;
					});
				};

				/**
				 * loadProductList by product list code (ajax)
				 *
				 * if setCurrent loaded product list will become currentProductList
				 */
				$scope.loadProductList = function (productListCode, setToCurrent) {
					var productList = {};
					var code;
					var arguments = {
						plugin: 'ProductList',
						controller: 'ProductList',
						action: 'findProductListByCode',
						arguments: {
							code: productListCode.trim().toUpperCase()
						}
					};

					if (productListCode.length > 8) {
						// currentList is always in wiget, so we show spinner while in (possible) transition
						if (setToCurrent) $scope.widgetInTransition = true;
						$http.get(url + '&' + $.param(arguments)).success(function (data) {
							productList = data.productList;
							if (setToCurrent) {
								// this product list is prestine and new, so this time we want to keep the code
								// that will be reset by updateCurrentProductList
								code = productList.code;
								UserProductList.setProductList(productList);
								$scope.currentProductList = UserProductList.getProductList();
								$scope.widgetInTransition = false;
								$scope.cplIsDirty = false;
							} else {
								return productList;
							}
						}).error(function (data) {
							if (setToCurrent) {
								$scope.widgetInTransition = false;
							}
							alert('Something went wrong. The product list could not be found (1421663462)\n\r' + data);
						});
					}
				};

				/**
				 * Share an existing product list wrappers
				 *
				 * @param {object} pList
				 */
				$scope.shareExisting = function (pList) {
					$scope.shareProductList = pList;
					$scope.openShareModal();
				};

				/**
				 * Opens the share modal that allows the user to save the currentProductList and generate a code
				 */
				$scope.openShareModal = function () {
					var shareCurrent;
					if (!$scope.shareProductList) {
						$scope.shareProductList = $scope.currentProductList;
						shareCurrent = true;
					} else {
						shareCurrent = false;
					}
					var shareModalInstance = $modal.open({
						templateUrl: 'shareModalContent.html',
						controller: 'ShareModalInstanceCtrl',
						size: '',
						resolve: {
							shareProductList: function () {
								return $scope.shareProductList;
							}
						}
					});

					shareModalInstance.result.then(function (pList) {
						if (shareCurrent) {
							UserProductList.setProductList(pList);
							$scope.currentProductList = pList;
						}
						$scope.shareProductList = null;
					});
				};

				/**
				 * Opens the load modal that allows the user to load the currentProductList by Code
				 */
				$scope.openLoadModal = function () {
					var loadModalInstance = $modal.open({
						templateUrl: 'loadModalContent.html',
						controller: 'LoadModalInstanceCtrl',
						size: '',
						resolve: {
							productListCodeRegex: function () {
								return $scope.productListCodeRegex;
							}
						}
					});

					loadModalInstance.result.then(function (loadedProductList) {
						UserProductList.setProductList(loadedProductList);
					}, function () {
					});
				};

				/**
				 * Open the save modal that allows the user to save/update the currentProductList and
				 * asiings it to his user account
				 */
				$scope.openSaveModal = function () {

					var saveModalInstance = $modal.open({
						templateUrl: 'saveModalContent.html',
						controller: 'SaveModalInstanceCtrl',
						size: '',
						resolve: {
							saveProductList: function () {
								return $scope.currentProductList;
							}
						}
					});

					saveModalInstance.result.then(function (pList) {
						if (typeof pList == 'object') {
							if (pList.lineItems.length > 0) {
								UserProductList.setProductList(pList);
								$scope.currentProductList = UserProductList.getProductList();
							}
						}
					}, function () {
					});
				};

				/**
				 * Sets the product family for a given product main group
				 *
				 * @param groupUid
				 */
				$scope.setProductFamiliesForGroup = function (groupUid) {
					if (groupUid > 0) {
						$scope.newLineItem.familyOptions = _.filter(
							productFamilies,
							function (family) {
								return family.mainGroupUid == groupUid;
							}
						);
					}
				};

				/**
				 * Click the product finder slider button
				 */
				$scope.openPFinder = function () {
					$('#togglePfinder').click();
				};
				/**
				 * Get the group title for a lineItem
				 *
				 * @param lineItem
				 * @returns {*}
				 */
				$scope.groupTitle = function (lineItem) {
					var value = 'n/a';

					if (lineItem.mainGroup) {
						group = _.find($scope.productMainGroups, function (group) {
							return group.uid == lineItem.mainGroup;
						});

						if (typeof group !== 'undefined') {
							value = group.title;
						}
					} else if (lineItem.groupTitle) {
						value = lineItem.groupTitle;
					}

					return value
				};

				/**
				 * Get the produdct configuratoin sring
				 *
				 * @param lineItem
				 * @returns {*}
				 */
				$scope.configuration = function (lineItem) {
					var value = 'n/a';

					if (lineItem.configuration) {
						value = lineItem.configuration;
						if (lineItem.variant) {
							value += ' (' + lineItem.variant + ')';
						}
					} else if (lineItem.family) {
						family = _.find($scope.productFamilies, function (family) {
							return family.uid == lineItem.family;
						});
						if (typeof family !== 'undefined') {
							value = family.title;
						}
					} else if (lineItem.label) {
						value = lineItem.label;
					}

					return value;
				};

				/**
				 * Legacy Wrapper for familyTitles
				 *
				 * @param lineItem
				 * @returns {*}
				 */
				$scope.familyTitle = function (lineItem) {
					return $scope.configuration(lineItem);
				};

				$scope.imageUrl = function (lineItem) {
					if (typeof lineItem.imageUrl !== 'undefined' && lineItem.imageUrl !== null && lineItem.imageUrl.length > 0) {
						return lineItem.imageUrl;
					} else {
						// TODO: Proper Dummy image handling
						return '/typo3conf/ext/baumer/Resources/Public/Images/Dummy83x56.png';
					}
				};

				/**
				 * Get the url for a line item
				 *
				 * @param lineItem
				 * @returns {*}
				 */
				$scope.url = function (lineItem) {
					var familyLink;

					if (lineItem.url) {
						return lineItem.url;
					} else {
						if (lineItem.encowayConfiguration) {
							return encowayConfigurationUrl
								.replace('XXXXX', encodeURIComponent(lineItem.encowayConfiguration))
								.replace('YYYYY', encodeURIComponent(lineItem.label));
						} else if (lineItem.amiradaConfiguration) {
							return amiradaConfigurationUrl.replace('XXXXX', lineItem.amiradaConfiguration);
						} else if (lineItem.mainGroup > 0) {
							familyLink = $('#panelPfinder').find('a[data-maingroup]');
							if (familyLink) {
								return familyLink.attr('href');
							} else {
								return false;
							}
						} else if(lineItem.csViewRecord > 0) {
							return directConfigurationUrl.replace('XXXXX', lineItem.csViewRecord);
						} else {
							return false;
						}
					}

				};

				/**
				 * init new line item (if not set)
				 */
				$scope.initNewLineItem = function () {
					if (typeof $scope.newLineItem == 'undefined') {
						$scope.newLineItem = {
							amount: 1,
							imageUrl: false
						};
					} else {
						$scope.newLineItem = angular.copy($scope.newLineItem);
					}
					$scope.addNewLineItemMode = true;
				};
			}]); // end baumerApp.controller

	/**
	 * Save Modal Instance Controller
	 */
	baumerApp.controller(
		'SaveModalInstanceCtrl',
		['$scope', '$http', '$modalInstance', 'saveProductList',
			function ($scope, $http, $modalInstance, saveProductList) {
				var url = '/index.php?eID=ajax';

				$scope.productList = saveProductList;
				$scope.feUserAuthorized = (window.feUser && window.feUser.uid > 0);
				$scope.isSaving = false;
				$scope.codeAvailable = false;
				$scope.productList.updateable = $scope.productList.uid > 0 && window.feUser.uid == $scope.productList.feUser;
				$scope.productList.update = $scope.productList.updateable;

				/*
				 * Save the current product list (from localStorage)
				 * Update or creates a product list via ajax
				 *
				 * Ajax return data includes the product list's url and code
				 */
				$scope.save = function () {
					var arguments = {
						plugin: 'ProductList',
						controller: 'ProductList',
						action: 'save',
						arguments: {
							productList: $scope.productList
						}
					};

					// let's save and generate a code
					$scope.isSaving = true;
					$http.get(url + '&' + $.param(arguments)).success(function (data) {
						$scope.isSaving = false;
						$modalInstance.close(data.productList);
					}).error(function (data) {
						alert('Something went wrong. The product list could not be saved (1423058384)\n\r' + data);
						$scope.isSaving = false;
						$modalInstance.close();

					});
				};

				$scope.close = function () {
					$modalInstance.close();
				};
			}]);

	/**
	 * Share Modal Instance Controller
	 */
	baumerApp.controller(
		'ShareModalInstanceCtrl',
		['$scope', '$http', '$modalInstance', 'shareProductList',
			function ($scope, $http, $modalInstance, shareProductList) {

				var url = '/index.php?eID=ajax';

				$scope.productList = shareProductList;
				$scope.isSaving = false;
				$scope.codeAvailable = false;

				if ($scope.productList.code) {
					// we already have a code
					$scope.codeAvailable = true;
					$scope.productList.linkUrl = productListUrl.replace('XXXXX', encodeURIComponent($scope.productList.code));
				}
				/*
				 * Save the current product list (from localStorage)
				 * Update or creates a product list via ajax
				 *
				 * Ajax return data includes the product list's url and code
				 */
				$scope.saveForSharing = function () {
					var productListUrl = window.productListUrl;
					var arguments = {
						plugin: 'ProductList',
						controller: 'ProductList',
						action: 'saveForSharing',
						arguments: {
							productList: {
								name: $scope.productList.name,
								description: $scope.productList.description
							},
							lineItems: $scope.productList.lineItems
						}
					};

					if($scope.productList.feUser > 0) {
						arguments.arguments.productList.feUser = $scope.productList.feUser;
					}

					if ($scope.productList.code) {
						// we already have a code
						$scope.codeAvailable = true;
						$scope.productList.linkUrl = productListUrl.replace('XXXXX', encodeURIComponent($scope.productList.code));
					} else {
						// let's save and generate a code
						$scope.isSaving = true;
						$http.post(url, $.param(arguments), { headers: {'Content-Type':'application/x-www-form-urlencoded'} }).success(function (data) {
							$scope.productList.linkUrl = data.productListLinkUrl;
							$scope.productList.code = data.productListCode;
							$scope.isSaving = false;
							$scope.codeAvailable = true;
						}).error(function (data) {
							alert('Something went wrong. The product list could not be saved (1421659008)\n\r' + data);
							$scope.isSaving = false;
						});
					}
				};

				$scope.close = function () {
					$modalInstance.close($scope.productList);
				};
			}]);


	/**
	 * Load Modal Instance Controller
	 */
	baumerApp.controller('LoadModalInstanceCtrl',
		['$scope', '$http', '$modalInstance', 'productListCodeRegex',
			function ($scope, $http, $modalInstance, productListCodeRegex) {
		var url = '/index.php?eID=ajax';

		$scope.productListCodeRegex = productListCodeRegex;
		$scope.load = function () {
			var arguments = {
				plugin: 'ProductList',
				controller: 'ProductList',
				action: 'findProductListByCode',
				arguments: {
					code: $scope.code.trim().toUpperCase()
				}
			};

			$scope.isLoading = true;
			$http.get(url + '&' + $.param(arguments)).success(function (data) {
				$scope.isLoading = false;
				$modalInstance.close(data.productList);
			}).error(function (data) {
				alert('Something went wrong. The product list could not be found (1421850452)\n\r' + data);
				$scope.isLoading = true;
			});
		};
	}]);
})();
