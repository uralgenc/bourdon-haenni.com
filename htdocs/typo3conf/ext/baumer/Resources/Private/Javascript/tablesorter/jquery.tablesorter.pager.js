(function($) {
	$.extend({
		tablesorterPager: new function() {

			function updatePageDisplay(c) {
				var s = $(c.cssPageDisplay,c.container).val((c.page+1) + c.seperator + c.totalPages);
			}

			function updatePagePaginator(c) {
				if($(c.cssNumber, c.container).length > 0) {
					$(c.cssNumber, c.container).remove();
					var html = '', currentPage = c.page + 1;

					for(var i = 1; i <= c.totalPages; i++) {
						if(i == currentPage) {
							html += '<li class="page current"><a href="#page' + i + '">' + i + '</a></li>';
						} else {
							html += '<li class="page"><a href="#page' + i + '">' + i + '</a></li>';
						}
					}

					$(c.cssPrev,c.container).after(html);
				}
			}

			function setPageSize(table,size) {
				var c = table.config;
				c.size = size;
				c.totalPages = Math.ceil(c.totalRows / c.size);
				c.pagerPositionSet = false;
				moveToPage(table);
				fixPosition(table);
			}

			function fixPosition(table) {
				var c = table.config;
				if(!c.pagerPositionSet && c.positionFixed) {
					var c = table.config, o = $(table);
					if(o.offset) {
						c.container.css({
							top: o.offset().top + o.height() + 'px',
							position: 'absolute'
						});
					}
					c.pagerPositionSet = true;
				}
			}

			function moveToFirstPage(table) {
				var c = table.config;
				c.page = 0;
				moveToPage(table);
			}

			function moveToLastPage(table) {
				var c = table.config;
				c.page = (c.totalPages-1);
				moveToPage(table);
			}

			function moveToNextPage(table) {
				var c = table.config;
				c.page++;
				if(c.page >= (c.totalPages-1)) {
					c.page = (c.totalPages-1);
				}
				moveToPage(table);
			}

			function moveToPrevPage(table) {
				var c = table.config;
				c.page--;
				if(c.page <= 0) {
					c.page = 0;
				}
				moveToPage(table);
			}

			function moveToPageNumber(table, element) {
				var number = element.text();
				var c = table.config;
				c.page = number - 1;

				moveToPage(table);
			}

			function moveToPage(table) {
				var c = table.config;
				if(c.page < 0 || c.page > (c.totalPages-1)) {
					c.page = 0;
				}

				renderTable(table,c.rowsCopy);
			}

			/*function generatePagerHtml(table) {

				$(table).before(
					'<div class="table-pager text-right">' +
					'<select class="pagesize">' +
					'<option value="10">10</option>' +
					'<option value="30">30</option>' +
					'<option value="40">40</option>' +
					'</select>' +
					'</div>'
				);

				$(table).after(
					'<div class="table-pager">' +
						'<div class="pager">' +
							'<ul class="pagination">' +
								'<li class="first">' +
									'<i class="fa fa-angle-double-left"></i>' +
								'</li>' +
								'<li class="prev">' +
								'<i class="fa fa-angle-left"></i>' +
									'</li>' +
								'<li class="page"></li>' +
								'<li class="next">' +
									'<i class="fa fa-angle-right"></i>' +
								'</li>' +
								'<li class="last">' +
									'<i class="fa fa-angle-double-right"></i>' +
								'</li>' +
							'</ul>' +
						'</div>' +
					'</div>'
				);
			}*/

			function renderTable(table,rows) {

				var c = table.config;
				var l = rows.length;
				var s = (c.page * c.size);
				var e = (s + c.size);
				if(e > rows.length ) {
					e = rows.length;
				}


				var tableBody = $(table.tBodies[0]);

				// clear the table body

				$.tablesorter.clearTableBody(table);

				for(var i = s; i < e; i++) {

					//tableBody.append(rows[i]);

					var o = rows[i];
					var l = o.length;
					for(var j=0; j < l; j++) {

						tableBody[0].appendChild(o[j]);

					}
				}

				fixPosition(table,tableBody);

				$(table).trigger("applyWidgets");

				if( c.page >= c.totalPages ) {
        			moveToLastPage(table);
				}

				updatePageDisplay(c);

				updatePagePaginator(c);
			}

			this.appender = function(table,rows) {

				var c = table.config;

				c.rowsCopy = rows;
				c.totalRows = rows.length;
				c.totalPages = Math.ceil(c.totalRows / c.size);

				renderTable(table,rows);
			};

			this.defaults = {
				size: 10,
				offset: 0,
				page: 0,
				totalRows: 0,
				totalPages: 0,
				container: null,
				cssNext: '.next',
				cssPrev: '.prev',
				cssFirst: '.first',
				cssLast: '.last',
				cssNumber: '.page',
				cssPageDisplay: '.pagedisplay',
				cssPageSize: '.pagesize',
				seperator: "/",
				positionFixed: true,
				appender: this.appender
			};

			this.construct = function(settings) {


				return this.each(function() {

					config = $.extend(this.config, $.tablesorterPager.defaults, settings);

					var table = this, pager = config.container;
					//generatePagerHtml(this);

					$(this).trigger("appendCache");

					config.size = parseInt($(".pagesize",pager).val());

					$(pager).on('click', config.cssFirst, function() {
						moveToFirstPage(table);
						return false;
					});
					$(pager).on('click', config.cssNext, function() {
						moveToNextPage(table);
						return false;
					});
					$(pager).on('click', config.cssPrev, function() {
						moveToPrevPage(table);
						return false;
					});
					$(pager).on('click', config.cssLast, function() {
						moveToLastPage(table);
						return false;
					});
					$(pager).on('change', config.cssPageSize, function() {
						setPageSize(table,parseInt($(this).val()));
						return false;
					});
					$(pager).on('click', config.cssNumber, function() {
						moveToPageNumber(table, $(this));
						return false;
					});
				});
			};

		}
	});
	// extend plugin scope
	$.fn.extend({
        tablesorterPager: $.tablesorterPager.construct
	});

})(jQuery);