/*
 * Form Angular JS Controller
 */
(function() {
	baumerApp.controller('FormController',
		['$http', '$scope', '$attrs', '$compile', 'ContactFormProductList', 'UserProductList',
		function($http, $scope, $attrs, $compile, ContactFormProductList, UserProductList) {

		$scope.master = {};
		$scope.form = {};
		$scope.lineItems = ContactFormProductList.getLineItems();
		$scope.validateAll = false;
		$scope.selectedAddress = null;
		var currentProductList = {};

		if(typeof $attrs.formname !== 'undefined') $scope.formName = $attrs.formname;
		if(typeof $attrs.formuid !== 'undefined') $scope.formUid = $attrs.formuid;

		if (typeof PRODUCT_MAIN_GROUPS == 'object') $scope.productMainGroups = PRODUCT_MAIN_GROUPS;
		if (typeof formSelectedInquiry == 'object') $scope.formSelectedInquiry = formSelectedInquiry;
		if (CONTACT_ADDRESS && typeof CONTACT_ADDRESS === 'object') {
			$scope.contactAddressData = CONTACT_ADDRESS;
		}

		if(typeof $scope.formSelectedInquiry !== 'undefined') {
			$scope.contactInquiry = $scope.formSelectedInquiry.selectedInquiry;
		}

		$scope.addDefaultLineItem = function() {
			var mainGroup = {
				title: 'n/a',
				contactAddress: null
			};
			if(typeof currentMainGroup !== 'undefined') {
				mainGroup = currentMainGroup;
			}

			var defaultLineItem = {
				amount: 1,
				mainGroup: mainGroup,
				groupTitle: '',
				groupId: window.defaultCatalogIdentifier,
				configuration: '',
				imageUrl: '',
				note: '',
				price: 0.00,
				shipping: 0,
				amiradaConfiguration: ''
			};

			ContactFormProductList.addLineItem(defaultLineItem);
			return false;
		};

		$scope.removeLineItem = function(idx) {
			ContactFormProductList.removeLineItemByIdx(idx);
		};

		$scope.$on('updateLineItems', function() {
			$scope.lineItems = ContactFormProductList.getLineItems();
			$scope.contactInquiry = 1;
		});

		$scope.processForm = function() {
			$scope.inProgress = true;
			document.body.style.cursor = 'wait';
			if($scope[$scope.formName].$valid && $scope.selectedAddress && typeof $scope.selectedAddress === 'object') {
				angular.element('#' + $scope.formName).prepend('<div class="overlay"><span class="spinner"></span></div>');

				var formFields = [], requestUrl = [];

				delete $scope[$scope.formName].productInquiry;
				delete $scope[$scope.formName].receiver;

				requestUrl.push('id=' + angular.element('#' + $scope.formName + ' [name="id"]').val());
				requestUrl.push('randomID=' + angular.element('[name="' + $scope.formName + '[randomID]"]').val());
				requestUrl.push('field=');
				requestUrl.push('uploadedFileName=');
				requestUrl.push('eID=formhandler-ajaxsubmit');
				requestUrl.push('uid=' + $scope.formUid);
				requestUrl = requestUrl.join('&').replace("/%20/g", '+');

				if(UserProductList.currentProductList.description){
					formFields.push($scope.formName + '[message]=' + UserProductList.currentProductList.description);
				}

				angular.forEach(angular.element('[name="' + $scope.formName +'"]').children('fieldset').first().find('input'), function (input, key) {
					formFields.push(angular.element(input).attr('name') + '=' + angular.element(input).val());
				});

				angular.forEach($scope[$scope.formName], function (formValue, key) {
					if (typeof formValue.$name !== 'undefined' && typeof formValue.$viewValue !== 'undefined') {
						formFields.push($scope.formName + '[' + formValue.$name + ']=' + formValue.$viewValue);
					}
				});
				
				// load currentProductList
				if($scope.formName == 'formDataInquiry') {
					currentProductList = UserProductList.getLineItems();
				} else {
					currentProductList = $scope.lineItems;
				}

				angular.forEach(currentProductList, function (lineItem, key) {
					if(typeof lineItem.configuration !== 'undefined') {
						formFields.push($scope.formName + '[products][' + key + '][productCode]=' + lineItem.configuration);
					} else {
						formFields.push($scope.formName + '[products][' + key + '][productCode]=' + lineItem.label);
					}
					if(typeof lineItem.productMainGroup !== 'undefined') {
						formFields.push($scope.formName + '[products][' + key + '][mainGroup]=' + lineItem.productMainGroup.title);
					} else if(typeof lineItem.groupTitle !== 'undefined') {
						formFields.push($scope.formName + '[products][' + key + '][mainGroup]=' + lineItem.groupTitle);
					}
					formFields.push($scope.formName + '[products][' + key + '][amount]='      + lineItem.amount);

					if(typeof lineItem.note !== 'undefined') {
						formFields.push($scope.formName + '[products][' + key + '][note]=' + lineItem.note);
					} else {
						formFields.push($scope.formName + '[products][' + key + '][note]=');
					}
					if(typeof lineItem.price !== 'undefined') {
						formFields.push($scope.formName + '[products][' + key + '][price]=' + lineItem.price);
					} else {
						formFields.push($scope.formName + '[products][' + key + '][price]=0.00');
					}
					if(typeof lineItem.shipping !== 'undefined') {
						formFields.push($scope.formName + '[products][' + key + '][shipping]=' + lineItem.shipping);
					} else {
						formFields.push($scope.formName + '[products][' + key + '][shipping]=0');
					}
				});

				formFields.push($scope.formName + '[toMail]=' + $scope.selectedAddress.email);
				formFields.push($scope.formName + '[toName]=' + $scope.selectedAddress.company);

				formFields = formFields.join('&').replace("/%20/g", '+');

				$http({
					method: 'POST',
					url: '/index.php?' + requestUrl,
					data: formFields,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function (data) {
					$scope.inProgress = false;
					if (data.redirect) {
						window.location = data.redirect;
					} else if (data.form) {
						angular.element('#' + $scope.formName).replaceWith($compile(data.form)($scope));
					} else {
						angular.element('.overlay').remove();
					}
					ContactFormProductList.clear();
					document.body.style.cursor = 'default';
				});
			} else {
				$scope.validateAll = true;
				$scope.inProgress = false;
				document.body.style.cursor = 'default';
			}
		};

	}]);
})();
