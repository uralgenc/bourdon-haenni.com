/*
 * Javascript
 */
// IE8 Polyfill for bootstrap-select
Object.keys = Object.keys || function(
	o, // object
	k, // key
	r  // result array
) {
	// initialize object and result
	r = [];
	// iterate over object keys
	for (k in o)
		// fill result array with non-prototypical keys
		r.hasOwnProperty.call(o, k) && r.push(k);
	// return result
	return r
};

/*
 *	Sidebar Pullout
 */
var panelIdName = '';

function sidebar(id){
	// switch id set vars
	switch(id) {
		case "toggleContactForm":
			panelIdName = 'panelContactForm';
			break;
		default:
			id = 'togglePfinder';
			panelIdName = 'panelPfinder';
	}
	// is sidebar out?
	// no -> pull out, set classes
	$('#pull-out .sidebar-content > div').hide();
	$('#' + panelIdName).show();
	if (!$( "#pull-out" ).hasClass( "out" )) {
		$("#"+id).addClass("active");
		if($(window).width() > 720) {
			$("#pull-out").attr('style', '');
			$("#pull-out").animate({
				right: '0px',
			}, 500, function() {
				$(this).addClass("out");
			});
		} else {
			$("#pull-out").attr('style', '');
			$("#pull-out").animate({
				left: '0px',
				paddingLeft: '53px'
			}, 500, function() {
				$(this).addClass("out");
			});
		}

	} else {
		// yes
		// is clicked button activ? -> slide out
		if ($("#" + id).hasClass("active")) {
			// yes hide sidebar
			if($(window).width() > 720) {
				$("#pull-out").animate({
					right: '-720px',
				}, 500, function() {
					$(this).removeClass("out");
					$("#"+id).removeClass("active");
				});
			} else {
				$("#pull-out").animate({
					left: '100%',
					paddingLeft: '0px'
				}, 500, function() {
					$(this).removeClass("out");
					$("#"+id).removeClass("active");
				});
			}
		} else {
			// no switch to tab
			$("#pull-out button").removeClass("active");
			$("#" + id).addClass("active");
		}
	}
}

(function($){
	/********************
	/* Resize input   	*
	/* Field in Header  *
	/* http://stackoverflow.com/questions/1288297/jquery-auto-size-text-input-not-textarea#answer-1288475
	********************/
	$.fn.autoGrowInput = function(o) {
    o = $.extend({
        maxWidth: 824,
        minWidth: 200,
        comfortZone: 10
    }, o);
    this.filter('input:text').each(function(){
        var minWidth = o.minWidth || $(this).width(),
            val = '',
            input = $(this),
            testSubject = $('<tester/>').css({
                position: 'absolute',
                top: -9999,
                left: -9999,
                width: 'auto',
                fontSize: input.css('fontSize'),
                fontFamily: input.css('fontFamily'),
                fontWeight: input.css('fontWeight'),
                letterSpacing: input.css('letterSpacing'),
                whiteSpace: 'nowrap'
            }),
            check = function() {
                if (val === (val = input.val())) {return;}
                // Enter new content into testSubject
                var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                testSubject.html(escaped);
                // Calculate new width + whether to change
                var testerWidth = testSubject.width(),
                    newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
                    currentWidth = input.width(),
                    isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
                                         || (newWidth > minWidth && newWidth < o.maxWidth);
                // Animate width
                if (isValidWidthChange) {
                    input.width(newWidth);
                }
            };
        testSubject.insertAfter(input);

        $(this).bind('keyup keydown blur update', check);
    });
	  return this;
	};

	$.fn.selectpicker.Constructor.prototype.createDropdown = function () {
		// Options
		// If we are multiple, then add the show-tick class by default
		var multiple = this.multiple ? ' show-tick' : '',
			inputGroup = this.$element.parent().hasClass('input-group') ? ' input-group-btn' : '',
			autofocus = this.autofocus ? ' autofocus' : '',
			btnSize = this.$element.parents().hasClass('form-group-lg') ? ' btn-lg' : (this.$element.parents().hasClass('form-group-sm') ? ' btn-sm' : '');
		// Elements
		var header = this.options.header ? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + '</div>' : '';
		var searchbox = this.options.liveSearch ? '<div class="bs-searchbox"><input type="text" class="input-block-level form-control" autocomplete="off" /><i class="fa fa-search"></i></div>' : '';
		var actionsbox = this.options.actionsBox ? '<div class="bs-actionsbox">' +
		'<div class="btn-group btn-block">' +
		'<button class="actions-btn bs-select-all btn btn-sm btn-default">' +
		this.options.selectAllText +
		'</button>' +
		'<button class="actions-btn bs-deselect-all btn btn-sm btn-default">' +
		this.options.deselectAllText +
		'</button>' +
		'</div>' +
		'</div>' : '';
		var drop =
			'<div class="btn-group bootstrap-select' + multiple + inputGroup + '">' +
			'<button type="button" class="btn dropdown-toggle selectpicker' + btnSize + '" data-toggle="dropdown"' + autofocus + '>' +
			'<span class="filter-option pull-left"></span>&nbsp;' +
			'<i class="fa fa-angle-down"></i>' +
			'</button>' +
			'<div class="dropdown-menu open">' +
			header +
			searchbox +
			actionsbox +
			'<ul class="dropdown-menu inner selectpicker" role="menu">' +
			'</ul>' +
			'</div>' +
			'</div>';

		return $(drop);
	};


	$(document).ready(function () {
		$('#pull-out').on('click', '.sidebar-flap button', function() {
			sidebar($(this).attr('id'));
		});

		/*
		 * Removing the scrollbar to action hover.
		 * This problem is of IE.
		 */
		//$('#pull-out .sidebar-flap button').hover(function() {
		//	$('body').css('overflow', 'hidden');
		//}, function() {
		//	$('body').css('overflow', '');
		//});

		// kontact ausfahren zum entwickeln
		//sidebar("toggleContactForm");

		// resizing inputfield in header
		//$('#searchfield').autoGrowInput();

		/*
		 * Main Menu
		 * Menu should open on click, overlay for rest of content
		 * click on overlay should close menu and hide overlay
		 */
		var objMenu = $('#nav_main');
		var objSubmenu = $('li.hassub');
		if(objMenu.length) {
			objMenu.on('click', '> li.hassub > a', function(event) {
				event.preventDefault();
				var hasSubmenu = $(this).parents("li").hasClass('active');
				objSubmenu.removeClass('active');
				if(!hasSubmenu) {
					$(this).parents('li').addClass('active');
				}
			});
			$('.nav-wrapper').click(function(event) {
				event.stopPropagation();
			});
			$('body').click(function() {
				objSubmenu.removeClass('active');
			});
		}


		// activate tooltips
		$('.tab-content.konfigurator .tip').tooltip({
			"html": true,
			"placement": "top"
		});
		$(function () {
			$('[data-toggle="tooltip"]').tooltip({
				"html": true,
				"placement": "top"
			})
		});

		/*
		 * Tablesorter
		 */
		$('.tablesorter').tablesorter();
		$('.tablesorter-pager').tablesorter().tablesorterPager({container: $('.table-pager'), positionFixed: false});

		/*
		 * Removed event button
		 */
		$('.nav-tab a, .filterCatHeader a').click(function(event) {
			event.preventDefault();
		});

		/*
		 * add class odd and even to tables
		 */
		var table = $('.table-striped');
		if(table.length) {
			table.find('tr:odd').addClass('even');
			table.find('tr:even').addClass('odd');
		}

		/*
		 * Widget hover animate
		 */
		$('#pl-widget').hover(function() {
			$(this).addClass('active');
		}, function() {
			$(this).removeClass('active');
		});

		var subnavi = $('.subnavi');
		var subnaviDropdown = $('.subnavi-dropdown', subnavi);
		if(subnaviDropdown.length) {
			$(document).click(function() {
				$('.toggle-dropdown', subnavi).removeClass('active');
				subnaviDropdown.slideUp(200);
			});
			$('.toggle-dropdown', subnavi).click(function(event) {
				event.preventDefault();
				event.stopImmediatePropagation();

				$(this).toggleClass('active');
				subnaviDropdown.slideToggle(200);
			});

		}
	});
})(jQuery);
