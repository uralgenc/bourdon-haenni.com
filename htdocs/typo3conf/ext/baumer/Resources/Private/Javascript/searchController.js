(function () {
	/**
	 * Returns an object (hashMap) containing the URLs query strings
	 * @type {{}}
	 */
	var qs = (function (a) {
		if (a == "") return {};
		var b = {};
		for (var i = 0; i < a.length; ++i) {
			var p = a[i].split('=', 2);
			if (p.length == 1)
				b[p[0]] = "";
			else
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	})(window.location.search.substr(1).split('&'));

	/**
	 * @ngdoc controller
	 * @name Baumer:SearchCtrl
	 * @requires esFactory
	 * @requires $timeout
	 * @requires $log
	 * @requires $window
	 * @requires $q
	 */
	baumerApp.controller('SearchCtrl',
		['$scope', 'esFactory', '$timeout', '$log', '$q', 'UserProductList',
			function ($scope, esFactory, $timeout, $log, $q, UserProductList) {

				$scope.searchIsAvailable = true;
				$scope.showSuggestion = false;
				$scope.showNoResults = false;
				$scope.suggestion = '';
				var ejs = null;
				var elasticsearchIndex;
				$scope.searchTypes = [];
				$scope.q = qs['q'] || '';

				// This global variables are assigned inside the template
				if (SEARCH_CONFIG) {
					//noinspection JSUnresolvedVariable
					ejs = esFactory(_.clone(SEARCH_CONFIG.ES_HOST));
					//noinspection JSUnresolvedVariable
					elasticsearchIndex = SEARCH_CONFIG.ES_INDEX;
					//noinspection JSUnresolvedVariable
					$scope.searchTypes = SEARCH_CONFIG.ES_TYPES;
					//noinspection JSUnresolvedVariable
					$scope.imageAttributeId = CS_ATTRIBUTES.image;
					//noinspection JSUnresolvedVariable
					$scope.datasheetAttributeId = CS_ATTRIBUTES.datasheet;
					//noinspection JSUnresolvedVariable
					$scope.galleryAttributeId = CS_ATTRIBUTES.gallery;
					//noinspection JSUnresolvedVariable
					$scope.fileAttributes = FILE_ATTRIBUTES;

					ejs.ping({
						requestTimeout: 5000
					}, function (error) {
						$scope.searchIsAvailable = !error;
					});
				} else {
					$scope.searchIsAvailable = false;
				}

				/**
				 * Reset all paginators
				 */
				function resetPaginators() {
					angular.forEach($scope.searchTypes, function (type) {
						type.currentPage = 1;
					});
				}

				/**
				 * We watch the q parameter to reset pagination if the query is changed
				 */
				$scope.$watch('q', function () {
					resetPaginators();
				});

				/**
				 * Construct the elasticsearch query
				 * @returns {{}}
				 */
				function buildQuery() {
					var query = {};
					if ($scope.q.length > 0) {
						query = {
							multi_match: {
								query: $scope.q,
								fields: ['title^5', 'content'],
								operator: 'and'
							}
						};
					} else {
						query = {
							match_all: {}
						}
					}
					return query;
				}

				/**
				 * Execute a search query for a single type
				 * @param type
				 * @returns {*}
				 */
				function searchType(type) {
					return ejs.search({
						index: elasticsearchIndex,
						type: $scope.searchTypes[type].type,
						body: {
							query: buildQuery(),
							suggest: {
								text: $scope.q,
								title: {
									text: $scope.q,
									term: {
										field: "title"
									}
								}
							},
							size: $scope.searchTypes[type].itemsPerPage,
							from: ($scope.searchTypes[type].currentPage - 1) * $scope.searchTypes[type].itemsPerPage
						}
					}).then(function (resp) {
						//noinspection JSUnresolvedVariable
						$scope.searchTypes[type].results = resp.hits.hits;
						$scope.searchTypes[type].suggestions = resp.suggest;
						//noinspection JSUnresolvedVariable
						$scope.searchTypes[type].totalItems = resp.hits.total;
					}, function (error) {
						$log.error(error);
						$scope.searchIsAvailable = false;
					});
				}

				/**
				 * Reset all filters
				 */
				$scope.resetFilters = function () {
					angular.forEach($scope.searchTypes, function (type) {
						type.hidden = false;
					});
					resetPaginators();
				};

				/**
				 * Given a elasticsearch FILES hit and a fieldname, return the value
				 * @param fieldName
				 * @param hit
				 * @returns {*}
				 */
				$scope.getFieldValue = function (fieldName, hit) {
					if (!$scope.fileAttributes[fieldName]) {
						return '';
					}
					//noinspection JSUnresolvedVariable
					if(typeof hit._source.data === 'undefined') {
						return '';
					}

					return hit._source.data.FileAttributes[$scope.fileAttributes[fieldName]].FormattedValue;
				};

				function checkSuggestion() {
					var showSuggestions = true;
					var bestSuggestion = {score: 0.0, text: ''};
					angular.forEach($scope.searchTypes, function (type) {
						if (type.results.length > 0) {
							showSuggestions = false;
						}
						if (type.suggestions &&
							type.suggestions.title &&
							type.suggestions.title.length > 0 &&
							type.suggestions.title[0].options.length > 0 &&
							type.suggestions.title[0].options[0].score > bestSuggestion.score) {
							bestSuggestion = type.suggestions.title[0].options[0];
						}
					});
					if (showSuggestions && bestSuggestion.score > 0.0) {
						$scope.showSuggestion = showSuggestions;
						$scope.showNoResults = false;
						$scope.suggestion = bestSuggestion.text;
					} else if(showSuggestions){
						$scope.showSuggestion = false;
						$scope.showNoResults = true;
					} else {
						$scope.showSuggestion = false;
						$scope.showNoResults = false;
					}
				}

				$scope.doSearch = function (q) {
					$scope.q = q;
					$scope.search();
				};

				/**
				 * Execute the search for all types
				 */
				$scope.search = function () {
					document.body.style.cursor = 'wait';
					var searchPromises = [];
					angular.forEach($scope.searchTypes, function (typeConfig, type) {
						searchPromises.push(searchType(type));
					});
					$q.all(searchPromises).then(function () {
						checkSuggestion();
						document.body.style.cursor = 'default';
					});
				};

				$scope.search();

				/**
				 * Add line item to currentProductList
				 * with timeout for user feedback
				 *
				 * @param {object} product
				 */
				$scope.addToPlWidget = function(product) {

					var imageUrl = '/typo3conf/ext/baumer/Resources/Public/Images/Dummy83x56.png';
					try {
						//noinspection JSUnresolvedVariable
						imageUrl = product._source.data.GroupedAttributes[$scope.imageAttributeId][0].Links[0].FormattedValue;
					} catch (e) {}

					//noinspection JSUnresolvedVariable
					var lineItem = {
						amount: 1,
						label: product._source.label,
						groupTitle: product._source.data.GroupedAttributes[$scope.mainGroupAttributeId][0].FormattedValue[0],
						imageUrl: imageUrl,
						csViewRecord: product._source.uid
					};
					// add with spinner and 1 sec. timeout
					product.adding = true;
					$timeout(function () {
						UserProductList.addLineItem(lineItem);
						product.adding = false;
					}, 500);
				};
			}
		]
	);
})();
