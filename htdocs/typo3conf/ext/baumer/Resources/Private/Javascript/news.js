/**
 * JS for news
 *
 * @author: Martin R. Krause <martin.r.krause@gmx.de>
 */
$(document).ready(function() {

	/**
	 * Binding to category selectors
	 *
	 * Add the selected category and reloads the news site
	 */
	$('.category-selector').click(function() {
		if (window.selectedCategories.constructor === Array) {
			window.selectedCategories.push($(this).attr('data-cat-uid'));
		}
		$(this).parent().addClass('checked');
		reloadNewsPage();
	});

	/**
	 * Binding to category de-selectors
	 *
	 * Removes category uid and reloads the news site
	 */
	$('.category-deselector').click(function() {
		if (window.selectedCategories.constructor === Array) {
			window.selectedCategories = _.without(
				window.selectedCategories,
				$(this).attr('data-cat-uid')
			);
			$(this).parent().removeClass('checked');
			reloadNewsPage();
		}
	});

	/**
	 * Open/Close all category selector foldouts
	 */
	$('.close-filter').click(function() {
		$('.filterCatFlyout').toggle();
		$(this).find('.inner-close-filter').toggle();
	});
});

/**
 * Replace selected categories in URL and refreshes the site
 */
function reloadNewsPage() {
	window.location = window.newsCategorySelectionUrl.replace(
		'XXXXX',
		_.without(window.selectedCategories, '').join(',')
	);
}
