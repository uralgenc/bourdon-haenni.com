plugin.Tx_Formhandler.settings.predef.newsletterEdit {
	templateFile = EXT:{$forms.paths.base}Newsletter/template-edit.html
	name = Newsletter Edit Profile
	formValuesPrefix = baumer-newsletter-edit
	formID = baumer-newsletter-edit
	langFile.3 = EXT:{$forms.paths.base}Newsletter/locallang.xml
	preProcessors {
		1.class = Typoheads\Formhandler\PreProcessor\LoadGetPost
		2.class = Baumer\Baumer\Forms\PreProcessor\LoadProfile
		2.config {
			1 {
				uid.mapping = uid
				salutation.mapping = gender
				email.mapping = email
				firstname.mapping = first_name
				lastname.mapping = last_name
			}
			select {
				table = fe_users
				where.data = GP:token
				andWhere = newsletter=1
				where.wrap = token='|'
			}
			# Page Uid to redirect
			redirectOnError = {$pageIDs.pages.newsletterAuthenticationError}
			evalancheConfig < plugin.Tx_Formhandler.settings.evalancheConfig
		}
	}
	validators {
		1.class = Validator_Default
		1.config.fieldConf {
			salutation.errorCheck.1 = required
			firstname.errorCheck.1 = required
			lastname.errorCheck.1 = required
			topic.errorCheck.1 = required
		}
	}

	saveInterceptors {
		1.class = Interceptor_CombineFields
		1.config {
			combineFields {
				# Combined field for e-mail finisher
				fullname {
					fields {
						1 = firstname
						2 = lastname
					}
				}
			}
		}
	}
	finishers {
		1 {
			class = Typoheads\Formhandler\Finisher\DB
			config {
				table = fe_users
				key = uid
				key_value.field = uid
				updateInsteadOfInsert = 1
				fields {
					first_name.mapping = firstname
					last_name.mapping = lastname
					email.mapping = email
					tstamp.mapping = TEXT
					tstamp.mapping.data = date : U
					pid = {$pageIDs.storage.felogin}
					newsletter_user {
						preProcessing = USER
						preProcessing.userFunc = Baumer\Baumer\Forms\Helper\FormhandlerUserFunc->checkNewsletterUser
						mapping = email
					}
				}
			}
		}

		# Evalanche API call
		2 < plugin.Tx_Formhandler.settings.finisher.3
		2.config.sourceOptionId = 254555
		2.config.defaultPermission = 1

		3 {
			class = Typoheads\Formhandler\Finisher\SubmittedOK
			config.returns = 1
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.predef.newsletterEdit.debug = 1
[global]

[globalVar = TSFE:id = 474]
plugin.Tx_Formhandler.settings.masterTemplateFile.1 = EXT:baumer/Resources/Private/Forms/masterTemplate2.html
[global]