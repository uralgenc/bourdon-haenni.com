plugin.Tx_Formhandler.settings.predef.newsletterSubscribe {
	templateFile = EXT:{$forms.paths.base}Newsletter/template-subscribe.html
	name = Newsletter Subscribe
	formValuesPrefix = baumer-newsletter-subscribe
	formID = baumer-newsletter-subscribe
	langFile.3 = EXT:{$forms.paths.base}Newsletter/locallang.xml
	preProcessors {
		1.class = Typoheads\Formhandler\PreProcessor\LoadGetPost
	}
	validators {
		1.class = Typoheads\Formhandler\Validator\DefaultValidator
		1.config.fieldConf {
			salutation.errorCheck.1 = required
			firstname.errorCheck.1 = required
			lastname.errorCheck.1 = required
			email.errorCheck {
				1 = required
				2 = email
			}
			topic.errorCheck.1 = required
		}
		2.class = Baumer\Baumer\Forms\Validator\NewsletterValidator
	}

	saveInterceptors {
		1.class = Typoheads\Formhandler\Interceptor\CombineFields
		1.config {
			combineFields {
				# Combined field for e-mail finisher
				fullname {
					fields {
						1 = firstname
						2 = lastname
					}
				}
			}
		}
	}
	finishers {
		1 {
			class = Typoheads\Formhandler\Finisher\DB
			config {
				table = fe_users
				key = email
				key_value.mapping = email
				updateInsteadOfInsert = 1
				fields {
					usergroup = {$recordIDs.fe_groups.newsletterRecipients}
					username.mapping = email
					first_name.mapping = firstname
					last_name.mapping = lastname
					email.mapping = email
					crdate.mapping = TEXT
					crdate.mapping.data = date : U
					tstamp.mapping = TEXT
					tstamp.mapping.data = date : U
					pid = {$pageIDs.storage.felogin}
					disable = 1
					disable {
						preProcessing = USER
						preProcessing.userFunc = Baumer\Baumer\Forms\Helper\FormhandlerUserFunc->checkIfUserActive
						mapping = email
					}
					newsletter = 1
					newsletter_user {
						preProcessing = USER
						preProcessing.userFunc = Baumer\Baumer\Forms\Helper\FormhandlerUserFunc->checkNewsletterUser
						mapping = email
					}
				}
			}
		}

		# Evalanche API call
		2 < plugin.Tx_Formhandler.settings.finisher.3
		2.config.sourceOptionId = 254555
		2.config.defaultPermission = 1

		3 {
			class = Typoheads\Formhandler\Finisher\SubmittedOK
			config.returns = 1
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.predef.newsletterSubscribe.debug = 1
[global]

[globalVar = TSFE:id = 474]
plugin.Tx_Formhandler.settings.masterTemplateFile.1 = EXT:baumer/Resources/Private/Forms/masterTemplate2.html
[global]