plugin.Tx_Formhandler.settings.predef.productInquiry < plugin.Tx_Formhandler.settings.predef.contactForm
plugin.Tx_Formhandler.settings.predef.productInquiry {
	#
	# This form inherits pretty much everything from contactForm (Resources/Private/Forms/ContactForm/setup.ts)
	#
	name = Product Inquiry
	templateFile = EXT:{$forms.paths.base}ProductInquiry/template.html
	langFile.3 = EXT:{$forms.paths.base}ProductInquiry/locallang.xlf
	formValuesPrefix = formDataInquiry

	validators.1.config.fieldConf.request >
	validators.1.config.fieldConf.message >

	markers {
		productInquiryName = TEXT
		productInquiryName.value = Product Inquiry
		productInquiryName.override.data = GP:formDataInquiry|productList

		country_name_en.select.where.dataWrap = tx_baumer_evalanche_country_id = {GP:formDataInquiry|country}
	}

	finishers {
		1.config {
			admin {
				to_email.data = GP:formDataInquiry|toMail
				to_name.data = GP:formDataInquiry|toName
			}
			user {
				to_email.data = GP:formDataInquiry|email
				to_name.data = GP:formDataInquiry|fullname
			}
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.predef.productInquiry.debug = 1
[global]
