plugin.Tx_Formhandler.settings.predef.registrationEditPasswordForm {
	templateFile = EXT:{$forms.paths.base}RegistrationForm/template-edit-password.html
	name = Registration Edit Password Form
	formValuesPrefix = baumer-registration-password
	formID = baumer-registration-password
	langFile.3 = EXT:{$forms.paths.base}RegistrationForm/locallang.xml

	preProcessors {
		1.class = Typoheads\Formhandler\PreProcessor\LoadGetPost
		2.class = Typoheads\Formhandler\PreProcessor\LoadDB
		2.config {
			1 {
				uid.mapping = uid
			}
			select {
				table = fe_users
				where.data = TSFE:fe_user|user|uid
				where.wrap = uid=|
			}
		}
	}
	validators {
		1.class = Typoheads\Formhandler\Validator\DefaultValidator
		1.config.fieldConf {
			password.errorCheck.1 = required
			password_confirmation.errorCheck {
				1 = required
				2 = equalsField
				2.field = password
			}
		}
	}

	finishers {
		1 {
			class = Typoheads\Formhandler\Finisher\DB
			config {
				key = uid
				key_value.mapping = uid
				updateInsteadOfInsert = 1
				table = fe_users
				fields {
					password.special = saltedpassword
					password.special {
						field = password
					}
				}
			}
		}
		3 {
			class = Typoheads\Formhandler\Finisher\SubmittedOK
			config.returns = 1
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.predef.registrationEditPasswordForm.debug = 1
[global]
