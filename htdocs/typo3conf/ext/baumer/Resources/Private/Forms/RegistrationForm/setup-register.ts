plugin.Tx_Formhandler.settings.predef.registrationForm {
	templateFile = EXT:{$forms.paths.base}RegistrationForm/template-register.html
	name = Registration Form
	formValuesPrefix = formDataRegistration
	formID = baumer-registration
	langFile.3 = EXT:{$forms.paths.base}RegistrationForm/locallang.xfl

	preProcessors {
		1.class = Typoheads\Formhandler\PreProcessor\LoadGetPost
		2.class = Baumer\Baumer\Forms\PreProcessor\ValidateAuthCode
		2.config {
			redirectPage = {$pageIDs.pages.login.success}
			errorRedirectPage = {$pageIDs.pages.login.failure}
			hiddenField = disable
			evalancheConfig < plugin.Tx_Formhandler.settings.evalancheConfig
		}
	}

	validators {
		1.class = Typoheads\Formhandler\Validator\DefaultValidator
		1.config.fieldConf {
			salutation.errorCheck.1 = required
			firstname.errorCheck.1 = required
			lastname.errorCheck.1 = required
			email.errorCheck {
				1 = required
				2 = email
			}
			password.errorCheck.1 = required
			password_confirmation.errorCheck {
				1 = required
				2 = equalsField
				2.field = password
			}
			company.errorCheck.1 = required
			agb.errorCheck.1 = required
		}
		2.class = Baumer\Baumer\Forms\Validator\RegistrationValidator
	}

	saveInterceptors {
		1.class = Typoheads\Formhandler\Interceptor\CombineFields
		1.config {
			combineFields {
				# Combined field for e-mail finisher
				fullname {
					fields {
						1 = firstname
						2 = lastname
					}
				}
			}
		}
		2.class = Typoheads\Formhandler\Interceptor\StdWrap
		2.config.fieldConf {
			# The following virtual fields are used by the e-mail finisher

			mail_to_email_admin = TEXT
			mail_to_email_admin.data = DB:tt_address:{$recordIDs.tt_address.defaultRecipients.registration}:email
			mail_sender_email_admin = TEXT
			mail_sender_email_admin.data = DB:tt_address:{$recordIDs.tt_address.defaultRecipients.registration}:email
			mail_sender_name_admin = TEXT
			mail_sender_name_admin.data = DB:tt_address:{$recordIDs.tt_address.defaultRecipients.registration}:name

			mail_sender_email_user = TEXT
			mail_sender_email_user.data = DB:tt_address:{$recordIDs.tt_address.defaultRecipients.registration}:email
			mail_sender_name_user = TEXT
			mail_sender_name_user.data = DB:tt_address:{$recordIDs.tt_address.defaultRecipients.registration}:name
		}
	}

	finishers {
		1 {
			class = Typoheads\Formhandler\Finisher\DB
			config {
				key = email
				key_value.mapping = email
				updateInsteadOfInsert = 1
				table = fe_users
				fields {
					usergroup = {$recordIDs.fe_groups.members}
					username.mapping = email
					gender.mapping = salutation
					first_name.mapping = firstname
					last_name.mapping = lastname
					email.mapping = email
					password.special = saltedpassword
					password.special {
						field = password
					}
					zip.mapping = postal_code
					city.mapping = city
					telephone.mapping = telephone
					address.mapping = address
					company.mapping = company
					country.mapping = country
					crdate.mapping = TEXT
					crdate.mapping.data = date : U
					tstamp.mapping = TEXT
					tstamp.mapping.data = date : U
					pid = {$pageIDs.storage.felogin}
					disable = 1
					newsletter.mapping = newsletter
					newsletter_user = 0
					terms_acknowledged.mapping = agb
				}
			}
		}
		2 {
			class = Typoheads\Formhandler\Finisher\GenerateAuthCode
			config {
				table = fe_users
			}
		}
		3 {
			class = Typoheads\Formhandler\Finisher\Mail
			config {
				mailer {
					class = Typoheads\Formhandler\Mailer\TYPO3Mailer
				}
				admin {
					to_email = mail_to_email_admin
					subject = {$forms.emailconfig.admin.subject}
					sender_email = {$forms.emailconfig.admin.sender_email}
					sender_name = {$forms.emailconfig.admin.sender_name}
				}
				user {
					to_email = email
					to_name = fullname

					subject = {$forms.emailconfig.user.subject}
					sender_email = {$forms.emailconfig.user.sender_email}
					sender_name = {$forms.emailconfig.user.sender_name}
				}
			}
		}
		# Evalanche API call
		4 < plugin.Tx_Formhandler.settings.finisher.2
		5 {
			class = Typoheads\Formhandler\Finisher\SubmittedOK
			config.returns = 1
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.predef.registrationForm.debug = 1
[global]
