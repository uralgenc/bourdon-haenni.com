plugin.Tx_Formhandler.settings.predef.registrationEditForm {
	templateFile = EXT:{$forms.paths.base}RegistrationForm/template-edit.html
	name = Registration Edit Form
	formValuesPrefix = baumer-registration-edit
	formID = baumer-registration-edit
	langFile.3 = EXT:{$forms.paths.base}RegistrationForm/locallang.xml

	preProcessors {
		1.class = Typoheads\Formhandler\PreProcessor\LoadGetPost
		2.class = Baumer\Baumer\Forms\PreProcessor\LoadProfile
		2.config {
			1 {
				uid.mapping = uid
				salutation.mapping = gender
				firstname.mapping = first_name
				lastname.mapping = last_name
				postal_code.mapping = zip
				city.mapping = city
				telephone.mapping = telephone
				address.mapping = address
				company.mapping = company
				country.mapping = country
				email.mapping = email
			}
			select {
				table = fe_users
				where.data = TSFE:fe_user|user|uid
				where.wrap = uid=|
			}
			evalancheConfig < plugin.Tx_Formhandler.settings.evalancheConfig
		}
	}
	markers {
		test = TEXT
		test.data = TSFE:fe_user|user|uid
		test.wrap = <div>hier <b>|</b></div>
	}
	validators {
		1.class = Typoheads\Formhandler\Validator\DefaultValidator
		1.config.fieldConf {
			salutation.errorCheck.1 = required
			firstname.errorCheck.1 = required
			lastname.errorCheck.1 = required
			company.errorCheck.1 = required
		}
	}

	finishers {
		1 {
			class = Typoheads\Formhandler\Finisher\DB
			config {
				key = uid
				key_value.mapping = uid
                updateInsteadOfInsert = 1
				table = fe_users
				fields {
					username.mapping = email
					first_name.mapping = firstname
					last_name.mapping = lastname
					zip.mapping = postal_code
					city.mapping = city
					telephone.mapping = telephone
					address.mapping = address
					company.mapping = company
				}
			}
		}
		# Evalanche API call
		2 < plugin.Tx_Formhandler.settings.finisher.2
		3 {
			class = Typoheads\Formhandler\Finisher\SubmittedOK
			config.returns = 1
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.predef.registrationEditForm.debug = 1
[global]
