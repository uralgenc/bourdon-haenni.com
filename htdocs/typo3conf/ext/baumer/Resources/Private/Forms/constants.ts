# Default configuration for EXT:formhandler
forms {
	countries {
		labelField = cn_short_en
	}
	paths {
		base = baumer/Resources/Private/Forms/
	}
	emailconfig {
		admin {
			subject = Message from inquiry form
			sender_email = info@baumer.com
			sender_name = Baumer.com
		}
		user {
			subject = Your message to baumer.com
			sender_email = info@baumer.com
			sender_name = Baumer.com
		}
	}
}

# English
[globalVar = GP:L = 1]
	forms.countries.labelField = cn_short_de
	forms.emailconfig.user.subject = Ihre Nachricht zu baumer.com
[global]

# German
[globalVar = GP:L = 2]
	forms.countries.labelField = cn_short_en
	forms.emailconfig.user.subject = Your message to baumer.com
[global]

# French
[globalVar = GP:L = 3]
	forms.countries.labelField = cn_short_fr
[global]

# Chinese
[globalVar = GP:L = 4]
	forms.countries.labelField = cn_short_zh
[global]

# Italian
[globalVar = GP:L = 5]
	forms.countries.labelField = cn_short_it
[global]

# Portuguese
[globalVar = GP:L = 6]
	# We don’t have static_info_tables_pt yet
	forms.countries.labelField = cn_short_en
[global]
