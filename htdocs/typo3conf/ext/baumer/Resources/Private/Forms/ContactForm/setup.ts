
plugin.Tx_Formhandler.settings.predef.contactForm {
	#
	# !!!
	#
	# This form config is inheritated by inquiryForm.
	# Be careful when making changes and always check both, the contact form and the
	# product inquiry form
	#
	templateFile = EXT:{$forms.paths.base}ContactForm/template.html
	langFile.2 = EXT:{$forms.paths.base}ContactForm/locallang.xlf
	name = Contact Form
	formValuesPrefix = formDataContact

	preProcessors {
		1.class = Typoheads\Formhandler\PreProcessor\LoadDefaultValues
		1.config {
			1 {
				firstname.defaultValue = TEXT
				firstname.defaultValue.data = TSFE:fe_user|user|first_name

				lastname.defaultValue = TEXT
				lastname.defaultValue.data = TSFE:fe_user|user|last_name

				email.defaultValue = TEXT
				email.defaultValue.data = TSFE:fe_user|user|email

				phone.defaultValue = TEXT
				phone.defaultValue.data = TSFE:fe_user|user|telephone

				country.defaultValue = CONTENT
				country.defaultValue {
					table = static_countries
					select {
						pidInList = 0
						selectFields = tx_baumer_evalanche_country_id
						where.dataWrap = uid IN ('{TSFE:fe_user|user|country}')
					}
					renderObj = TEXT
					renderObj {
						field = tx_baumer_evalanche_country_id
					}
				}

				company.defaultValue = TEXT
				company.defaultValue.data = TSFE:fe_user|user|company

				street.defaultValue = TEXT
				street.defaultValue.data = TSFE:fe_user|user|address

				zip.defaultValue = TEXT
				zip.defaultValue.data = TSFE:fe_user|user|zip

				city.defaultValue = TEXT
				city.defaultValue.data = TSFE:fe_user|user|city
			}
		}
	}

	validators {
		1.class = Typoheads\Formhandler\Validator\DefaultValidator
		1.config.fieldConf {
			request.errorCheck.1 = required
			salutation.errorCheck.1 = required
			firstname.errorCheck.1 = required
			lastname.errorCheck.1 = required
			email.errorCheck {
				1 = required
				2 = email
			}
			message.errorCheck.1 = required
			country.errorCheck.1 = required
			company.errorCheck.1 = required
		}
	}

	markers {
		label_terms = COA
		label_terms {
			10 = TEXT
			10 {
				data = LLL:EXT:{$forms.paths.base}ContactForm/locallang.xlf:label_terms1
			}
			20 = TEXT
			20 {
				data = LLL:EXT:{$forms.paths.base}ContactForm/locallang.xlf:label_terms2
				typolink.parameter = {$pageIDs.pages.terms}
				typolink.target = _blank
			}
			30 = TEXT
			30 {
				data = LLL:EXT:{$forms.paths.base}ContactForm/locallang.xlf:label_terms3
			}
		}
		country_name_en = CONTENT
		country_name_en {
			table = static_countries
			select {
				selectFields = cn_short_en
				pidInList = 0
				where.dataWrap = tx_baumer_evalanche_country_id = {GP:formDataContact|country}
			}
			renderObj = TEXT
			renderObj {
				field = cn_short_en
			}
		}
		messageHtml = TEXT
		messageHtml {
			data = GP:formDataContact|message
			br = 1
			removeBadHTML = 1
		}
		requestName = TEXT
		requestName {
			dataWrap = LLL:EXT:{$forms.paths.base}ContactForm/locallang.xlf:label_request.{GP:formDataContact|request}
			wrap3 = {|}
			insertData = 1
		}


	}

	saveInterceptors {
		1.class = Typoheads\Formhandler\Interceptor\CombineFields
		1.config {
			combineFields {
				# Combined field for e-mail finisher
				fullname {
					fields {
						1 = firstname
						2 = lastname
					}
				}
			}
		}
	}

	finishers {
		1 {
			# E-mail configuration
			class = Typoheads\Formhandler\Finisher\Mail
			config {
				mailer {
					class = Mailer_TYPO3Mailer
				}
				admin {
					to_email = TEXT
					to_email.data = GP:formDataContact|toMail
					to_name = TEXT
					to_name.data = GP:formDataContact|toName

					subject = {$forms.emailconfig.admin.subject}
					sender_email = {$forms.emailconfig.admin.sender_email}
					sender_name = {$forms.emailconfig.admin.sender_name}
				}
				user {
					to_email = TEXT
					to_email.data = GP:formDataContact|email

					to_name = TEXT
					to_name.data = GP:formDataContact|fullname

					subject = {$forms.emailconfig.user.subject}
					sender_email = {$forms.emailconfig.user.sender_email}
					sender_name = {$forms.emailconfig.user.sender_name}
				}
			}
		}

		# Evalanche API call
		3 < plugin.Tx_Formhandler.settings.finisher.1
		3.config.sourceOptionId = 251866

		4 {
			class = Typoheads\Formhandler\Finisher\SubmittedOK
			config.returns = 1
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.predef.contactForm.debug = 1
[global]
