plugin.Tx_Formhandler.settings {
	singleErrorTemplate.totalWrap = <span class="error"><span class="fa fa-warning"></span> |</span>
	isErrorMarker.default =
	masterTemplateFile.1 = EXT:{$forms.paths.base}masterTemplate.html
	langFile.1 = EXT:{$forms.paths.base}locallang.xlf
	formValuesPrefix = formData
	formID = formData

	paths {
		base = typo3conf/ext/{$forms.paths.base}
	}

	ajax {
		class = AjaxHandler_JQuery
		config {
			ajaxSubmit = 1
			jsPosition = footer
		}
	}

	requiredMarker = required="required"

	evalancheConfig {
		# Authentication
		auth {
			username = onedrop
			password = 1Drop1
		}
		#Api Url
		xmlRpcUrl = https://newsletter.baumer.com/xmlrpc.php

		# Pool ID to write/update to
		poolId = 10895875

		# 0 Unconfirmed -> no newsletter
		# 1 Opt-in (confirmed) -> sends double opt in mail
		# 2 Double opt-in (confirmed) -> is a newsletter user with confirmed email
		# 3 Opt-out (confirmed) -> gets no newsletter anymore
		defaultPermission = 0
		contact_source {
			0 = 252300
			1 = 252301
			2 = 252302
		}
	}
	mailer.class = Typoheads\Formhandler\Mailer\TYPO3Mailer

	markers {
		# Load options for country dropdown
		country_options = CONTENT
		country_options {
			table = static_countries
			select {
				pidInList = 0
				orderBy = {$forms.countries.labelField}
				selectFields = {$forms.countries.labelField},tx_baumer_evalanche_country_id
				# where = cn_eu_member = 1
			}

			renderObj = COA
			renderObj {
				# value
				10 = TEXT
				10.wrap = <option value="|"
				10.field = tx_baumer_evalanche_country_id

				# selected
				12 = TEXT
				12.wrap = ###selected_country_|###>
				12.field = tx_baumer_evalanche_country_id

				# label
				13 = TEXT
				13.wrap = |</option>
				13.field = {$forms.countries.labelField}
			}
		}

		topic_options = CONTENT
		topic_options {
			table = sys_category
			select {
				where = parent={$recordIDs.sys_category.productMainGroup}
				andWhere = tx_baumer_evalanche_product_group_hidden = 0
				pidInList = {$pageIDs.storage.categories}
			}

			renderObj = COA
			renderObj {
				# value
				10 = TEXT
				10.wrap = <option value="|"
				10.field = tx_baumer_evalanche_product_group

				# selected
				12 = TEXT
				12.wrap = ###selected_topic_|###>
				12.field = tx_baumer_evalanche_product_group

				# label
				13 = TEXT
				13.wrap = |</option>
				13.field = title
			}
		}
		product_table_html = USER
		product_table_html.userFunc = Baumer\Baumer\Forms\Helper\FormhandlerUserFunc->getProductsTable
		product_table_html.html = 1

		product_table_text = USER
		product_table_text.userFunc = Baumer\Baumer\Forms\Helper\FormhandlerUserFunc->getProductsTable
		product_table_text.html = 0

		form_uid = USER
		form_uid.userFunc = Baumer\Baumer\Forms\Helper\FormhandlerUserFunc->getFormUid
	}
	finisher {
		1 {
			class = Baumer\Baumer\Forms\Finisher\ContactFormFinisher
			config < plugin.Tx_Formhandler.settings.evalancheConfig
			config {
				# This setting is needed to determine from which form a request was sent
				# Default is "not defined"
				sourceOptionId = 251855
			}
		}
		2 {
			class = Baumer\Baumer\Forms\Finisher\RegistrationFormFinisher
			config < plugin.Tx_Formhandler.settings.evalancheConfig
			config.sourceOptionId = 251969
		}
		3 {
			class = Baumer\Baumer\Forms\Finisher\NewsletterFinisher
			config < plugin.Tx_Formhandler.settings.evalancheConfig
			config {
				# This setting is needed to determine from which form a request was sent
				# Default is "not defined"
				sourceOptionId = 251855
				action = subscribe
			}
		}
	}
}

[applicationContext = Development/Debugging]
	plugin.Tx_Formhandler.settings.debug = 1
[global]

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/ContactForm/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/ProductInquiry/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/RegistrationForm/setup-register.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/RegistrationForm/setup-edit.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/RegistrationForm/setup-edit-password.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/Newsletter/setup-subscribe.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/Newsletter/setup-edit.ts">
