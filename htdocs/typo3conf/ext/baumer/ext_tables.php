<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_address',
    [
        'tx_baumer_pages' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang.xml:tt_address.tx_baumer_pages',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'maxitems' => 20,
                'wizards' => [
                    'suggest' => [
                        'type' => 'suggest',
                    ],
                ],
            ],
        ],
        'continent' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang.xlf:tt_address.continent',
            'config' => [
                'type' => 'input',
                'size' => '60',
                'autocomplete' => false,
            ],
        ],
        'latitude' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang.xlf:tt_address.latitude',
            'config' => [
                'type' => 'input',
                'size' => '20',
                'autocomplete' => false,
            ],
        ],
        'longitude' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang.xlf:tt_address.longitude',
            'config' => [
                'type' => 'input',
                'size' => '20',
                'autocomplete' => false,
            ],
        ],
    ],
    true
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_address', 'tx_baumer_pages', '', 'after:description');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_address', 'continent', '', 'after:country');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_address', 'latitude', '', 'after:tx_baumer_pages');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_address', 'longitude', '', 'after:latitude');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Baumer');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TypoScript/tsconfig.txt">');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'Flyout',
    'Flyout'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'ProductfinderList',
    'Productfinder List'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Contactbox',
    'Contact Box'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'Glossary',
    'Glossary'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'Configurator',
    'Encoway Configurator'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'CsView',
    'Contentserv View'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'DownloadCenter',
    'Download Center'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'ProductList',
    'FE-User Stored Product Lists'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'ProductListCheckout',
    'My Products List'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'MediaLibrary',
    'Media Library (Mediathek)'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Baumer.'.$_EXTKEY,
    'ProductGroupsJson',
    'Product Groups and Families JSON (Don\'t use in BE)'
);

$TCA['tt_content']['types']['list']['subtypes_excludelist'][$extensionName . '_glossary'] = 'layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$extensionName . '_glossary'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $extensionName . '_glossary',
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/Glossary.xml'
);

$TCA['tt_content']['types']['list']['subtypes_excludelist'][$extensionName . '_productfindermaingroups'] = 'layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$extensionName . '_productfindermaingroups'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $extensionName . '_productfindermaingroups',
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/ProductMainGroup.xml'
);

\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Baumer.Baumer', 'Page');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Baumer.Baumer', 'Content');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Baumer.Baumer', 'CoreContent');

$GLOBALS['TYPO3_CONF_VARS']['FluidTYPO3.FluidcontentCore']['variants']['table'][] = 'Baumer';

if (!isset($GLOBALS['TCA']['sys_category']['ctrl']['type'])) {
    if (file_exists($GLOBALS['TCA']['sys_category']['ctrl']['dynamicConfigFile'])) {
        require_once($GLOBALS['TCA']['sys_category']['ctrl']['dynamicConfigFile']);
    }
    // no type field defined, so we define it here. This will only happen the first time the extension is installed!!
    $GLOBALS['TCA']['sys_category']['ctrl']['type'] = 'tx_extbase_type';
    $tempColumns = [];
    $tempColumns[$GLOBALS['TCA']['sys_category']['ctrl']['type']] = [
        'exclude' => 1,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer.tx_extbase_type',
        'config' => [
            'type' => 'select',
            'items' => [],
            'size' => 1,
            'maxitems' => 1,
        ]
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $tempColumns, 1);
}

$tmp_baumer_columns = [
    'content_serv_id' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_category.content_serv_id',
        'config' => [
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $tmp_baumer_columns);
$GLOBALS['TCA']['sys_category']['types']['Tx_Baumer_Category']['showitem'] = $TCA['sys_category']['types']['1']['showitem'];
$GLOBALS['TCA']['sys_category']['types']['Tx_Baumer_Category']['showitem'] .= ',--div--;LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_category,';
$GLOBALS['TCA']['sys_category']['types']['Tx_Baumer_Category']['showitem'] .= 'content_serv_id';
$GLOBALS['TCA']['sys_category']['columns'][$TCA['sys_category']['ctrl']['type']]['config']['items'][] = ['LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:sys_category.tx_extbase_type.Tx_Baumer_Category', 'Tx_Baumer_Category'];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', $GLOBALS['TCA']['sys_category']['ctrl']['type'], '', 'after:' . $TCA['sys_category']['ctrl']['label']);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_encowayconfiguration');
$GLOBALS['TCA']['tx_baumer_domain_model_encowayconfiguration'] = [
    'ctrl' => [
        'title'    => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_encowayconfiguration',
        'label' => 'shortcode',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'delete' => 'deleted',
        'enablecolumns' => [],
        'searchFields' => 'shortcode,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/EncowayConfiguration.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/gear_in.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_lineitem');
$GLOBALS['TCA']['tx_baumer_domain_model_lineitem'] = [
    'ctrl' => [
        'title'    => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem',
        'label' => 'configuration',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',

        'delete' => 'deleted',
        'enablecolumns' => [],
        'searchFields' => 'title,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/LineItem.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/gear_in.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_productlist');
$GLOBALS['TCA']['tx_baumer_domain_model_productlist'] = [
    'ctrl' => [
        'title'    => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist',
        'label' => 'code',
        'label_alt_force' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',

        'delete' => 'deleted',
        'enablecolumns' => [],
        'searchFields' => 'name,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/ProductList.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/folder_wrench.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_csview');
$GLOBALS['TCA']['tx_baumer_domain_model_csview'] = [
    'ctrl' => [
        'title'    => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csview',
        'label' => '',
        'label_alt' => 'content_serv_id,label',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',

        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'label, content_serv_id,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/CsView.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/gear_in.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_csviewrecord');
$GLOBALS['TCA']['tx_baumer_domain_model_csviewrecord'] = [
    'ctrl' => [
        'title'    => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecord',
        'label' => '',
        'label_alt' => 'content_serv_id,label',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'dividers2tabs' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',

        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'label, content_serv_id,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/CsViewRecord.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/gear_in.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_csviewrecordrelation');
$GLOBALS['TCA']['tx_baumer_domain_model_csviewrecordrelation'] = [
    'ctrl' => [
        'title'    => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecordrelation',
        'label' => '',
        'label_alt' => 'link,cs_view_record',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => '',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/CsViewRecordRelation.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/gear_in.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_baumer_domain_model_youtubevideo', 'EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_csh_tx_baumer_domain_model_youtubevideo.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_youtubevideo');
$GLOBALS['TCA']['tx_baumer_domain_model_youtubevideo'] = [
    'ctrl' => [
        'title' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,description,video_id,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/YoutubeVideo.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_baumer_domain_model_youtubevideo.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_baumer_domain_model_priceshipping', 'EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_csh_tx_baumer_domain_model_priceshipping.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumer_domain_model_priceshipping');
$GLOBALS['TCA']['tx_baumer_domain_model_priceshipping'] = [
    'ctrl' => [
        'title' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_priceshipping',
        'label' => 'product_code',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'product_code,sap_id,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/PriceShipping.php',
        'iconfile' => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/tx_baumer_domain_model_priceshipping.png'
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable('tt_address', 'tt_address');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    $_EXTKEY,
    'tx_baumer_domain_model_encowayconfiguration'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    $_EXTKEY,
    'tx_baumer_domain_model_youtubevideo'
);


$tmpCol = [
    'file_language' => [
        'exclude' => 1,
        'label' => 'File content\'s language',
        'config' => [
            'type' => 'select',
            'foreign_table' => 'sys_language',
            'size' => 15,
            'minitems' => 1,
            'maxitems' => 100
        ]
    ]
];
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file', $tmpCol, 1);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_file', 'file_language', '', 'after:download_name');

/** @var array $TYPO3_CONF_VARS */
$TYPO3_CONF_VARS['FE']['pageOverlayFields'] .= ',navigation_column,navigation_layout,navigation_icon,navigation_text,tx_baumer_title_tag';
