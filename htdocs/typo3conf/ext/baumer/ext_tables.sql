#
# Table structure for table "fe_users"
#
CREATE TABLE fe_users (
  gender int(11) unsigned DEFAULT '99' NOT NULL,
  status int(11) unsigned DEFAULT '0' NOT NULL,
  newsletter_user int(11) unsigned DEFAULT '0' NOT NULL,
  newsletter int(11) unsigned DEFAULT '0' NOT NULL,
  terms_acknowledged int(11) unsigned DEFAULT '0' NOT NULL,
  token varchar(32) DEFAULT '' NOT NULL,
);

#
# Table structure for table "static_countries"
#
CREATE TABLE static_countries (
	tx_baumer_evalanche_country_id int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table "sys_category"
#
CREATE TABLE sys_category (
  tx_baumer_evalanche_product_group varchar(255) DEFAULT '' NOT NULL,
  tx_baumer_evalanche_product_group_hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
  content_serv_id varchar(255) DEFAULT '' NOT NULL,
  documenttype int(11) unsigned DEFAULT '0' NOT NULL,
  tx_extbase_type varchar(255) DEFAULT '' NOT NULL,
	amirada_maingroup_id varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'sys_file'
#
CREATE TABLE sys_file (
	mam_file_id int(11) unsigned DEFAULT '0' NOT NULL,
	mam_file_size int(11) unsigned DEFAULT '0' NOT NULL,
	file_language VARCHAR(255) DEFAULT '2' NOT NULL,
	tika_content longtext
);

#
# Table structure for table 'sys_file_metadata'
#
CREATE TABLE sys_file_metadata (
	documenttype int(11) unsigned DEFAULT '0' NOT NULL,
	amirada_download_id varchar(255) DEFAULT '' NOT NULL,
  data longtext
);

#
# Table structure for table 'sys_file_lang_ref'
#
CREATE TABLE sys_file_lang_ref (
	mam_file_id int(11) unsigned DEFAULT '0' NOT NULL,
	trans_mam_file_id int(11) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE tt_address (
	categories int(11) DEFAULT '0' NOT NULL,
	tx_baumer_pages VARCHAR(255) DEFAULT NULL,
	continent VARCHAR(255) DEFAULT NULL,
	latitude DECIMAL(13,10) DEFAULT '0.0000000000',
	longitude DECIMAL(13,10) DEFAULT '0.0000000000',
);

#
# Table structure for table 'tx_baumer_cs_import_queue'
#
CREATE TABLE tx_baumer_cs_worker_queue (

	uid int(11) NOT NULL auto_increment,
	action varchar(255) DEFAULT '' NOT NULL,
	parameters text,
	in_progress tinyint(4) unsigned DEFAULT '0' NOT NULL,
	failure tinyint(4) unsigned DEFAULT '0' NOT NULL,
	message text,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
);

#
# Table structure for table 'tx_baumer_domain_model_csview'
#
CREATE TABLE tx_baumer_domain_model_csview (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	content_serv_id varchar(255) DEFAULT '' NOT NULL,
	label varchar(255) DEFAULT '' NOT NULL,
	class int(11) unsigned NOT NULL default '0',
	data longtext,
	cs_view_records int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY content_serv_id (content_serv_id),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_baumer_domain_model_csviewrecord'
#
CREATE TABLE tx_baumer_domain_model_csviewrecord (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

  sap_id varchar(255) DEFAULT '' NOT NULL,
	content_serv_id varchar(255) DEFAULT '' NOT NULL,
	label varchar(255) DEFAULT '' NOT NULL,
	data longtext,
	cs_views int(11) DEFAULT '0' NOT NULL,
	site varchar(255) DEFAULT '' NOT NULL,

	sorting int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY content_serv_id (content_serv_id),
 KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_baumer_domain_model_cs_view_csviewrecord_mm'
#
CREATE TABLE tx_baumer_domain_model_cs_view_csviewrecord_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  sorting_foreign int(11) DEFAULT '0' NOT NULL,

  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_baumer_domain_model_csviewrecordrelation'
#
CREATE TABLE tx_baumer_domain_model_csviewrecordrelation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	cs_view_record int(11) DEFAULT '0' NOT NULL,
	tt_content int(11) DEFAULT '0' NOT NULL,

  sorting  int(11) DEFAULT '0' NOT NULL,
  link varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY sorting (sorting)
);

CREATE TABLE sys_file (
	mam_file_id int(11) unsigned DEFAULT '0' NOT NULL
);

#
# Table structure for table 'tx_baumer_domain_model_encowayconfiguration'
#
CREATE TABLE tx_baumer_domain_model_encowayconfiguration (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	shortcode varchar(255) DEFAULT '' NOT NULL,
	configuration_data blob,
  content_serv_id varchar(255) DEFAULT '' NOT NULL,
	completed tinyint(4) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	categories int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)
);

#
# Table structure for table 'tx_baumer_domain_model_productlist'
#
CREATE TABLE tx_baumer_domain_model_productlist (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,
  description text,
	fe_user int(11) unsigned DEFAULT '0',
	line_items int(11) unsigned DEFAULT '0' NOT NULL,
	code varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)
);

CREATE TABLE tx_baumer_domain_model_lineitem (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	product_list int(11) DEFAULT '0' NOT NULL,

	amount int(11) DEFAULT '1' NOT NULL,
    price DECIMAL(10,2) DEFAULT '0.00' NOT NULL,
    shipping int(11) DEFAULT '0' NOT NULL,
	encoway_configuration int(11) unsigned DEFAULT '0',
	amirada_configuration text,
	configuration varchar(255) DEFAULT '' NOT NULL,
	main_group int(11) DEFAULT '0',
	group_title varchar(255) DEFAULT '' NOT NULL,
	label varchar(255) DEFAULT '' NOT NULL,
	note text,
	image_url varchar(255) DEFAULT '' NOT NULL,
	cs_view_record int(11) DEFAULT '0',
	attributes text,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY product_list (product_list)
);

#
# Table structure for table 'tx_baumer_domain_model_youtubevideo'
#
CREATE TABLE tx_baumer_domain_model_youtubevideo (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	video_id varchar(255) DEFAULT '' NOT NULL,
	thumbnail_url varchar(255) DEFAULT '' NOT NULL,
	playlist_id varchar(255) DEFAULT '' NOT NULL,
	language varchar(255) DEFAULT '' NOT NULL,
	duration varchar(255) DEFAULT '' NOT NULL,
	published_at int(11) unsigned DEFAULT '0' NOT NULL,
	view_count int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'pages'
#
CREATE TABLE pages (
    navigation_column int(11) unsigned DEFAULT '0' NOT NULL,
    navigation_layout varchar(255) DEFAULT '' NOT NULL,
    navigation_icon varchar(255) DEFAULT '' NOT NULL,
    navigation_image varchar(255) DEFAULT '' NOT NULL,
    navigation_text text,
		tx_baumer_title_tag varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'pages_language_overlay'
#
CREATE TABLE pages_language_overlay (
    navigation_column int(11) unsigned DEFAULT '0' NOT NULL,
    navigation_layout varchar(255) DEFAULT '' NOT NULL,
    navigation_icon varchar(255) DEFAULT '' NOT NULL,
    navigation_image varchar(255) DEFAULT '' NOT NULL,
    navigation_text text,
		tx_baumer_title_tag varchar(255) DEFAULT '' NOT NULL
);


#
# Table structure for table 'tx_baumer_domain_model_priceshipping'
#
CREATE TABLE tx_baumer_domain_model_priceshipping (

  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,

  product_code varchar(255) DEFAULT '' NOT NULL,
  sap_id varchar(255) DEFAULT '' NOT NULL,
  description varchar(255) DEFAULT '' NOT NULL,
  price decimal(10,3) DEFAULT '0.000' NOT NULL,
  lead_time varchar(255) DEFAULT '' NOT NULL,

  tstamp int(11) unsigned DEFAULT '0' NOT NULL,
  crdate int(11) unsigned DEFAULT '0' NOT NULL,
  cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
  deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
  hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
  starttime int(11) unsigned DEFAULT '0' NOT NULL,
  endtime int(11) unsigned DEFAULT '0' NOT NULL,

  sys_language_uid int(11) DEFAULT '0' NOT NULL,
  l10n_parent int(11) DEFAULT '0' NOT NULL,
  l10n_diffsource mediumblob,

  PRIMARY KEY (uid),
  KEY parent (pid),
  KEY language (l10n_parent,sys_language_uid)
);
