<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Log PIM operations into sys_log
$GLOBALS['TYPO3_CONF_VARS']['LOG']['Baumer']['Baumer']['Command']['PimCommandController'] = [
    'writerConfiguration'    => [
        \TYPO3\CMS\Core\Log\LogLevel::INFO => [
            \TYPO3\CMS\Core\Log\Writer\DatabaseWriter::class => []
        ]
    ],
    'processorConfiguration' => [
        \TYPO3\CMS\Core\Log\LogLevel::INFO => [
            \TYPO3\CMS\Core\Log\Processor\MemoryPeakUsageProcessor::class => []
        ]
    ]
];

/** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher');
$signalSlotDispatcher->connect(
    'TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Backend',
    'beforeGettingObjectData',
    'Baumer\\Baumer\\Service\\ExtbaseForceLanguage',
    'forceLanguageForQueries'
);


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'Flyout',
    [
        'FlyoutHtml' => 'htmlFlyout',
    ],
    // non-cacheable actions
    [
        'FlyoutHtml' => 'htmlFlyout',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'CsEndpoint',
    [
        'CsEndpoint' => 'updateProduct,deleteProduct,updateView,deleteView',
    ],
    // non-cacheable actions
    [
        'CsEndpoint' => 'updateProduct,deleteProduct,updateView,deleteView',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'Contactbox',
    [
        'Address' => 'listByPage',
    ],
    []
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'Glossary',
    [
        'Glossary' => 'list,show',
    ],
    // non-cacheable actions
    [
        'Glossary' => 'list,show',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'CsView',
    [
        'CsView'  => 'configurator, selector',
        'Encoway' => 'load'
    ],
    // non-cacheable actions
    [
        'CsView'  => 'configurator',
        'Encoway' => 'load'
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'DownloadCenter',
    [
        'CsView' => 'downloadCenter',
    ],
    // non-cacheable actions
    [
        'CsView' => 'downloadCenter',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'Configurator',
    [
        'Encoway' => 'configurator, setParameter, unsetParameter, heartBeat, reset, saveConfiguration, getParameters, load, saveNamedConfiguration, typekey, variant, getPriceShippingByTypekey, confirmOperation',
    ],
    // non-cacheable actions
    [
        'Encoway' => 'configurator, setParameter, unsetParameter, heartBeat, reset, saveConfiguration, getParameters, load, saveNamedConfiguration, typekey, variant, getPriceShippingByTypekey, confirmOperation',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'ProductList',
    [
        'ProductList' => 'list, updateLineItem, deleteLineItem, findProductListByCode, saveForSharing, delete'
    ],
    [
        'ProductList' => 'list, updateLineItem, deleteLineItem, findProductListByCode, delete'
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'ProductListCheckout',
    [
        'ProductList' => 'current, saveForSharing'
    ],
    [
        'ProductList' => 'current, saveForSharing'
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'ProductGroupsJson',
    [
        'ProductList' => 'productGroupsJson'
    ],
    []
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Baumer.' . $_EXTKEY,
    'MediaLibrary',
    [
        'MediaLibrary' => 'video'
    ],
    // non-cacheable actions
    [
        'MediaLibrary' => 'video'
    ]
);

/**
 * register the migration Command Controller
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Baumer\Baumer\Command\MigrationCommandController';

if (TYPO3_MODE === 'BE') {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Baumer\Baumer\Command\MigrationCommandController';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Baumer\Baumer\Command\PimCommandController';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Baumer\Baumer\Command\CategoryImportCommandController';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Baumer\Baumer\Command\MamCommandController';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Baumer\Baumer\Command\ElasticCommandController';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'Baumer\Baumer\Command\GoogleCommandController';
}

// Ajax requests eID script
if (TYPO3_MODE === 'FE') {
    $GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['ajax'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('baumer') . 'Classes/Service/BaumerAjaxEid.php';
}

$extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);

if (TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl') && (!isset($extensionConfiguration['setRealurlConfigByDefault']) ||
        $extensionConfiguration['setRealurlConfigByDefault'] == 1)
) {
    require_once(TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/RealUrl/realurl_conf.php');
    $GLOBALS[ 'TYPO3_CONF_VARS' ][ 'EXTCONF' ][ 'realurl' ][ 'encodeSpURL_postProc' ][] = 'EXT:' . $_EXTKEY . '/Classes/Hooks/RealUrl.php:&Baumer\Baumer\Hooks\RealUrl->user_encodeSpURL_postProc';
    $GLOBALS[ 'TYPO3_CONF_VARS' ][ 'EXTCONF' ][ 'realurl' ][ 'decodeSpURL_preProc' ][] = 'EXT:' . $_EXTKEY . '/Classes/Hooks/RealUrl.php:&Baumer\Baumer\Hooks\RealUrl->user_decodeSpURL_preProc';

    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['getHost'][] = 'EXT:' . $_EXTKEY . '/Classes/Hooks/RealUrl.php:&Baumer\Baumer\Hooks\RealUrl->getHost';
}
