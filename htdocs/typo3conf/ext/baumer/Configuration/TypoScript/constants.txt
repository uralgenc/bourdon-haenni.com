# “Include static (from extensions)” should always contain this template only. All other extensions’
# static templates (except EXT:fluidcontent_core) are included right here.
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer_product_finder/Configuration/TypoScript/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:solr/Configuration/TypoScript/Solr/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Configuration/TypoScript/Elasticsearch/constants.txt">


<INCLUDE_TYPOSCRIPT: source="DIR:EXT:baumer/Configuration/TypoScript/Constants/" extensions="txt">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:baumer/Configuration/TypoScript/Constants/global/" extensions="txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:baumer/Resources/Private/Forms/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:umantis/Configuration/TypoScript/constants.ts">

plugin.tx_baumer.view {
  templateRootPath = EXT:baumer/Resources/Private/Templates/
  partialRootPath = EXT:baumer/Resources/Private/Partials/
  layoutRootPath = EXT:baumer/Resources/Private/Layouts/
}

plugin.tx_baumer.settings {
  # cat=BAUMER/settings; type=boolean; label=Use TypoScript settings instead of FlexForm values
  useTypoScript = 0
  # cat=BAUMER/settings; type=int+; label=main menu entry level - other menus calculated based on this
  entryLevel = 0
  # cat=BAUMER/settings; type=boolean; label=enable the top right corner search field
  enableSearch = 1
  # cat=BAUMER/settings; type=small; label=name attribute of search field
  searchFieldName = q
  # cat=BAUMER/settings; type=boolean; label=enables a "well" class on the side bar of templates using side bar
  addWell = 0
  # cat=BAUMER/settings; type=boolean; label=make the well's padding smaller than standard
  wellSmall = 0
  # cat=BAUMER/settings; type=options[left,right]; label=Placement of sidebar
  position = left
  # cat=BAUMER/settings; type=int+; label=Width of sidebar (what remains of a 12-column total is used by main content)
  distribution = 4
  # cat=BAUMER/settings; type=int+; label=Number of grid columns: Set this to the same number used in your custom Bootstrap CSS if you've switched from the default 12-grid
  gridColumns = 12
  # cat=BAUMER/settings; type=int+; label=Uid of the search page used for the searchbox on top of the page
  searchPageUid = 217
  # cat=BAUMER/settings; type=int+; label=Uid of the footer page used to modify footer and its navigation
  footerPageUid = 305
  # cat=BAUMER/settings; type=int+; label=Uid of the downloads page
  downloadsPageUid = 27
  # cat=BAUMER/settings; type=int+; label=Uid of the jobs page where the plugin is located
  jobPageUid = 82
  # cat=BAUMER/settings; type=int+; label=Uid of the product list single view
  productListPageUid = 283
  # cat=BAUMER/settings; type=int+; label=Uid of the product list storage folder
  productListStoragePid = 271
  # cat=BAUMER/settings; type=int+; label=Uid of the category whose children are document types
  documenttype_parentcategory = 5
  # cat=BAUMER/settings; type=int+; label=Uid of tt_address record responsible for products as fallback
  defaultContactAddress = 1685
  # cat=BAUMER/settings; type=int+; label=Uid of page with product inquiry form
  productInquiryFormPid = 284
  # cat=BAUMER/settings; type=int+; label=Days a product list is available by code
  productListCodeValidityPeriod = 90
  # cat=BAUMER/settings; type=int+; label=Uid of page with product finder
  productFinderPid = 272
  # cat=BAUMER/settings; type=int+; label=Uid of address storage
  addressStorage = 262

  # cat=BAUMER.Contentserv/settings; type=int+; label=CS View ID: ID of the Contentserv View that holds the attributes that should be imported as categories
  categoryView = 161
  # cat=BAUMER.Contentserv/settings; type=int+; label=CS Views Folder-ID: ID of the Contentserv View Folder containing all online views
  viewsFolderId = 144
  # cat=BAUMER.Contentserv/settings; type=int+; label=CS Attribute-ID: ID of the Attribute that holds the KMAT number
  kmatAttributeId = 361

  configurator {
    attributes {
      # cat=BAUMER.Contentserv/Configurator/005; type=int+; label=Configurator - Techdata: ID of the Contentserv Attribute that holds the Attributes that should be displayed as technical data
      techdata = 1929
      # cat=BAUMER.Contentserv/Configurator/010; type=int+; label=Configurator - Product Image: ID of the Contentserv Attribute that holds the Attribute that should be used as product image
      productImage = 1932
      # cat=BAUMER.Contentserv/Configurator/020; type=int+; label=Configurator - Downloads: ID of the Contentserv Attribute that holds the Attributes to be displayed as downloads
      downloads = 1930
      # cat=BAUMER.Contentserv/Configurator/030; type=int+; label=Configurator - Accessories: ID of the Contentserv Attribute that holds the Attributes to be displayed as accessories
      accessories = 1931
      # cat=BAUMER.Contentserv/Configurator/040; type=int+; label=Configurator - Datasheet: ID of the Contentserv Attribute that holds the Attribute that contains the File reference to the datasheet
      datasheet = 1935
      # cat=BAUMER.Contentserv/Configurator/050; type=int+; label=Configurator - Main Group: ID of the Contentserv Attribute that holds the Attribute that contains the main group
      maingroup = 1933
      # cat=BAUMER.Contentserv/Configurator/060; type=int+; label=Configurator - User Benefits: ID of the Contentserv Attribute that holds the Attribute that contains the benefits
      benefits = 1936
      # cat=BAUMER.Contentserv/Configurator/070; type=int+; label=Configurator - Product highlights: ID of the Contentserv Attribute that holds the Attribute that contains the product highlights
      highlights = 1937
      # cat=BAUMER.Contentserv/Configurator/080; type=int+; label=Configurator - Pictograms: ID of the Contentserv Attribute that holds the Attribute that contains the pictograms
      pictograms = 1978
    }
    encoway {
      # cat=BAUMER.Encoway/Configurator/010; type=string; label=Encoway - IP: The IP-address/hostname on which the webserver can reach encoway
      host = 10.10.25.9
      # cat=BAUMER.Encoway/Configurator/020; type=string; label=Encoway - BasePath: The basic path to the API endpoints (with leading and trailing slash!)
      basePath = /baumer/api/
      endpoints {
        # cat=BAUMER.Encoway/Configurator/100; type=string; label=Encoway - API - Configuration: The endpoint which serves the basic initialize configuration (only trailing slash!)
        initializeConfigurations = configurations/
        # cat=BAUMER.Encoway/Configurator/105; type=string; label=Encoway - API - Configuration: The endpoint which serves the basic get set containers and parameters configuration (only trailing slash!)
        setGetConfigurations = configurations/
        # cat=BAUMER.Encoway/Configurator/110; type=string; label=Encoway - API - Typekey: The endpoint which serves the typekey resolution (only trailing slash!)
        typekey = typekey/
        # cat=BAUMER.Encoway/Configurator/120; type=string; label=Encoway - API - Variant: The endpoint which serves the variant lookup (only trailing slash!)
        variant = variant/
      }
    }
  }

  productfinder {
    attributes {
      # cat=BAUMER.Contentserv/Pfinder/110; type=int+; label=PFinder - Filters: ID of the Contentserv Attribute that holds the Attributes to be used in filters of the pfinder
      filter = 1898
      # cat=BAUMER.Contentserv/Pfinder/120; type=int+; label=PFinder - Table columns: ID of the Contentserv Attribute that holds the Attributes to be displayed as table columns of the pfinder
      list = 1921
      # cat=BAUMER.Contentserv/Pfinder/130; type=int+; label=PFinder - Gallery attributes: ID of the Contentserv Attribute that holds the Attributes to be displayed in the gallery view of the pfinder
      gallery = 1922
      # cat=BAUMER.Contentserv/Pfinder/140; type=int+; label=PFinder - Product image: ID of the Contentserv Attribute that holds the Attribute that should be used as product image
      image = 1923
      # cat=BAUMER.Contentserv/Pfinder/150; type=int+; label=PFinder - Datasheet: ID of the Contentserv Attribute that holds the Attribute that contains the File reference to the the products datasheet
      datasheet = 1924
      # cat=BAUMER.Contentserv/Pfinder/160; type=int+; label=PFinder - Sorting: ID of the Contentserv Attribute that holds the Attribute that contains the manual product sort order
      sorting = 2119
    }
  }

  downloadcenter {
    attributes {
      # cat=BAUMER.Contentserv/Downloadcenter/210; type=int+; label=MAM - Maingroup: ID of the Contentserv MAM-Attribute containing the maingroup
      maingroup = 53
      # cat=BAUMER.Contentserv/Downloadcenter/220; type=int+; label=MAM - Filetype: ID of the Contentserv MAM-Attribute containing the filetype
      filetype = 54
      # cat=BAUMER.Contentserv/Downloadcenter/230; type=int+; label=MAM - Mediatype: ID of the Contentserv MAM-Attribute containing the mediatype
      mediatype = 51
      # cat=BAUMER.Contentserv/Downloadcenter/240; type=int+; label=MAM - Title: ID of the Contentserv MAM-Attribute containing the title
      title = 57
    }
  }

  mamImport {
    # cat=BAUMER.Contentserv/MAM; type=int+; label=MAM - StateID: ID of the Workflow state that files need to have to be imported
    allowedState = 8
    # cat=BAUMER.Contentserv/MAM; type=int+; label=MAM - Language AttributeID: ID of the Attribute holding the files language
    languageAttributeId = 59
  }
}
