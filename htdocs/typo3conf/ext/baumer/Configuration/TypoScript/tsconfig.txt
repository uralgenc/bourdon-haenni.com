TCEMAIN.table.tt_content {
    # Pages will *not* have "(copy)" appended:
    disablePrependAtCopy = 1
    # Pages will *not* be hidden upon copy:
    disableHideAtCopy = 1
}
TCEMAIN.table.pages {
    # Pages will *not* have "(copy)" appended:
    disablePrependAtCopy = 1
    # Pages will *not* be hidden upon copy:
    disableHideAtCopy = 1
}

TCEFORM.pages.author_email.label = Google Site Verification Code
TCEFORM.pages_language_overlay.author_email.label = Google Site Verification Code