<?php

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Sebastian Fischer <typo3@evoweb.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

if (is_array($GLOBALS[ 'TYPO3_CONF_VARS' ][ 'EXTCONF' ][ 'realurl' ])) {
    $register = [
        'productfinder' => [
            [
                'GETvar' => 'tx_baumer_csview[action]',
                'valueMap' => [
                    'productfinder' => 'selector',
                ],
            ],
            [
                'GETvar' => 'tx_baumer_csview[action]',
                'noMatch' => 'bypass',
            ],
        ],
        'sortiment-parent' => [
            [
                'GETvar' => 'tx_baumer_csview[csView]',
                'lookUpTable' => [
                    'table' => 'tx_baumer_domain_model_csview',
                    'id_field' => 'uid',
                    'alias_field' => 'label',
                    'addWhereClause' => ' AND NOT deleted AND NOT hidden',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => [
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ],
                    'enable404forInvalidAlias' => '1',
                ],
            ],
        ],
    ];

    require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('baumer') . 'Classes/UserFunc/RealUrlHandler.php';
    $conf = new \Baumer\Baumer\UserFunc\RealUrlHandler();
    $conf->register($register);
}
