<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_baumer_domain_model_csviewrecordrelation'] = [
    'ctrl' => $GLOBALS['TCA']['tx_baumer_domain_model_csviewrecordrelation']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'link, tt_content, cs_view_record',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden;;1, link, tt_content, cs_view_record'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'crdate' => [
            'exclude' => 0,
            'label' => 'Creation date',
            'config' => [
                'type' => 'none',
                'format' => 'date',
                'eval' => 'date',
            ]
        ],
        'tt_content' => [
            'config' => [
                'type' => 'passthrough',
                'readOnly' => 1,
            ]
        ],
        'link' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang.xlf:flux.contentservTeaserView.fields.settings.selection.link',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'max' => 1024,
                'eval' => 'trim',
                'wizards' => [
                    'link' => [
                        'type' => 'popup',
                        'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel',
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
                        'module' => [
                            'name' => 'wizard_element_browser',
                            'urlParameters' => [
                                'mode' => 'wizard'
                            ]
                        ],
                        'JSopenParams' => 'width=800,height=600,status=0,menubar=0,scrollbars=1'
                    ]
                ],
                'softref' => 'typolink'
            ]
        ],
        'sorting' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecordrelation.sap_id',
            'config' => [
                'type' => 'input',
            ]
        ],
        'cs_view_record' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang.xlf:flux.contentservTeaserView.fields.settings.selection.record',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'tx_baumer_domain_model_csviewrecord',
                'minitems' => 1,
                'maxitems' => 1,
                'readOnly' => 1,
            ],
        ],
    ],
];
