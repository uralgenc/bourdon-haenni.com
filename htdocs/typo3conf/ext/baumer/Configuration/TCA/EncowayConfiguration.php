<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_baumer_domain_model_encowayconfiguration'] = [
    'ctrl' => $GLOBALS['TCA']['tx_baumer_domain_model_encowayconfiguration']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'shortcode, product_family',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden;;1, shortcode, product_family, '],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'shortcode' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_encowayconfiguration.shortcode',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'content_serv_id' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_encowayconfiguration.content_serv_id',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'readOnly' => '1'
            ],
        ],
        'completed' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_encowayconfiguration.completed',
            'config' => [
                'type' => 'check',
                'default' => '0',
            ],
        ],
        'configuration_data' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
    ],
];
