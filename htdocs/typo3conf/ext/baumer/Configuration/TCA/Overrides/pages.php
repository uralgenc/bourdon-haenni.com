<?php
defined('TYPO3_MODE') or die();
$ll = 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:';
$table = 'pages';
$tcaPages = [
    'columns' => [
        'navigation_column' => [
            'exclude' => 1,
            'label' => 'Navigation column for "4 Column Grid Block"-Layout',
            "config" => [
                'type' => 'select',
                'items' => [
                    ['First column', '1'],
                    ['Second column', '2'],
                    ['Third column', '3'],
                    ['Fourth column', '4']
                ]
            ]
        ],
        'navigation_layout' => [
            'exclude' => 1,
            'label' => 'Navigation layout',
            "config" => [
                'type' => 'select',
                'items' => [
                    ['1 Column', 'sub-col-1'],
                    ['4 Column Grid Block', 'sub-col-4'],
                    ['4 Column Grid Images', 'sub-articles']
                ]
            ]
        ],
        'navigation_icon' => [
            'exclude' => 1,
            'label' => 'Navigation icon',
            "config" => [
                'type' => 'select',
                'items' => [
                    ['No Icon', ''],
                    ['Lock', 'lock']
                ]
            ]
        ],
        'navigation_text' => [
            'exclude' => 1,
            'label' => 'Navigation text for "4 Column Grid Images"-Layout',
            'config' => [
                'type' => 'text',
                'cols' => '40',
                'rows' => '5',
                'wrap' => 'off',
            ]
        ],
        'tx_baumer_title_tag' => [
            'label' => $ll . $table . '.tx_baumer_title_tag',
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
            ]
        ]
    ]
];
$tcaPagesLanguageOverlay = $tcaPages;
\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
    $GLOBALS['TCA']['pages'],
    $tcaPages
);
\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
    $GLOBALS['TCA']['pages_language_overlay'],
    $tcaPagesLanguageOverlay
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'navigation_layout',
    1,
    'after:content_from_pid'
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'navigation_column',
    1,
    'after:navigation_layout'
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'navigation_icon',
    1,
    'after:navigation_column'
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'navigation_text',
    1,
    'after:navigation_icon'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'tx_baumer_title_tag',
    '',
    'after:nav_title'
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages_language_overlay',
    'navigation_column',
    1,
    'after:content_from_pid'
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages_language_overlay',
    'navigation_layout',
    1,
    'after:navigation_column'
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages_language_overlay',
    'navigation_icon',
    1,
    'after:navigation_layout'
);
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages_language_overlay',
    'navigation_text',
    1,
    'after:navigation_icon'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages_language_overlay',
    'tx_baumer_title_tag',
    '',
    'after:nav_title'
);