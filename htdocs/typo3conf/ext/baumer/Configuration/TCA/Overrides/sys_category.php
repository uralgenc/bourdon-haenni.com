<?php
defined('TYPO3_MODE') or die();

$tempSysCategory = [
    'tx_baumer_evalanche_product_group' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:sys_category.evalanche_id',
        'config' => [
            'type' => 'input',
            'size' => '30'
        ]
    ],
    'tx_baumer_evalanche_product_group_hidden' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:sys_category.hide_in_newsletter',
        'config' => [
            'type' => 'check',
            'default' => '0'
        ]
    ],
    'amirada_maingroup_id' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:sys_category.amirada_maingroup_id',
        'config' => [
            'type' => 'input',
            'size' => '30'
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $tempSysCategory, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', 'amirada_maingroup_id;;;;1-1-1', '', 'after:description');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', 'tx_baumer_evalanche_product_group;;;;1-1-1', '', 'after:description');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', 'tx_baumer_evalanche_product_group_hidden;;;;1-1-1', '', 'after:description');
