<?php
defined('TYPO3_MODE') or die();

$tempFEUsers = [
    'gender' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:fe_users.gender',
        'config' => [
            'type' => 'radio',
            'items' => [
                ['Undefined', '99'],
                ['Male', '0'],
                ['Female', '1']
            ],
        ]
    ],
    'token' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:fe_users.token',
        'config' => [
            'type' => 'text',
            'rows' => '1',
            'cols' => '32'
        ]
    ],
    'newsletter' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:fe_users.receive_newsletter',
        'config' => [
            'type' => 'check',
            'default' => '0',
            'readOnly' => '1',
        ]
    ],
    'newsletter_user' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:fe_users.has_newsletter_user',
        'config' => [
            'type' => 'check',
            'default' => '0',
            'readOnly' => '1',
        ]
    ],
    'status' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:fe_users.status',
        'config' => [
            'type' => 'input',
            'size' => '10'
        ]
    ],
    'terms_acknowledged' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:fe_users.acknowledged_terms',
        'config' => [
            'type' => 'check',
            'default' => '0',
            'readOnly' => '1',
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempFEUsers, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'gender;;;;1-1-1', '', 'after:tx_extbase_type');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'token;;;;1-1-1', '', 'after:tx_extbase_type');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'status;;;;1-1-1', '', 'after:tx_extbase_type');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'newsletter;;;;1-1-1', '', 'after:tx_extbase_type');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'newsletter_user;;;;1-1-1', '', 'after:tx_extbase_type');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'terms_acknowledged;;;;1-1-1', '', 'after:tx_extbase_type');
