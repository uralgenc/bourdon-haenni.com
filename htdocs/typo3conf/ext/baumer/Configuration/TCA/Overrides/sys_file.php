<?php

// We have a serious problem here, the vidi query parser does not handle this setting well.
// There is currently no possibility to have two fields referencing to sys_categories.
// We need to wait for the solution done by vidi, Fabien is informed and told me to work
// on it soonish.


$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['baumer']);
$documentTypeParentId = intval($extConf['documentTypeCategoryId']);

$GLOBALS['TCA']['sys_file_metadata']['columns']['documenttype'] = $GLOBALS['TCA']['sys_file_metadata']['columns']['categories'];
$GLOBALS['TCA']['sys_file_metadata']['columns']['documenttype']['label'] = 'Document Type';
$GLOBALS['TCA']['sys_file_metadata']['columns']['documenttype']['config']['MM_match_fields']['fieldname'] = 'documenttype';
$GLOBALS['TCA']['sys_file_metadata']['columns']['documenttype']['config']['MM_opposite_field'] = 'documenttype';
$GLOBALS['TCA']['sys_file_metadata']['columns']['documenttype']['config']['foreign_table_where'] = 'AND sys_category.sys_language_uid IN (-1, 0) AND (sys_category.uid = ' .  $documentTypeParentId . ' OR sys_category.parent = ' . $documentTypeParentId . ') ORDER BY sys_category.sorting ASC';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_file_metadata', 'documenttype');

// use own renderer to preselect available category records
$GLOBALS['TCA']['sys_file']['grid']['columns']['metadata.documenttype'] = [
    'renderers' => [
        new Baumer\Baumer\Grid\DocumentTypeRendererComponent(),
    ],
    'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:sys_file.document_type',
    'editable' => false,
    'visible' => true,
    'sortable' => false,
];
$GLOBALS['TCA']['sys_file']['grid']['columns']['metadata.categories']['renderers'][1] = new Baumer\Baumer\Grid\CategoryRendererComponent();

// preselect category renderer to avoid usage of documenttype records
$GLOBALS['TCA']['sys_file_metadata']['columns']['categories']['config']['foreign_table_where'] = 'AND sys_category.sys_language_uid IN (-1, 0) AND sys_category.uid <> ' . $documentTypeParentId . ' AND sys_category.parent <> ' . $documentTypeParentId . ' ORDER BY sys_category.sorting ASC';

$GLOBALS['TCA']['sys_file']['columns']['tika_content'] = [
    'label' => 'Extracted content',
    'config' => [
        'type' => 'passthrough'
    ]
];
$GLOBALS['TCA']['sys_file_metadata']['columns']['data'] = [
    'label' => 'data',
    'config' => [
        'type' => 'passthrough'
    ]
];

/** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
$db = $GLOBALS['TYPO3_DB'];
$enableFieldsSysCategory = 'deleted = 0 AND hidden = 0 AND sys_language_uid = 0';

$categories = $db->exec_SELECTgetRows('uid, title, parent', 'sys_category', 'parent = ' . $documentTypeParentId . ' AND '. $enableFieldsSysCategory);

$documenttypeSuggestions = [];
foreach ($categories as $category) {
    $documenttypeSuggestions[$category['uid']] = $category['title'];
}
asort($documenttypeSuggestions);

$categorySuggestions = [];
$categories = $db->exec_SELECTgetRows('uid, title, parent', 'sys_category', $enableFieldsSysCategory);
foreach ($categories as $category) {
    if ($category['uid'] != $documentTypeParentId && $category['parent'] > 0 && $category['parent'] != $documentTypeParentId) {
        $categorySuggestions[$category['uid']] = $category['title'];
    }
}

asort($categorySuggestions);

// remove categories facet from vidi
unset($GLOBALS['TCA']['sys_file']['grid']['facets'][4]);
// add own facet configuration for categories and documenttypes
// FIXME - Update issue
//$GLOBALS['TCA']['sys_file']['grid']['facets'] = array_merge($GLOBALS['TCA']['sys_file']['grid']['facets'], array(
//		new Fab\Vidi\Facet\StandardFacet(
//			'metadata.documenttype',
//			'Document Types',
//			$documenttypeSuggestions
//		),
//		new Fab\Vidi\Facet\StandardFacet(
//			'metadata.categories',
//			'Categories',
//			$categorySuggestions
//		),
//	)
//);

// Add column to display file language

//$GLOBALS['TCA']['sys_file']['grid']['columns']['metadata.language'] = array(
//	'renderer' => new  Fab\Media\Grid\MetadataRendererComponent(array('property' => 'language')),
//	'editable' => FALSE,
//	'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:sys_file.file_language',
//	'sortable' => FALSE,
//	'visible' => TRUE
//);

$GLOBALS['TCA']['sys_file']['grid']['facets'][] = new Fab\Vidi\Facet\StandardFacet(
    'metadata.language',
    'File Language',
    ['de', 'en', 'fr', 'etc...'] // auto-suggestions
);
