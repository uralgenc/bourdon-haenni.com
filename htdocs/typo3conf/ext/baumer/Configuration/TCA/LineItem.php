<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_baumer_domain_model_lineitem'] = [
    'ctrl' => $GLOBALS['TCA']['tx_baumer_domain_model_lineitem']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'product_list, amount, express, title, encoway_configuration, amirada_configuration, note',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden;;1, product_list, amount, price, shipping, configuration, encoway_configuration, amirada_configuration, main_group, group_title, note'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'encoway_configuration' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.encoway_configuration',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'tx_baumer_domain_model_encowayconfiguration',
                'minitems' => 0,
                'maxitems' => 1,
                'items' => [
                    ['none', '']
                ]
            ],
        ],
        'amirada_configuration' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.amirada_configuration',
            'config' => [
                'type' => 'text',
            ],
        ],
        'product_list' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'tx_baumer_domain_model_productlist',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'attributes' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.attributes',
            'config' => [
                'type' => 'text',
            ],
        ],
        'crdate' => [
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'tstamp' => [
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'amount' => [
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.amount',
            'config' => [
                'type' => 'input',
                'eval' => 'int,required'
            ]
        ],
        'note' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.note',
            'config' => [
                'type' => 'text',
            ],
        ],
        'price' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.price',
            'config' => [
                'type' => 'input',
                'eval' => 'double2'
            ],
        ],
        'shipping' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.shipping',
            'config' => [
                'type' => 'input',
                'eval' => 'int'
            ],
        ],
        'configuration' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.configuration',
            'config' => [
                'type' => 'input',
            ],
        ],
        'group_title' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.group_title',
            'config' => [
                'type' => 'input',
            ],
        ],
        'main_group' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.mainGroup',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'sys_category',
                'foreign_table_where' => ' AND sys_category.parent = 20',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'image_url' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.image_url',
            'config' => [
                'type' => 'input',
            ],
        ],
        'label' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.label',
            'config' => [
                'type' => 'input',
            ],
        ],
        'cs_view_record' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_lineitem.cs_view_record',
            'config' => [
                'type' => 'input',
            ],
        ],
    ]
];
