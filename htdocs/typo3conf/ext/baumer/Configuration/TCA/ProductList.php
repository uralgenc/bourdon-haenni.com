<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_baumer_domain_model_productlist'] = [
    'ctrl' => $GLOBALS['TCA']['tx_baumer_domain_model_productlist']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'name, code, description, fe_user, named_encoway_configurations',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden;;1, name, code, description, fe_user, line_items, '],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'crdate' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist.crdate',
            'config' => [
                'type' => 'none',
                'format' => 'date',
                'eval' => 'date',
            ]
        ],
        'name' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist.name',
            'config' => [
                'type' => 'input',
            ]
        ],
        'code' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist.code',
            'config' => [
                'type' => 'input',
            ]
        ],
        'description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist.description',
            'config' => [
                'type' => 'text',
            ]
        ],
        'fe_user' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist.fe_user',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'fe_users',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'line_items' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:zollner_messe/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_productlist.line_items',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_baumer_domain_model_lineitem',
                'foreign_field' => 'product_list',
                'maxitems'      => 9999,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                ],
            ],

        ],
    ],
];
