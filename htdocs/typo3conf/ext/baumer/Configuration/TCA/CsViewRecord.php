<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_baumer_domain_model_csviewrecord'] = [
    'ctrl' => $GLOBALS['TCA']['tx_baumer_domain_model_csviewrecord']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, label, sap_id, content_serv_id, data',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, label, sap_id, content_serv_id, data, cs_views, site'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'pid' => [
            'exclude' => 1,
            'label' => 'PID',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_baumer_domain_model_csviewrecord',
                'foreign_table_where' => 'AND tx_baumer_domain_model_csviewrecord.pid=###CURRENT_PID### AND tx_baumer_domain_model_csviewrecord.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'sorting' => [
            'exclude' => 1,
            'label' => 'Sorting',
            'config' => [
                'type' => 'input',
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'crdate' => [
            'exclude' => 0,
            'label' => 'Creation date',
            'config' => [
                'type' => 'none',
                'format' => 'date',
                'eval' => 'date',
            ]
        ],
        'label' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecord.label',
            'config' => [
                'type' => 'input',
            ]
        ],
        'sap_id' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecord.sap_id',
            'config' => [
                'type' => 'input',
            ]
        ],
        'data' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecord.data',
            'config' => [
                'type' => 'text',
            ]
        ],
        'content_serv_id' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecord.content_serv_id',
            'config' => [
                'type' => 'input',
            ]
        ],
        'cs_views' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecord.cs_views',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'tx_baumer_domain_model_csview',
                'MM' => 'tx_baumer_domain_model_cs_view_csviewrecord_mm',
                'MM_opposite_field' => 'cs_view_records'
            ]
        ],
        'site' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_csviewrecord.site',
            'config' => [
                'type' => 'input',
            ]
        ],
    ],
];
