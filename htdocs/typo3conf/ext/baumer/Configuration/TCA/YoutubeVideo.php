<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_baumer_domain_model_youtubevideo'] = [
    'ctrl' => $GLOBALS['TCA']['tx_baumer_domain_model_youtubevideo']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, video_id, title, description, thumbnail_url, playlist_id, language, published_at, view_count,',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, video_id, title, description;;;richtext:rte_transform[mode=ts_links], thumbnail_url, playlist_id, language, published_at, view_count, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_baumer_domain_model_youtubevideo',
                'foreign_table_where' => 'AND tx_baumer_domain_model_youtubevideo.pid=###CURRENT_PID### AND tx_baumer_domain_model_youtubevideo.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'starttime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
            ],
        ],
        'video_id' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.video_id',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'title' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.title',
            'config' => [
                'type' => 'input',
                'size' => 100,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'wizards' => [
                    'RTE' => [
                        'icon' => 'wizard_rte2.gif',
                        'notNewRecords' => 1,
                        'RTEonly' => 1,
                        'script' => 'wizard_rte.php',
                        'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
                        'type' => 'script'
                    ]
                ]
            ],
        ],
        'thumbnail_url' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.thumbnail_url',
            'config' => [
                'type' => 'input',
                'size' => 100,
                'eval' => 'trim'
            ],
        ],
        'playlist_id' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.playlist_id',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'language' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.language',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'published_at' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.published_at',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
            ],
        ],
        'view_count' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.view_count',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'duration' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:baumer/Resources/Private/Language/locallang_db.xlf:tx_baumer_domain_model_youtubevideo.duration',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
    ],
];
