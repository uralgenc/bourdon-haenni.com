<?php
namespace Baumer\Baumer\FileUpload;

use Fab\Media\FileUpload\UploadedFileAbstract;
use TYPO3\CMS\Core\Resource\File;

/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans HÃ¶chtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html
 * GNU General Public License, version 3 or later
 */

/**
 * Class DownloadedFile
 *
 * @package Baumer\Baumer\FileUpload
 */
class DownloadedFile extends UploadedFileAbstract
{
    /**
     * @var string
     */
    protected $inputName = '';
    /**
     * @var string
     */
    protected $originalName = '';

    /**
     * @param string $data
     * @param string $fileName
     */
    public function __construct($data, $fileName)
    {
        $temp = tempnam(sys_get_temp_dir(), 'TMP_');
        file_put_contents($temp, $data);
        $this->inputName = $temp;
        $this->name = basename($temp);
        $this->originalName = basename($fileName);
        $this->uploadFolder = sys_get_temp_dir();
    }

    /**
     * Save the file to the specified path
     *
     * @return boolean TRUE on success
     */
    public function save()
    {
        return rename($this->inputName, $this->getFileWithAbsolutePath());
    }

    /**
     * Get the file size
     *
     * @return integer file-size in byte
     */
    public function getSize()
    {
        return filesize($this->inputName);
    }

    /**
     * Get the mime type of the file.
     *
     * @return int
     */
    public function getMimeType()
    {
        if (function_exists('finfo_file')) {
            $fileInfo = new \finfo();
            return $fileInfo->file($this->inputName, FILEINFO_MIME_TYPE);
        } elseif (function_exists('mime_content_type')) {
            return mime_content_type($this->inputName);
        }
        return false;
    }

    /**
     * Get the original filename.
     *
     * @return string filename
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }
}
