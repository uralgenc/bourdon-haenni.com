<?php
namespace Baumer\Baumer\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Baumer\Baumer\Domain\Model\PriceShipping;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use Baumer\Baumer\Domain\Model\EncowayConfiguration;
use Baumer\Baumer\Domain\Model\CsViewRecord;

/**
 * EncowayController
 *
 * @author Hans Höchtl <extern.hans.hoechtl@typovision.de>
 */
class EncowayController extends ActionController
{

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $feUser = null;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $feUserRepository = null;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRecordRepository
     * @inject
     */
    protected $csViewRecordRepository = null;

    /**
     * @var \Baumer\Baumer\Domain\Repository\ProductListRepository
     * @inject
     */
    protected $productListRepository = null;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = null;

    /**
     * configurationRepository
     *
     * @var \Baumer\Baumer\Domain\Repository\EncowayConfigurationRepository
     * @inject
     */
    protected $encowayConfigurationRepository = null;

    /**
     * encowayServiceFactory
     *
     * @var \Baumer\Baumer\Factory\EncowayServiceFactory
     * @inject
     */
    protected $encowayServiceFactory = null;

    /**
     * @var \Baumer\Baumer\Domain\Repository\PriceShippingRepository
     * @inject
     */
    protected $priceShippingRepository = null;

    /*
     * ============================================================
     *   Initialisation
     * ============================================================
     */

//	/**
//	 * Initializes the controllers view
//	 * @return void
//	 */
//	public function initializeView() {
//		if ($this->request->getFormat() === 'json') {
//			/** @noinspection PhpDeprecationInspection */
//			$this->view =
//				$this->objectManager->create('TYPO3\\CMS\\Extbase\\Mvc\\View\\JsonView');
//			$this->view->setControllerContext($this->controllerContext);
//		}
//	}

    /**
     * Initialize the actions and check if there's a frontendUser logged in
     * @return void
     */
    public function initializeAction()
    {
        if ($GLOBALS['TSFE']->loginUser === true) {
            $this->feUser = $this->feUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        }
    }

    /*
     * ============================================================
     *   Plugin actions
     * ============================================================
     */

    /**
     * Load Configurator FE-application
     *
     * @param CsViewRecord $csViewRecord
     * @return void
     */
    public function configuratorAction(CsViewRecord $csViewRecord)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        /** @var QueryResultInterface $productLists */
        $productLists = $this->productListRepository->findByFeUser($this->feUser);
        $this->view->assign('csViewRecord', $csViewRecord);
        $this->view->assign('productLists', $productLists);
    }

    /**
     * Load configurator with a stored configuration
     * identified by it's shortcode
     *
     * @param string $shortCode
     * @return void
     */
    public function loadAction($shortCode)
    {
        /** @var EncowayConfiguration $configuration */
        $configuration = $this->encowayConfigurationRepository->findByShortcode($shortCode);
        $csViewRecord = $this->csViewRecordRepository->findOneByContentServId($configuration->getContentServId());
        $encowayService = $this->encowayServiceFactory->resetService(
            $csViewRecord->getSapId(), $this->settings['encoway']
        );
        $encowayService->loadConfiguration($configuration->getConfigurationData());
        $this->redirect(
            'configurator',
            'CsView',
            null,
            [
                'csViewRecord' => $csViewRecord->getUid()
            ]
        );
    }

    /**
     * Reset the configuration state for a csViewRecord
     *
     * @param CsViewRecord $csViewRecord
     * @return void
     */
    public function resetAction(CsViewRecord $csViewRecord)
    {
        $this->encowayServiceFactory->resetService($csViewRecord->getSapId(), $this->settings['encoway']);
        $this->getParametersAction($csViewRecord);
    }



    /*
     * ============================================================
     *   Ajax Actions
     * ============================================================
     */

    /**
     * Ajax operation to store the current configuration
     *
     * @param CsViewRecord $csViewRecord
     * @param string $productCode
     * @return void
     */
    public function saveConfigurationAction(CsViewRecord $csViewRecord, $productCode)
    {
        $success = true;
        $configurationUid = 0;
        $configurationShortCode = '';
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $response = $encowayService->saveConfiguration();
        if ($response != 404 && $response != 500) {
            // Check for already stored configuration with
            // same shortcode and productfamily and update
            $configuration = $this->encowayConfigurationRepository->findByShortcodeAndContentServId($productCode, $csViewRecord->getContentServId());
            if ($configuration) {
                $configuration->setConfigurationData($response);
                $this->encowayConfigurationRepository->update($configuration);
            } else {
                $configuration = new EncowayConfiguration();
                $configuration->setPid($this->settings['encoway']['storage']['encowayConfigurations']);
                $configuration->setShortcode($productCode);
                $configuration->setContentServId($csViewRecord->getContentServId());
                $configuration->setConfigurationData($response);
                $this->encowayConfigurationRepository->add($configuration);
            }
            /** @var PersistenceManager $persistenceManager */
            $persistenceManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
            $persistenceManager->persistAll();
            $configurationUid = $configuration->getUid();
            $configurationShortCode = $configuration->getShortcode();
        } else {
            $this->throwStatus($response, 'Saving configuration failed (1422970356)');
        }
        $uriBuilder = $this->controllerContext->getUriBuilder();
        $uriBuilder->setCreateAbsoluteUri(true);
        $uriBuilder->setArguments([
            'tx_baumer_csview' => [
                'shortCode' => $configurationShortCode,
                'controller' => 'Encoway',
                'csViewRecord' => $csViewRecord->getUid(),
                'action' => 'load'
            ]
        ]);
        echo json_encode([
            'success' => $success,
            'encowayConfiguration' => $configurationUid,
            'encowayConfigurationShortCode' => $configurationShortCode,
            'link' => $uriBuilder->build()
        ]);
        exit;
    }

    /**
     * Filter out parameters from the container that are optional and
     * not selectable
     *
     * @param array $container
     * @return array
     */
    protected function filterUnselectableParameters($container)
    {
        $selectableParameters = [];
        foreach ($container['parameters'] as $parameter) {
            $hasSelectableValue = false;
            foreach ($parameter['values'] as $valueIdx => $value) {
                if ($value['selectable']) {
                    $hasSelectableValue = true;
                }
                if ($value['value'] === '&') {
                    unset($parameter['values'][$valueIdx]);
                }
            }
            $parameter['values'] = array_values($parameter['values']);
            if ($hasSelectableValue) {
                $selectableParameters[] = $parameter;
            }
        }
        $container['parameters'] = $selectableParameters;
        return $container;
    }

    /**
     * First we do a neutral request to get the parameter and container structure containing
     * the SAP ids.
     * The containers starting with "A" are always available, the containers starting with "B"
     * are optional and are only visible, if the typekey is already complete (contains no "#" anymore)
     *
     * @param CsViewRecord $csViewRecord
     * @return array
     */
    protected function getFilteredParametersAndTypekey(CsViewRecord $csViewRecord)
    {
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $parameterContainers = $encowayService->getParameters();

        // Handle encoway session timeout and empty response
        if ($parameterContainers == 404 || $parameterContainers == 409) {
            $encowayService = $this->encowayServiceFactory->resetService($csViewRecord->getSapId(), $this->settings['encoway']);
            $parameterContainers = $encowayService->getParameters();
        }
        $typekey = $encowayService->getTypekey();

        return [$typekey, $parameterContainers];
    }

    /**
     * Ajax operation to refresh the current configuration
     * Ajax operation to refresh the current configuration
     *
     * @param CsViewRecord $csViewRecord
     * @return void
     */
    public function getParametersAction(CsViewRecord $csViewRecord)
    {
        $success = true;
        list($typekey, $containers) = $this->getFilteredParametersAndTypekey($csViewRecord);
        echo json_encode([
            'success' => $success,
            'container' => $containers,
            'typekey' => $typekey
        ]);
        exit;
    }

    /**
     * Ajax operation to set a parameters value
     * outputs string JSON containing operation success and updated
     * containers with parameters
     *
     * @param CsViewRecord $csViewRecord
     * @param string $parameterId
     * @param string $parameterValue
     * @return void
     *
     */
    public function setParameterAction(CsViewRecord $csViewRecord, $parameterId, $parameterValue)
    {
        $success = true;
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $response = $encowayService->setParameter($parameterId, $parameterValue);
        if ($response['status'] != 'SUCCESS') {
            $success = false;
        }
        list($typekey, $containers) = $this->getFilteredParametersAndTypekey($csViewRecord);
        echo json_encode([
            'success' => $success,
            'status' => $response['status'],
            'container' => $containers,
            'affectedParameters' => $response['affectedParameters'],
            'typekey' => $typekey
        ]);
        exit;
    }

    /**
     * Ajax operation for Encoway accept operation. This is necessary after a forced parameter set operation with
     * a conflict.
     *
     * @param CsViewRecord $csViewRecord
     * @param boolean $confirm
     * @return void
     */
    public function confirmOperationAction(CsViewRecord $csViewRecord, $confirm)
    {
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $response = $encowayService->setConfirmOperation((boolean)$confirm);
        echo json_encode([
            'success' => $response,
        ]);
        exit;
    }

    /**
     * Ajax operation to unset a parameters value.
     * Outputs string JSON containing operation success and
     * updated containers with parameters
     *
     * @param CsViewRecord $csViewRecord
     * @param string $parameterId
     * @return void
     */
    public function unsetParameterAction(CsViewRecord $csViewRecord, $parameterId)
    {
        $success = true;
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $response = $encowayService->unsetParameter($parameterId);
        if ($response != 200) {
            $success = false;
        }
        list($typekey, $containers) = $this->getFilteredParametersAndTypekey($csViewRecord);
        echo json_encode([
            'success' => $success,
            'container' => $containers,
            'typekey' => $typekey
        ]);
        exit;
    }

    /**
     * Ajax operation to keep configurator session alive
     * Should be called every 30s.
     * Outputs string JSON containing status of session
     * if success is false terminate configurator
     *
     * @param CsViewRecord $csViewRecord
     * @return void
     */
    public function heartBeatAction(CsViewRecord $csViewRecord)
    {
        $success = true;
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $response = $encowayService->heartBeat();
        if ($response != 200) {
            $success = false;
        }
        echo json_encode(['success' => $success]);
        exit;
    }

    /**
     * Ajax operation to get the typekey of the current
     * configuration state. E.g. MEX5-##2.###
     *
     * @param CsViewRecord $csViewRecord
     */
    public function typekeyAction(CsViewRecord $csViewRecord)
    {
        $success = true;
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $typekey = $encowayService->getTypekey();
        if ((int)$typekey > 300 && (int)$typekey < 500) {
            $success = false;
        }
        echo json_encode([
            'success' => $success,
            'typekey' => $typekey
        ]);
        exit;
    }

    /**
     * Ajax operation to get the variant of a finished configuration
     *
     * @param CsViewRecord $csViewRecord
     */
    public function variantAction(CsViewRecord $csViewRecord)
    {
        $success = true;
        $encowayService = $this->encowayServiceFactory->getService($csViewRecord->getSapId(), $this->settings['encoway']);
        $variant = $encowayService->getVariant();
        if ((int)$variant > 300 && (int)$variant < 500) {
            $success = false;
        }
        echo json_encode([
            'success' => $success,
            'variant' => $variant
        ]);
        exit;
    }

    /**
     * Get price and delivery information from database
     * according to typeKey
     *
     * @param string $typeKey
     */
    public function getPriceShippingByTypekeyAction($typeKey)
    {
        $success = false;
        $priceShipping = $this->priceShippingRepository->findOneByProductCode($typeKey);
        if ($priceShipping instanceof PriceShipping) {
            $success = true;
        }
        echo json_encode([
            'success' => $success,
            'priceShipping' => $priceShipping
        ]);
        exit;
    }
}
