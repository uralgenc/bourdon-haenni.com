<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace Baumer\Baumer\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class MediaLibraryController
 *
 * @package Baumer\Baumer\Controller
 */
class MediaLibraryController extends ActionController
{

    /**
     * @var \Baumer\Baumer\Domain\Repository\YoutubeVideoRepository
     * @inject
     */
    protected $youtubeVideoRepository;

    public function videoAction()
    {
        $youtubeVideos = $this->youtubeVideoRepository->findAll();
        $youtubeVideosAr = [];
        /** @var \Baumer\Baumer\Domain\Model\YoutubeVideo $youtubeVideo */
        foreach ($youtubeVideos as $youtubeVideo) {
            $youtubeVideosAr[] = $youtubeVideo->toArray(false);
        }
        $this->view->assign('youtubeVideos', json_encode($youtubeVideosAr));
    }
}
