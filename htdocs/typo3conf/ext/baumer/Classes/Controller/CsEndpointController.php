<?php
namespace Baumer\Baumer\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Martin R. Krause <martin.r.krause@gmx.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * ContentServ Endpoint controller
 *
 * API methods, called exclusively by ContentServ (PIM) to notify TYPO3 about changes in product (CsViewRecords) or
 * views (CsView)
 *
 * @author Martin R. Krause <martin.r.krause@gmx.de>
 */
class CsEndpointController extends ActionController
{

    const UPDATE_PRODUCT_ACTION = 'updateProduct';
    const DELETE_PRODUCT_ACTION = 'deleteProduct';
    const UPDATE_VIEW_ACTION = 'updateView';
    const DELETE_VIEW_ACTION = 'deleteView';


    /**
     * Creates an updateProduct action with a given ContentServ ID in the CS worker queue
     *
     * HTTP: GET http://[domain/ip]/CsEndpoint/updateProduct/$contentServId
     *
     * @param string $contentServId
     * @return string JSON response
     */
    public function updateProductAction($contentServId)
    {
        /** @var DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        if ($db->exec_INSERTquery(
            'tx_baumer_cs_worker_queue',
            [
                'action' => self::UPDATE_PRODUCT_ACTION,
                'parameters' => serialize($this->request->getArguments()),
                'tstamp' => time(),
                'crdate' => time()
            ]
        )) {
            return (json_encode(['status' => 'success', 'data' => '', 'message' => 'Update of product with ID ' . $contentServId .' has been added to the CS worker queue.']));
        } else {
            return (json_encode(['status' => 'failure', 'data' => '', 'message' => 'Update of product with ID ' . $contentServId .' could not been added to the CS worker queue. 1458062228']));
        }
    }

    /**
     * Deletes a given product (CsViewRecord) from TYPO3 DB and Elasticsearch index
     *
     * HTTP: GET http://[domain/ip]/CsEndpoint/deleteProduct/$contentServId
     *
     * @param string $contentServId
     * @return string
     */
    public function deleteProductAction($contentServId)
    {
        /** @var DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        if ($db->exec_INSERTquery(
            'tx_baumer_cs_worker_queue',
            [
                'action' => self::DELETE_PRODUCT_ACTION,
                'parameters' => serialize($this->request->getArguments()),
                'tstamp' => time(),
                'crdate' => time()
            ]
        )) {
            return (json_encode(['status' => 'success', 'data' => '', 'message' => 'Deletion of product with ID ' . $contentServId .' has been added to the CS worker queue.']));
        } else {
            return (json_encode(['status' => 'failure', 'data' => '', 'message' => 'Deletion of product with ID ' . $contentServId .' could not been added to the CS worker queue. 1458062228']));
        }
    }

    /**
     * Updates/Inserts a given View (CsView) into TYPO3 DB and Elasticsearch index
     *
     * HTTP: GET http://[domain/ip]/CsEndpoint/updateView/$contentServId
     *
     * @param string $contentServId
     * @return string
     */
    public function updateViewAction($contentServId)
    {
        /** @var DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        if ($db->exec_INSERTquery(
            'tx_baumer_cs_worker_queue',
            [
                'action' => self::UPDATE_VIEW_ACTION,
                'parameters' => serialize($this->request->getArguments()),
                'tstamp' => time(),
                'crdate' => time()
            ]
        )) {
            return (json_encode(['status' => 'success', 'data' => '', 'message' => 'Update of view with ID ' . $contentServId .' has been added to the CS worker queue.']));
        } else {
            return (json_encode(['status' => 'failure', 'data' => '', 'message' => 'Update of view with ID ' . $contentServId .' could not been added to the CS worker queue. 1458062228']));
        }
    }

    /**
     * Deletes a given view (CsView) from TYPO3 DB and Elasticsearch index
     *
     * HTTP: GET http://[domain/ip]/CsEndpoint/deleteProduct/$contentServId
     *
     * @param string $contentServId
     * @return string
     */
    public function deleteViewAction($contentServId)
    {
        /** @var DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        if ($db->exec_INSERTquery(
            'tx_baumer_cs_worker_queue',
            [
                'action' => self::DELETE_VIEW_ACTION,
                'parameters' => serialize($this->request->getArguments()),
                'tstamp' => time(),
                'crdate' => time()
            ]
        )) {
            return (json_encode(['status' => 'success', 'data' => '', 'message' => 'Deletion of view with ID ' . $contentServId .' has been added to the CS worker queue.']));
        } else {
            return (json_encode(['status' => 'failure', 'data' => '', 'message' => 'Deletion of view with ID ' . $contentServId .' could not been added to the CS worker queue. 1458062228']));
        }
    }
}
