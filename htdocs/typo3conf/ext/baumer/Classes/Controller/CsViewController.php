<?php
namespace Baumer\Baumer\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Baumer\Baumer\Domain\Model\CsView;
use Baumer\Baumer\Domain\Model\CsViewRecord;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class CsViewController
 *
 * @package Baumer\Baumer\Controller
 */
class CsViewController extends ActionController
{
    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRepository
     * @inject
     */
    protected $csViewRepository;
    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRecordRepository
     * @inject
     */
    protected $csViewRecordRepository;

    /**
     * @var \Baumer\Baumer\Service\Contentserv\ReferenceResolution
     * @inject
     */
    protected $referenceResolution;
    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Connection
     * @inject
     */
    protected $elasticConnection;

    /**
     * @var \TYPO3\CMS\Core\Resource\FileRepository
     * @inject
     */
    protected $fileRepository;
    /**
     * @var ResourceFactory
     */
    protected $factory;
    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $db;

    public function initializeAction()
    {
        $this->factory = GeneralUtility::makeInstance(ResourceFactory::class);
        $this->db = $GLOBALS['TYPO3_DB'];
    }

    /**
     * @param CsViewRecord $csViewRecord
     */
    public function configuratorAction(CsViewRecord $csViewRecord)
    {
        // no configurator view for csViewRecords without a kmat number
        if (!$csViewRecord->extractAttributeById($this->settings['productfinder']['attributes']['kmat'])['Value']) {
            /** @var TypoScriptFrontendController $tsfe  */
            $tsfe = $GLOBALS['TSFE'];
            $tsfe->pageNotFoundAndExit();
        } else {
            $csViewRecord = $this->referenceResolution->resolveFileReferences($csViewRecord);
            $viewRecordData = $csViewRecord->getData();
            $productImageUrl = @$viewRecordData['GroupedAttributes'][$this->settings['configurator']['attributes']['productImage']][0]['Links'][0]['FormattedValue'];
            $mainGroup = '';
            $mainGroupTitle = '';
            $mainGroupAttribute = $csViewRecord->extractGroupedAttributeById(
                $this->settings['configurator']['attributes']['maingroup']
            );
            if ($mainGroupAttribute) {
                $mainGroup = (int)@$mainGroupAttribute[0]['Value'][0];
                $mainGroupTitle = @$mainGroupAttribute[0]['FormattedValue'][0];
            }

            $datasheets = $csViewRecord->resolveDatasheets($this->settings['configurator']['attributes']['datasheet']);

            // check each download if file is accessible
            // todo: Does this work with the CDN?
            if (is_array(
                $viewRecordData['GroupedAttributes'][$this->settings['configurator']['attributes']['downloads']]
            )) {
                foreach ($viewRecordData['GroupedAttributes'][$this->settings['configurator']['attributes']['downloads']] as $downloadIndex => $download) {
                    if (is_array($download['Links'])) {
                        foreach ($download['Links'] as $linksIndex => $link) {
                            if (!$link['FormattedValue']) {
                                unset($viewRecordData['GroupedAttributes'][$this->settings['configurator']['attributes']['downloads']][$downloadIndex]['Links'][$linksIndex]);
                            }
                        }
                        if (count(
                                $viewRecordData['GroupedAttributes'][$this->settings['configurator']['attributes']['downloads']][$downloadIndex]['Links']
                            ) == 0
                        ) {
                            unset($viewRecordData['GroupedAttributes'][$this->settings['configurator']['attributes']['downloads']][$downloadIndex]);
                        }
                    } else {
                        unset($viewRecordData['GroupedAttributes'][$this->settings['configurator']['attributes']['downloads']][$downloadIndex]);
                    }
                }
            }

            $csViewRecord->setData(json_encode($viewRecordData));

            $GLOBALS['TSFE']->getPageRenderer()->setTitle($mainGroupTitle . ': ' . $csViewRecord->getLabel());

            $this->view->assign(
                'configuratorRecord',
                json_encode(
                    [
                        'uid' => $csViewRecord->getUid(),
                        'label' => $csViewRecord->getLabel(),
                        'image' => $productImageUrl,
                        'mainGroup' => $mainGroup,
                        'mainGroupTitle' => $mainGroupTitle,
                        'datasheets' => $datasheets
                    ]
                )
            );
            $this->view->assign('record', $csViewRecord);
        }
    }

    /**
     * @param CsView $csView
     */
    public function selectorAction(CsView $csView)
    {
        $this->view->assign('view', $csView);
        $this->view->assign('selectorConfig', json_encode($this->elasticConnection->getElasticConfig(CsView::$CLASS_TYPE_MAPPING[CsView::CLASS_SELECTOR])));
        $this->view->assign('csAttributes', json_encode($this->settings['productfinder']['attributes']));
    }
}
