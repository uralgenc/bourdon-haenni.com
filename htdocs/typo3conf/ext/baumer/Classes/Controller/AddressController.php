<?php
namespace Baumer\Baumer\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Lars Trebing <lars.trebing@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class AddressController extends ActionController
{

    /**
     * @var \Baumer\Baumer\Domain\Repository\AddressRepository
     * @inject
     */
    protected $addressRepository = null;

    /**
     * @var \TYPO3\CMS\Frontend\Page\PageRepository
     * @inject
     */
    protected $pageRepository = null;

    /**
     * List addresses that are relevant for the current page (used for the contact box plugin)
     *
     * @return void
     */
    public function listByPageAction()
    {
        $rootline = $this->pageRepository->getRootLine($GLOBALS['TSFE']->id, $GLOBALS['TSFE']->MP);
        $addresses = $this->addressRepository->findByRootline($rootline);
        $this->view->assign('addresses', $addresses);
    }
}
