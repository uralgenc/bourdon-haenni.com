<?php
namespace Baumer\Baumer\Controller;

/**
     * Copyright notice
     *
     * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
     *
     * @author Hans Höchtl <hhoechtl@1drop.de>
     *
     * @license http://www.gnu.org/licenses/gpl.html
     * GNU General Public License, version 3 or later
     */

/**
 * Class ContactAddressDataTrait
 */
trait ContactAddressDataTrait
{

    /**
     * @var \Baumer\Baumer\Domain\Repository\AddressRepository
     * @inject
     */
    protected $addressRepository;

    /**
     * @param mixed $mainGroupUid
     * @return array
     */
    protected function loadContactAddressData($mainGroupUid = null)
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        /** @var \Baumer\Baumer\Domain\Model\Address $contactAddress */
        $contactAddress = null;


        $categoryIds = [];
        $pageId = $GLOBALS['TSFE']->id;

        $siteRootUid = $this->getSiteRootUid();

        $res = $db->exec_SELECTquery('sys_category.uid',
            'sys_category INNER JOIN sys_category_record_mm ON sys_category.uid = sys_category_record_mm.uid_local',
            'sys_category_record_mm.tablenames = "pages" AND sys_category_record_mm.uid_foreign = ' . $pageId);

        if ($res->num_rows > 0) {
            while (($row = $db->sql_fetch_assoc($res))) {
                $categoryIds[] = $row['uid'];
            }

            if (!empty($categoryIds)) {
                $contactAddressesResult = $this->addressRepository->findByCategoryIds($categoryIds);
                foreach ($contactAddressesResult as $contactAddressResult) {
                    $contactAddresses[] = $contactAddressResult;
                }
            }
        } else {
            $contactAddressesResult = $this->addressRepository->findByPageId($siteRootUid);
            foreach ($contactAddressesResult as $contactAddressResult) {
                $contactAddresses[] = $contactAddressResult;
            }
        }

        if (!$contactAddresses) {
            $contactAddresses[] = $this->addressRepository->findDefault();
        }

        if ($contactAddresses) {
            foreach ($contactAddresses as &$contactAddress) {
                if ($contactAddress) {
                    $contactAddress = $contactAddress->toProcessedArray();
                }
            }
            return $contactAddresses;
        }
    }

    /**
     * @return int|null
     */
    protected function getSiteRootUid()
    {
        $GLOBALS['TSFE']->getPageAndRootline();
        $siteRootPids = $GLOBALS['TSFE']->getStorageSiterootPids();
        if (is_array($siteRootPids) && isset($siteRootPids['_SITEROOT'])) {
            return $siteRootPids['_SITEROOT'];
        }

        return null;
    }
}
