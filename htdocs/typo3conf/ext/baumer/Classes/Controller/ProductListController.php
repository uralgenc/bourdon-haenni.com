<?php
namespace Baumer\Baumer\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>, Typovision GmbH
 *  (c) 2015 Martin R. Krause <martin.r.krause@gmx.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter;
use Baumer\Baumer\Domain\Model\LineItem;
use Baumer\Baumer\Domain\Model\ProductList;
use Baumer\Baumer\Domain\Model\Category;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * ProductListController
 *
 * @author Hans Höchtl <extern.hans.hoechtl@typovision.de>
 */
class ProductListController extends ActionController
{

    use ContactAddressDataTrait;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $feUser = null;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $feUserRepository = null;

    /**
     * @var \Baumer\Baumer\Domain\Repository\ProductListRepository
     * @inject
     */
    protected $productListRepository = null;

    /**
     * @var \Baumer\Baumer\Domain\Repository\LineItemRepository
     * @inject
     */
    protected $lineItemRepository = null;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = null;

    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $db;

    /**
     * persistence manager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;


    /*
     * ============================================================
     *   Initialisation
     * ============================================================
     */

    /**
     * Initialize the actions and check if there's a frontendUser logged in
     * @return void
     */
    public function initializeAction()
    {
        if ($GLOBALS['TSFE']->loginUser === true) {
            $this->feUser = $this->feUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        }
        $this->db = $GLOBALS['TYPO3_DB'];

        $foreignPluginContext = GeneralUtility::_GET('tx_baumer_productfinderheader');
        if (empty($foreignPluginContext)) {
            $foreignPluginContext = GeneralUtility::_GET('tx_baumer_configurator');
        }
        if (empty($foreignPluginContext)) {
            $foreignPluginContext = GeneralUtility::_GET('tx_baumer_productfinderlist');
        }

        if (isset($foreignPluginContext['mainGroup']) && !$this->request->hasArgument('mainGroup')) {
            $this->request->setArgument('mainGroup', $foreignPluginContext['mainGroup']);
        }
    }

    /*
     * ============================================================
     *   Plugin actions
     * ============================================================
     */

    /**
     * List configurations stored for FE-User
     * @return void
     */
    public function listAction()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $productLists = $this->productListRepository->findByFeUser($this->feUser);
        $productListsJson = [];
        /** @var ProductList $productList */
        foreach ($productLists as $productList) {
            $productListsJson[] = $productList->toArray();
        }
        $this->view->assign('productListsJson', json_encode($productListsJson));
    }

    /**
     * Show the user current product list (from localStorage)
     * or load from DB if code was given
     * @return void
     */
    public function currentAction()
    {
        if ($this->request->hasArgument('code')) {
            $code = $this->request->getArgument('code');
        } else {
            $code = false;
        }
        if ($code) {
            $this->view->assign('code', $code);
            /** @noinspection PhpUndefinedMethodInspection */
            /** @var QueryResultInterface $productLists */
            $productLists = $this->productListRepository->findByCode($code);

            if ($productLists->current() instanceof ProductList) {
                /** @var ProductList $productList */
                $productList = $productLists->current();
                $now = new \DateTime;
                $validityInterval = $this->settings['productListCodeValidityPeriod'] * 86400;
                if ($productList->getCrdate()->getTimestamp() > ($now->getTimestamp() - $validityInterval)) {
                    $this->view->assignMultiple([
                        'currentProductListJson' => json_encode($productList->toArray()),
                        'outOfCodeValidityPeriod' => false
                    ]);
                } else {
                    $this->view->assignMultiple([
                        'currentProductListJson' => json_encode([]),
                        'outOfCodeValidityPeriod' => true
                    ]);
                }
            }
        }
    }

    /**
     * Build Json for all product Groups and Families
     * @return void
     */
    public function productGroupsJsonAction()
    {
        $this->loadAndAssignProductMainGroups();
        $this->loadAndAssignProductFamilies();
        $this->loadAndAssignSelectedInquiry();
        $this->view->assign('currentContactAddressJson', json_encode($this->loadContactAddressData()));
        $this->view->assign(
            'productFinderPageUid',
            $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_baumerproductfinder.']['settings.']['productfinderUid']
        );
    }


    /*
     * ============================================================
     *   Ajax actions
     * ============================================================
     */

    /**
     * Ajax find a product list by code
     *
     * @param string $code
     * @return void
     */
    public function findProductListByCodeAction($code)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        /** @var QueryResultInterface $productLists */
        $productLists = $this->productListRepository->findByCode($code);
        if ($productLists->current() instanceof ProductList) {
            $productList = $productLists->current();
            header('Content-Type: application/json');
            $now = new \DateTime;
            $validityInterval = $this->settings['productListCodeValidityPeriod'] * 86400;
            if ($productList->getCrdate()->getTimestamp() > ($now->getTimestamp() - $validityInterval)) {
                echo json_encode([
                    'success' => true,
                    'productList' => $productList->toArray()
                ]);
            } else {
                $this->throwStatus(404, 'Product list with ID ' . $code . ' has expired.');
            }
            exit;
        } else {
            $this->throwStatus(404, 'Product list with ID ' . $code . ' could not be found.');
        }
    }

    /**
     * Ajax operation to delete a product list
     * outputs JSON containing success and updated containers with parameters
     *
     * @param int $productListId
     * @return void
     */
    public function deleteAction($productListId)
    {
        $productList = $this->productListRepository->findByUid($productListId);
        if ($productList->getFeUser()->getUid() === $this->feUser->getUid()) {
            $this->productListRepository->remove($productList);
            $this->persistenceManager->persistAll();
            header('Content-Type: application/json');
            echo json_encode([
                'success' => true,
            ]);
            exit;
        } else {
            $this->throwStatus(403, 'You do not have permission to edit this product list.');
        }
    }


    /**
     * initialize saveForSharingAction
     * Allow creation of product list
     * @return void
     */
    protected function initializeSaveForSharingAction()
    {
        $propertyMappingConfiguration = $this->arguments->getArgument('productList')->getPropertyMappingConfiguration();
        $propertyMappingConfiguration->allowProperties('name', 'description', 'feUser');
        $propertyMappingConfiguration->setTypeConverterOption(
            'TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter',
            PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED, false
        );
        $propertyMappingConfiguration->setTypeConverterOption(
            'TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter',
            PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true
        );
    }
    /**
     * Save a new product list and return it and its code URL
     *
     * The share product list action save a
     * product list completely as new and returns its to code for access.
     * Product lists which have a code are a "snapshot"
     * of a product list and should never change.
     * The existence of a code of the product list marks it as immutable.
     *
     * @param ProductList $productList
     * @return void
     */
    public function saveForSharingAction(ProductList $productList)
    {
        $lineItems = [];
        // lineItem cannot be mapped, because an array is to exotic for Extbase
        if ($this->request->hasArgument('lineItems')) {
            /** @var array $lineItems */
            $lineItems = $this->request->getArgument('lineItems');
        } else {
            $this->throwStatus(400, 'arguments[lineItems] was missing in request (1421834321)');
        }

        // first persist the product list to get an uid
        $productList->setPid((int)$this->settings['productListStoragePid']);
        $productList->generateCode();
        $this->productListRepository->add($productList);
        $this->persistenceManager->persistAll();

        // now save and add the lineItem
        foreach ($lineItems as $lineItemData) {
            $lineItemData['product_list'] = $productList->getUid();
            $lineItemData['pid'] = (int)$this->settings['productListStoragePid'];
            $lineItem = $this->lineItemRepository->createAndSaveFromArray($lineItemData, $productList);
            if ($lineItem instanceof LineItem) {
                $productList->addLineItem($lineItem);
            } else {
                $this->throwStatus(500, 'Saving a line item of the product list failed (1421834314)');
            }
        }

        // now persist the product list again, to have proper index
        // and object cache (Horray for extbase!)
        $this->productListRepository->add($productList);
        $this->persistenceManager->persistAll();

        header('Content-Type: application/json');
        echo json_encode([
            'success' => true,
            'productListLinkUrl' => $productList->getLinkUrl(true),
            'productListCode' => $productList->getCode()
        ]);

        exit;
    }

    /**
     * Save/update a product list and set the current frontend user
     * @return void
     */
    public function saveAction()
    {

        // We process the product list manually,
        // as extbase is not cooperative with ajax request
        if ($this->request->hasArgument('productList')) {
            /** @var array $lineItems */
            $productListParams = $this->request->getArgument('productList');
            if ($productListParams['uid'] > 0 &&
                $productListParams['update'] == 'true' &&
                $this->feUser->getUid() == $productListParams['feUser']) {
                $productList = $this->productListRepository->findByIdentifier((int)$productListParams['uid']);
                $new = false;
                if (!$productList instanceof ProductList) {
                    $this->throwStatus(400, 'product list with uid ' . $productListParams['uid'] . ' does not exist (1423065872)');
                    exit;
                }
            } else {
                // let's make a new product list
                $productList = new ProductList();
                $new = true;
            }

            // set values
            $productList->generateCode();
            $productList->setFeUser($this->feUser);
            $productList->setName($productListParams['name']);
            $productList->setDescription($productListParams['description']);
            $productList->setPid((int)$this->settings['productListStoragePid']);
            if ($new) {
                $productList->setCrdate(new \DateTime());
            }
        } else {
            $this->throwStatus(400, 'productList argument was missing in request (1423059697)');
            exit;
        }
        if ($new) {
            $this->productListRepository->add($productList);
        } else {
            $this->productListRepository->update($productList);
        }
        $this->persistenceManager->persistAll();

        // now save/update and add the lineItemss
        $lineItems = $productListParams['lineItems'];
        if (count($lineItems) == 0) {
            $this->throwStatus(400, 'lineItems were missing in request (1423059697)');
        }
        if ($productList->getLineItems() instanceof ObjectStorage) {
            foreach ($productList->getLineItems() as $oldLineItem) {
                $this->persistenceManager->remove($oldLineItem);
                $productList->removeLineItem($oldLineItem);
            }
            $this->persistenceManager->persistAll();
        }
        foreach ($lineItems as $lineItemData) {
            if ($new) {
                unset($lineItemData['uid']);
            }
            $lineItemData['product_list'] = $productList->getUid();
            $lineItemData['pid'] = (int)$this->settings['productListStoragePid'];
            $lineItem = $this->lineItemRepository->createAndSaveFromArray($lineItemData, $productList);
            if ($lineItem instanceof LineItem) {
                $productList->addLineItem($lineItem);
            } else {
                $this->throwStatus(500, 'Saving a line item of the product list failed (1421834314)');
            }
        }

        // now persist the product list again,
        // to have proper index and object cache (Horray for extbase!)
        $this->persistenceManager->update($productList);
        $this->persistenceManager->persistAll();
        header('Content-Type: application/json');
        echo json_encode([
            'success' => true,
            'productList' => $productList->toArray()
        ]);

        exit;
    }

    /*
    * ============================================================
    *   Private methods
    * ============================================================
    */

    /**
     * Load and assign JSON from product main groups
     * @return void
     */
    private function loadAndAssignProductMainGroups()
    {
        $parentUid = $this->settings['productfinder']['productMainGroupUid'];
        $currentCategoryUid = 0;

        $mainGroupsArray = [];
        /** @noinspection PhpUndefinedMethodInspection */
        $mainGroups = $this->categoryRepository->findByParent((int)$parentUid);

        $pageId = $GLOBALS['TSFE']->id;

        $res = $this->db->exec_SELECTquery(
            'sys_category.uid',
            'sys_category INNER JOIN sys_category_record_mm ON sys_category.uid = sys_category_record_mm.uid_local',
            'sys_category_record_mm.uid_foreign = ' . $pageId . ' AND sys_category.parent = ' . $parentUid);
        while (($row = $this->db->sql_fetch_assoc($res))) {
            if ($row['uid']) {
                $currentCategoryUid = (int)$row['uid'];
                break;
            }
        }

        if (!$currentCategoryUid && $this->request->hasArgument('mainGroup')) {
            $currentCategoryUid = $this->request->getArgument('mainGroup');
        }

        /** @var Category $mainGroup */
        foreach ($mainGroups as $mainGroup) {
            $mainGroupArray = $mainGroup->toArray(false);
            $mainGroupArray['contactAddress'] = $this->loadContactAddressData($mainGroup->getUid());
            if ($mainGroup->getUid() == $currentCategoryUid) {
                $mainGroupArray['current'] = true;
            }
            $mainGroupsArray[] = $mainGroupArray;
        }

        $this->view->assign('currentMainGroup', json_encode($currentCategoryUid));
        $this->view->assign('productMainGroupsJson', json_encode($mainGroupsArray));
    }

    /**
     * Load and assign JSON from product families
     * @return void
     */
    private function loadAndAssignProductFamilies()
    {
        //		$families = $this->productFamilyRepository->findAll();
//		$familiesArray = array();
//		/** @var ProductFamily $family */
//		foreach ($families as $family) {
//			$familiesArray[] = $family->toArray(TRUE);
//		}
        //TODO: adapt to new CS view logic
        $familiesArray = [];
        $this->view->assign('productFamiliesJson', json_encode($familiesArray));
    }

    /**
     * @return void
     */
    private function loadAndAssignSelectedInquiry()
    {
        $categoryMapping = $this->settings['contactForm']['categoryMapping'];
        $defaultUid = $this->settings['contactForm']['defaultUid'];
        $pageId = $GLOBALS['TSFE']->id;

        $res = $this->db->exec_SELECTquery('sys_category.uid',
            'sys_category INNER JOIN sys_category_record_mm ON sys_category.uid = sys_category_record_mm.uid_local',
            'sys_category_record_mm.tablenames = \'pages\' AND sys_category_record_mm.uid_foreign = ' . $pageId);
        while (($row = $this->db->sql_fetch_assoc($res))) {
            if (!empty($categoryMapping[$row['uid']])) {
                $this->view->assign('formSelectedInquiryJson',
                    json_encode(['selectedInquiry' => $categoryMapping[$row['uid']]]));
                return;
            }
        }
        $this->view->assign('formSelectedInquiryJson', json_encode(['selectedInquiry' => $categoryMapping[$defaultUid]]));
    }
}
