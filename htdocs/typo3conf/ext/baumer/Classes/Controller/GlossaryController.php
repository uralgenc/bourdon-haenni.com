<?php
namespace Baumer\Baumer\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use Baumer\Baumer\Domain\Model\Page;

/**
 * Class GlossaryController
 *
 * @package Baumer\Baumer\Controller
 */
class GlossaryController extends ActionController
{

    /**
     * @var \Baumer\Baumer\Domain\Repository\PageRepository
     * @inject
     */
    protected $pageRepository;

    /**
     * list action
     * @return void
     */
    public function listAction()
    {
        $categories = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $this->settings['categories'], true);
        $categoryConjunction = $this->settings['categoryConjunction'];
        /** @var QueryResultInterface $glossaryPages */
        $glossaryPages = $this->pageRepository->findByCategories($categories, $categoryConjunction);
        $this->view->assign('glossaryEntries', $glossaryPages);
    }

    /**
     * @param \Baumer\Baumer\Domain\Model\Page $page
     * @return void
     */
    public function showAction(Page $page)
    {
        $this->view->assign('page', $page);
    }
}
