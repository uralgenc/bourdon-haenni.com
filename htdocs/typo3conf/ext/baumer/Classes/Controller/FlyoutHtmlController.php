<?php
namespace Baumer\Baumer\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <hans.hoechtl@typovision.de>, typovision GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Error\Http\ServiceUnavailableException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;

/**
 * FlyoutHtmlController
 */
class FlyoutHtmlController extends ActionController
{

    /**
     * Initialize Query object with configured parameters
     *
     * @param string $q
     *
     * @return \Tx_Solr_Query
     */
    protected function initQuery($q)
    {
        $suggestConf = $this->settings['flyout'];
        $solrConfiguration = \Tx_Solr_Util::getSolrConfigurationFromPageId($GLOBALS['TSFE']->id, false, $GLOBALS['TSFE']->sys_language_uid);
        /** @var \Tx_Solr_Query $query */
        $query = GeneralUtility::makeInstance('Tx_Solr_Query', '*' . $q . '* OR ' . $q);

        /*
         * Add configured filters
         */
        if (is_array($suggestConf['filter']) && !empty($suggestConf['filter'])) {
            foreach ($suggestConf['filter'] as $filter) {
                $query->addFilter($filter);
            }
        }

        /*
         * Group by configured field
         */
        if (!empty($suggestConf['groupField'])) {
            $query->setGrouping(true);
            $query->addGroupField($suggestConf['groupField']);
            $query->setNumberOfResultsPerGroup($suggestConf['resultsPerGroup']);
        } else {
            $query->setResultsPerPage($suggestConf['resultsPerPage']);
        }

        /*
         * Add default parameters
         */
        $query->setUserAccessGroups(explode(',', $GLOBALS['TSFE']->gr_list));
        $site = \Tx_Solr_Site::getSiteByPageId($GLOBALS['TSFE']->id);
        $query->setSiteHashFilter($site->getDomain());
        $query->setSpellchecking(true);

        $query->setFieldList($suggestConf['returnFieldsQuery']);
        $query->setHighlighting(true);

        /*
         * Add Boost Queries
         */
        if (!empty($solrConfiguration['search.']['query.']['boostFunction'])) {
            $query->setBoostFunction($solrConfiguration['search.']['query.']['boostFunction']);
        }

        if (!empty($solrConfiguration['search.']['query.']['boostQuery'])) {
            $query->setBoostQuery($solrConfiguration['search.']['query.']['boostQuery']);
        }

        if (!empty($solrConfiguration['search.']['query.']['boostQuery.'])) {
            $boostQueries       = [];
            $boostConfiguration = $solrConfiguration['search.']['query.']['boostQuery.'];

            foreach ($boostConfiguration as $boostQuery) {
                $boostQueries[] = $boostQuery;
            }

            $query->setBoostQuery($boostQueries);
        }

        return $query;
    }

    /**
     * action htmlFlyout
     *
     * @return string
     */
    public function htmlFlyoutAction()
    {

        /** @var \Tx_Solr_ConnectionManager $solrConnection */
        $solrConnection = GeneralUtility::makeInstance('Tx_Solr_ConnectionManager')->
                            getConnectionByPageId($GLOBALS['TSFE']->id, $GLOBALS['TSFE']->sys_language_uid);
        /** @var \Tx_Solr_Search $search */
        $search = GeneralUtility::makeInstance('Tx_Solr_Search', $solrConnection);

        try {
            $q = strtolower(trim($this->request->getArgument('q')));

            // Empty paramter is not allowed
            if (empty($q)) {
                throw new NoSuchArgumentException('Empty query provided, please check your query parameter');
            }
            //Check for availability
            if (!$search->ping()) {
                throw new ServiceUnavailableException('Search server not available');
            }

            $query = $this->initQuery($q);
            // $query->setResultsPerPage(6);
            $this->view->assign('q', $q);

            $flyoutResults = json_decode($search->search($query, 0, $this->settings['flyout']['resultsPerPage'])->getRawResponse());
            $this->view->assign('status', 'ok');
            $this->view->assign('flyoutResults', $flyoutResults->response->docs);

            $query->setQueryString('*:*');
            $query->addQueryParameter('facet.prefix', $q);
            $query->setFaceting(true);
            $query->addFacetField($this->settings['flyout']['autocompleteField']);
            $query->addQueryParameter('facet.limit', $this->settings['flyout']['autocompleteLimit']);
            $autocompleteResults = json_decode($search->search($query, 0, $this->settings['flyout']['resultsPerPage'])->getRawResponse());
            $this->view->assign('autocompleteResults', get_object_vars($autocompleteResults->facet_counts->facet_fields->{$this->settings['flyout']['autocompleteField']}));
        } catch (NoSuchArgumentException $e) {
            $this->view->assign('status', 'error');
            $this->view->assign('errorMsg', $e->getMessage());
        } catch (ServiceUnavailableException $e) {
            $this->view->assign('status', 'error');
            $this->view->assign('errorMsg', $e->getMessage());
        }
    }
}
