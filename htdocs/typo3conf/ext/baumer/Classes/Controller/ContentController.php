<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Controller;

use Baumer\Baumer\Domain\Model\CsView;

class ContentController extends \FluidTYPO3\Fluidcontent\Controller\ContentController
{

    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRecordRepository
     * @inject
     */
    protected $csViewRecordRepository;
    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRepository
     * @inject
     */
    protected $csViewRepository;
    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Connection
     * @inject
     */
    protected $elasticConnection;

    /**
     * CE: Views/Configurator
     * Find the CsViewRecord that belongs to a configurator view by the
     * configured contentServId. Renders the Encoway Configurator Controller.
     * @return void
     */
    public function configuratorAction()
    {
        $data = $this->getData();
        $csViewRecord = null;
        if (isset($data['contentServId']) && !empty($data['contentServId'])) {
            $csViewRecord = $this->csViewRecordRepository->findOneByContentServId($data['contentServId']);
        }
        $this->view->assign('csViewRecord', $csViewRecord);
    }

    /**
     * CE: Views/Selector
     * Find the CsView that belongs to a selector view by the
     * configured contentServId. Renders the Encoway Configurator Controller.
     * @return void
     */
    public function selectorAction()
    {
        $data = $this->getData();
        $csView = null;
        if (isset($data['contentServId'])) {
            $csView = $this->csViewRepository->findSelectorByContentServId($data['contentServId']);
        }
        $this->view->assign('csView', $csView);
    }

    /**
     * List all available selectors
     */
    public function listSelectorsAction()
    {
        $selectors = $this->csViewRepository->findByClass(CsView::CLASS_SELECTOR);
        $this->view->assign('selectors', $selectors);
    }

    /**
     * Download Center
     */
    public function downloadCenterAction()
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $currentContentElement = $this->getRecord();
        $categoryUids = array_map(
            function ($row) {
                return $row['uid_local'];
            },
            $db->exec_SELECTgetRows(
                'uid_local',
                'sys_category_record_mm',
                'tablenames = "tt_content" AND fieldname = "downloadcenter_categories" AND uid_foreign = ' . $currentContentElement['uid'])
        );

        $this->view->assign('categories', $categoryUids);
        $this->view->assign('selectorConfig', json_encode($this->elasticConnection->getElasticConfig('FILE')));
        $this->view->assign('csAttributes', json_encode($this->settings['downloadcenter']));
    }

    /**
     * Distributor Map
     */
    public function distributorMapAction()
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $currentContentElement = $this->getRecord();
        $categoryUids = array_map(
            function ($row) {
                return $row['uid_local'];
            },
            $db->exec_SELECTgetRows(
                'uid_local',
                'sys_category_record_mm',
                'tablenames = "tt_content" AND fieldname = "distributor-map_categories" AND uid_foreign = ' . $currentContentElement['uid'])
        );

        $this->view->assign('categories', $categoryUids);
    }
}
