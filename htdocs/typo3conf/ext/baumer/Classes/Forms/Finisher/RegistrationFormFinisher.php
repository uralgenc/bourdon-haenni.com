<?php
namespace Baumer\Baumer\Forms\Finisher;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Martin Heigermoser <martin.heigermoser@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use Baumer\Baumer\Service\EvalancheService;

/**
 * Class RegistrationFormFinisher
 *
 * @author Martin Heigermoser <martin.heigermoser@typovision.de>
 *
 * @package Baumer\Baumer\Forms\Finisher
 */
class RegistrationFormFinisher extends AbstractFinisher
{

    /**
     * Write form data to Evalanche
     *
     * @return array
     */
    public function process()
    {
        $poolId = (int) $this->settings['poolId'];
        $email = $this->gp['email'];
        $newsletter = $this->gp['newsletter'];
        $this->evalancheService = new EvalancheService($this->settings);
        $profile = $this->evalancheService->checkProfileStatus($email, $poolId, $this->settings['sourceOptionId']);

        // Set permission to 1 to start double opt-in
        if ($newsletter == 1) {
            $this->settings['defaultPermission'] = 1;
        }

        $fields = [
            'EMAIL' => $email,
            'PERMISSION' => $this->settings['defaultPermission'],
            'SALUTATION' => $this->getEvalancheGenderIDByFEUsersGenderID($this->gp['salutation']),
            'FIRSTNAME' => $this->gp['firstname'],
            'NAME' => $this->gp['lastname'],
            'COMPANY' => $this->gp['company'],
            'ADDRESS' => $this->gp['address'],
            'ZIPCODE' => $this->gp['postal_code'],
            'CITY' => $this->gp['city'],
            'COUNTRY' => $this->gp['country'],
            'PHONENUMBER' => $this->gp['telephone'],
            'SOURCE' => $profile['source'],
            'KONTAKTQUELLE' => $profile['contact_source'],
        ];

        $feUser = $this->generateToken($fields, $email);

        switch ($profile['status']) {
            case 'newProfile':
            case 'existingProfileNoNewsletter':
                break;
            case 'registeredProfileApprovedNewsletter':
                unset($fields['PERMISSION']);
                break;
            default:
                $fields = false;
                break;
        }
        $this->updateFeUser($feUser, $fields, $profile);

        $this->evalancheService->updateProfile($fields, $email, $poolId);

        return $this->gp;
    }
}
