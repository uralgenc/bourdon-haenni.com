<?php
namespace Baumer\Baumer\Forms\Finisher;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/
use TYPO3\CMS\Core\Type\Exception\InvalidEnumerationValueException;

/**
 * Class ContactFormFinisher
 *
 * @package Baumer\Baumer\Forms\Finisher
 */

abstract class AbstractFinisher extends \Typoheads\Formhandler\Finisher\AbstractFinisher
{

    /**
     * @var \Baumer\Baumer\Service\EvalancheService
     */
    protected $evalancheService;

    /**
     * @param array $fields
     * @param string $identifier
     *
     * @return array|NULL
     */
    protected function generateToken(&$fields, $identifier)
    {
        $feUser = $this->checkIfUserExists($identifier);
        if ($feUser != null) {
            if (strlen($feUser['token']) == 0) {
                $token = md5($identifier . $feUser['crdate']);
                $fields['TOKEN'] = $token;

                /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $databaseConnection */
                $databaseConnection = $GLOBALS['TYPO3_DB'];
                $databaseConnection->exec_UPDATEquery('fe_users', 'uid = ' . $feUser['uid'], ['token' => $token]);
            }
        }

        return $feUser;
    }

    /**
     * @param string $email
     *
     * @return array|NULL
     */
    protected function checkIfUserExists($email)
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $databaseConnection */
        $databaseConnection = $GLOBALS['TYPO3_DB'];
        $feUser = $databaseConnection->exec_SELECTgetRows(
            'uid, email',
            'fe_users',
            'email = ' . $databaseConnection->fullQuoteStr($email, 'fe_users') . ' AND deleted = 0'
        );

        return (count($feUser) == 0) ? null : $feUser[0];
    }

    /**
     * Prepend date/time to inquiry text to build up a history of messages in Evalanche
     *
     * @param string $inquiryText
     *
     * @return string
     */
    protected function buildInquiryText($inquiryText)
    {
        $text = '### ' . date('d.m.Y - H:i', time()) . " ### \n";
        $text .= trim($inquiryText);

        return $text;
    }

    /**
     * Concatenate previous inquiry texts
     *
     * @param array $profile
     *
     * @return string
     */
    protected function buildInquiryHistoryText(array $profile)
    {
        return $profile['INQUIRYTEXT'] . "\n\n" . $profile['INQUIRYHISTORY'];
    }

    /**
     * Update fe_user with data from evalanche
     *
     * @param array $feUser
     * @param array $fields
     * @param array $profile
     */
    protected function updateFeUser($feUser, &$fields, $profile)
    {
        $fields['EXTERNALID'] = $feUser['uid'];

        $updateFields = [
            'status' => isset($fields['PERMISSION']) ? $fields['PERMISSION'] : $profile['PERMISSION'],
        ];

        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $databaseConnection */
        $databaseConnection = $GLOBALS['TYPO3_DB'];
        $databaseConnection->exec_UPDATEquery('fe_users', 'uid = ' . $feUser['uid'], $updateFields);
    }

    /**
     * @param integer $genderID
     * @return integer
     * @throws InvalidEnumerationValueException if given $genderID is not known
     */
    protected function getEvalancheGenderIDByFEUsersGenderID($genderID)
    {
        $genderID = intval($genderID);
        $mapping = [
            99 => 0, // undefined/other
            0 => 2, // male
            1 => 1, // female
        ];
        if (!isset($mapping[$genderID])) {
            throw new InvalidEnumerationValueException('Gender ID ' . $genderID . ' not known', 1411732378);
        }
        return $mapping[$genderID];
    }
}
