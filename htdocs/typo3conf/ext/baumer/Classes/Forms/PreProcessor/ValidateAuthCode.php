<?php
namespace Baumer\Baumer\Forms\PreProcessor;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use Baumer\Baumer\Service\EvalancheService;

/**
 * Class ValidateAuthCode
 *
 * @package Baumer\Baumer\Forms\PreProcessor
 */
class ValidateAuthCode extends \Typoheads\Formhandler\PreProcessor\ValidateAuthCode
{

    /**
     * @var \Baumer\Baumer\Service\EvalancheService
     */
    private $evalancheService;

    /**
     * The main method called by the controller
     **/
    public function process()
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $databaseConnection */
        $databaseConnection = $GLOBALS['TYPO3_DB'];
        if (strlen(trim($this->gp['authCode'])) > 0) {
            try {
                $authCode = trim($this->gp['authCode']);
                $table = trim($this->gp['table']);
                if ($this->settings['table']) {
                    $table = $this->utilityFuncs->getSingle($this->settings, 'table');
                }
                $uidField = trim($this->gp['uidField']);
                if ($this->settings['uidField']) {
                    $uidField = $this->utilityFuncs->getSingle($this->settings, 'uidField');
                }
                if (strlen($uidField) === 0) {
                    $uidField = 'uid';
                }
                $uid = trim($this->gp['uid']);

                if (!(strlen($table) > 0 && strlen($uid) > 0)) {
                    $this->utilityFuncs->throwException('validateauthcode_insufficient_params');
                }

                $uid = $databaseConnection->fullQuoteStr($uid, $table);

                // Check if table is valid
                $existingTables = array_keys($databaseConnection->admin_get_tables());
                if (!in_array($table, $existingTables)) {
                    $this->utilityFuncs->throwException('validateauthcode_insufficient_params');
                }

                // Check if uidField is valid
                $existingFields = array_keys($databaseConnection->admin_get_fields($table));
                if (!in_array($uidField, $existingFields)) {
                    $this->utilityFuncs->throwException('validateauthcode_insufficient_params');
                }

                $hiddenField = 'disable';
                if ($this->settings['hiddenField']) {
                    $hiddenField = $this->utilityFuncs->getSingle($this->settings, 'hiddenField');
                } elseif ($GLOBALS['TCA'][$table]['ctrl']['enablecolumns']['disable']) {
                    $hiddenField = $GLOBALS['TCA'][$table]['ctrl']['enablecolumns']['disable'];
                }
                $selectFields = '*';
                if ($this->settings['selectFields']) {
                    $selectFields = $this->utilityFuncs->getSingle($this->settings, 'selectFields');
                }
                $hiddenStatusValue = 1;
                if (isset($this->settings['hiddenStatusValue'])) {
                    $hiddenStatusValue = $this->utilityFuncs->getSingle($this->settings, 'hiddenStatusValue');
                }
                $hiddenStatusValue = $databaseConnection->fullQuoteStr($hiddenStatusValue, $table);
                $enableFieldsWhere = '';
                if (intval($this->utilityFuncs->getSingle($this->settings, 'showDeleted')) !== 1) {
                    $enableFieldsWhere = $this->cObj->enableFields($table, 1);
                }
                $query = $databaseConnection->SELECTquery($selectFields, $table, $uidField . '=' . $uid . ' AND ' . $hiddenField . '=' . $hiddenStatusValue . $enableFieldsWhere);
                $this->utilityFuncs->debugMessage('sql_request', [$query]);
                $res = $databaseConnection->sql_query($query);
                if ($databaseConnection->sql_error()) {
                    $this->utilityFuncs->debugMessage('error', [$databaseConnection->sql_error()], 3);
                }
                if (!$res || $databaseConnection->sql_num_rows($res) === 0) {
                    $this->utilityFuncs->throwException('validateauthcode_no_record_found');
                }

                $row = $databaseConnection->sql_fetch_assoc($res);
                $databaseConnection->sql_free_result($res);
                $this->utilityFuncs->debugMessage('Selected row: ', [], 1, $row);

                $localAuthCode = \TYPO3\CMS\Core\Utility\GeneralUtility::hmac(serialize($row), 'formhandler');

                $this->utilityFuncs->debugMessage('Comparing auth codes: ', [], 1, ['Calculated:' => $localAuthCode, 'Given:' => $authCode]);
                if ($localAuthCode !== $authCode) {
                    $this->utilityFuncs->throwException('validateauthcode_invalid_auth_code');
                }
                $activeStatusValue = 0;
                if (isset($this->settings['activeStatusValue'])) {
                    $activeStatusValue = $this->utilityFuncs->getSingle($this->settings, 'activeStatusValue');
                }
                $res = $databaseConnection->exec_UPDATEquery($table, $uidField . '=' . $uid, [$hiddenField => $activeStatusValue]);

                // report to evalanche that email is valid
                if ($row['newsletter'] == 1) {
                    $this->evalancheService = new EvalancheService($this->settings['evalancheConfig.']);
                    $poolId = (int) $this->settings['evalancheConfig.']['poolId'];
                    $email = $this->gp['email'];
                    $fields = [
                        'PERMISSION' => $this->settings['evalancheConfig.']['defaultPermission'],
                    ];
                    $this->evalancheService->updateProfile($fields, $email, $poolId);
                }

                if (!$res) {
                    $this->utilityFuncs->throwException('validateauthcode_update_failed');
                }

                $this->utilityFuncs->doRedirectBasedOnSettings($this->settings, $this->gp);
            } catch (\Exception $e) {
                $redirectPage = $this->utilityFuncs->getSingle($this->settings, 'errorRedirectPage');
                if ($redirectPage) {
                    $this->utilityFuncs->doRedirectBasedOnSettings($this->settings, $this->gp, 'errorRedirectPage');
                } else {
                    throw new \Exception($e->getMessage());
                }
            }
        }

        return $this->gp;
    }
}
