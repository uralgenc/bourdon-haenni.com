<?php
namespace Baumer\Baumer\Forms\PreProcessor;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Baumer\Baumer\Service\EvalancheService;

/**
 * Class LoadProfile
 *
 * @package Baumer\Baumer\Forms\PreProcessor
 **/

class LoadProfile extends \Typoheads\Formhandler\PreProcessor\LoadDB
{

    /**
     * @var \Baumer\Baumer\Service\EvalancheService
     */
    private $evalancheService;

    /**
     * Main method called by the controller
     *
     * @return array
     */
    public function process()
    {
        $this->data = $this->loadDB($this->settings['select.']);

        foreach ($this->settings as $step => $stepSettings) {
            $step = preg_replace('/\.$/', '', $step);

            if ($step !== 'select') {
                if (intval($step) === 1) {
                    $this->loadDBToGP($stepSettings);
                } elseif (is_numeric($step)) {
                    $this->loadDBToSession($stepSettings, $step);
                }
            }
        }
        // Load additional data from Evalanche
        $this->evalancheService = new EvalancheService($this->settings['evalancheConfig.']);
        $poolId = (int) $this->settings['evalancheConfig.']['poolId'];
        if (strlen($this->gp['email']) > 0) {
            // User data loaded via token
            $email = $this->gp['email'];
        } elseif ($GLOBALS['TSFE']->fe_user->user != null && isset($GLOBALS['TSFE']->fe_user->user['uid'])) {
            // Check if fe_user is logged in
            $email = $GLOBALS['TSFE']->fe_user->user['email'];
        } else {
            // Redirect if no user was found
            $url = $this->cObj->typoLink_URL([
                'parameter' => $this->settings['redirectOnError'],
                'additionalParams.' => [
                    'data' => 'TSFE:sys_language_uid',
                    'wrap' => '&L=|'
                ]
            ]);
            \TYPO3\CMS\Core\Utility\HttpUtility::redirect($url);
        }

        $profile = $this->evalancheService->checkProfileStatus($email, $poolId);
        $this->gp['topic'] = GeneralUtility::trimExplode('|', $profile['profile']['RAW_PRODUCT_GROUP'], true);

        return $this->gp;
    }
}
