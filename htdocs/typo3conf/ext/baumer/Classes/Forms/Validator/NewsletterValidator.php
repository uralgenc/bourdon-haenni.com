<?php
namespace Baumer\Baumer\Forms\Validator;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

/**
 * Class NewsletterValidator
 *
 * @package Baumer\Baumer\Forms\Validator
 */
class NewsletterValidator extends \Typoheads\Formhandler\Validator\DefaultValidator
{

    /**
     * Validate the submitted values using given settings
     *
     * @param array &$errors Reference to the errors array to store the errors occurred
     *
     * @return boolean
     */
    public function validate(&$errors)
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $databaseConnection */
        $databaseConnection = $GLOBALS['TYPO3_DB'];
        $feUser = $databaseConnection->exec_SELECTgetRows(
            'uid, email',
            'fe_users',
            'email = ' . $databaseConnection->fullQuoteStr($this->gp['email'], 'fe_users') . ' AND newsletter=1 AND deleted = 0'
        );

        if (count($feUser) == 1) {
            $errors['email'] = 'already_used';
        }

        return empty($errors);
    }
}
