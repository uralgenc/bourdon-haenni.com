<?php
namespace Baumer\Baumer\Forms\Helper;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FormhandlerUserFunc
 *
 * @package Baumer\Baumer\Forms\Helper
 */
class FormhandlerUserFunc
{

    /**
     * Check if user is already registered
     *
     * @param string $value
     * @param array $conf
     *
     * @return integer
     */
    public function checkNewsletterUser($value, $conf)
    {
        $feUser = $this->getFeUser($conf['value']);
        $result = (count($feUser) == 0) ? 1 : 0;

        return $result;
    }

    /**
     * Check if user is an active user
     *
     * @param string $value
     * @param array $conf
     *
     * @return integer
     */
    public function checkIfUserActive($value, $conf)
    {
        $feUser = $this->getFeUser($conf['value']);
        $result = (count($feUser) > 0 && $feUser[0]['disable'] == 0) ? 0 : 1;

        return $result;
    }

    /**
     * @param $email
     *
     * @return array|NULL
     */
    protected function getFeUser($email)
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $databaseConnection */
        $databaseConnection = $GLOBALS['TYPO3_DB'];
        $feUser = $databaseConnection->exec_SELECTgetRows(
            'uid, email, disable',
            'fe_users',
            'email = ' . $databaseConnection->fullQuoteStr($email, 'fe_users') . ' AND deleted = 0'
        );

        return $feUser;
    }

    public function getProductsTable($value, $conf)
    {
        $formData = GeneralUtility::_GP('formDataContact');
        if (empty($formData)) {
            $formData = GeneralUtility::_GP('formDataInquiry');
        }

        if (is_array($formData)) {
            if ($conf['html']) {
                $html = '';
                if (array_key_exists('products', $formData) && is_array($formData['products'])) {
                    foreach ($formData['products'] as $product) {
                        $html .= '<tr class="panel">';
                        $html .= '<td>';
                        $html .= '<p><small>'.$product['mainGroup'].'</small></p>';
                        $html .= '<p>' . $product['productCode'] . '</p>';
                        $html .= '</td>';
                        $html .= '<td>';
                        $html .= (strlen($product['note']) > 0 ? nl2br($product['note']) : '');
                        $html .= '</td>';
                        $html .= '<td>';
                        $html .= $product['amount'];
                        $html .= '</td>';
                        $html .= '<td>';
                        $html .= ($product['price'] > 0 ? '€ ' . number_format(floatval($product['price']), 2) : 'On request');
                        $html .= '</td>';
                        $html .= '<td>';
                        $html .= ($product['shipping'] > 0 ? $product['shipping'] . ' days' : 'On request');
                        $html .= '</td>';
                        $html .= '</tr>';
                    }
                }

                return $html;
            } else {
                $text = '';
                if (array_key_exists('products', $formData) && is_array($formData['products'])) {
                    foreach ($formData['products'] as $product) {
                        $text .= 'Main Group: ' . $product['mainGroup'] . "\n";
                        $text .= 'Product Code: ' . $product['productCode'] . "\n";
                        $text .= 'Note: ' . (strlen($product['note']) > 0 ? nl2br($product['note']) : '') . "\n";
                        $text .= 'Amount: ' . $product['amount'] . "\n";
                        $text .= 'Price: ' . ($product['price'] > 0 ? '€ ' . number_format(floatval($product['price']), 2) : 'On request') . "\n";
                        $text .= 'Shipping: ' . ($product['shipping'] > 0 ? $product['shipping'] . ' days': 'On request') . "\n";
                        $text .= "\n";
                    }
                }

                return $text;
            }
        }

        return '';
    }

    public function getFormUid()
    {
        return $this->cObj->data['uid'];
    }
}
