<?php
namespace Baumer\Baumer\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions GmbH & Co. KG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Baumer\Baumer\Domain\Model\CsView;
use Baumer\Baumer\Domain\Model\CsViewRecord;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class CsViewRecordRepository
 * @method CsViewRecord findOneByContentServId($contentServId)
 * @method QueryResultInterface findByContentServId($contentServIds)
 * @package Baumer\Baumer\Domain\Repository
 */
class CsViewRecordRepository extends Repository
{

    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Set Query defaults
     */
    public function initializeObject()
    {
        /** @var $defaultQuerySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $defaultQuerySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $defaultQuerySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($defaultQuerySettings);
    }

    /**
     * Find by a given list of contentServIds
     *
     * @param array $contentServIds
     * @return array|QueryResultInterface
     */
    public function findByContentServIds($contentServIds)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->in('contentServId', $contentServIds)
        );

        return $query->execute();
    }

    /**
     * Delete CsViews that are not part of $validCsViews
     *
     * @param array $validContentServIds
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteOrphansComparingContentServIds($validContentServIds)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->logicalNot(
                    $query->in('contentServId', $validContentServIds)
                )
            )
        );
        /** @var CsViewRecord $poorOrphanViewRecord */
        foreach ($query->execute() as $poorOrphanViewRecord) {
            $this->remove($poorOrphanViewRecord);
        }
    }
}
