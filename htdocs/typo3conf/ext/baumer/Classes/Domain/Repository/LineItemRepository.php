<?php
namespace Baumer\Baumer\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin R. Krause <martin.r.krause@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use SebastianBergmann\Diff\Line;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\Repository;
use Baumer\Baumer\Domain\Model\EncowayConfiguration;
use Baumer\Baumer\Domain\Model\LineItem;

/**
 * The repository for LineItem
 *
 * @author Martin R. Krause <martin.r.krause@typovision.de>
 */
class LineItemRepository extends Repository
{

    /**
     * configurationRepository
     *
     * @var \Baumer\Baumer\Domain\Repository\EncowayConfigurationRepository
     */
    protected $encowayConfigurationRepository = null;

    /**
     * Set Query defaults
     * @return void
     */
    public function initializeObject()
    {
        /** @var $defaultQuerySettings Typo3QuerySettings */
        $defaultQuerySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $defaultQuerySettings->setRespectStoragePage(false);
        $defaultQuerySettings->setRespectSysLanguage(false);
        $this->setDefaultQuerySettings($defaultQuerySettings);
        $encowayConfigurationRepositoryClass = 'Baumer\\Baumer\\Domain\\Repository\\EncowayConfigurationRepository';
        $this->encowayConfigurationRepository = $this->objectManager->get($encowayConfigurationRepositoryClass);
    }

    /**
     * Create a new line item for a "classical" value array
     *
     * @param array $valueArray
     * @return bool|LineItem
     */
    public function createAndSaveFromArray(array $valueArray)
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $attributesConf = [
            'required' => ['product_list', 'amount', 'sorting', 'pid'],
            'allowed'  => [
                'uid', 'product_list', 'configuration', 'main_group', 'group_title', 'label', 'cs_view_record',
                'amount', 'sorting', 'pid', 'title', 'express', 'encoway_configuration', 'price', 'shipping',
                'amirada_configuration', 'note', 'image_url', 'attributes']
        ];
        $valid = false;
        $validValues = [];
        $lineItem = null;
        $sorting = 1;
        foreach ($valueArray as $key => $value) {
            if (!isset($validValues['sorting'])) {
                $validValues['sorting'] = $sorting;
            }
            $sorting = (int)$validValues['sorting'];

            $key = GeneralUtility::camelCaseToLowerCaseUnderscored($key);
            if (in_array($key, $attributesConf['allowed'])) {
                if ($key === 'amount'
                    || $key === 'pid'
                    || $key === 'group'
                    || $key === 'uid'
                ) {
                    $validValues[$key] = intval($value);
                } elseif ($key === 'encoway_configuration') {
                    if (is_numeric($value)) {
                        $validValues[$key] = (int)$value;
                    } else {
                        // encoway configuration short code was given
                        $encowayConfiguration = $this->encowayConfigurationRepository->findByShortcode($value);
                        if ($encowayConfiguration instanceof EncowayConfiguration) {
                            $validValues[$key] = $encowayConfiguration->getUid();
                        }
                    }
                } elseif (is_array($value)) {
                    $validValues[$key] = json_encode($value);
                } elseif ($key == 'price') {
                    $validValues[$key] = round(floatval($value), 2);
                } else {
                    $validValues[$key] = trim($value);
                }
            }
        }

        $requiredCount = 0;
        foreach ($validValues as $key => $_) {
            if (in_array($key, $attributesConf['required'])) {
                $requiredCount++;
            }
        }

        // check if all required fields are set and a product configuration ist present
        if (count($attributesConf['required']) === $requiredCount
            && (
                isset($validValues['configuration'])
                || isset($validValues['encoway_configuration']) || isset($validValues['cs_view_record'])
            )
        ) {
            $valid = true;
        }

        if ($valid) {
            $validValues['deleted'] = 0;
            $validValues['tstamp'] = time();
            if ($validValues['uid']) {
                $uid = $validValues['uid'];
                unset($validValues['uid']);
                $res = $db->exec_UPDATEquery(
                    'tx_baumer_domain_model_lineitem',
                    'uid = ' . $uid,
                    $validValues
                );
                $lineItem = $this->findByIdentifier($uid);
            } else {
                $res = $db->exec_INSERTquery(
                    'tx_baumer_domain_model_lineitem',
                    $validValues
                );
                $lineItem = $this->findByIdentifier($db->sql_insert_id());
            }
        }
        $result = null;
        if ($lineItem instanceof LineItem) {
            $result = $lineItem;
        }
        return $result;
    }

    /**
     * Find by uid without using the session cache
     *
     * @param $identifier
     *
     * @return object
     */
    public function findByIdentifier($identifier)
    {
        $query = $this->createQuery();
        $object = $query->matching($query->equals('uid', (int)$identifier))->execute()->getFirst();

        return $object;
    }
}
