<?php
namespace Baumer\Baumer\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions GmbH & Co. KG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Baumer\Baumer\Domain\Model\CsView;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class CsViewRepository
 *
 * @package Baumer\Baumer\Domain\Repository
 * @method CsView findOneByContentServId($contentServId)
 * @method QueryResultInterface findByContentServId($contentServId)
 * @method QueryResultInterface findByClass($classId)
 */
class CsViewRepository extends Repository
{

    /**
     * Set Query defaults
     */
    public function initializeObject()
    {
        /** @var $defaultQuerySettings Typo3QuerySettings */
        $defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $defaultQuerySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($defaultQuerySettings);
    }

    /**
     * @param array $parentIds
     * @param array $childrenIds
     * @return int Number of affected items
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteByParentIdsAndCurrentChildren($parentIds, $childrenIds)
    {
        $deletionCount = 0;
        $query = $this->createQuery();
        $constraints = [];
        foreach ($parentIds as $parentId) {
            $constraints[] = $query->logicalAnd([
                $query->like('data', '%"ParentID":"' . $parentId . '"%'),
                $query->logicalNot($query->in('content_serv_id', $childrenIds))
            ]);
        }
        $query->matching(
            $query->logicalOr($constraints)
        );

        foreach ($query->execute() as $csView) {
            $deletionCount++;
            $this->remove($csView);
        }
        return $deletionCount;
    }

    /**
     * @param string $contentServId
     * @return CsView|null
     */
    public function findSelectorByContentServId($contentServId)
    {
        $query = $this->createQuery();
        $result = $query->matching($query->logicalAnd([
            $query->equals('content_serv_id', $contentServId),
            $query->equals('class', CsView::CLASS_SELECTOR)
        ]))->setLimit(1)->execute();
        if ($result instanceof QueryResultInterface) {
            return $result->getFirst();
        } elseif (is_array($result)) {
            return isset($result[0]) ? $result[0] : null;
        }
    }

    /**
     * @return array|QueryResultInterface
     */
    public function findAllSubselectors()
    {
        $query = $this->createQuery();
        $query->matching(
            $query->like('data', '%isVirtualView%')
        );
        return $query->execute();
    }

    /**
     * Delete CsViews that are not part of $validCsViews
     *
     * @param array $validCsViews
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteOrphans($validCsViews)
    {
        $validUids = [];
        /** @var CsView $csView */
        foreach ($validCsViews as $csView) {
            $validUids[] = $csView->getUid();
        }
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->logicalNot(
                    $query->in('uid', $validUids
                    )
                )
            )
        );
        /** @var CsView $poorOrphanView */
        foreach ($query->execute() as $poorOrphanView) {
            $this->remove($poorOrphanView);
        }
    }
}
