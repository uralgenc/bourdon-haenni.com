<?php
namespace Baumer\Baumer\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for Pages
 */
class PageRepository extends Repository
{

    protected $defaultOrderings = [
        'title' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Find all pages by categories and provided conjunction
     *
     * @param $categories
     * @param $conjunction
     * @return QueryInterface
     */
    public function findByCategories($categories, $conjunction)
    {
        $query = $this->createQuery();
        $categoryConstraint = $this->createCategoryConstraint($query, $categories, $conjunction);
        if ($categoryConstraint) {
            $constraints[] = $categoryConstraint;
        }
        if ($GLOBALS['TSFE']->sys_language_uid > 0) {
            $constraints[] = $query->in('uid', $this->getTranslatedPids($GLOBALS['TSFE']->sys_language_uid));
        }
        if (!empty($constraints)) {
            $query->matching($query->logicalAnd($constraints));
        }
        return $query->execute();
    }

    /**
     * Helper function for page select condition
     * @param integer $sysLanguageUid
     * @return array
     */
    protected function getTranslatedPids($sysLanguageUid)
    {
        $translatedPids = [];
        /** @var DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $translatedPages = $db->exec_SELECTgetRows(
            'pid',
            'pages_language_overlay',
            'hidden = 0 AND deleted = 0 AND sys_language_uid = ' . $sysLanguageUid
        );
        foreach ($translatedPages as $translatedPage) {
            $translatedPids[] = $translatedPage['pid'];
        }
        return $translatedPids;
    }

    /**
     * Return a category constraint created by
     * a given list of categories and a junction string
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param array $categories
     * @param string $conjunction
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
     */
    protected function createCategoryConstraint(QueryInterface $query, $categories, $conjunction)
    {
        $constraint = null;
        $categoryConstraints = [];

        // If "ignore category selection" is used, nothing needs to be done
        if (empty($conjunction)) {
            return $constraint;
        }

        if (!is_array($categories)) {
            $categories = GeneralUtility::intExplode(',', $categories, true);
        }
        foreach ($categories as $category) {
            $categoryConstraints[] = $query->contains('categories', $category);
        }

        if ($categoryConstraints) {
            switch (strtolower($conjunction)) {
                case 'or':
                    $constraint = $query->logicalOr($categoryConstraints);
                    break;
                case 'notor':
                    $constraint = $query->logicalNot($query->logicalOr($categoryConstraints));
                    break;
                case 'notand':
                    $constraint = $query->logicalNot($query->logicalAnd($categoryConstraints));
                    break;
                case 'and':
                default:
                    $constraint = $query->logicalAnd($categoryConstraints);
            }
        }

        return $constraint;
    }
}
