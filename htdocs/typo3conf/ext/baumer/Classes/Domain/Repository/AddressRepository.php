<?php
namespace Baumer\Baumer\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 b:dreizehn, Germany <typo3@b13.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;
use Baumer\Baumer\Domain\Model\Address;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for the domain model Address
 *
 */
class AddressRepository extends Repository
{

    /**
     * @var array
     */
    protected $settings;

    /**
     * @return array
     */
    public function initializeObject()
    {
        if (! isset($this->settings)) {
            /** @var ConfigurationManagerInterface $configurationManager */
            $configurationManager = $this->objectManager->get(ConfigurationManagerInterface::class);
            $this->settings       = $configurationManager->getConfiguration(
                ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'baumer', 'tx_baumer');
        }

        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function injectSettings($settings)
    {
        $this->settings = $settings;
    }

    /**
     * Finds relevant tt_address records for a given rootline. “Relevant” means
     * that the record is either directly linked to the page or has at least
     * one category in common with the page. If a page has both directly and
     * indirectly linked addresses, only those with a direct link are returned.
     * If no addresses are found for a page, the search is repeated with the
     * parent page until the site root is reached.
     *
     * @param array $rootline
     *
     * @return Address[]
     */
    public function findByRootline(array $rootline)
    {
        $addresses = [ ];
        foreach ($rootline as $pageRecord) {
            $addresses = $this->findByPageId($pageRecord['uid']);
            if (count($addresses) > 0) {
                break;
            }
            $addresses = $this->findByCategoryIds(GeneralUtility::intExplode(',', $pageRecord['categories'], true));
            if (count($addresses) > 0) {
                break;
            }
        }

        return $addresses;
    }

    /**
     * @param integer $pageId
     *
     * @return Address[]
     */
    public function findByPageId($pageId)
    {
        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Query $query */
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(true);
        // $query->contains() doesn't fully support "group"
        // fields in Extbase, so we use our own SQL
        $query->statement('SELECT * FROM tt_address WHERE FIND_IN_SET('.$pageId.', tx_baumer_pages)');

        return $query->execute();
    }

    /**
     * @param integer[] $categoryIds
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface|NULL
     */
    public function findByCategoryIds(array $categoryIds)
    {
        $result = null;

        if (count($categoryIds)) {
            $addressPid = $this->settings['addressStorage'];

            $query = $this->createQuery();
            $query->getQuerySettings()->setStoragePageIds([$addressPid]);
            $constraints = [ ];
            foreach ($categoryIds as $categoryId) {
                $constraints[] = $query->contains('categories', $categoryId);
            }
            $query->matching($query->logicalOr($constraints));

            $result = $query->execute();
        }

        return $result;
    }

    public function findDefault()
    {
        $addressId = $this->settings['productfinder']['defaultContactAddress'];

        return $this->findByUid($addressId);
    }

    public function findAll()
    {
        $query = $this->createQuery();
        $querySettings = $query->getQuerySettings();
        $querySettings->setRespectStoragePage(false);
        $query->setQuerySettings($querySettings);
        $query->matching($query->greaterThan('uid', 0));
        return $query->execute();
    }
}
