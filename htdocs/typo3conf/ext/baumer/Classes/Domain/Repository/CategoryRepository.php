<?php
namespace Baumer\Baumer\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use Baumer\Baumer\Domain\Model\FileReference;

/**
 * The repository for Categories
 */
class CategoryRepository extends \TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository
{

    /**
     * @param array $uids
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUids($uids)
    {
        $query = $this->createQuery();
        foreach ($uids as $key => $value) {
            $constraints[] = $query->equals('uid', $value);
        }
        return $query->matching(
            $query->logicalAnd(
                $query->logicalOr(
                    $constraints
                ),
                $query->equals('hidden', 0),
                $query->equals('deleted', 0)
            )
        )->execute();
    }

    /**
     * @param integer $contentServId
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByContentServId($contentServId)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);

        return $query->matching(
            $query->logicalAnd(
                $query->logicalOr(
                    $query->equals('contentServId', $contentServId),
                    $query->like('contentServId', '%/' . $contentServId)
                ),
                $query->equals('sysLanguageUid', 0)
            )
        )->execute();
    }

    /**
     * @param int $contentServId
     * @return null|\Baumer\Baumer\Domain\Model\Category
     */
    public function findOneByContentServId($contentServId)
    {
        $categories = $this->findByContentServId($contentServId);
        if ($categories->count() > 0) {
            return $categories->getFirst();
        }
        return null;
    }

    /**
     * @return \Baumer\Baumer\Domain\Model\Category
     */
    public function findAllContentServId()
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);

        return $query->matching(
            $query->logicalAnd(
                $query->greaterThan('contentServId', 0),
                $query->equals('sysLanguageUid', 0)
            )
        )->execute();
    }

    /**
     * @param string $mainGroupId
     *
     * @return \Baumer\Baumer\Domain\Model\Category
     */
    public function findByAmiradaMaingroupId($mainGroupId)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);

        return $query->matching(
            $query->logicalAnd(
                $query->equals('amiradaMaingroupId', $mainGroupId),
                $query->equals('sysLanguageUid', 0)
            )
        )->execute()->getFirst();
    }

    /**
     * Find Categories for a given file reference
     *
     * @param \Traversable $fileReferences
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findForFileReferences(\Traversable $fileReferences)
    {
        $fileUids = [];
        foreach ($fileReferences as $fileReference) {
            $fileUids[] = (int)$fileReference->getOriginalResource()->getOriginalFile()->getUid();
        }
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $res = $db->exec_SELECTgetRows(
            'uid_local',
            'sys_category_record_mm',
            'tablenames = "sys_file_metadata"
				AND fieldname = "categories"
				AND uid_foreign IN (SELECT uid FROM sys_file_metadata WHERE file IN ('
            . implode(',', $fileUids) .
            ') AND sys_language_uid = 0)',
            '',
            'sorting'
        );
        $uids = [];
        foreach ($res as $elem) {
            $uids[] = (int)$elem['uid_local'];
        }
        if (count($uids) > 0) {
            return $this->findByUids($uids);
        } else {
            return null;
        }
    }
}
