<?php
namespace Baumer\Baumer\Domain\Repository;

use Baumer\Baumer\Domain\Model\PriceShipping;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class PriceShippingRepository
 *
 * @package Baumer\Baumer\Domain\Repository
 * @method PriceShipping findOneByProductCode($productCode)
 */
class PriceShippingRepository extends Repository
{

    /**
     * Set Query defaults
     */
    public function initializeObject()
    {
        /** @var $defaultQuerySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $defaultQuerySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $defaultQuerySettings->setRespectStoragePage(false);
        $defaultQuerySettings->setLanguageMode('content_fallback');
        $this->setDefaultQuerySettings($defaultQuerySettings);
    }
}
