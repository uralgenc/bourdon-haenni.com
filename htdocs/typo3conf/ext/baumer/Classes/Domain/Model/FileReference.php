<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>, typovision
 *           Anja Leichsenring <anja.leichsenring@typovision.de>, typovision
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * FileReference
 */
class FileReference extends \TYPO3\CMS\Extbase\Domain\Model\FileReference
{

    /**
     * We need this property so that the Extbase persistence can properly persist the object
     *
     * @var integer
     */
    protected $uidLocal;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Baumer\Baumer\Domain\Model\Category>
     */
    protected $categories;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Baumer\Baumer\Domain\Model\FileReference>
     */
    protected $translations;

    /**
     * @param \TYPO3\CMS\Core\Resource\ResourceInterface $originalResource
     */
    public function setOriginalResource(\TYPO3\CMS\Core\Resource\ResourceInterface $originalResource)
    {
        $this->originalResource = $originalResource;
        $this->uidLocal = (int)$originalResource->getUid();
    }

    /**
     * @return ObjectStorage
     */
    public function getCategories()
    {
        if (!$this->categories instanceof ObjectStorage) {
            $this->categories = $this->categoryRepository->findForFileReferences(
                new \ArrayObject([$this])
            );
        }

        return $this->categories;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title !== null ? $this->title : $this->getOriginalResource()->getTitle();
    }


    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description !== null ? $this->description : $this->getOriginalResource()->getDescription();
    }

    /**
     * Get alternative
     *
     * @return string
     */
    public function getAlternative()
    {
        return $this->alternative !== null ? $this->alternative : $this->getOriginalResource()->getAlternative();
    }

    /**
     * Get File UID
     *
     * @return int
     */
    public function getFileUid()
    {
        return $this->uidLocal;
    }

    /**
     * Get link
     *
     * @return mixed
     * @return void
     */
    public function getLink()
    {
        return $this->link !== null ? $this->link : $this->getOriginalResource()->getLink();
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->getOriginalResource()->getOriginalFile()->getProperty('extension');
    }

    /**
     * @return integer
     */
    public function getSize()
    {
        return $this->getOriginalResource()->getOriginalFile()->getProperty('size');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getOriginalResource()->getOriginalFile()->getProperty('name');
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getOriginalResource()->getOriginalFile()->getProperty('identifier');
    }

    /**
     * @param bool $relativeToCurrentScript
     * @return mixed
     */
    public function getPublicUrl($relativeToCurrentScript = false)
    {
        return $this->getOriginalResource()->getOriginalFile()->getPublicUrl($relativeToCurrentScript);
    }

    /**
     * Get file's language ISO2 Code
     *
     * @return string
     */
    public function getLanguage()
    {
        if ($this->getOriginalResource()->hasProperty('file_language')) {
            $sysLanguageUid = $this->getOriginalResource()->getProperty('file_language');
            /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */

            $db = $GLOBALS['TYPO3_DB'];
            $res = $db->exec_SELECTgetSingleRow(
                'static_languages.lg_iso_2 as iso2',
                'sys_language JOIN static_languages ON static_languages.uid = sys_language.static_lang_isocode',
                'sys_language.uid = ' . (int)$sysLanguageUid
            );

            if ($res['iso2']) {
                return $res['iso2'];
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
}
