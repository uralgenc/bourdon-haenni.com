<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Baumer\Baumer\Domain\Model\Behavior\JsonData;
use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Frontend\ContentObject\AbstractContentObject;

/**
 * Class CsViewRecord
 *
 * @package Baumer\Baumer\Domain\Model
 */
class CsViewRecordRelation extends AbstractEntity
{
    use JsonData;
    use ToArrayTrait;



    /**
     * @var AbstractContentObject
     */
    protected $ttContent;


    /**
     * @var string
     */
    protected $link;

    /**
     * @var \Baumer\Baumer\Domain\Model\CsViewRecord
     */
    protected $csViewRecord;

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return CsViewRecord
     */
    public function getCsViewRecord()
    {
        return $this->csViewRecord;
    }


    /**
     * @param CsViewRecord $csViewRecord
     * @return void
     */
    public function setCsViewRecord($csViewRecord)
    {
        $this->csViewRecord = $csViewRecord;
    }

    /**
     * @return AbstractContentObject
     */
    public function getTtContent()
    {
        return $this->ttContent;
    }

    /**
     * @param AbstractContentObject $ttContent
     */
    public function setTtContent($ttContent)
    {
        $this->ttContent = $ttContent;
    }

    /**
     * @param array $jsonAr
     * @return void
     */
    public function fromJson($jsonAr)
    {
        //		foreach ($jsonAr as $field => $value) {
//			switch ($field) {
//				case 'Link':
//					$this->link = $value;
//					break;
//				case 'ID':
//					$this->contentServId = (int)$value;
//					break;
//			}
//		}
//		$this->data = json_encode($jsonAr);
    }

    /**
     * Add deserialized data array
     *
     * @param array $array (by reference)
     * @return void
     */
    protected function addToAttributesArray(&$array)
    {
        //		$array['data'] = json_decode($this->data, true);
    }
}
