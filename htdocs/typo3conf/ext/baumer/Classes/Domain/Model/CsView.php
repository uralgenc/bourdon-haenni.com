<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Baumer\Baumer\Domain\Model\Behavior\JsonData;
use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Class View
 *
 * @package Baumer\Baumer\Domain\Model
 */
class CsView extends AbstractEntity
{
    use JsonData;
    use ToArrayTrait;

    const CLASS_SELECTOR = 1463;
    const CLASS_CONFIGURATOR = 1951;

    /**
     * @var array
     */
    public static $CLASS_TYPE_MAPPING = [
        self::CLASS_SELECTOR => 'PFINDER',
        self::CLASS_CONFIGURATOR => 'PRODUCT'
    ];

    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRecordRepository
     * @inject
     */
    protected $csViewRecordRepository;

    /**
     * @var string
     */
    protected $contentServId;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var integer
     */
    protected $class;

    /**
     * View records
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Baumer\Baumer\Domain\Model\CsViewRecord> $csViewRecords
     * @lazy
     */
    protected $csViewRecords;

    /**
     * @return string
     */
    public function getContentServId()
    {
        return $this->contentServId;
    }

    /**
     * @param string $contentServId
     * @return void
     */
    public function setContentServId($contentServId)
    {
        $this->contentServId = $contentServId;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return void
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return integer
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param integer $class
     * @return void
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * Adds a CsViewRecord
     *
     * @param CsViewRecord $csViewRecord
     * @return void
     */
    public function addRecord(CsViewRecord $csViewRecord)
    {
        $this->getRecords()->attach($csViewRecord);
    }

    /**
     * Returns the records (CsViewRecord)
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage $records
     */
    public function getRecords()
    {
        if (!$this->csViewRecords instanceof ObjectStorage) {
            $this->csViewRecords = new ObjectStorage();
        }
        return $this->csViewRecords;
    }

    /**
     * Clears all related records
     *
     * @return void
     */
    public function clearRecords()
    {
        $this->csViewRecords = new ObjectStorage();
    }

    /**
     * @param array $jsonAr
     * @return void
     */
    public function fromJson($jsonAr)
    {
        foreach ($jsonAr as $field => $value) {
            switch ($field) {
                case 'Label':
                    $this->setLabel($value);
                    break;
                case 'ID':
                    $this->setContentServId((int)$value);
                    break;
                case 'Class':
                    $this->class = isset($value['ID']) ? (int)$value['ID'] : null;
                    break;
            }
        }
        $this->data = json_encode($jsonAr);
    }

    /**
     * If the view contains products their IDs will be returned.
     * !!! Be sure that the view has been stored together with the product Selection => expand
     *
     * @return array of product ids
     */
    public function getProductIds()
    {
        $productIds = [];
        $data = $this->getData();
        if (isset($data['Selection']) && is_array($data['Selection'])) {
            foreach ($data['Selection'] as $product) {
                $productIds[] = $product['ID'];
            }
        }

        return $productIds;
    }

    /**
     * @return array
     */
    public function getCsViewRecordContentservIds()
    {
        $recordCsIds = [];
        /** @var CsViewRecord $csViewRecord */
        foreach ($this->getRecords() as $csViewRecord) {
            $recordCsIds[] = $csViewRecord->getContentServId();
        }
        return $recordCsIds;
    }

    /**
     * @param int $contentservId
     * @return CsViewRecord
     */
    public function removeCsViewRecordByContentservId($contentservId)
    {
        /** @var CsViewRecord $csViewRecord */
        foreach ($this->getRecords() as $csViewRecord) {
            if ((int)$csViewRecord->getContentServId() === (int)$contentservId) {
                $this->getRecords()->detach($csViewRecord);
                return $csViewRecord;
            }
        }
    }

    /**
     * @param string $contentservId
     * @return CsViewRecord|null
     */
    public function getCsViewRecordByContentservId($contentservId)
    {
        /** @var CsViewRecord $csViewRecord */
        foreach ($this->csViewRecords as $csViewRecord) {
            if ((int)$csViewRecord->getContentServId() === (int)$contentservId) {
                return $csViewRecord;
            }
        }
        return null;
    }

    /**
     * @return array All AttributeIds used for products in the view
     */
    public function getAllAttributeIds()
    {
        $attributeIds = [];
        $data = $this->getData();
        if (isset($data['Attributes'])) {
            foreach ($data['Attributes'] as $attribute) {
                if (is_numeric($attribute['ID'])) {
                    $attributeIds[] = (int)$attribute['ID'];
                }
                if ($attribute['Type'] === 'attributereference') {
                    $attributeIds = array_merge($attributeIds,
                        GeneralUtility::intExplode(',', $attribute['Value'], true));
                }
            }
        }

        return $attributeIds;
    }

    /**
     * @param int $attributeId
     * @return array
     */
    public function getAllReferencedAttributeIds($attributeId = 0)
    {
        $attributeIds = [];
        $data = $this->getData();
        if (isset($data['Attributes'])) {
            foreach ($data['Attributes'] as $attribute) {
                if (is_numeric($attribute['ID']) &&
                    $attribute['Type'] === 'attributereference' &&
                    ($attributeId === 0 || ($attributeId > 0 && $attributeId === (int)$attribute['ID']))
                ) {
                    $attributeIds = array_merge($attributeIds,
                        GeneralUtility::intExplode(',', $attribute['Value'], true));
                }
            }
        }
        return $attributeIds;
    }

    /**
     * @return array Key => Multivalue array of attributereferences (Groups)
     */
    public function getAttributeIdGroups()
    {
        $attributeIdGroups = [];
        $data = $this->getData();
        if (isset($data['Attributes'])) {
            foreach ($data['Attributes'] as $attribute) {
                if (is_numeric($attribute['ID']) && $attribute['Type'] === 'attributereference') {
                    $attributeIdGroups[$attribute['ID']] = GeneralUtility::intExplode(',', $attribute['Value'], true);
                }
            }
        }
        return $attributeIdGroups;
    }

    /**
     * Add deserialized data array
     *
     * @param array $array (by reference)
     * @return void
     */
    protected function addToAttributesArray(&$array)
    {
        $array['data'] = json_decode($this->data, true);
    }
}
