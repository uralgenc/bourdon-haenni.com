<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>, Typovision GmbH
 *  (c) 2015 Martin R. Krause <martin.r.krause@gmx.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use \TYPO3\CMS\Extbase\Domain\Model\FrontendUser;

/**
 * ProductList
 *
 * @author Hans Höchtl <extern.hans.hoechtl@typovision.de>
 */
class ProductList extends AbstractEntity
{

    use ToArrayTrait;

    /**
     * name
     *
     * @var string
     * @validate NotEmpty
     */
    protected $name = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * feUser
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $feUser = null;

    /**
     * line items
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Baumer\Baumer\Domain\Model\LineItem>
     * @cascade remove
     */
    protected $lineItems = null;

    /**
     * Hash code to identify the product list
     *
     * Product list with a code are considered a snapshot
     * of a specific state of a product list.
     * There a product list with a code is immutable.
     * When trying to update anything on a product list
     * with a code, the controller should instead make a new product list.
     *
     * @var string $code
     */
    protected $code;

    /**
     * Link to the current view of this product list (when code is set)
     *
     * @var string
     */
    protected $linkUrl;

    /**
     * @var \DateTime $crdate
     */
    protected $crdate;

    /**
    * Further process the attributes array
    * See ToArrayTrait
    *
    * @param array $array (by reference)
    * @return void
    */
    protected function addToAttributesArray(&$array)
    {
        $array['crdate'] = $this->getCrdate()->getTimestamp();
        if ($this->getFeUser() instanceof FrontendUser) {
            $array['feUser'] = $this->getFeUser()->getUid();
        }
    }

    /**
     * Generates short hash code, used to identify the product list
     *
     * @return void
     */
    public function generateCode()
    {
        if (strlen($this->code) < 9) {
            $characters = '123456789ABCDEFHJKLMNPRTVWXYZ';
            $length = 8;
            $code = '';
            for ($p = 0; $p < $length; $p++) {
                $code .= $characters[mt_rand(0, strlen($characters) - 1)];
            }
            $code = substr($code, 0, 4) . '-' . substr($code, -4);

            // ensure it's unique
            $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                'uid',
                'tx_baumer_domain_model_productlist',
                'code = "' . $code . '"'
            );
            if ($count > 0) {
                $this->generateCode();
            } else {
                $this->code = $code;
            }
        }
    }

    /**
     * Generate link Url
     *
     * @param bool $absolute
     * @return mixed
     */
    public function getLinkUrl($absolute = false)
    {
        $linkUrl = '';
        if ($this->getCode()) {
            $typolinkConf = [
                'typolink.' => [
                    'returnLast' => 'url',
                ]
            ];
            $productListPage = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_baumer.']['settings.']['productListPage'];
            if ($absolute) {
                $typolinkConf['typolink.']['forceAbsoluteUrl'] = true;
            }
            $typolinkConf['typolink.']['parameter'] = $productListPage;
            $typolinkConf['typolink.']['useCacheHash'] = true;
            $typolinkConf['typolink.']['additionalParams'] =    '&tx_baumer_productlistcheckout[controller]=ProductList' .
                                                                '&tx_baumer_productlistcheckout[action]=current' .
                                                                '&tx_baumer_productlistcheckout[code]=' . urlencode($this->getCode());
            $this->linkUrl = $GLOBALS['TSFE']->cObj->cObjGetSingle('TEXT', $typolinkConf);

            $linkUrl = $this->linkUrl;
        }
        return $linkUrl;
    }

    /*
     * Just plain and boring getters and setters below
     */

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    public function getFeUser()
    {
        return $this->feUser;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feUser
     *
     * @return void
     */
    public function setFeUser($feUser)
    {
        $this->feUser = $feUser;
    }

    /**
     * Adds a LineItem
     *
     * @param LineItem $lineItem
     * @return void
     */
    public function addLineItem(LineItem $lineItem)
    {
        if (!$this->lineItems instanceof ObjectStorage) {
            $this->lineItems = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage');
        }
        $this->lineItems->attach($lineItem);
    }

    /**
     * Removes a LineItem
     *
     * @param LineItem $lineItem
     * @return void
     */
    public function removeLineItem(LineItem $lineItem)
    {
        $this->lineItems->detach($lineItem);
    }

    /**
     * Returns the line items
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Baumer\Baumer\Domain\Model\LineItem> $lineItems
     */
    public function getLineItems()
    {
        return $this->lineItems;
    }

    /**
     * Sets the line items
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Baumer\Baumer\Domain\Model\LineItem> $lineItems
     * @return void
     */
    public function setNamedEncowayConfigurations(ObjectStorage $lineItems)
    {
        $this->lineItems = $lineItems;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return void
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * @param \DateTime $crdate
     * @return void
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }
}
