<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>, typovision
 *           Anja Leichsenring <anja.leichsenring@typovision.de>, typovision
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;

/**
 * Category
 */
class Category extends \TYPO3\CMS\Extbase\Domain\Model\Category
{

    use ToArrayTrait;

    /**
     * @var int The uid of the localized record. In TYPO3 v4.x the property "uid" holds the uid of the record in default language (the translationOrigin).
     */
    protected $_localizedUid;

    /**
     * @var int The uid of the language of the object. In TYPO3 v4.x this is the uid of the language record in the table sys_language.
     */
    protected $_languageUid;

    /**
     * l10nParent
     *
     * @var integer
     */
    protected $l10nParent;

    /**
     * contentServId
     *
     * @var string
     */
    protected $contentServId = '';

    /**
     * amiradaMaingroupId
     *
     * @var string
     */
    protected $amiradaMaingroupId = '';

    /**
     * @param integer $languageUid
     * @return void
     */
    public function setLanguageUid($languageUid)
    {
        $this->_languageUid = $languageUid;
    }

    /**
     * @return integer
     */
    public function getLanguageUid()
    {
        return $this->_languageUid;
    }

    /**
     * @param integer $l10nParent
     * @return void
     */
    public function setL10nParent($l10nParent)
    {
        $this->l10nParent = $l10nParent;
    }

    /**
     * @return integer
     */
    public function getL10nParent()
    {
        return $this->l10nParent;
    }

    /**
     * @param integer $localizedUid
     */
    public function setLocalizedUid($localizedUid)
    {
        $this->_localizedUid = $localizedUid;
    }

    /**
     * @return integer
     */
    public function getLocalizedUid()
    {
        return $this->_localizedUid;
    }

    /**
     * Returns the contentServId
     *
     * @return string $contentServId
     */
    public function getContentServId()
    {
        return $this->contentServId;
    }

    /**
     * Sets the contentServId
     *
     * @param string $contentServId
     *
     * @return void
     */
    public function setContentServId($contentServId)
    {
        $this->contentServId = $contentServId;
    }

    /**
     * @return string
     */
    public function getAmiradaMaingroupId()
    {
        return $this->amiradaMaingroupId;
    }

    /**
     * @param string $amiradaMaingroupId
     *
     * @return void
     */
    public function setAmiradaMaingroupId($amiradaMaingroupId)
    {
        $this->amiradaMaingroupId = $amiradaMaingroupId;
    }
}
