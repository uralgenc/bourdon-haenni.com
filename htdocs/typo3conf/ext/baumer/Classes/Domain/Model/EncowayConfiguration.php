<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>, Typovision GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * EncowayConfiguration
 */
class EncowayConfiguration extends AbstractEntity
{

    /**
     * shortcode
     *
     * @var string
     * @validate NotEmpty
     */
    protected $shortcode = '';

    /**
     * completed
     *
     * @var bool
     */
    protected $completed = false;

    /**
     * configurationData
     *
     * @var string
     * @ignorevalidation
     */
    protected $configurationData = '';

    /**
     * contentservId
     *
     * @var string
     */
    protected $contentServId = null;

    /**
     * Returns the shortcode
     *
     * @return string $shortcode
     */
    public function getShortcode()
    {
        return $this->shortcode;
    }

    /**
     * Sets the shortcode
     *
     * @param string $shortcode
     * @return void
     */
    public function setShortcode($shortcode)
    {
        $this->shortcode = $shortcode;
    }

    /**
     * Returns the configurationData
     *
     * @return string $configurationData
     */
    public function getConfigurationData()
    {
        return $this->configurationData;
    }

    /**
     * Sets the configurationData
     *
     * @param string $configurationData
     * @return void
     */
    public function setConfigurationData($configurationData)
    {
        $this->configurationData = $configurationData;
    }

    /**
     * Returns the ContentservID
     *
     * @return string
     */
    public function getContentServId()
    {
        return $this->contentServId;
    }

    /**
     * Sets the contentservId
     *
     * @param string $contentServId
     * @return void
     */
    public function setContentServId($contentServId)
    {
        $this->contentServId = $contentServId;
    }

    /**
     * @return boolean
     */
    public function isCompleted()
    {
        return $this->completed;
    }

    /**
     * @param boolean $completed
     *
     * @return void
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }
}
