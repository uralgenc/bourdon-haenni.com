<?php
namespace Baumer\Baumer\Domain\Model\Behavior;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Martin R. Krause <martin.r.krause@gmx.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use \TYPO3\CMS\Extbase\Reflection\ObjectAccess;

/**
 * @author  Martin R. Krause <martin.r.krause@gmx.de>
 */
trait ToArrayTrait
{

    /**
     * returns a plain array with attribute values
     *
     * @param boolean $withRelations
     * @return array
     */
    public function toArray($withRelations = true)
    {
        $array = [];
        $properties = ObjectAccess::getGettablePropertyNames($this);

        foreach ($properties as $property) {
            $value = ObjectAccess::getProperty($this, $property);
            // get all non object values
            if (!is_object($value)) {
                $array[$property] = $value;
            } elseif ($withRelations) {
                // get all object form ObjectStorage
                if ($value instanceof ObjectStorage) {
                    if ($value->count() > 0) {
                        foreach ($value as $child) {
                            if (method_exists($child, 'toArray')) {
                                $array[$property][] = $child->toArray();
                            }
                        }
                    }
                }
            }
        }

        $this->addToAttributesArray($array);

        return $array;
    }

    /**
     * Further process the attributes array
     * (can be individually overwritten in each usage of this trait)
     *
     * @param array $array (by reference)
     * @return void
     */
    protected function addToAttributesArray(&$array)
    {
    }
}
