<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>, Typovision GmbH
     *  (c) 2015 Martin R. Krause <martin.r.krause@gmx.de>
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/
use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * LineItem
 *
 * @author Hans Höchtl <extern.hans.hoechtl@typovision.de>
 * @author Martin R. Krause <martin.r.krause@gmx.de>
 */
class LineItem extends AbstractEntity
{

    use ToArrayTrait;

    /**
     * encowayConfiguration
     *
     * @var \Baumer\Baumer\Domain\Model\EncowayConfiguration
     */
    protected $encowayConfiguration = null;

    /**
     * amirada product configuration
     *
     * @var string $amiradaConfiguration
     */
    protected $amiradaConfiguration;

    /**
     * @var \Baumer\Baumer\Domain\Model\ProductList
     */
    protected $productList = null;

    /**
     * @var \Baumer\Baumer\Domain\Model\Category
     */
    protected $mainGroup;

    /**
     * Group title (if no group was assignes, e.g. when from Amirada Configuration)
     *
     * @var string
     */
    protected $groupTitle;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var integer $amount
     */
    protected $amount;

    /**
     * Item price in EUR cent
     *
     * @var float $price
     */
    protected $price;

    /**
     * Delivery time in days
     *
     * @var integer
     */
    protected $shipping;

    /**
     * A note (given by the user)
     *
     * @var string $note
     */
    protected $note;

    /**
     * crdate
     *
     * @var \DateTime
     */
    protected $crdate;

    /**
     * Typolink LinkConf to product
     *
     * @var string
     */
    protected $url;

    /**
     * Url to rendered list image
     * Needed as rendering of lineItems happens with angular
     *
     * @var string $imageUrl;
     */
    protected $imageUrl;

    /**
     * Product name of this line item
     *
     * @var string $configuration
     */
    protected $configuration;

    /**
     * @var int $csViewRecord
     */
    protected $csViewRecord;

    /**
     * Attribute list from product configuration
     * JSON-String
     *
     * @var string
     */
    protected $attributes;

    /**
     * Return the string representative of the product
     *
     * Encoway takes precedence over amirada, over a title given by the users
     *
     * @return string
     */
    public function getConfiguration()
    {
        if ($this->getEncowayConfiguration() instanceof EncowayConfiguration) {
            $this->configuration = $this->getEncowayConfiguration();
        }

        return $this->configuration;
    }


    /**
     * Return group title, from related record, if present
     *
     * @return string
     */
    public function getGroupTitle()
    {
        if ($this->mainGroup instanceof Category) {
            $this->groupTitle = $this->mainGroup->getTitle();
        }

        if (strlen($this->groupTitle) === 0) {
            $this->mainGroup = null;
        }

        return $this->groupTitle;
    }

    /**
     * Get the imageUrl (based on the available configuration)
     *
     * @return string
     */
    public function getImageUrl()
    {
        if (!$this->imageUrl) {
            $setup = [
                'width' => '83c',
                'height' => '56c',
                'treatIdAsReference' => true,
                'ext' => 'jpg'
            ];

            $imageResource = null;

            if ($this->encowayConfiguration instanceof EncowayConfiguration) {
                $this->imageUrl = '';
            } elseif (strlen($this->getAmiradaConfiguration()) > 0) {
                $imageIdentifier = '/typo3conf/ext/baumer/Resources/Public/Images/Dummy83x56.png';
                $setup['treatIdAsReference'] = false;
                $imageResource = $GLOBALS['TSFE']->cObj->getImgResource($imageIdentifier, $setup);
            } else {
                // TODO: Proper Dummy image handling
                $imageIdentifier = '/typo3conf/ext/baumer/Resources/Public/Images/Dummy83x56.png';
                $setup['treatIdAsReference'] = false;
                $imageResource = $GLOBALS['TSFE']->cObj->getImgResource($imageIdentifier, $setup);
            }

            if (!empty($imageResource)) {
                $this->imageUrl = rawurldecode($imageResource[3]);
            }
        }

        return $this->imageUrl;
    }

    /**
     * Get typolink configuration
     *
     * @return string
     */
    public function getUrl()
    {
        if (!$this->url) {
            if ($this->encowayConfiguration instanceof EncowayConfiguration) {
                $typolinkConf = [
                    'parameter' => $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_baumer.']['settings.']['productfinder.']['detailUid'],
                    'additionalParams' =>   '&tx_baumer_configurator[shortCode]=' . $this->getEncowayConfiguration() .
                                            '&tx_baumer_configurator[action]=load&tx_baumer_configurator[controller]=Encoway'
                ];
            } elseif (strlen($this->getAmiradaConfiguration()) > 0) {
                $typolinkConf = [
                    'parameter' => $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_baumerproductfinder.']['settings.']['productfinderUid'],
                    'additionalParams' =>   '&tx_baumerproductfinder_productfinder[url]=' . $this->getAmiradaConfiguration() .
                                            '&tx_baumerproductfinder_productfinder[controller]=ProductFinder'
                ];
            } else {
                $typolinkConf = [];
            }
            if ($typolinkConf) {
                $typolinkConf['returnLast'] = 'url';
                $this->url = $GLOBALS['TSFE']->cObj->cObjGetSingle('TEXT', ['typolink.' => $typolinkConf]);
            } else {
                $this->url = '';
            }
        }

        return $this->url;
    }

    /**
     * Further process the attributes array for JSON output
     *
     * @param array $array (by reference)
     * @return void
     */
    public function addToAttributesArray(&$array)
    {
        // $array['mainGroup'] = $this->getMainGroup();
        $array['encowayConfiguration'] = $this->getEncowayConfiguration();
    }

    /*
     * Just boring getters and setters below
     */

    /**
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * @return string
     */
    public function getEncowayConfiguration()
    {
        $shortcode = '';
        if ($this->encowayConfiguration instanceof EncowayConfiguration) {
            $shortcode = $this->encowayConfiguration->getShortcode();
        }
        return $shortcode;
    }

    /**
     * @param EncowayConfiguration $encowayConfiguration
     *
     * @return void
     */
    public function setEncowayConfiguration($encowayConfiguration)
    {
        $this->encowayConfiguration = $encowayConfiguration;
    }

    /**
     * @return ProductList
     */
    public function getProductList()
    {
        return $this->productList;
    }

    /**
     * @return string
     */
    public function getAmiradaConfiguration()
    {
        return $this->amiradaConfiguration;
    }

    /**
     * @param string $amiradaConfiguration
     * @return void
     */
    public function setAmiradaConfiguration($amiradaConfiguration)
    {
        $this->amiradaConfiguration = $amiradaConfiguration;
    }

    /**
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param integer $amount
     * @return void
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param string $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }


    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return void
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return string
     */
    public function getAttributes()
    {
        return json_decode($this->attributes);
    }

    /**
     * @param mixed $attributes
     * @return void
     */
    public function setAttributes($attributes)
    {
        if (is_array($attributes)) {
            $this->attributes = json_encode($attributes);
        } else {
            $this->attributes = $attributes;
        }
    }

    /**
     * @param \Baumer\Baumer\Domain\Model\Category $group
     * @return void
     */
    public function setMainGroup($group)
    {
        $this->mainGroup = $group;
    }

    /**
     * @param string $groupTitle
     * @return void
     */
    public function setGroupTitle($groupTitle)
    {
        $this->groupTitle = $groupTitle;
    }

    /**
     * @param string $configuration
     * @return void
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param string $imageUrl
     * @return void
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @param ProductList $productList
     * @return void
     */
    public function setProductList($productList)
    {
        $this->productList = $productList;
    }

    /**
     * @param \DateTime $crdate
     * @return void
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }

    /**
     * @return integer
     */
    public function getMainGroup()
    {
        $mainGroupUid = null;
        if ($this->mainGroup instanceof Category) {
            $mainGroupUid = $this->mainGroup->getUid();
        }
        return $mainGroupUid;
    }

    /**
     * @return int
     */
    public function getCsViewRecord()
    {
        return $this->csViewRecord;
    }

    /**
     * @param int $csViewRecord
     */
    public function setCsViewRecord($csViewRecord)
    {
        $this->csViewRecord = $csViewRecord;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
