<?php
namespace Baumer\Baumer\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Baumer\Baumer\Domain\Model\Behavior\JsonData;
use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;
use Baumer\Baumer\Utility\ContentservAttributes;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Class CsViewRecord
 *
 * @package Baumer\Baumer\Domain\Model
 */
class CsViewRecord extends AbstractEntity
{
    use JsonData;
    use ToArrayTrait;

    const ATTRIBUTE_TYPE_FILE = 'file';
    const BAUMER_SITE_KEY = 'baumer';
    const BOURDON_SITE_KEY = 'bourdon';

    /**
     * @var string
     */
    protected $contentServId;

    /**
     * sapId
     *
     * @var string
     */
    protected $sapId = '';

    /**
     * @var string
     */
    protected $label;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Baumer\Baumer\Domain\Model\CsView> $csViews
     */
    protected $csViews;

    /**
     * @var integer
     */
    protected $sorting;

    /**
     * Either self::BOURDON_SITE_KEY or self::BAUMER_SITE_KEY
     *
     * @var string
     */
    protected $site;

    /**
     * @return string
     */
    public function getContentServId()
    {
        return $this->contentServId;
    }

    /**
     * @param string $contentServId
     * @return void
     */
    public function setContentServId($contentServId)
    {
        $this->contentServId = $contentServId;
    }

    /**
     * @return string
     */
    public function getSapId()
    {
        return $this->sapId;
    }

    /**
     * @param string $sapId
     * @return void
     */
    public function setSapId($sapId)
    {
        $this->sapId = $sapId;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return void
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return bool
     */
    public function isFolder()
    {
        $data = $this->getData();
        if (isset($data['IsFolder']) && $data['IsFolder'] === 1) {
            return true;
        }
        return false;
    }

    /**
     * Group attributes of this product based on the grouping of a given CsView.
     *
     * Used to simplify processes in the FE
     *
     * @param CsView $csView
     * @param array $validAttributeGroupIds
     */
    public function groupAttributesForView(CsView $csView, $validAttributeGroupIds = [])
    {
        $data = $this->getData();
        if (!empty($attributeGroupIds = $csView->getAttributeIdGroups())) {
            foreach ($attributeGroupIds as $groupId => $attributeIds) {
                if (in_array($groupId, $validAttributeGroupIds)) {
                    $data['GroupedAttributes'][$groupId] = [];
                    foreach ($attributeIds as $attributeId) {
                        $data['GroupedAttributes'][$groupId][] = $this->extractAttributeById($attributeId);
                    }
                }
            }
        }
        $this->setData(json_encode($data));
    }

    /**
     * Get a list of all attributes IDs of this product
     */
    public function getAttributeIds()
    {
        $attributeIds = [];
        $data = $this->getData();
        if (isset($data['Attributes'])) {
            foreach ($data['Attributes'] as $attribute) {
                $attributeIds[] = $attribute['ID'];
            }
        }
        return $attributeIds;
    }

    /**
     * @param int $attributeId
     * @return null
     */
    public function extractAttributeById($attributeId)
    {
        $data = $this->getData();
        if (isset($data['Attributes'])) {
            foreach ($data['Attributes'] as $attribute) {
                if (is_numeric($attribute['ID']) && (int)$attribute['ID'] === (int)$attributeId) {
                    return $attribute;
                }
            }
        }
        return null;
    }

    /**
     * @param $attributeId
     * @return array|null
     */
    public function extractGroupedAttributeById($attributeId)
    {
        $data = $this->getData();
        if (isset($data['GroupedAttributes']) && isset($data['GroupedAttributes'][$attributeId])) {
            return $data['GroupedAttributes'][$attributeId];
        }
        return null;
    }

    /**
     * @param string $attributeType
     * @return array
     */
    public function extractAttributesByType($attributeType)
    {
        $result = [];
        $data = $this->getData();
        if (isset($data['Attributes'])) {
            foreach ($data['Attributes'] as $attribute) {
                if (isset($attribute['Type']) && $attribute['Type'] === $attributeType) {
                    $result[] = $attribute;
                }
            }
        }
        return $result;
    }


    /**
     * @param string $datasheetsAttributeId
     * @return array
     * @throws \TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException
     */
    public function resolveDatasheets($datasheetsAttributeId)
    {
        $datasheets = [];
        /** @var ResourceFactory $factory */
        $factory = GeneralUtility::makeInstance(ResourceFactory::class);
        /** @var \TYPO3\CMS\Dbal\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $datasheetAttribute = $this->extractGroupedAttributeById($datasheetsAttributeId);
        if (is_array($datasheetAttribute) && array_key_exists('Links', $datasheetAttribute[0])) {
            foreach ($datasheetAttribute[0]['Links'] as $fileLink) {
                $sysFile = $db->exec_SELECTgetSingleRow('*', 'sys_file', 'mam_file_id = ' . $fileLink['ID']);
                if ($sysFile) {
                    $mamDatasheet = $factory->getFileObject($sysFile['uid'], $sysFile);
                    $jsonData = $mamDatasheet->getProperty('data');
                    $decodedData = json_decode($jsonData, true);
                    if (!isset($decodedData['Attributes'])) {
                        continue;
                    }
                    $languageAttribute = ContentservAttributes::extractAttributeFieldValueByName($decodedData['Attributes'], 'Language', 'FormattedValue');
                    if (empty($languageAttribute)) {
                        continue;
                    }

                    $translatedFileLanguages = str_replace("\n", '/', $languageAttribute);
                    $datasheets[] = [
                        'url' => $mamDatasheet->getPublicUrl(),
                        'lang' => $translatedFileLanguages,
                        'size' => $mamDatasheet->getSize()
                    ];
                }
            }
        }
        return $datasheets;
    }

    /**
     * @param int $attributeId
     * @param array $attributeData
     */
    public function updateAttribute($attributeId, $attributeData)
    {
        $data = $this->getData();
        if (isset($data['Attributes'])) {
            $found = false;
            foreach ($data['Attributes'] as $k => $attribute) {
                if (is_numeric($attribute['ID']) && (int)$attribute['ID'] === (int)$attributeId) {
                    $data['Attributes'][$k] = $attributeData;
                    $found = true;
                }
            }
            if (!$found) {
                $data['Attributes'][] = $attributeData;
            }
        }
        if (isset($data['GroupedAttributes'])) {
            foreach ($data['GroupedAttributes'] as $k => $groupedAttribute) {
                foreach ($groupedAttribute as $i => $attribute) {
                    if (is_numeric($attribute['ID']) && (int)$attribute['ID'] === (int)$attributeId) {
                        $data['GroupedAttributes'][$k][$i] = $attributeData;
                    }
                }
            }
        }
        $this->setData(json_encode($data));
    }

    /**
     * @param array $jsonAr
     * @return void
     */
    public function fromJson($jsonAr)
    {
        foreach ($jsonAr as $field => $value) {
            switch ($field) {
                case 'Label':
                    $this->label = $value;
                    break;
                case 'ID':
                    $this->contentServId = (int)$value;
                    break;
                case 'SAPID':
                    $this->sapId = (int)$value;
                    break;
            }
        }
        $this->data = json_encode($jsonAr);
    }

    /**
     * Add deserialized data array
     *
     * @param array $array (by reference)
     * @return void
     */
    protected function addToAttributesArray(&$array)
    {
        $data = $this->getData();
        $array['contentServViewId'] = $this->getCsViewIds();
        $array['data'] = $data;
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     * @return void
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getCsViews()
    {
        if (!$this->csViews instanceof ObjectStorage) {
            $this->csViews = new ObjectStorage();
        }
        return $this->csViews;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $csViews
     */
    public function setCsViews($csViews)
    {
        $this->csViews = $csViews;
    }

    /**
     * Adds a CsView
     *
     * @param CsView $csView
     * @return void
     */
    public function addView(CsView $csView)
    {
        $this->getCsViews()->attach($csView);
    }

    /**
     * Get contentServ ID of views
     *
     */
    private function getCsViewIds()
    {
        $contentServIds = [];
        /** @var CsView $csView */
        foreach ($this->getCsViews() as $csView) {
            $contentServIds[] = $csView->getContentServId();
        }
        return $contentServIds;
    }

    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }
}
