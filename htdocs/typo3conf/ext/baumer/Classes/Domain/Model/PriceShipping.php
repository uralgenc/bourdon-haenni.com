<?php
namespace Baumer\Baumer\Domain\Model;

use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class PriceShipping
 *
 * @package Baumer\Baumer\Domain\Model
 */
class PriceShipping extends AbstractEntity implements \JsonSerializable
{

    use ToArrayTrait;
    /**
     * @var string
     */
    protected $productCode;
    /**
     * @var string
     */
    protected $sapId;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var double
     */
    protected $price;
    /**
     * @var string
     */
    protected $leadTime;

    /**
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @param string $productCode
     * @return void
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;
    }

    /**
     * @return string
     */
    public function getSapId()
    {
        return $this->sapId;
    }

    /**
     * @param string $sapId
     * @return void
     */
    public function setSapId($sapId)
    {
        $this->sapId = $sapId;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getLeadTime()
    {
        return $this->leadTime;
    }

    /**
     * @param string $leadTime
     * @return void
     */
    public function setLeadTime($leadTime)
    {
        $this->leadTime = $leadTime;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'productCode' => $this->productCode,
            'sapId' => $this->sapId,
            'price' => $this->price,
            'leadTime' => $this->leadTime
        ];
    }
}
