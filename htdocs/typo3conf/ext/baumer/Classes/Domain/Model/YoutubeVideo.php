<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace Baumer\Baumer\Domain\Model;

use Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Class YoutubeVideo
 *
 * @package Baumer\Baumer\Domain\Model
 */
class YoutubeVideo extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    use ToArrayTrait;

    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var string
     */
    protected $videoId;
    /**
     * @var string
     */
    protected $thumbnailUrl;
    /**
     * @var string
     */
    protected $playlistId;
    /**
     * @var string
     */
    protected $language;
    /**
     * @var \DateTime
     */
    protected $publishedAt;
    /**
     * @var integer
     */
    protected $viewCount;
    /**
     * @var string
     */
    protected $duration;

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     *
     * @return void
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }
    /**
     * categories
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
     */
    protected $categories;

    /**
     * construction
     */
    public function __construct()
    {
        $this->initializeObjectStorages();
    }

    /**
     * initialize object storages
     */
    public function initializeObjectStorages()
    {
        $this->categories = new ObjectStorage();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories
     * @return void
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
     * @return void
     */
    public function addCategory($category)
    {
        $this->categories->attach($category);
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     *
     * @return void
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @return string
     */
    public function getThumbnailUrl()
    {
        return $this->thumbnailUrl;
    }

    /**
     * @param string $thumbnailUrl
     *
     * @return void
     */
    public function setThumbnailUrl($thumbnailUrl)
    {
        $this->thumbnailUrl = $thumbnailUrl;
    }

    /**
     * @return string
     */
    public function getPlaylistId()
    {
        return $this->playlistId;
    }

    /**
     * @param string $playlistId
     *
     * @return void
     */
    public function setPlaylistId($playlistId)
    {
        $this->playlistId = $playlistId;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     *
     * @return void
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     *
     * @return void
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return int
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }

    /**
     * @param int $viewCount
     *
     * @return void
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;
    }

    /**
     * Add to object array (for JSON)
     *
     * @return array
     */
    protected function addToAttributesArray(&$array)
    {
        $array['publishedAt'] = $this->getPublishedAt()->getTimestamp()*1000;
        $array['categories'] = [];
        $categories = $this->getCategories();
        /** @var \TYPO3\CMS\Extbase\Domain\Model\Category $category */
        foreach ($categories as $category) {
            $array['categories'][] = $category->getTitle();
        }
        $di = new \DateInterval($this->getDuration());
        $duration = '';
        if ($di->h > 0) {
            $duration .= $di->h.':';
        }
        $duration = $duration . str_pad($di->i, 2, '0', STR_PAD_LEFT) . ':' . str_pad($di->s, 2, '0', STR_PAD_LEFT);
        $array['duration'] = $duration;
    }
}
