<?php
namespace Baumer\Baumer\Service;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class EvalancheService
 *
 * @package Baumer\Baumer\Service
 */
class EvalancheService
{

    /**
     * @var \Baumer\Baumer\Service\EvalancheService
     */
    protected $curlService;

    /**
     * @var string
     */
    protected $xmlRpcUrl = '';

    /**
     * @var array
     */
    protected $settings;

    /**
     * @var array
     */
    private $params;

    public function __construct($settings)
    {
        $this->curlService = new CurlService();
        $this->curlService->setHeader('Content-Type', 'text/xml');

        $this->settings = $settings;

        $this->xmlRpcUrl = $this->settings['xmlRpcUrl'];
        $this->params = $this->settings['auth.'];
    }

    /**
     * Parse array to XMLRPC request and makes a RPC call via cURL
     *
     * @param $method string
     * @param array|NULL $params
     *
     * @return array
     * @throws \Exception
     */
    public function call($method, $params = null)
    {
        $post = \xmlrpc_encode_request($method, $params, ['output_type' => 'xml', 'encoding' => 'UTF-8', 'escaping' => 'markup']);

        $response = \xmlrpc_decode($this->curlService->post($this->xmlRpcUrl, $post), 'UTF-8');

        if (is_array($response) && xmlrpc_is_fault($response)) {
            throw new \Exception($response['faultString'], $response['faultCode']);
        }

        return $response;
    }

    /**
     * Update an user profile
     *
     * @param array $fields
     * @param string $email
     * @param integer $poolId
     * @param integer $profileId
     * @return mixed
     */
    public function updateProfile($fields, $email = '', $poolId = 0, $profileId = 0)
    {
        return $this->doProfileUpdate($poolId, $fields, $email, $profileId);
    }

    /**
     * Return a list of all pools
     *
     * @return array
     */
    protected function getPools()
    {
        $params = array_merge($this->params, [
            'mandator_id' => 0
        ]);
        $method = 'getPools';

        return $this->call($method, $params);
    }

    /**
     * Return a specific pool
     *
     * @param string $poolId
     *
     * @return array
     */
    protected function getPoolAttributes($poolId)
    {
        $params = [$this->params, $poolId];
        $method = 'getPoolAttributes';

        return $this->call($method, $params);
    }

    /**
     * Check whether an e-mail address already exists in the Evalache database
     *
     * @param string $email
     * @param integer $poolId
     *
     * @return array
     */
    protected function doProfileFindByEmail($email, $poolId)
    {
        $params = [$this->params, $poolId, ['EMAIL' => $email]];
        $method = 'doProfileFindBy';

        return $this->call($method, $params);
    }

    /**
     * Create or update a profile
     *
     * @param string $poolId
     * @param array $fields
     * @param string $email
     * @param integer $profileId
     *
     * @return array
     */
    protected function doProfileUpdate($poolId, array $fields, $email = '', $profileId = 0)
    {
        // Before creating a new user, check if they are already registered
        if ($profileId == 0) {
            $response = $this->doProfileFindByEmail($email, $poolId);
            if (count($response) > 0 && strlen($email) > 0) {
                $profileId = end($response)['PROFILEID'];
            }
        }

        $params = [$this->params, $poolId, $profileId, $fields];
        $method = 'doProfileUpdate';

        return $this->call($method, $params);
    }

    /**
     * Check whether the Evalanche service is up
     *
     * @throws \Exception
     */
    protected function checkService()
    {
        $response = $this->call('isAlive', null);

        if (is_array($response) && xmlrpc_is_fault($response)) {
            throw new \Exception($response['faultString'], $response['faultCode']);
        }
    }

    /**
     * Check whether the given user has any newsletter subscriptions
     *
     * @param string $email
     * @param integer $poolId
     * @param integer $sourceId
     *
     * @return array
     */
    public function checkProfileStatus($email, $poolId, $sourceId = null)
    {
        $profile = $this->doProfileFindByEmail($email, $poolId);
        $profileStatus = [
            'status' => 'newProfile',
            'profile' => null,
            'contact_source' => $this->settings['contact_source.'][0],
            'source' => [$sourceId],
        ];
        if (is_array($profile) && count($profile) > 0) {
            $profile = end($profile);
            $permission = $profile['RAW_PERMISSION'];
            $contactSource = $profile['RAW_KONTAKTQUELLE'];
            $profileStatus['profile'] = $profile;
            switch ($permission) {
                case 0:
                    $profileStatus['status'] = 'existingProfileNoNewsletter';
                    break;
                case 1:
                    $profileStatus['status'] = 'registeredProfileApprovedNewsletter';
                    break;
            }

            // Source is a multiple value field, add possible new values
            $source = GeneralUtility::trimExplode('|', $profile['RAW_SOURCE'], true);
            if ($sourceId !== null && !in_array($sourceId, $source)) {
                $source[] = $sourceId;
            }
            $profileStatus['source'] = $source;

            // Set source when CRM or WEB/CRM is already set
            if ($contactSource == $this->settings['contact_source.'][1] || $contactSource == $this->settings['contact_source.'][2]) {
                $profileStatus['contact_source'] = $this->settings['contact_source.'][2];
            }
        }

        return $profileStatus;
    }
}
