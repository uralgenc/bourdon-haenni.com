<?php
namespace Baumer\Baumer\Service;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions GmbH & Co. KG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Error\Http\ServiceUnavailableException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * EncowayService
 * @author Hans Höchtl <hhoechtl@1drop.de>
 */
class EncowayService
{

    const MIME_TYPE_JSON = 'application/json';
    const MIME_TYPE_GUI_JSON = 'application/vnd.encoway.conf.src.gui+json';
    const MIME_TYPE_STATUS_JSON = 'application/vnd.encoway.conf.status+json';
    const MIME_OCTET_STREAM = 'application/octet-stream';
    const MIME_TYPE_OPERATION_JSON = 'application/vnd.encoway.conf.operation+json';

    /**
     * Internal ConfigurationId specific to the service
     *
     * @var string
     */
    protected $configurationId;

    /**
     * Internal articleName
     *
     * @var string
     */
    protected $articleName;

    /**
     * @var CurlService
     */
    protected $curlService;

    /**
     * @var string
     */
    protected $configurationsEndpoint;
    /**
     * @var string
     */
    protected $typekeyEndpoint;
    /**
     * @var string
     */
    protected $variantEndpoint;

    /**
     * Initialize the service with the language
     *
     * @param array $settings
     */
    public function __construct($settings)
    {
        $this->settings = $settings;
        $this->curlService = GeneralUtility::makeInstance(CurlService::class);
        $this->curlService->setJsonAssoc(false);
        $this->curlService->setUserAgent(null);
        $this->curlService->setOpt(CURLOPT_CONNECTTIMEOUT, $this->settings['connectiontimeout']);
        $this->curlService->setHeader('Accept-Language', $this->settings['lang']);

        $apiBase = 'http://' . $this->settings['host'] . $this->settings['basePath'];
        $this->configurationsEndpoint = $apiBase . $this->settings['endpoints']['initializeConfigurations'];
        $this->setGetConfigurationsEndpoint = $apiBase . $this->settings['endpoints']['setGetConfigurations'];
        $this->typekeyEndpoint = $apiBase . $this->settings['endpoints']['typekey'];
        $this->variantEndpoint = $apiBase . $this->settings['endpoints']['variant'];
    }

    /**
     * @param string $articleName
     * @throws \Exception
     */
    public function initializeConfiguration($articleName)
    {
        $this->articleName = $articleName;
        if (strlen($articleName) < 18) {
            $articleName = str_pad($articleName, 18, '0', STR_PAD_LEFT);
        }
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_JSON);
        /** @var \stdClass $response */
        $response = $this->curlService->post($this->configurationsEndpoint, json_encode(['articleName' => $articleName]));
        if ($response === false) {
            throw new ServiceUnavailableException('Configurator not available', 503);
        } elseif (property_exists($response, 'message')) {
            throw new \Exception($response->message, 404);
        } elseif (property_exists($response, 'configurationStarted') && $response->configurationStarted == true) {
            $this->configurationId = $response->configurationId;
        } else {
            throw new \Exception('Unknown response from encoway: ' . $this->curlService->http_error_message, 500);
        }
    }

    /**
     * Drop the current configuration and start fresh
     *
     * @throws \Exception
     */
    public function reset()
    {
        $this->cancelConfiguration();
        $this->curlService = new CurlService();
        $this->curlService->setUserAgent(null);
        $this->curlService->setHeader('Accept-Language', $this->settings['lang']);
        $this->initializeConfiguration($this->articleName);
    }

    /**
     * Cancel the current active configuration session
     *
     * @return integer Status code
     */
    public function cancelConfiguration()
    {
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_JSON);
        $this->curlService->delete($this->configurationsEndpoint . $this->configurationId);
        $this->configurationId = null;
        return $this->curlService->http_status_code;
    }

    /**
     * Save the configuration as a binary file
     *
     * @return string|null Content of the file
     * @throws \ErrorException
     */
    public function saveConfiguration()
    {
        $this->curlService->unsetHeader('Content-Type');
        $this->curlService->setHeader('Accept', self::MIME_OCTET_STREAM);
        $response = $this->curlService->get($this->configurationsEndpoint . $this->configurationId);
        if ($this->curlService->http_status_code != 200) {
            $response = $this->curlService->http_status_code;
        }
        return $response;
    }

    /**
     * Load the configuration from binary file
     *
     * @param string $configData
     *
     * @return int|null
     */
    public function loadConfiguration($configData)
    {
        $this->curlService->setHeader('Content-Type', self::MIME_OCTET_STREAM);
        $this->curlService->unsetHeader('Accept');
        $response = $this->curlService->post($this->configurationsEndpoint . $this->configurationId, $configData);
        if ($this->curlService->http_status_code != 200) {
            $response = $this->curlService->http_status_code;
        }
        return $response;
    }

    /**
     * Get all containers and parameters
     *
     * @param boolean $onlyContainers don't output parameters, only containers
     * @param boolean $neutral request neutral language
     *
     * @throws \ErrorException
     * @return integer|array array containing containers with parameters or server status code if error occurred
     */
    public function getParameters($onlyContainers = false, $neutral = false)
    {
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_GUI_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_GUI_JSON);
        $additionalParams = '?';
        if ($onlyContainers) {
            $additionalParams .= 'parameterMappingType=NONE';
        }
        if ($neutral) {
            $this->curlService->setHeader('Accept-Language', 'neutral');
        }
        $response = json_decode($this->curlService->get($this->configurationsEndpoint . $this->configurationId . $additionalParams), true);
        if ($this->curlService->http_status_code != 200) {
            $response = $this->curlService->http_status_code;
        } else {
            $response = $response['rootContainer']['children'];
        }
        return $response;
    }

    /**
     * Get only containers
     *
     * @return integer|array array containing containers or server status code if error occurred
     */
    public function getContainers()
    {
        return $this->getParameters(true);
    }

    /**
     * Get the parameters of a single container
     *
     * @param string $containerId
     *
     * @return integer|\stdClass Container object or server status code if error occurred
     * @throws \ErrorException
     */
    public function getContainer($containerId)
    {
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_JSON);
        $response = $this->curlService->get($this->setGetConfigurationsEndpoint . $this->configurationId . '/containers/' . $containerId);
        if ($this->curlService->http_status_code != 200) {
            $response = $this->curlService->http_status_code;
        }
        return $response;
    }

    /**
     * Get a single parameter and it's values
     *
     * @param string $parameterId
     *
     * @return \stdClass|integer object containing parameter or server status code if error occurred
     * @throws \ErrorException
     */
    public function getParameter($parameterId)
    {
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_JSON);
        $response = $this->curlService->get($this->setGetConfigurationsEndpoint . $this->configurationId . '/parameters/' . $parameterId);
        if ($this->curlService->http_status_code != 200) {
            $response = $this->curlService->http_status_code;
        }
        return $response;
    }

    /**
     * Select a value for a parameter
     *
     * @param string $parameterId
     * @param string $value
     *
     * @return array response
     */
    public function setParameter($parameterId, $value)
    {
        $response = $this->executeSetParameter($parameterId, $value);
        if ($response['status'] !== 'SUCCESS') {
            // let's try again and this time force the value (thus allowing conflict resolution)
            // Please note that setting a multivalue parameter value is not possible with force == true. Therefore, we
            // need this two step procedure.
            $response = $this->executeSetParameter($parameterId, $value, true);
        };

        return $response;
    }

    /**
     * PUT/POST the set parameter method to the Encoway API
     * PUT is set, POST is forceSet
     *
     * @param $parameterId
     * @param $value
     * @param bool $force
     * @return array
     */
    private function executeSetParameter($parameterId, $value, $force = false)
    {
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_JSON);
        $this->curlService->setOpt(CURLOPT_URL, $this->setGetConfigurationsEndpoint . $this->configurationId . '/parameters/' . $parameterId);
        $this->curlService->setOpt(CURLOPT_CUSTOMREQUEST, $force ? 'POST' : 'PUT');
        $this->curlService->setOpt(CURLOPT_POSTFIELDS, json_encode(['value' => $value]));
        $response = $this->curlService->execute();

        return (array) $response;
    }

    /**
     * Unset a previously set parameter value
     *
     * @param string $parameterId
     * @return int status code of request
     */
    public function unsetParameter($parameterId)
    {
        $this->curlService->unsetHeader('Content-Type');
        $this->curlService->setHeader('Accept', self::MIME_TYPE_JSON);
        $this->curlService->delete($this->setGetConfigurationsEndpoint . $this->configurationId . '/parameters/' . $parameterId);
        return $this->curlService->http_status_code;
    }

    /**
     * Get the status of the current configuration (used for heart beat)
     *
     * @return integer status code of heart beat
     * @throws \ErrorException
     */
    public function getStatus()
    {
        $this->curlService->unsetHeader('Content-Type');
        $this->curlService->setHeader('Accept', self::MIME_TYPE_STATUS_JSON);
        $this->curlService->get($this->configurationsEndpoint . $this->configurationId);
        return $this->curlService->http_status_code;
    }

    /**
     * Get the typekey of the current configuration state (e.g. MEX5-##2.##)
     *
     * @return int|string
     * @throws \ErrorException
     */
    public function getTypekey()
    {
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_GUI_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_GUI_JSON);
        $response = $this->curlService->get($this->typekeyEndpoint . $this->configurationId);
        if ($this->curlService->http_status_code != 200) {
            $response = $this->curlService->http_status_code;
        } else {
            $response = trim($response, '"');
        }
        return $response;
    }

    /**
     * Get the variant key of the finished configuration (if available).
     *
     * @return int|string
     * @throws \ErrorException
     */
    public function getVariant()
    {
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_GUI_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_GUI_JSON);
        $response = $this->curlService->get($this->variantEndpoint . $this->configurationId);
        if ($this->curlService->http_status_code != 200) {
            $response = $this->curlService->http_status_code;
        } else {
            $response = trim($response, '"');
            $response = substr($response, -8);
        }
        return $response;
    }

    /**
     * alias for getStatus
     *
     * @return integer status code of heart beat
     */
    public function heartBeat()
    {
        return $this->getStatus();
    }


    /**
     * @return string
     */
    public function getConfigurationId()
    {
        return $this->configurationId;
    }

    /**
     * @param string $configurationId
     *
     * @return void
     */
    public function setConfigurationId($configurationId)
    {
        $this->configurationId = $configurationId;
    }

    /**
     * @param bool $confirmOperation
     * @return bool
     */
    public function setConfirmOperation($confirmOperation)
    {
        $value = $confirmOperation ? 'ACCEPT_ENFORCEMENT' : 'DECLINE_ENFORCEMENT';
        $this->curlService->setHeader('Content-Type', self::MIME_TYPE_OPERATION_JSON);
        $this->curlService->setHeader('Accept', self::MIME_TYPE_JSON);
        $this->curlService->setOpt(CURLOPT_URL, $this->configurationsEndpoint . $this->configurationId);
        $this->curlService->setOpt(CURLOPT_CUSTOMREQUEST, 'POST');
        $this->curlService->setOpt(CURLOPT_POSTFIELDS, json_encode(['operationKey' => $value]));
        $response = $this->curlService->execute();
        return $response->status == 'SUCCESS' ? true : false;
    }
}
