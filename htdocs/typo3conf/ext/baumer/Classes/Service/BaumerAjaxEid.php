<?php
/***************************************************************
 * Copyright notice
 *
 * 2015 Martin R. Krause <martin.r.krause@gmx.de>
 * All rights reserved
 *
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/*
 * EId Script
 *
 * Sends request to AjaxDispatcher
 *
 * To make a Ajax request to an Extbase controller build make a GET (!) call to the URL
 *
 * 		var url = 'index.php?eID=ajax';
 *
 * Add parameters this way:
 *
 * 		var arguments = {
 * 			extension : 'Baumer', // this is optional and defaults to "Baumer"
 * 			plugin : 'ProductList',
 * 			controller: 'ProductList',
 * 			action: 'deletedLineItem',
 * 			arguments : {
 * 				// argument become arguments to controller action
 * 				argumentName : 'argumentValue
 * 			}
 * 		};
 *
 * 		// here an Angular JS example with $http object:
 * 		$http.get(url, $.param(arguments)...
 **/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Utility\EidUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

// Init TSFE and other FE stuff
EidUtility::initTCA();
/** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $GLOBALS['TSFE'] */
$GLOBALS['TSFE'] = GeneralUtility::makeInstance(TypoScriptFrontendController::class, $GLOBALS['TYPO3_CONF_VARS'], 0, 0);
$GLOBALS['TSFE']->tmpl = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\TypoScript\TemplateService');
$GLOBALS['TSFE']->tmpl->init();
$GLOBALS['TSFE']->initFEuser();
$GLOBALS['TSFE']->initUserGroups();
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->settingLanguage();
$GLOBALS['TSFE']->newCObj();
$GLOBALS['TSFE']->set_no_cache();



/** @var \TYPO3\CMS\Core\Core\Bootstrap $bootstrap */
// $bootstrap = \TYPO3\CMS\Core\Core\Bootstrap::getInstance()->configure();

/** @var \Baumer\Baumer\Service\AjaxDispatcher $dispatcher */
$dispatcher = GeneralUtility::makeInstance('Baumer\\Baumer\\Service\\AjaxDispatcher');

echo $dispatcher->initCallArguments()->dispatch();
