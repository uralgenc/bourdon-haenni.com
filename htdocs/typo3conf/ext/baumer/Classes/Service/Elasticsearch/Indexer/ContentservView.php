<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Service\Elasticsearch\Indexer;

use Baumer\Baumer\Domain\Model\CsViewRecord;
use Baumer\Baumer\Domain\Model\CsView;
use Baumer\Baumer\Service\Elasticsearch\Indexer;
use Baumer\Baumer\Utility\TsfeUtil;
use Elastica\Document;
use Elastica\Query\Term as TermQuery;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class ContentservView extends Indexer
{
    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRepository
     * @inject
     */
    protected $csViewRepository;
    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRecordRepository
     * @inject
     */
    protected $csViewRecordRepository;
    /**
     * @var \Baumer\Baumer\Service\Contentserv\ReferenceResolution
     * @inject
     */
    protected $referenceResolution;
    /**
     * @var \Baumer\Baumer\Service\ExtbaseForceLanguage
     * @inject
     */
    protected $extbaseForceLanguageService;
    /**
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     * @inject
     */
    protected $uriBuilder;

    /**
     * @param int $contentServClass
     * @param array $storagePids
     * @param string $indexSuffix
     */
    public function indexRecordsByViewClass($contentServClass, $storagePids, $indexSuffix = '')
    {
        /** @var $pageSpecificQuerySettings Typo3QuerySettings */
        $pageSpecificQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $pageSpecificQuerySettings->setRespectStoragePage(true);
        $pageSpecificQuerySettings->setStoragePageIds($storagePids);
        $this->csViewRepository->setDefaultQuerySettings($pageSpecificQuerySettings);
        $views = $this->csViewRepository->findByClass($contentServClass);
        /** @var CsView $view */
        foreach ($views as $view) {
            $this->indexViewRecords($view, $indexSuffix);
        }
    }

    /**
     * Delete the give csViewRecord form
     * all language indizes.
     *
     * @param CsViewRecord $csViewRecord
     */
    public function deleteViewRecord($csViewRecord)
    {
        $esClient = $this->esService->getEsClient();
        foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $langId => $indexName) {
            $esIndex = $esClient->getIndex($indexName);
            $selectorType = $esIndex->getType(CsView::$CLASS_TYPE_MAPPING[CsView::CLASS_SELECTOR]);
            $selectorType->deleteById($csViewRecord->getUid());
            $configuratorType = $esIndex->getType(CsView::$CLASS_TYPE_MAPPING[CsView::CLASS_CONFIGURATOR]);
            $configuratorType->deleteById($csViewRecord->getUid());
            $esIndex->refresh();
        }
    }

    /**
     * Index (Add/Update) a single CsViewRecord
     *
     * @param CsViewRecord $csViewRecord
     * @param string $indexSuffix
     */
    public function indexViewRecord(CsViewRecord $csViewRecord, $indexSuffix = '')
    {
        $esClient = $this->esService->getEsClient();
        $document = null;
        foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $langId => $indexName) {
            $esIndex = $esClient->getIndex($indexName . $indexSuffix);
            if (!$esIndex->exists()) {
                return;
            }
            /** @var $pageSpecificQuerySettings Typo3QuerySettings */
            $pageSpecificQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
            $pageSpecificQuerySettings->setRespectStoragePage(true);
            $pageSpecificQuerySettings->setStoragePageIds([$csViewRecord->getPid()]);
            $this->csViewRecordRepository->setDefaultQuerySettings($pageSpecificQuerySettings);
            // This forces the extbase repositories to read language overlays
            $this->extbaseForceLanguageService->setOverrideLanguage(true);
            $this->extbaseForceLanguageService->setLanguageUid($langId);
            /** @var CsViewRecord $csViewRecord */
            $csViewRecord = $this->csViewRecordRepository->findByUid($csViewRecord->getUid());
            if ($csViewRecord->getUid() > 0 && strlen($csViewRecord->getLabel()) > 0) {
                $csViewRecord = $this->referenceResolution->resolveFileReferences($csViewRecord);
                $data = $csViewRecord->toArray(false);
                if (!empty($data)) {
                    $document = new Document($csViewRecord->getUid(), $data);
                    $targetUri = $this->buildUriForRecord($csViewRecord, $langId);
                    if (!empty($targetUri)) {
                        $document->set('url', $targetUri);
                    }
                }
            }
            if ($document instanceof Document) {
                $selectorType = $esIndex->getType(CsView::$CLASS_TYPE_MAPPING[CsView::CLASS_SELECTOR]);
                if (!empty($document)) {
                    $selectorType->addDocument($document);
                }
                $configuratorType = $esIndex->getType(CsView::$CLASS_TYPE_MAPPING[CsView::CLASS_CONFIGURATOR]);
                if (!empty($document)) {
                    $configuratorType->addDocument($document);
                    $esIndex->refresh();
                }
                $esIndex->refresh();
            }
        }

        $this->extbaseForceLanguageService->setOverrideLanguage(false);
    }


    /**
     * Index CsViewRecords by a given uid of the owning CsView
     *
     * @param CsView $view
     * @param string $indexSuffix
     */
    public function indexViewRecords(CsView $view, $indexSuffix = '')
    {
        $esClient = $this->esService->getEsClient();
        foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $langId => $indexName) {
            $esIndex = $esClient->getIndex($indexName . $indexSuffix);
            if (!$esIndex->exists()) {
                return;
            }
            // This forces the extbase repositories to read language overlays
            $this->extbaseForceLanguageService->setOverrideLanguage(true);
            $this->extbaseForceLanguageService->setLanguageUid($langId);
            /** @var CsView $view */
            $documents = [];
            /** @var \Baumer\Baumer\Domain\Model\CsViewRecord $viewRecord */
            foreach ($view->getRecords() as $viewRecord) {
                if ($viewRecord->getUid() > 0) {
                    $viewRecord = $this->referenceResolution->resolveFileReferences($viewRecord);
                    $data = $viewRecord->toArray(false);
                    if (!empty($data)) {
                        $document = new Document($viewRecord->getUid(), $data);
                        $targetUri = $this->buildUriForRecord($viewRecord, $langId);
                        if (!empty($targetUri)) {
                            $document->set('url', $targetUri);
                        }
                        $documents[] = $document;
                    }
                }
            }
            $selectorType = $esIndex->getType(CsView::$CLASS_TYPE_MAPPING[$view->getClass()]);
            if (!empty($documents)) {
                $selectorType->addDocuments($documents);
                $esIndex->refresh();
            }

            $this->extbaseForceLanguageService->setOverrideLanguage(false);
        }
    }

    /**
     * Generate the target URL for the given Record.
     * The created URL depends on the record:
     *    - Configurator page if SapId (KMAT) is set
     *    - Selector of Sub-Products, if current record is a folder
     *    - Product details page (same as configurator), but not displaying Encoway (as no SapId is set)
     *
     * @param CsViewRecord $viewRecord
     * @param int $languageUid
     * @return string
     */
    public function buildUriForRecord(CsViewRecord $viewRecord, $languageUid)
    {
        $rootPid = 1;
        if (isset($this->settings['_rootPid'])) {
            $rootPid = $this->settings['_rootPid'];
        }
        if (!(isset($GLOBALS['TSFE']) && $GLOBALS['TSFE'] instanceof TypoScriptFrontendController) || (int)$GLOBALS['TSFE']->id !== (int)$rootPid) {
            TsfeUtil::initializeTsfe($rootPid, 0, false);
        }
        // If the record has a SapID (KMAT) he jump to the configurator
        if (!empty($viewRecord->getSapId())) {
            $this->uriBuilder->reset()
                ->setTargetPageUid($this->settings['csViewPageUid'])
                ->setArguments(['L' => $languageUid])
                ->uriFor('configurator', ['csViewRecord' => $viewRecord], 'CsView', 'Baumer', 'CsView');
            $recordUri = $this->uriBuilder->buildFrontendUri();
        } else {
            // If the record is a folder with filter attributes (indicator is that a view has been created during import),
            // something like MEX => (MEX5, MEX7, ...), we go to the sub-selector of the grouped family product
            $subSelectorView = $this->csViewRepository->findOneByContentServId($viewRecord->getContentServId());
            if ($viewRecord->isFolder() && $subSelectorView instanceof CsView) {
                $this->uriBuilder->reset()
                    ->setTargetPageUid($this->settings['csViewPageUid'])
                    ->setArguments(['L' => $languageUid])
                    ->uriFor('selector', ['csView' => $subSelectorView], 'CsView', 'Baumer', 'CsView');
                $recordUri = $this->uriBuilder->buildFrontendUri();
            } else {
                // We redirect to the configurator (just product details => configurator not displayed because KMAT is missing)
                $this->uriBuilder->reset()
                    ->setTargetPageUid($this->settings['csViewPageUid'])
                    ->setArguments(['L' => $languageUid])
                    ->uriFor('configurator', ['csViewRecord' => $viewRecord], 'CsView', 'Baumer',
                        'CsView');
                $recordUri = $this->uriBuilder->buildFrontendUri();
            }
        }

        return $recordUri;
    }
}
