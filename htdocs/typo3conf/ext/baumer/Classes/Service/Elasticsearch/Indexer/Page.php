<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Service\Elasticsearch\Indexer;

use Baumer\Baumer\Service\Elasticsearch\Indexer;
use Baumer\Baumer\Service\Elasticsearch\Site;
use Baumer\Baumer\Utility\HtmlContentExtractor;
use Baumer\Baumer\Utility\TsfeUtil;
use FluidTYPO3\Vhs\Service\PageSelectService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use Elastica\Document as ElasticaDocument;

class Page extends Indexer
{
    /**
     * @var ContentObjectRenderer
     */
    protected $cObj;
    /**
     * @var Site
     */
    protected $site;
    /**
     * @var PageSelectService
     */
    protected $pageSelectService;
    /**
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     * @inject
     */
    protected $uriBuilder;

    public function initializeObject()
    {
        parent::initializeObject();
        $this->cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $this->pageSelectService = $this->objectManager->get(PageSelectService::class);
    }


    /**
     * Index pages beyond a defined root page
     *
     * @param int $rootPid
     * @param string $indexSuffix
     */
    public function indexPages($rootPid, $indexSuffix = '')
    {
        // todo: refactor and reactivate to make page index work.
        if (false) {
            $esClient = $this->esService->getEsClient();
            $this->site = GeneralUtility::makeInstance(Site::class, $rootPid);
            $pages = $this->site->getPages();
            foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $langId => $indexName) {
                $esIndex = $esClient->getIndex($indexName . $indexSuffix);
                $esType = $esIndex->getType('PAGE');

                $pageTreeAvailableInLanguage = !$this->pageSelectService->hidePageForLanguageUid($rootPid, $langId);
                if (!$pageTreeAvailableInLanguage) {
                    continue;
                }

                TsfeUtil::initializeTsfe($rootPid, $langId);
                foreach ($pages as $page) {
                    $document = new ElasticaDocument($page);
                    $pageRow = $this->pageSelectService->getPage($page);
                    /** @var HtmlContentExtractor $contentExtractor */
                    $contentExtractor = GeneralUtility::makeInstance(
                        HtmlContentExtractor::class,
                        $this->getRenderedPageHtml($page, $langId)
                    );
                    $pageRow = $this->dropUnusedColumnsFromPage($pageRow);
                    $pageRow['content'] = $contentExtractor->getIndexableContent();
                    $pageRow['url'] = $this->buildUriForPage($page);
                    $document->setData($pageRow);
                    $esType->addDocument($document);
                }
            }
        }
    }

    /**
     * Build the page URL
     *
     * @param int $pageUid
     * @return string
     */
    protected function buildUriForPage($pageUid)
    {
        return $this->uriBuilder->reset()->setTargetPageUid($pageUid)->buildFrontendUri();
    }

    /**
     * @param array $pageRow
     * @return array
     */
    protected function dropUnusedColumnsFromPage($pageRow)
    {
        $usedColumns = [
            'uid',
            'pid',
            'tstamp',
            'sorting',
            'deleted',
            'hidden',
            'crdate',
            'title',
            'doktype',
            'starttime',
            'endtime',
            'keywords',
            'description',
            'abstract',
            'nav_title',
            'navigation_text',
            'content',
        ];
        foreach ($pageRow as $col => $val) {
            if (!in_array($col, $usedColumns)) {
                unset($pageRow[$col]);
            }
        }
        return $pageRow;
    }

    /**
     * Render every tt_content element on the page via TypoScript and return the HTML
     *
     * @param int $pageUid
     * @param int $sysLanguageUid
     * @return string
     */
    protected function getRenderedPageHtml($pageUid, $sysLanguageUid = 0)
    {
        $contentRecords = BackendUtility::getRecordsByField(
            'tt_content',
            'pid',
            $pageUid,
            $this->buildTcaWhereClause('tt_content')
        );
        if (!$contentRecords) {
            return '';
        }
        $pageHtmlContent = '';
        foreach ($contentRecords as $contentRecord) {
            if ($sysLanguageUid > 0) {
                $localizedRecord = BackendUtility::getRecordLocalization(
                    'tt_content',
                    $contentRecord['uid'],
                    $sysLanguageUid
                );
                if ($localizedRecord) {
                    $pageHtmlContent .= $this->renderRecord($localizedRecord);
                }
            } else {
                $pageHtmlContent .= $this->renderRecord($contentRecord);
            }
        }

        return $pageHtmlContent;
    }


    /**
     * @see \FluidTYPO3\Vhs\ViewHelpers\Content\AbstractContentViewHelper::renderRecord
     * @param array $row
     * @return string|NULL
     */
    protected function renderRecord(array $row)
    {
        $html = '';
        /** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe */
        $tsfe = $GLOBALS['TSFE'];
        if (0 < $tsfe->recordRegister['tt_content:' . $row['uid']]) {
            return null;
        }
        $conf = [
            'tables' => 'tt_content',
            'source' => $row['uid'],
            'dontCheckPid' => 1,
        ];
        $parent = $tsfe->currentRecord;
        // If the currentRecord is set, we register, that this record has invoked this function.
        // It's should not be allowed to do this again then!!
        if (false === empty($parent)) {
            ++$tsfe->recordRegister[$parent];
        }
        try {
            $html = $tsfe->cObj->cObjGetSingle('RECORDS', $conf);
        } catch (\Exception $e) {
        }

        $tsfe->currentRecord = $parent;
        if (false === empty($parent)) {
            --$tsfe->recordRegister[$parent];
        }

        return $html;
    }

    /**
     * @see \ApacheSolrForTypo3\Solr\IndexQueue\Initializer\AbstractInitializer::buildPagesClause
     */
    protected function buildPagesClause()
    {
        $pages = $this->site->getPages();

        return 'uid IN(' . implode(',', $pages) . ')';
    }

    /**
     * @see \ApacheSolrForTypo3\Solr\IndexQueue\Initializer\AbstractInitializer::buildTcaWhereClause
     * @param string $type
     * @return string Conditions to only add indexable items to the Index Queue
     */
    protected function buildTcaWhereClause($type = 'pages')
    {
        $tcaWhereClause = '';
        $conditions = [];

        if (isset($GLOBALS['TCA'][$type]['ctrl']['delete'])) {
            $conditions['delete'] = $GLOBALS['TCA'][$type]['ctrl']['delete'] . ' = 0';
        }

        if (isset($GLOBALS['TCA'][$type]['ctrl']['enablecolumns']['disabled'])) {
            $conditions['disabled'] = $GLOBALS['TCA'][$type]['ctrl']['enablecolumns']['disabled'] . ' = 0';
        }

        if (isset($GLOBALS['TCA'][$type]['ctrl']['enablecolumns']['endtime'])) {
            // only include records with a future endtime or default value (0)
            $endTimeFieldName = $GLOBALS['TCA'][$type]['ctrl']['enablecolumns']['endtime'];
            $conditions['endtime'] = '(' . $endTimeFieldName . ' > ' . time() . ' OR ' . $endTimeFieldName . ' = 0)';
        }

        if (BackendUtility::isTableLocalizable($type)) {
            $conditions['languageField'] = [
                $GLOBALS['TCA'][$type]['ctrl']['languageField'] . ' = 0',
                // default language
                $GLOBALS['TCA'][$type]['ctrl']['languageField'] . ' = -1'
                // all languages
            ];
            if (isset($GLOBALS['TCA'][$type]['ctrl']['transOrigPointerField'])) {
                $conditions['languageField'][] = $GLOBALS['TCA'][$type]['ctrl']['transOrigPointerField'] .
                                                 ' = 0'; // translations without original language source
            }
            $conditions['languageField'] = '(' . implode(' OR ', $conditions['languageField']) . ')';
        }

        if (!empty($GLOBALS['TCA'][$type]['ctrl']['versioningWS'])) {
            // versioning is enabled for this table: exclude draft workspace records
            $conditions['versioningWS'] = 'pid != -1';
        }

        if (count($conditions)) {
            $tcaWhereClause = ' AND ' . implode(' AND ', $conditions);
        }

        return $tcaWhereClause;
    }

    /**
     * @see \ApacheSolrForTypo3\Solr\IndexQueue\Initializer\AbstractInitializer::buildUserWhereClause
     * @return string Conditions to add items to the Index Queue based on TypoScript configuration
     */
    protected function buildUserWhereClause()
    {
        $condition = '';
        if (isset($this->settings['pageIndex']['additionalWhereClause'])) {
            $condition = ' AND ' . $this->settings['pageIndex']['additionalWhereClause'];
        }

        return $condition;
    }
}
