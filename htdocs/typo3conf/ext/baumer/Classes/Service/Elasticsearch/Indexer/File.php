<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Service\Elasticsearch\Indexer;

use Baumer\Baumer\Service\Elasticsearch\Indexer;
use Elastica\Document as ElasticaDocument;
use TYPO3\CMS\Backend\Configuration\TranslationConfigurationProvider;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class File extends Indexer
{

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceFactory
     * @inject
     */
    protected $resourceFactory;

    /**
     * @var array
     */
    protected $systemLanguages;

    /**
     * Index all sys_file records that are imported by MAM into elasticsearch.
     *
     * @param string $indexSuffix
     * @param int $modifiedAfterTstamp
     */
    public function indexFiles($indexSuffix = '', $modifiedAfterTstamp = 0)
    {
        $esClient = $this->esService->getEsClient();

        $sysFiles = $this->db->exec_SELECTgetRows(
            // SELECT
            'sf.*, GROUP_CONCAT(sflr.trans_mam_file_id) as translatedMamIds',
            // FROM
            'sys_file sf LEFT JOIN sys_file_lang_ref sflr ON sflr.mam_file_id = sf.mam_file_id',
            // WHERE
            ($modifiedAfterTstamp > 0 ? 'sf.tstamp >= ' . $modifiedAfterTstamp . ' AND ' : '') .
            'sf.mam_file_id > 0 AND sf.identifier NOT LIKE "%secure%" AND NOT sf.missing AND sf.mime_type NOT LIKE "image%"',
            // GROUP BY
            'sf.mam_file_id');
        foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $langId => $indexName) {
            $esIndex = $esClient->getIndex($indexName . $indexSuffix);
            $esType = $esIndex->getType('FILE');
            if (!$esIndex->exists()) {
                return;
            }
            $groupedFileMamIds = [];

            foreach ($sysFiles as $sysFile) {
                $mamFileId = (int)$sysFile['mam_file_id'];
                $translatedMamFileIds = GeneralUtility::intExplode(',', $sysFile['translatedMamIds'], true);

                // This is used to prevent duplicates in the index as a file might have translations
                if (!in_array($mamFileId, $groupedFileMamIds)) {
                    $groupedFileMamIds[] = $mamFileId;
                    $groupedFileMamIds = array_merge($groupedFileMamIds, $translatedMamFileIds);

                    $this->indexFile($sysFile, $langId, $esType);
                }
            }
        }
    }

    /**
     * Import a single sys_file record array in a given sys_language_uid
     * into a given elasticsearch type (index is defined by type).
     *
     * @param array $sysFile
     * @param int $sysLanguageUid
     * @param \Elastica\Type $esType
     * @throws \TYPO3\CMS\Core\Resource\Exception\FileDoesNotExistException
     */
    public function indexFile($sysFile, $sysLanguageUid, $esType)
    {
        if (empty($this->systemLanguages)) {
            /** @var TranslationConfigurationProvider $translationConfigurationProvider */
            $translationConfigurationProvider = GeneralUtility::makeInstance(TranslationConfigurationProvider::class);
            $this->systemLanguages = $translationConfigurationProvider->getSystemLanguages();
        }
        $sysFile = $this->checkMamFileGroupByLanguage($sysFile, $sysLanguageUid);
        $document = new ElasticaDocument($sysFile['uid'], $sysFile);
        $file = $this->resourceFactory->getFileObject($sysFile['uid'], $sysFile);

        //TODO: switch to EXT:media thumbnail generator
        $thumbnailUrl = $this->generateThumbnail($file);
        if ($thumbnailUrl !== false) {
            $document->set('thumbnail', '/' . $thumbnailUrl);
        }
        if (!empty($sysFile['tika_content'])) {
            $document->set('content', $sysFile['tika_content']);
        }
        unset($sysFile['tika_content']);

        // Add download link for current file
        $fileLanguages = GeneralUtility::intExplode(',', $sysFile['file_language'], true);
        if (!empty($fileLanguages)) {
            $document->set('file_languages', $fileLanguages);
        }
        // Get the localized metadata for the current file
        $localizedMetadata = $this->getLocalizedMetadataBySysFileUidAndSysLanguageUid($sysFile, $sysLanguageUid);
        // Fallback if no translated metadata is available (for consistency in index => data must be present)
        if (empty($localizedMetadata)) {
            $localizedMetadata = $this->getLocalizedMetadataBySysFileUidAndSysLanguageUid($sysFile, 0);
        }

        // Build download URLs for translated versions of this file
        $downloads = $this->buildDownloadUrls($sysFile);
        // Add the current file language to the download list
        $fileLanguage = $this->extractAttribute($localizedMetadata['data']['FileAttributes'], 'Language', 'FormattedValue');
        $downloads[$fileLanguage] = [
            'lang' => $fileLanguage,
            'size' => $file->getSize(),
            'url' => '/fileadmin/mam' . $file->getIdentifier()
        ];
        $document->set('downloads', $downloads);


        if ($localizedMetadata && isset($localizedMetadata['data']) && is_array($localizedMetadata['data'])) {
            // We add a pseudo attribute to make all file languages filterable
            $localizedMetadata['data']['FileAttributes']['LANG'] = [
                'Name' => 'Downloadable languages',
                'TranslatedName' => $this->extractAttribute($localizedMetadata['data']['FileAttributes'], 'Language', 'TranslatedName'),
                'Type' => 'valuerange',
                'Value' => null,
                'FormattedValue' => []
            ];
            foreach ($downloads as $download) {
                $localizedMetadata['data']['FileAttributes']['LANG']['FormattedValue'][] = $download['lang'];
            }

            $document->set('data', $localizedMetadata['data']);
            if (isset($localizedMetadata['data']['FileAttributes'])) {
                $document->set('title', $this->extractAttribute($localizedMetadata['data']['FileAttributes'], 'Title', 'FormattedValue'));
            }
        }

        // Get the sys_category records related with the sys_file_metadata
        if (!empty($localizedMetadata)) {
            $categoryUids = $this->getSysCategoryUidsBySysFileMetadataUid($localizedMetadata['uid']);
            $document->set('categories', $categoryUids);
        }

        // Because of the extracted text, we add every document to reduce memory usage
        $esType->addDocument($document);
    }

    /**
     * @param \TYPO3\CMS\Core\Resource\File $file
     * @return mixed
     */
    protected function generateThumbnail($file)
    {
        if (GeneralUtility::inList($GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            strtolower($file->getExtension()))
        ) {
            $processedFile = $file->process(ProcessedFile::CONTEXT_IMAGECROPSCALEMASK, [
                'width' => '150',
                'fileExtension' => 'jpg',
                'additionalParameters' => '-density 175 -quality 75 -flatten'
            ]);
            $thumbnailUrl = parse_url($processedFile->getPublicUrl(), PHP_URL_PATH);

            return $thumbnailUrl;
        }

        return false;
    }

    /**
     * @param array $sysFile
     * @return array
     */
    protected function buildDownloadUrls($sysFile)
    {
        $downloads = [];

        // Get Links for translated files through sys_file_lang_ref
        $translatedFiles = $this->db->exec_SELECTgetRows('sfm.data, sf.identifier, sf.size',
            'sys_file sf, sys_file_lang_ref sflr, sys_file_metadata sfm',
            'sf.mam_file_id = sflr.trans_mam_file_id AND ' .
            'sfm.file = sf.uid AND sfm.sys_language_uid = 0 AND sflr.mam_file_id = ' . $sysFile['mam_file_id']);
        foreach ($translatedFiles as $translatedFile) {
            $decodedData = json_decode($translatedFile['data'], true);
            if (!isset($decodedData['Attributes'])) {
                continue;
            }
            $languageAttribute = $this->extractAttribute($decodedData['Attributes'], 'Language', 'FormattedValue');
            if (empty($languageAttribute)) {
                continue;
            }

            $translatedFileLanguages = str_replace("\n", '/', $languageAttribute);
            $downloads[$translatedFileLanguages] = [
                'lang' => $translatedFileLanguages,
                'size' => $translatedFile['size'],
                'url' => '/fileadmin/mam' . $translatedFile['identifier']
            ];
        }

        return $downloads;
    }

    /**
     * @param array $attributes
     * @param string $name
     * @param string $field
     * @return string
     */
    protected function extractAttribute($attributes, $name, $field = 'Value')
    {
        if (is_array($attributes) && !empty($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute['Name'] == $name) {
                    return $attribute[$field];
                    break;
                }
            }
        }

        return false;
    }

    /**
     * @param int $sysFileMetadataUid
     * @return array
     */
    protected function getSysCategoryUidsBySysFileMetadataUid($sysFileMetadataUid)
    {
        $categories = $this->db->exec_SELECTgetRows('uid_local', 'sys_category_record_mm',
            'tablenames = "sys_file_metadata" AND fieldname="categories" AND uid_foreign = ' . $sysFileMetadataUid);
        $categoryUids = array_map(function ($row) {
            return (int)$row['uid_local'];
        }, $categories);

        return $categoryUids;
    }

    /**
     * @param array $sysFile
     * @param int $sysLanguageUid
     * @return array
     */
    protected function getLocalizedMetadataBySysFileUidAndSysLanguageUid($sysFile, $sysLanguageUid)
    {
        $localizedMetadata = $this->db->exec_SELECTgetSingleRow('uid, data', 'sys_file_metadata',
            'file = ' . $sysFile['uid'] . ' AND sys_language_uid = ' . $sysLanguageUid);
        if (is_array($localizedMetadata) && !empty($localizedMetadata['data'])) {
            $data = json_decode($localizedMetadata['data'], true);
            if (isset($data['Attributes'])) {
                $identifiedAttributes = [];
                foreach ($data['Attributes'] as $attr) {
                    $attrKey = $attr['ID'];
                    unset($attr['ID']);
                    $identifiedAttributes[$attrKey] = $attr;
                }
                $data['FileAttributes'] = $identifiedAttributes;
                unset($data['Attributes']);
                $localizedMetadata['data'] = $data;
            }

            return $localizedMetadata;
        }

        return false;
    }

    /**
     * Given a record from sys_file this will check wether there is a
     * translated version of this file (according to MAM) and return that record.
     * If this is not the case it will return the original record
     *
     * @param array $sysFile
     * @param int $sysLanguageUid
     * @return array
     */
    protected function checkMamFileGroupByLanguage($sysFile, $sysLanguageUid)
    {
        if (empty($sysFile['mam_file_id']) || empty($sysFile['file_language'])) {
            return $sysFile;
        }
        $fileLanguages = GeneralUtility::intExplode(',', $sysFile['file_language']);
        foreach ($fileLanguages as $fileLanguage) {
            if ($fileLanguage === (int)$sysLanguageUid) {
                return $sysFile;
            }
        }
        $alternativeFiles = $this->db->exec_SELECTgetRows(
            'sf.*',
            'sys_file sf',
            'sf.mam_file_id IN (SELECT sflr.trans_mam_file_id FROM sys_file_lang_ref sflr WHERE sflr.mam_file_id = ' . $sysFile['mam_file_id'] . ')'
        );
        foreach ($alternativeFiles as $alternativeFile) {
            $fileLanguages = GeneralUtility::intExplode(',', $alternativeFile['file_language']);
            foreach ($fileLanguages as $fileLanguage) {
                if ($fileLanguage === (int)$sysLanguageUid) {
                    return $alternativeFile;
                }
            }
        }
        return $sysFile;
    }
}
