<?php
namespace Baumer\Baumer\Service\Elasticsearch;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * Class Connection
 *
 * @package Baumer\Baumer\Service\Elasticsearch
 */
class Connection implements SingletonInterface
{

    /**
     * @var ConfigurationManager
     */
    protected $configurationManager;

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * Dependency injection of the command controller
     *
     * @param ConfigurationManager $configurationManager
     * @return void
     */
    public function injectConfigurationManager(ConfigurationManager $configurationManager)
    {
        $this->configurationManager = $configurationManager;
        $this->settings = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'baumer'
        );
    }

    /**
     * @param string $host
     * @param int $port
     * @param string $transport
     * @return \Elastica\Client|null
     */
    public function getEsClient($host = '', $port = 9200, $transport = 'http')
    {
        $client = null;
        $esConfiguration = [
            'host' => $host,
            'port' => $port,
            'transport' => $transport
        ];

        if (empty($host)) {
            $esConfiguration = $this->settings['elasticsearch']['connection'];
        }

        $client = new \Elastica\Client($esConfiguration);
        return $client;
    }

    /**
     * @param string $type
     * @return string JSON with elasticsearch config (to be used in a view)
     */
    public function getElasticConfig($type)
    {
        $esSettings = $this->settings['elasticsearch']['connection'];
        $proxyPath = $esSettings['proxyPath'];
        return [
            'ES_HOST' => ['host' => [
                'path' => $proxyPath,
                'host' => '',
                'port' => 80,
                'protocol' => 'http'
            ]],
            'ES_INDEX' => $esSettings['index'],
            'ES_TYPE' => $type
            ];
    }

    /**
     * @param string $index ES index name
     * @param string $host ES host
     * @param int $port ES port
     * @param string $transport ES transport e.g. 'http'
     *
     * @return \Elastica\Index
     */
    public function getEsIndex($index, $host = '', $port = 9200, $transport = 'http')
    {
        $client = $this->getEsClient($host, $port, $transport);
        $index = $client->getIndex($index);

        return $index;
    }
}
