<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Service\Elasticsearch;

use Baumer\Baumer\Domain\Model\CsView;
use Elastica\Type\Mapping;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class Indexer
{
    /**
     * @var array
     */
    protected $settings;
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;
    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Connection
     * @inject
     */
    protected $esService;
    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager;
    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection $db
     */
    protected $db;

    /**
     * Initialize the indexer
     */
    public function initializeObject()
    {
        $this->settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'baumer');
        $this->db = &$GLOBALS['TYPO3_DB'];
    }

    /**
     * @param array $settings
     */
    public function injectSettings(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param string $indexSuffix
     */
    public function ensureIndexAndTypeMappingsAllLanguages($indexSuffix = '')
    {
        foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $indexName) {
            $this->ensureIndexAndTypeMappings($indexName, $indexSuffix);
        }
    }

    /**
     * Reformats a given settings TS array with correct types and formats
     *
     * @param array $settings
     * @return array
     */
    public static function tsToSettings($settings)
    {
        array_walk_recursive($settings, function (&$v) {
            if ($v === 'false') {
                $v = false;
            }
            if ($v === 'true') {
                $v = true;
            }
            if (strpos($v, ',') !== false) {
                $v = GeneralUtility::trimExplode(',', $v, true);
            }
        });
        if (isset($settings['analysis']) && isset($settings['analysis']['analyzer']) &&
            is_array($settings['analysis']['analyzer'])
        ) {
            foreach ($settings['analysis']['analyzer'] as &$analyzer) {
                if (isset($analyzer['filter']) && is_array($analyzer['filter'])) {
                    $analyzer['filter'] = array_values($analyzer['filter']);
                }
            }
        }

        return $settings;
    }

    /**
     * Creates the index if it doesn't exist and ensures the type mappings
     * for the Contentserv classes.
     *
     * @param string $indexName
     * @param string $indexSuffix
     */
    public function ensureIndexAndTypeMappings($indexName, $indexSuffix = '')
    {
        $esIndex = $this->esService->getEsIndex($indexName . $indexSuffix);
        if (!$esIndex->exists()) {
            //TODO build settings from TS
            $indexSettings = isset($this->settings['elasticsearch']['settings'][$indexName]) ? self::tsToSettings($this->settings['elasticsearch']['settings'][$indexName]) : [];
            $esIndex->create(['settings' => $indexSettings]);
        }
        // Ensure a type mapping for every CsView class
        foreach (CsView::$CLASS_TYPE_MAPPING as $classId => $esTypeName) {
            $classType = $esIndex->getType($esTypeName);
            $this->ensureTypeMapping($indexName, $classType);
        }
        // Ensure type mapping for downloads
        $downloadType = $esIndex->getType('FILE');
        $this->ensureTypeMapping($indexName, $downloadType);
        // Ensure type mapping for pages
        $pageType = $esIndex->getType('PAGE');
        $this->ensureTypeMapping($indexName, $pageType);
    }

    /**
     * The elasticsearch mappings are defined in TS for
     * every elasticsearch-type that is needed.
     * As the mapping must be reproduced for every language index,
     * the $indexName must be specified.
     *
     * @param string $indexName
     * @param string $type
     * @return array
     */
    protected function getMappingForIndex($indexName, $type)
    {
        $mapping = $this->settings['elasticsearch']['mappings'][$indexName][$type];
        $mapping = $this->castMappingArrayFromTs($mapping);

        return $mapping;
    }

    /**
     * As TS only uses strings, but elasticsearch type mapping
     * is sensitive to types, we must convert some boolean values
     * and probably other things recursively in the mapping.
     *
     * @param array $mapping
     * @return array
     */
    protected function castMappingArrayFromTs($mapping)
    {
        foreach ($mapping as $key => $value) {
            if (is_array($value)) {
                $mapping[$key] = $this->castMappingArrayFromTs($value);
            } elseif (in_array($key, ['store', 'include_in_all', 'payloads'])) {
                $mapping[$key] = (strtolower($value) === 'true' || $value === '1');
            } elseif ($key === 'copy_to') {
                $mapping[$key] = GeneralUtility::trimExplode(',', $value, true);
            }
        }

        return $mapping;
    }


    /**
     * Iterates over all language indizes and changes the alias to the
     * given $indexSuffix.
     *
     * @param string $indexSuffix
     */
    public function ensureAliasForAllIndizesWithSuffix($indexSuffix)
    {
        foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $indexName) {
            $this->ensureAliasForIndex($indexName . $indexSuffix, $indexName);
        }
    }

    /**
     * Remove any existing alias pointing to $indexAlias or delete
     * an index, that occupies the alias name and let the alias
     * point to $indexName.
     *
     * @param string $indexName
     * @param string $indexAlias
     */
    public function ensureAliasForIndex($indexName, $indexAlias)
    {
        $esClient = $this->esService->getEsClient();
        $esClient->getStatus()->refresh();
        // Ensure that the alias doesn't exist as an index. If so drop that one
        if ($esClient->getStatus()->indexExists($indexAlias)) {
            $esClient->getIndex($indexAlias)->delete();
        }
        // The overwrite param only works of the alias already exists
        $overwriteExistingAlias = $esClient->getStatus()->aliasExists($indexAlias);
        $esIndex = $esClient->getIndex($indexName);
        $esIndex->addAlias($indexAlias, $overwriteExistingAlias);
    }

    /**
     * Given an index alias like "de" this method looks for
     * all indizes, that are timestamp indizes like de_144329299
     * and deletes all those indizes, that are not currently set as
     * alias.
     *
     * @param string $alias
     */
    public function cleanOldIndizesForAlias($alias)
    {
        $esClient = $this->esService->getEsClient();
        $existingIndizes = $esClient->getStatus()->getIndexNames();
        $esClient->getStatus()->refresh();
        if ($esClient->getStatus()->aliasExists($alias)) {
            $activeIndizes = array_map(function ($esIndex) {
                /** @noinspection PhpUndefinedMethodInspection */
                return $esIndex->getName();
            }, $esClient->getStatus()->getIndicesWithAlias($alias));

            foreach ($existingIndizes as $existingIndex) {
                // Look for all indizes that match the alias scheme and are not in use
                if (strpos($existingIndex, $alias . '_') === 0 && !in_array($existingIndex, $activeIndizes)) {
                    $esClient->getIndex($existingIndex)->delete();
                }
            }
        }
    }

    /**
     * @param string $indexName
     * @param \Elastica\Type $esType
     */
    protected function ensureTypeMapping($indexName, $esType)
    {
        if (!$esType->exists() || $esType->count() === 0) {
            $mapping = $this->getMappingForIndex($indexName, $esType->getName());
            $esMapping = new Mapping($esType, $mapping['properties']);
            if (isset($mapping['dynamic_templates'])) {
                $dynamicTemplates = [];
                foreach ($mapping['dynamic_templates'] as $templateName => $dynamic_template) {
                    $dynamicTemplates[] = [$templateName => $dynamic_template];
                }
                $esMapping->setParam('dynamic_templates', $dynamicTemplates);
            }
            if (isset($mapping['_source'])) {
                $excludes = GeneralUtility::trimExplode(',', $mapping['_source']['excludes'], true);
                $esMapping->setSource(['excludes' => $excludes]);
            }
            $esType->setMapping($esMapping);
        }
    }
}
