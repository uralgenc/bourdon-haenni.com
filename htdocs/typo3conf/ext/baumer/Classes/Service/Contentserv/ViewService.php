<?php
namespace Baumer\Baumer\Service\Contentserv;

/***************************************************************
     *  Copyright notice
     *
     *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *  A copy is found in the textfile GPL.txt and important notices to the license
     *  from the author is found in LICENSE.txt distributed with these scripts.
     *
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Class ViewImportService
 *
 * @package Baumer\Baumer\Service\Contentserv
 */
class ViewService
{

    /**
     * @var \Baumer\Baumer\Service\Contentserv\CsConnection
     * @inject
     */
    protected $csConnection;

    /**
     * @var string
     */
    protected $auth;

    /**
     * @throws \Baumer\Baumer\Exception\AuthenticationFailedException
     * @throws \Baumer\Baumer\Exception\UnsupportedCommandException
     * @return void
     */
    public function initializeObject()
    {
        $this->auth = $this->csConnection->authenticate('View');
    }

    /**
     * Returns the class definition of a view/folder
     *
     * @param int $folderId
     * @return array|null
     */
    public function getClassOfView($folderId)
    {
        $response = $this->csConnection->runCommand('admin/rest/view/' . $folderId, $this->auth);
        if (isset($response['View'])) {
            $classId = $response['View']['Class']['ID'];
            $response = $this->csConnection->runCommand('admin/rest/view/class/' . $classId, $this->auth);
            if (isset($response['Class'])) {
                return $response['Class'];
            }
        }
        return null;
    }

    /**
     * @param int $attributeId
     * @param mixed $lang
     * @param string $expand
     * @return null
     */
    public function getAttribute($attributeId, $lang = 1, $expand = '*')
    {
        $response = $this->csConnection->runCommand('admin/rest/view/attribute/' . $attributeId,
            $this->auth,
            ['lang' => $lang]);
        if (isset($response['Attribute'])) {
            return $response['Attribute'];
        }
        return null;
    }

    /**
     * Returns a list of attributeIDs used in the class of the specified folder.
     *
     * @param int $folderId
     * @return array
     */
    public function getAttributesOfClassOfView($folderId)
    {
        $attributeIds = [];
        $class = $this->getClassOfView($folderId);
        if (!empty($class) && isset($class['Attributes'])) {
            foreach ($class['Attributes'] as $attribute) {
                $attributeIds[] = $attribute['ID'];
            }
        }
        return $attributeIds;
    }

    /**
     * @param int $folderId
     * @param int $lang
     * @param string $expand
     * @return array|null
     */
    public function getTree($folderId, $lang = 1, $expand = '*')
    {
        $response = $this->csConnection->runCommand('admin/rest/view/tree/' . $folderId,
            $this->auth,
            ['expand' => $expand, 'lang' => $lang]);
        if (isset($response['View'])) {
            return $response['View'];
        }
        return null;
    }

    /**
     * Get all children of a given view ID (up to 10 levels deep)
     *
     * @param int $folderId
     * @param int $lang
     * @param string $expand
     * @return array|null
     */
    public function getChildren($folderId, $lang = 1, $expand = '*')
    {
        $response = $this->csConnection->runCommand('admin/rest/view/children/' . $folderId,
            $this->auth,
            ['expand' => $expand, 'lang' => $lang, 'depth' => 10, 'limit' => 1000, 'includedfolder=false']);
        if (isset($response['Views'])) {
            return $response['Views'];
        }
        return [];
    }

    /**
     * @param $viewId
     * @param int $lang
     * @param string $expand
     * @return null|array
     */
    public function getView($viewId, $lang = 1, $expand = '*')
    {
        $response = $this->csConnection->runCommand('admin/rest/view/' . $viewId,
            $this->auth,
            ['expand' => $expand, 'lang' => $lang]);
        if (isset($response['View'])) {
            return $response['View'];
        }
        return null;
    }

    /**
     * @param $folderId
     * @param \DateTime $from
     * @param \DateTime $to
     * @return null
     */
    public function getChangedViews($folderId, $from = null, $to = null)
    {
        if (!$from) {
            $from = new \DateTime();
            $from->sub(new \DateInterval('P1D'));
        }
        $parameters['begin'] = $from->format('Y-m-d');

        if ($to) {
            $parameters['end'] = $to->format('Y-m-d H:i:s');
        }

        $response = $this->csConnection->runCommand('admin/rest/view/changes/' . $folderId,
            $this->auth,
            $parameters);
        if (isset($response['Views'])) {
            return $response['Views'];
        }
        return null;
    }

    /**
     * @param int $folderId
     * @param int $lang
     * @param string $expand
     * @return array
     */
    public function getLeafViews($folderId, $lang = 1, $expand = '*')
    {
        $viewTree = $this->getTree($folderId, $lang, $expand);
        $leafs = $this->getLeafs($viewTree);
        return $leafs;
    }

    /**
     * Execute CS Workflow action for view
     *
     * @param $itemId
     * @param $actionId
     * @param int $lang
     * @return bool
     */
    public function updateWorkflow($itemId, $actionId, $lang = 1)
    {
        $response = $this->csConnection->runCommand('admin/rest/view/executeworkflowaction/' . $itemId . '/' . $actionId,
            $this->auth,
            ['lang' => $lang],
            'POST'
        );
        return ($response['Action'] && $response['Action']['Success']);
    }

    /**
     * @param array $tree
     * @return array
     */
    protected function getLeafs($tree)
    {
        $leafs = [];
        if (isset($tree['IsFolder'])) {
            if ((int)$tree['IsFolder'] === 0) {
                $leafs[] = $tree;
            } elseif (isset($tree['Views'])) {
                foreach ($tree['Views'] as $subTree) {
                    $leafs = array_merge($leafs, $this->getLeafs($subTree));
                }
            }
        }
        return $leafs;
    }
}
