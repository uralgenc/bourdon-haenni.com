<?php
namespace Baumer\Baumer\Service\Contentserv;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class ViewImportService
 *
 * @package Baumer\Baumer\Service\Contentserv
 */
class ProductService
{

    /**
     * @var \Baumer\Baumer\Service\Contentserv\CsConnection
     * @inject
     */
    protected $csConnection;
    /**
     * @var string
     */
    protected $auth;
    /**
     * @var array
     */
    protected $settings;

    /**
     * @throws \Baumer\Baumer\Exception\AuthenticationFailedException
     * @throws \Baumer\Baumer\Exception\UnsupportedCommandException
     * @return void
     */
    public function initializeObject()
    {
        $this->auth = $this->csConnection->authenticate('PIM');
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        /** @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
        $this->settings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'baumer', 'tx_baumer');
    }

    /**
     * @param int $parentId
     * @param array $attributeIds
     * @param int $lang
     * @param array $attributeIdGroups
     * @return array|null
     */
    public function searchChildren($parentId, $attributeIds = [], $lang = 1, $attributeIdGroups = [])
    {
        $articleIds = [];
        $response = $this->csConnection->runCommand('admin/rest/product/tree/' . $parentId, $this->auth, ['limit' => 500]);
        if (isset($response['Product']) && isset($response['Product']['Products'])) {
            foreach ($response['Product']['Products'] as $product) {
                $articleIds[] = $product['ID'];
            }
            return $this->search($articleIds, $attributeIds, $lang, $attributeIdGroups);
        }
        return null;
    }


    /**
     * Load a single product with all attributes from ContentServ
     *
     * @param $contentServId
     * @param int|string $lang
     * @return array|bool
     */
    public function getProduct($contentServId, $lang = 1)
    {
        $response = $this->csConnection->runCommand('admin/rest/product/' . $contentServId, $this->auth, ['lang' => $lang]);
        if (isset($response['Product'])) {
            $product = $this->formatAndGroupAttributes($response['Product']);
            return $product;
        } else {
            return false;
        }
    }

    /**
     * @param array $articleIds The list of ProductIDs
     * @param array $attributeIds A list of attributeIds that should be returned with the product
     * @param int $lang Language of the fields to be returned
     * @param array $attributeIdGroups If defined will add an additional GroupedAttributes to the product
     * @return array|null An array of products or null if nothing was found
     */
    public function search($articleIds, $attributeIds = [], $lang = 1, $attributeIdGroups = [])
    {
        // Convert [1,2,3] to "PdmarticleID=1 OR PdmarticleID=2 OR PdmarticleID=3"
        $articleIds = array_map(function ($id) {
            return 'PdmarticleID=' . $id;
        }, $articleIds);
        $filter = implode(' OR ', $articleIds);

        // We always need the KMAT Attribute ID for the product
        $attributeIds[] = $this->settings['pimImport']['kmatAttributeId'];
        // Convert [1,2,3] to "Attributes.1,Attributes.2,Attributes.3"
        $attributeIds = array_map(function ($attributeId) {
            return 'Attributes.' . $attributeId;
        }, $attributeIds);
        $expand = implode(',', $attributeIds);

        $response = $this->csConnection->runCommand('admin/rest/product/search/', $this->auth,
            ['filter' => $filter, 'expand' => $expand, 'lang' => $lang, 'limit' => 500]);

        if (isset($response['Products'])) {
            // If the returned attributes should not be grouped
            if (empty($attributeIdGroups)) {
                return $response['Products'];
            }

            /*
             * We group the attributes according to the $attributeIdGroups if defined:
             * [223 => [1,2,4], 415 => [2,5,7]]
             */
            $products = $response['Products'];
            foreach ($products as $pIdx => $product) {
                $products[$pIdx] = $this->formatAndGroupAttributes($product, $attributeIdGroups);
            }
            return $products;
        }

        return null;
    }

    /**
     * Execute CS Workflow action for product
     *
     * @param $itemId
     * @param $actionId
     * @param int $lang
     * @return bool
     */
    public function updateWorkflow($itemId, $actionId, $lang = 1)
    {
        $response = $this->csConnection->runCommand('admin/rest/product/executeworkflowaction/' . $itemId . '/' . $actionId,
            $this->auth,
            ['lang' => $lang],
            'POST'
        );
        return ($response['Action'] && $response['Action']['Success']);
    }

    /**
     * Reformat and group some attributes
     *
     * @param $product
     * @param array $attributeIdGroups
     * @return mixed
     */
    private function formatAndGroupAttributes($product, $attributeIdGroups = [])
    {
        if (isset($product['Attributes'])) {
            $product['GroupedAttributes'] = [];
            foreach ($product['Attributes'] as $aIdx => $attribute) {

                // Store the SAP ID
                if ((int)$attribute['ID'] === (int)$this->settings['pimImport']['kmatAttributeId'] &&
                    intval($attribute['Value']) > 0
                ) {
                    $product['SAPID'] = $attribute['Value'];
                }

                // We split values of ValueRange attributes by newlines
                if ($attribute['Type'] === 'valuerange') {
                    $attribute['Value'] = explode(PHP_EOL, $attribute['Value']);
                    $attribute['FormattedValue'] = explode(PHP_EOL, $attribute['FormattedValue']);
                    $product['Attributes'][$aIdx] = $attribute;
                }

                $attributeGroupIds = [];
                foreach ($attributeIdGroups as $groupId => $groupAttributeIds) {
                    if (in_array((int)$attribute['ID'], $groupAttributeIds)) {
                        $attributeGroupIds[] = $groupId;
                    }
                }
                if (!empty($attributeGroupIds)) {
                    foreach ($attributeGroupIds as $attributeGroupId) {
                        if (!isset($product['GroupedAttributes'][$attributeGroupId])) {
                            $product['GroupedAttributes'][$attributeGroupId] = [];
                        }
                        $product['GroupedAttributes'][$attributeGroupId][] = $attribute;
                    }
                }
            }
        }
        return $product;
    }
}
