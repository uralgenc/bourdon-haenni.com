<?php
namespace Baumer\Baumer\Service\Contentserv;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Anja Leichsenring <maddy@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use Baumer\Baumer\Exception\AuthenticationFailedException;
use Baumer\Baumer\Exception\InvalidUrlException;
use Baumer\Baumer\Exception\UnsupportedCommandException;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * reads connection details from extension configuration
 * connects to ContentServ target system
 */
class CsConnection
{

    const MAM_ENDPOINT = 'typo3file';

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager;

    /**
     * @var \Baumer\Baumer\Service\CurlService
     * @inject
     */
    protected $curlService;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * @var \TYPO3\CMS\Core\Log\Logger
     */
    protected $logger;

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * constructor
     */
    public function __construct()
    {
        $this->logger = GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
    }

    /**
     * @return void
     */
    public function initializeObject()
    {
        $this->settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
        if (!empty($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['baumer'])) {
            $this->settings['extConf'] = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['baumer']);
            $this->baseUrl = $this->settings['extConf']['baseUrl'];
        } else {
            throw new \RuntimeException(
                'No Settings for Contentserv found, can\'t continue, please provide settings in the Extension Manager',
                1413363589);
        }
    }

    /**
     * @param string $command the command without the URL
     * @param string $authParameter the auth string provided by $this->authenticate
     * @param array $parameters further url parameter
     * @param string $method the method that should be used
     *
     * @throws \Baumer\Baumer\Exception\AuthenticationFailedException
     * @return array
     */
    public function runCommand($command, $authParameter, array $parameters = [], $method = 'GET')
    {
        $parameters = $this->addAuthParameter($authParameter, $parameters);
        return $this->fetchJson($this->buildCommand($this->baseUrl, $command), $parameters, true, $method);
    }

    /**
     * @param string $command
     * @param string $authParameter
     * @param array $parameters
     *
     * @throws InvalidUrlException
     * @throws \ErrorException
     *
     * @return array
     */
    public function getFileContent($command, $authParameter, $parameters = [])
    {
        if (GeneralUtility::isValidUrl($command)) {
            // contentserv returns a absolute path for the download of files
            $urlParts = parse_url($command);
            if (!is_array($urlParts)) {
                throw new InvalidUrlException('given command is seriously invalid');
            }
            $command = $urlParts['path'];
            $baseUrl = $urlParts['host'];
            parse_str($urlParts['query'], $parameters);
        } else {
            $baseUrl = $this->baseUrl;
        }
        $parameters = $this->addAuthParameter($authParameter, $parameters);
        return $this->curlService->get($this->buildCommand($baseUrl, $command), $parameters);
    }

    /**
     * @param string $authParameter
     * @param array $parameters
     *
     * @return array
     */
    protected function addAuthParameter($authParameter, $parameters = [])
    {
        if (!isset($parameters['CSTicketID'])) {
            $parameters['CSTicketID'] = $authParameter;
        }
        return $parameters;
    }

    /**
     * @param string $serviceName needs to be one of PIM, MAM, View, Core
     *
     * @throws \Baumer\Baumer\Exception\AuthenticationFailedException
     * @throws \Baumer\Baumer\Exception\UnsupportedCommandException
     * @return string the parameter to append to the command
     */
    public function authenticate($serviceName)
    {
        switch ($serviceName) {
            case 'PIM':
                $command = 'admin/rest/product/pin';
                break;
            case 'MAM':
                $command = 'admin/rest/' . self::MAM_ENDPOINT . '/pin';
                break;
            case 'View':
                $command = 'admin/rest/view/pin';
                break;
            case 'Core':
                $command = 'admin/rest/core/pin';
                break;
            default:
                throw new UnsupportedCommandException('command for ' . $serviceName . ' can not be determined', 1407312550);
        }

        $baseUrl = $this->settings['extConf']['baseUrl'];
        $username = $this->settings['extConf']['username'];
        $password = $this->settings['extConf']['password'];

        $parameters = [
            'ctsUser' => $username,
            'ctsPassword' => $password
        ];

        $authResult = $this->fetchJson($this->buildCommand($baseUrl, $command), $parameters, true);
        $params = [];
        parse_str($authResult['Ticket']['Parameter'], $params);
        if (is_array($params) && array_key_exists('CSTicketID', $params)) {
            $authParameter = $params['CSTicketID'];
        } else {
            throw new AuthenticationFailedException('authentication failed', 1407310596);
        }

        return $authParameter;
    }

    /**
     * fetches csv from content serv
     *
     * @return string
     */
    public function fetchValueListsCsv()
    {
        $baseUrl = $this->settings['extConf']['baseUrl'];
        $queryParams = [
            'ctsUser' => $this->settings['extConf']['username'],
            'ctsPassword' => $this->settings['extConf']['password'],
            'forward' => $this->settings['pimImport']['csvSource'],
            'excel' => 'false'
        ];

        $result = $this->fetchCsv($queryParams, $baseUrl);

        return $result;
    }

    /**
     * fetches csv from content serv
     *
     * @return string
     */
    public function fetchAttributesCsv()
    {
        $baseUrl = $this->settings['extConf']['baseUrl'];
        $queryParams = [
            'ctsUser' => $this->settings['extConf']['username'],
            'ctsPassword' => $this->settings['extConf']['password'],
            'forward' => 'core|extensions|item|dialogs|listItemconfiguration.php',
            'module' => 'pdm',
            'class' => 'Pdmarticle',
            'ItemConfigurationIsLink' => 0,
            'treeRecordId' => 'Settings~Configurations@0',
            'CSGuiListProxyFrame' => 'right',
            //'BuilderStoreActionIdIFrame' => '74ddca08fb',
            //'BuilderStoreAction' => 'csvexportmarked',
            'displayWaitingDialog' => false,
            'CSVExport' => true,
            'CSBuilderMarkAction' => '212',
            'CSBuilderMarkAllAction' => true,
        ];

        $result = $this->fetchCsv($queryParams, $baseUrl);

        return $result;
    }

    /**
     * @param $queryParams
     * @param $baseUrl
     *
     * @return string
     */
    protected function fetchCsv($queryParams, $baseUrl)
    {
        $url = $this->buildUrl($baseUrl, 'admin/forward.php', $queryParams);
        try {
            $result = $this->curlService->get($url);
        } catch (InvalidUrlException $e) {
            // TODO Handle wrong auth
            $this->logger->alert('Failed to fetch CSV from ' . $url);
            echo $e->getMessage();
        }

        return $result;
    }


    /**
     * parses csv to an array
     *
     * @return array
     */
    public function getParsedValueListCsv()
    {
        $result = $this->fetchValueListsCsv();
        $rows = GeneralUtility::trimExplode("\n", $result);
        $csv = [];
        $mapping = [];
        foreach ($rows as $rowKey => $rowValue) {
            $parsedRow = str_getcsv($rowValue, ';');
            if ($rowKey == 0) {
                $mapping = $parsedRow;
                $mapping[0] = preg_replace('/^' . pack('H*', 'EFBBBF') . '/', '', $mapping[0]);
            } else {
                $cleanRow = [];
                foreach ($parsedRow as $parsedRowKey => $parsedRowValue) {
                    $cleanRow[lcfirst(trim($mapping[$parsedRowKey]))] = $parsedRowValue;
                }
                if ($cleanRow['typeID']) {
                    $csv[$cleanRow['typeID']][] = $cleanRow;
                }
            }
        }

        return $csv;
    }

    /**
     * parses csv to an array
     *
     * @return array
     */
    public function getParsedAttributesCsv()
    {
        $result = $this->fetchAttributesCsv();
        $rows = GeneralUtility::trimExplode("\n", $result);
        $csv = [];
        //@todo find out why this return exists
        return $csv;
        $mapping = [];
        foreach ($rows as $rowKey => $rowValue) {
            $parsedRow = str_getcsv($rowValue, ';');
            if ($rowKey == 0) {
                $mapping = $parsedRow;
                $mapping[0] = preg_replace('/^' . pack('H*', 'EFBBBF') . '/', '', $mapping[0]);
            } else {
                $cleanRow = [];
                foreach ($parsedRow as $parsedRowKey => $parsedRowValue) {
                    $cleanRow[lcfirst(trim($mapping[$parsedRowKey]))] = $parsedRowValue;
                }
                if ($cleanRow['typeID']) {
                    $csv[$cleanRow['typeID']][] = $cleanRow;
                }
            }
        }

        return $csv;
    }

    /**
     * convert a baseUrl, a endpoint ( usually a webservice url and a array of
     * parameters and converts them to a URI
     *
     * @param string $baseUrl
     * @param string $endpoint
     * @param array $parameters
     *
     * @return string a complete URI
     *
     * @deprecated please use buildCommand and the curl service methods get / post,
     * they accept a data array as second argument
     *
     */
    public function buildUrl($baseUrl, $endpoint, array $parameters)
    {
        $url = $this->buildCommand($baseUrl, $endpoint);
        return (!empty($parameters)) ? $url . '?' . http_build_query($parameters) : $url;
    }

    /**
     * @param string $baseUrl
     * @param string $endpoint
     *
     * @return string
     */
    public function buildCommand($baseUrl, $endpoint)
    {
        return rtrim($baseUrl, '/') . '/' . rtrim($endpoint, '/');
    }

    /**
     * @param string $url the url to fetch
     * @param array $data a array of data to send
     * @param boolean $assoc return the fetch json as associative array
     * @param string $method the method that should be used
     *
     * @throws InvalidUrlException
     * @throws \ErrorException
     * @return array response
     */
    public function fetchJson($url, $data = [], $assoc = true, $method = 'GET')
    {
        if (GeneralUtility::isValidUrl($url)) {
            try {
                $this->curlService->setOpt(CURLOPT_CONNECTTIMEOUT, 30);
                $this->curlService->setOpt(CURLOPT_TIMEOUT, 300);
                $this->curlService->setJsonAssoc($assoc);
                if ($method === 'POST') {
                    $response = $this->curlService->post($url, $data);
                } else {
                    $response = $this->curlService->get($url, $data);
                }
            } catch (\ErrorException $e) {
                $this->logger->error($e->getMessage());
                throw $e;
            }
        } else {
            $this->logger->alert('Tried to fetch URL ' . $url . ' but the url is invalid');
            throw new InvalidUrlException('URL ' . $url . ' is not valid', 1412842883);
        }
        if ($this->curlService->error_code === CURLE_OPERATION_TIMEOUTED) {
            throw new \RuntimeException('Operation on URL ' . $url . ' timed out', 1450270699);
        }

        return $response;
    }
}
