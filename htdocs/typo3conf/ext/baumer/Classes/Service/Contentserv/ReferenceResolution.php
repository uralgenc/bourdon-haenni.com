<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Service\Contentserv;

use Baumer\Baumer\Domain\Model\CsViewRecord;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\ResourceFactory;

class ReferenceResolution
{
    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $db;
    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceStorage
     */
    protected $mamStorage;

    public function initializeObject()
    {
        $this->db = &$GLOBALS['TYPO3_DB'];
        //TODO: make contentServ storage configurable
        $storageRecord = $this->db->exec_SELECTgetSingleRow('*', 'sys_file_storage', 'uid = 3');
        $this->mamStorage = ResourceFactory::getInstance()->createStorageObject($storageRecord);
    }

    /**
     * @param int $mamFileId
     * @return null|FileInterface
     */
    public function getFileByMamFileId($mamFileId)
    {
        $file = null;
        $fileRow = $this->db->exec_SELECTgetSingleRow('identifier', 'sys_file', 'mam_file_id=' . intval($mamFileId));
        if (is_array($fileRow) && isset($fileRow['identifier'])) {
            $file = $this->mamStorage->getFile($fileRow['identifier']);
        }

        return $file;
    }

    /**
     * @param CsViewRecord $csViewRecord
     * @return CsViewRecord
     */
    public function resolveFileReferences(CsViewRecord $csViewRecord)
    {
        $fileAttributes = $csViewRecord->extractAttributesByType(CsViewRecord::ATTRIBUTE_TYPE_FILE);

        foreach ($fileAttributes as $fileAttribute) {
            if (!empty($fileAttribute['Links'])) {
                foreach ($fileAttribute['Links'] as $k => $fileLink) {
                    $mamFile = $this->getFileByMamFileId($fileLink['ID']);
                    if ($mamFile instanceof FileInterface) {
                        $fileUrl = '/' . $mamFile->getPublicUrl();
                        $fileAttribute['Links'][$k]['FormattedValue'] = $fileUrl;
                        $fileAttribute['Links'][$k]['FileSize'] = $mamFile->getSize();
                        $fileAttribute['Links'][$k]['FileExtension'] = $mamFile->getExtension();
                    }
                }
                $csViewRecord->updateAttribute($fileAttribute['ID'], $fileAttribute);
            }
        }

        return $csViewRecord;
    }
}
