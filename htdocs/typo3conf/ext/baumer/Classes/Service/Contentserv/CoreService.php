<?php
namespace Baumer\Baumer\Service\Contentserv;

/***************************************************************
     *  Copyright notice
     *
     *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *  A copy is found in the textfile GPL.txt and important notices to the license
     *  from the author is found in LICENSE.txt distributed with these scripts.
     *
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Class CoreService
 *
 * @package Baumer\Baumer\Service\Contentserv
 */
class CoreService
{

    /**
     * @var \Baumer\Baumer\Service\Contentserv\CsConnection
     * @inject
     */
    protected $csConnection;

    /**
     * @var string
     */
    protected $auth;

    /**
     * @throws \Baumer\Baumer\Exception\AuthenticationFailedException
     * @throws \Baumer\Baumer\Exception\UnsupportedCommandException
     * @return void
     */
    public function initializeObject()
    {
        $this->auth = $this->csConnection->authenticate('Core');
    }

    /**
     * @param int $valueRangeId
     * @param int $lang
     * @param string $expand
     * @return
     */
    public function getValueRange($valueRangeId, $lang = 1, $expand = '*')
    {
        $response = $this->csConnection->runCommand('admin/rest/core/valuerange/' . $valueRangeId,
            $this->auth,
            ['expand' => $expand,
            'lang' => $lang]);
        if (isset($response['Valuerange'])) {
            return $response['Valuerange'];
        }
    }
}
