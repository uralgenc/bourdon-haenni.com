<?php
namespace Baumer\Baumer\Service;

/***************************************************************
 *  Copyright notice
 *  (c) 2014 Anja Leichsenring <maddy@typovision.de>
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Baumer\Baumer\Service\Contentserv\CsConnection;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\CommandUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * handle files in local storage provided by MAM Service
 */
class FileImportService
{

    const IS_ARCHIVE = 10;

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceStorage
     */
    protected $storage;

    /**
     * @var \ApacheSolrForTypo3\Tika\Service\Extractor\TextExtractor
     * @inject
     */
    protected $textExtractor;

    /**
     * @var \Baumer\Baumer\Service\Contentserv\CsConnection
     * @inject
     */
    protected $contentServConnection;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var string
     */
    protected $authParameter;

    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $db;

    /**
     * This storage is not persisted. It's generated via API
     * to be writable
     *
     * @var \TYPO3\CMS\Core\Resource\ResourceStorage
     */
    protected $writeStorage;
    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceStorage
     */
    protected $readOnlyStorage;
    /**
     * @var int
     */
    protected $storageUid;
    /**
     * @var \Fab\Media\Index\MediaIndexer
     */
    protected $mediaIndexer;
    /**
     * @var array
     */
    protected $settings;

    /**
     * @param string $authParameter
     * @return void
     */
    public function setAuthParameter($authParameter)
    {
        $this->authParameter = $authParameter;
    }

    /**
     * @param int $storageUid
     * @return void
     */
    public function setStorageUid($storageUid)
    {
        $this->storageUid = $storageUid;
    }

    /**
     * @param array $settings
     * @return void
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return void
     */
    public function initialStorage()
    {
        $this->db = &$GLOBALS['TYPO3_DB'];
        $storageRecord = $this->db->exec_SELECTgetSingleRow('*', 'sys_file_storage', 'uid = ' . $this->storageUid);
        // Override writable permission for programmatic access
        $storageRecord['is_writable'] = '1';
        $this->writeStorage = ResourceFactory::getInstance()->createStorageObject($storageRecord);
        $this->readOnlyStorage = ResourceFactory::getInstance()->getStorageObject($this->storageUid);
        //$this->mediaIndexer = GeneralUtility::makeInstance( 'Fab\\Media\\Index\\MediaIndexer', $this->readOnlyStorage );
    }

    /**
     * @param array $folder
     * @param integer $sysLanguageUid
     * @throws \TYPO3\CMS\Core\Resource\Exception\FileOperationErrorException
     */
    public function importFolder($folder = [], $sysLanguageUid)
    {
        if (!is_array($folder) || empty($folder)) {
            throw new \RuntimeException('no folder given, no clue what to do', 1409573378);
        }

        // If neither Label nor IsFolder is set, skip this.
        if (isset($folder['Label']) && isset($folder['IsFolder'])) {
            foreach ($this->getResourcesInFolder($folder) as $resource) {
                if (!$this->isFolder($resource)) {
                    if (isset($resource['Label']) && isset($resource['IsFolder']) && isset($resource['State']['ID'])) {
                        if (!$this->fileExistsInFolder($resource) &&
                            !$this->isArchived($resource) &&
                            $resource['State']['ID'] == 8) {
                            // file does not exist, import it
                            $this->importFile($resource, $sysLanguageUid);
                        } elseif ($this->fileExistsInFolder($resource) && !$this->isArchived($resource)) {
                            // file exists, update it
                            $this->updateFile($resource, $sysLanguageUid);
                        } elseif ($this->fileExistsInFolder($resource) && $this->isArchived($resource)) {
                            // file exists and is deleted remotely, delete it
                            $file = $this->storage->getFile($resource['Label']);
                            $this->storage->deleteFile($file);
                        }
                    }
                } else {
                    if (isset($resource['File'])) {
                        $file = true;
                    } else {
                        $file = (isset($resource['Files']) ? true : false);
                    }
                    if ($file) {
                        $this->importFolder($resource, $sysLanguageUid);
                    }
                }
            }
        }
    }

    /**
     * @param array $resource
     * @return boolean
     */
    protected function isFolder($resource)
    {
        return (boolean)$resource['IsFolder'];
    }

    /**
     * @param array $folder
     * @return array
     */
    protected function getResourcesInFolder($folder)
    {
        return $folder['Files'] ? $folder['Files'] : [];
    }

    /**
     * @param array $file
     * @param integer $sysLanguageUid
     * @throws \Baumer\Baumer\Exception\InvalidUrlException
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     */
    protected function importFile($file, $sysLanguageUid)
    {
        $fileName = $this->extractAttribute($file['Attributes'], 'FileName');
        if ($fileName) {
            $downloadUrl = $this->contentServConnection
                ->getFileContent('admin/rest/' . CsConnection::MAM_ENDPOINT . '/downloadurl/' . $file['ID'], $this->authParameter);
            $data = $this->contentServConnection->getFileContent($downloadUrl, $this->authParameter);

            /** @var \Baumer\Baumer\FileUpload\DownloadedFile $downloadedFile */
            $downloadedFile = GeneralUtility::makeInstance('Baumer\\Baumer\\FileUpload\\DownloadedFile', $data, $fileName);
            $folderName = $this->getFolderName($downloadedFile->getType(), $file);
            /** @var File $fileObject */
            $fileObject = $this->writeStorage->addFile($downloadedFile->getFileWithAbsolutePath(), $this->writeStorage->getFolderInFolder($folderName, $this->writeStorage->getRootLevelFolder()), $fileName);
            $fileWithPath = PATH_site . 'fileadmin/mam' . $fileObject->getIdentifier();
            $this->convertEpsToSvg($fileWithPath, $fileObject);
            $this->setMamFields($file, $fileObject, $sysLanguageUid);
            $this->updateFileTikaContent($fileObject);
        }
    }

    /**
     * @param File $file
     */
    public function updateFileTikaContent($file)
    {
        if (ExtensionManagementUtility::isLoaded('tika')) {
            if ($this->textExtractor->canExtractText($file)) {
                try {
                    $extractedText = $this->textExtractor->extractText($file);
                    $this->db->exec_UPDATEquery('sys_file', 'uid = ' . $file->getUid(), ['tika_content' => $extractedText]);
                } catch (\Exception $e) {
                    GeneralUtility::devLog('Tika failed to read file', 'baumer', GeneralUtility::SYSLOG_SEVERITY_ERROR, ['file' => $file]);
                }
            }
        }
    }

    /**
     * @param array $file
     * @param integer $sysLanguageUid
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     */
    protected function updateFile($file, $sysLanguageUid)
    {
        /** @var File $fileObject */
        $fileObject = $this->getFileByMamFileId($this->extractAttribute($file['Attributes'], 'MamfileID'));
        $fileName = $this->extractAttribute($file['Attributes'], 'FileName');
        if ($fileName) {
            $this->ensureFilePresents($file, $fileObject);
            if ($this->extractAttribute($file['Attributes'], 'Secure') && strpos($fileObject->getIdentifier(), 'secure') === false) {
                $folderName = $this->getFolderName($fileObject->getType(), $file);
                $fileObject = $this->writeStorage->moveFile($fileObject, $this->writeStorage->getFolderInFolder($folderName, $this->writeStorage->getRootLevelFolder()));
            }
            $this->setMamFields($file, $fileObject, $sysLanguageUid);
        }
    }

    /**
     * @param int $type
     * @param array $file
     * @return string
     */
    protected function getFolderName($type, $file)
    {
        $folder =  '';
        switch ($type) {
            case File::FILETYPE_TEXT;
                $folder .= 'binary/';
                break;
            case File::FILETYPE_IMAGE;
                $folder .= 'images/';
                break;
            case File::FILETYPE_AUDIO;
                $folder .= 'audio/';
                break;
            case File::FILETYPE_VIDEO;
                $folder .= 'videos/';
                break;
            case File::FILETYPE_APPLICATION;
                $folder .= 'documents/';
                break;
        }
        if ($this->extractAttribute($file['Attributes'], 'Secure')) {
            $folder .= 'secure/';
        }

        if (!file_exists(PATH_site . 'fileadmin/mam/' . $folder) && !is_writeable(PATH_site . 'fileadmin/mam/' . $folder)) {
            mkdir(PATH_site . 'fileadmin/mam/' . $folder, 0775, true);
            chmod(PATH_site . 'fileadmin/mam/' . $folder, 0775);
        }

        return $folder;
    }

    /**
     * @param array $attributes
     * @param string $name
     * @param string $field
     * @return string
     */
    protected function extractAttribute($attributes, $name, $field = 'Value')
    {
        if (is_array($attributes) && !empty($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute['Name'] == $name) {
                    return $attribute[$field];
                    break;
                }
            }
        }

        return false;
    }

    /**
     * set the MamfileID when importing or updating the file
     * sinc
     *
     * @param array $file
     * @param File $fileObject
     * @param integer $sysLanguageUid
     */
    protected function setMamFields(array $file, File $fileObject, $sysLanguageUid)
    {
        $mamfileLanguageUids = [];
        // Mam ID and File Language
        $mamfileId = $this->extractAttribute($file['Attributes'], 'MamfileID');
        $mamfileLanguage = $this->extractAttribute($file['Attributes'], 'Language');
        if (!empty($mamfileLanguage)) {
            $mamfileLanguages = explode("\n", $mamfileLanguage);
            foreach ($mamfileLanguages as $mamfileLanguage) {
                if (array_key_exists($mamfileLanguage, $this->settings['mamImport']['attributeLanguageMapping'])) {
                    $mamfileLanguageUids[] = $this->settings['mamImport']['attributeLanguageMapping'][$mamfileLanguage];
                }
            }
        }

        if (empty($mamfileLanguageUids)) {
            $mamfileLanguageUids[] = $this->settings['mamImport']['defaultLanguage'];
        }

        $this->db->exec_UPDATEquery(
            'sys_file',
            'uid = ' . intval($fileObject->getUid()),
            [
                'mam_file_id' => $mamfileId,
                'file_language' => implode(',', $mamfileLanguageUids),
                'mam_file_size' => $this->extractAttribute($file['Attributes'], 'FileSize'),
                'tstamp' => time()
            ]
        );

        // Language Reference
        $refCount = $this->db->exec_SELECTcountRows('*', 'sys_file_lang_ref', 'mam_file_id = '.$mamfileId);
        if ($refCount <= 0) {
            $mamfileTranslations = $this->extractAttribute($file['Attributes'], 'Translation', 'Links');
            if (is_array($mamfileTranslations)) {
                foreach ($mamfileTranslations as $mamfileTranslation) {
                    $this->db->exec_INSERTquery('sys_file_lang_ref', ['mam_file_id' => $mamfileId, 'trans_mam_file_id' => $mamfileTranslation['ID']]);
                }
            }
        }

        if ($sysLanguageUid == $this->settings['mamImport']['defaultLanguage']) {
            $this->addDefaultMetaData($file, $fileObject);
        }
        $this->addLanguageOverlay($file, $fileObject, $sysLanguageUid);
    }

    /**
     * @param array $file
     * @param File $fileObject
     */
    protected function addDefaultMetaData($file, $fileObject)
    {
        // Metadata
        /** @noinspection PhpInternalEntityUsedInspection */
        $metaData = $fileObject->_getMetaData();
        $this->db->exec_UPDATEquery('sys_file_metadata', 'sys_language_uid = 0 AND uid = '.$metaData['uid'], ['title' => $file['Label'], 'data' => json_encode($file), 'height' => $this->extractAttribute($file['Attributes'], 'ImageHeight'), 'width' => $this->extractAttribute($file['Attributes'], 'ImageWidth')]);

        $this->db->exec_DELETEquery('sys_category_record_mm', 'tablenames = "sys_file_metadata" AND fieldname = "categories" AND uid_foreign = "' . $metaData['uid'] . '"');

        // Category
        foreach ($this->settings['mamImport']['mamCategoriesMapping'] as $attributeName) {
            $attribute = $this->extractAttribute($file['Attributes'], $attributeName);
            if ($attribute === null) {
                continue;
            }
            $attributeCategory = $this->db->exec_SELECTquery('uid, parent', 'sys_category', 'content_serv_id LIKE "%/'.$attribute.'" AND sys_language_uid = 0');
            if ($attributeCategory->num_rows > 0) {
                $attributeCategory = $attributeCategory->fetch_assoc();
                if ($attributeCategory['uid'] > 0) {
                    $this->db->exec_INSERTquery('sys_category_record_mm', ['tablenames' => 'sys_file_metadata', 'fieldname' => 'categories', 'uid_foreign' => $metaData['uid'], 'uid_local' => $attributeCategory['uid']]);
                }
                if ($attributeCategory['parent'] > 0) {
                    $this->db->exec_INSERTquery('sys_category_record_mm', ['tablenames' => 'sys_file_metadata', 'fieldname' => 'categories', 'uid_foreign' => $metaData['uid'], 'uid_local' => $attributeCategory['parent']]);
                }
            }
        }
    }

    /**
     * @param array $file
     * @param File $fileObject
     * @param int $sysLanguageUid
     */
    protected function addLanguageOverlay($file, $fileObject, $sysLanguageUid)
    {
        // Metadata
        /** @noinspection PhpInternalEntityUsedInspection */
        $metaData = $fileObject->_getMetaData();
        $metaDataExists = $this->db->exec_SELECTquery('*', 'sys_file_metadata', 'l10n_parent = '.$metaData['uid'].' AND sys_language_uid = '.$sysLanguageUid);
        if ($metaDataExists->num_rows <= 0) {
            $this->db->exec_INSERTquery('sys_file_metadata', ['sys_language_uid' => $sysLanguageUid, 'l10n_parent' => $metaData['uid'], 'file' => intval($fileObject->getUid()), 'title' => $file['Label'], 'data' => json_encode($file)]);
            $metaDataId = $this->db->sql_insert_id();
        } else {
            $metaDataExists = $metaDataExists->fetch_assoc();
            $this->db->exec_UPDATEquery('sys_file_metadata', 'sys_language_uid = '.$sysLanguageUid.' AND l10n_parent = '.$metaData['uid'], ['title' => $file['Label'], 'data' => json_encode($file)]);
            $metaDataId = $metaDataExists['uid'];
        }

        $this->db->exec_DELETEquery('sys_category_record_mm', 'tablenames = "sys_file_metadata" AND fieldname = "categories" AND uid_foreign = "' . $metaDataId . '"');

        // Category
        foreach ($this->settings['mamImport']['mamCategoriesMapping'] as $attributeName) {
            $attribute = $this->extractAttribute($file['Attributes'], $attributeName);
            if ($attribute === null) {
                continue;
            }
            $attributeCategory = $this->db->exec_SELECTquery('uid, parent', 'sys_category', 'content_serv_id LIKE "%/'.$attribute.'" AND l10n_parent = 0');
            if ($attributeCategory->num_rows > 0) {
                $attributeCategory = $attributeCategory->fetch_assoc();
                if ($attributeCategory['uid'] > 0) {
                    $this->db->exec_INSERTquery('sys_category_record_mm', ['tablenames' => 'sys_file_metadata', 'fieldname' => 'categories', 'uid_foreign' => $metaDataId, 'uid_local' => $attributeCategory['uid']]);
                }
                if ($attributeCategory['parent'] > 0) {
                    $this->db->exec_INSERTquery('sys_category_record_mm', ['tablenames' => 'sys_file_metadata', 'fieldname' => 'categories', 'uid_foreign' => $metaDataId, 'uid_local' => $attributeCategory['parent']]);
                }
            }
        }
    }

    /**
     * @param array $file
     * @return boolean
     */
    protected function fileExistsInFolder($file)
    {
        $result = false;
        $mamfileId = $this->extractAttribute($file['Attributes'], 'MamfileID');
        $fileObject = $this->getFileByMamFileId($mamfileId);
        if ($fileObject) {
            return true;
        }
        return $result;
    }

    /**
     * @param $mamFileId
     * @return null|\TYPO3\CMS\Core\Resource\FileInterface
     */
    protected function getFileByMamFileId($mamFileId)
    {
        $file = null;
        $fileRow = $this->db->exec_SELECTgetSingleRow('identifier', 'sys_file', 'mam_file_id=' . intval($mamFileId));
        if (is_array($fileRow) && isset($fileRow['identifier'])) {
            $file = $this->writeStorage->getFile($fileRow['identifier']);
        }

        return $file;
    }

    /**
     * @param integer $uid
     * @return null|integer
     */
    protected function getFileSizeByUid($uid)
    {
        $file = null;
        $fileRow = $this->db->exec_SELECTgetSingleRow('mam_file_size', 'sys_file', 'uid=' . intval($uid));
        if (is_array($fileRow) && isset($fileRow['mam_file_size'])) {
            return $fileRow['mam_file_size'];
        }

        return null;
    }

    /**
     * @param array $resource
     * @return boolean
     */
    protected function isArchived($resource)
    {
        return $resource['State']['ID'] == self::IS_ARCHIVE;
    }

    /**
     * @param array $file
     * @param File $fileObject
     * @throws \Baumer\Baumer\Exception\InvalidUrlException
     */
    protected function ensureFilePresents($file, &$fileObject)
    {
        $fileWithPath = PATH_site . 'fileadmin/mam' . $fileObject->getIdentifier();
        if (file_exists($fileWithPath) === false || $this->extractAttribute($file['Attributes'], 'FileSize') != $this->getFileSizeByUid($fileObject->getUid())) {
            $this->getFolderName($fileObject->getType(), $file);
            $downloadUrl = $this->contentServConnection
                    ->getFileContent('admin/rest/' . CsConnection::MAM_ENDPOINT . '/downloadurl/' . $file['ID'], $this->authParameter);
            $data = $this->contentServConnection->getFileContent($downloadUrl, $this->authParameter);
            file_put_contents($fileWithPath, $data);
            $this->db->exec_UPDATEquery(
                'sys_file',
                'uid = ' . intval($fileObject->getUid()),
                [
                    'mam_file_size' => $this->extractAttribute($file['Attributes'], 'FileSize'),
                    'tstamp' => time()
                ]
            );
            $this->updateFileTikaContent($fileObject);
            $this->convertEpsToSvg($fileWithPath, $fileObject);
            $fileObject->setMissing(false);
        }
    }

    /**
     * @param string $filename
     * @param File $fileObject
     */
    protected function convertEpsToSvg($filename, &$fileObject)
    {
        if ($fileObject->getExtension() === 'eps' && $fileObject->getMimeType() == 'application/octet-stream') {
            if (CommandUtility::checkCommand('inkscape')) {
                $filename = substr($filename, 0, -3);
                $status = CommandUtility::exec('inkscape -f ' . CommandUtility::escapeShellArgument($filename . 'eps') . ' -l ' . CommandUtility::escapeShellArgument($filename . 'svg'));
                if ($status === '' && file_exists($filename . 'svg') === true) {
                    $fileObject->setIdentifier(substr($fileObject->getIdentifier(), 0, -3) . 'svg');
                    $this->db->exec_UPDATEquery(
                        'sys_file',
                        'uid = ' . intval($fileObject->getUid()),
                        [
                            'identifier' => substr($fileObject->getIdentifier(), 0, -3) . 'svg', 'extension' => 'svg', 'mime_type' => 'image/svg+xml',
                            'tstamp' => time()
                        ]
                    );
                    unlink($filename . 'eps');
                }
            }
        }
    }
}
