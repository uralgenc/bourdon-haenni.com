<?php
namespace Baumer\Baumer\Service;

/***************************************************************
 * Copyright notice
 *
 *  2010 Daniel Lienert <daniel@lienert.cc>, Michael Knoll <mimi@kaktusteam.de>
 * 2015 Martin R. Krause <martin.r.krause@gmx.de>
 * All rights reserved
 *
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Dispatcher;
use \TYPO3\CMS\Extbase\Mvc\Web\Request;
use \TYPO3\CMS\Extbase\Mvc\Web\Response;
use \TYPO3\CMS\Extbase\Object\ObjectManager;
use \TYPO3\CMS\Extbase\Core\Bootstrap;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * Utility to include defined frontend libraries as jQuery and related CSS
 *
 *
 * @package Utility
 * @author Daniel Lienert <daniel@lienert.cc>
 * @author Martin R. Krause <martin.r.krause@gmx.de>
 */
class AjaxDispatcher
{
    /**
     * Array of all request Arguments
     *
     * @var array
     */
    protected $requestArguments = [];

    /**
     * Extbase Object Manager
     * @var ObjectManager
     */
    protected $objectManager;


    /**
     * @var string
     */
    protected $extensionName;


    /**
     * @var string
     */
    protected $pluginName;


    /**
     * @var string
     */
    protected $controllerName;


    /**
     * @var string
     */
    protected $actionName;


    /**
     * @var array
     */
    protected $arguments = [];


    /**
     * @var integer
     */
    protected $pageUid;

    /**
     * Called by ajax.php / eID.php
     * Builds an extbase context and returns the response
     *
     * ATTENTION: You should not call this method without initializing the dispatcher. Use initAndDispatch() instead!
     */
    public function dispatch()
    {
        $configuration['extensionName'] = $this->extensionName;
        $configuration['pluginName'] = $this->pluginName;

        /** @var Bootstrap $bootstrap */
        $bootstrap = GeneralUtility::makeInstance(Bootstrap::class);
        $bootstrap->initialize($configuration);

        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        $request = $this->buildRequest();
        /** @var Response $response */
        $response = $this->objectManager->getEmptyObject(Response::class);

        /** @var \TYPO3\CMS\Extbase\Mvc\Dispatcher $dispatcher */
        $dispatcher = $this->objectManager->get(Dispatcher::class);
        $dispatcher->dispatch($request, $response);

        $response->sendHeaders();
        return $response->getContent();
    }


    /**
     * @param null $pageUid
     * @return AjaxDispatcher
     */
    public function init($pageUid = null)
    {
        define('TYPO3_MODE', 'FE');
        $this->pageUid = $pageUid;
        global $TYPO3_CONF_VARS;

        $GLOBALS['TSFE'] = GeneralUtility::makeInstance(TypoScriptFrontendController::class, $TYPO3_CONF_VARS, $pageUid, '0', 1, '', '', '', '');
        $GLOBALS['TSFE']->sys_page = GeneralUtility::makeInstance(PageRepository::class);

        $GLOBALS['TSFE']->initFeuser();

        return $this;
    }

    /**
     * Build a request object
     *
     * @return  Request $request
     */
    protected function buildRequest()
    {
        $request = $this->objectManager->get(Request::class);
        /* @var $request Request */
        $request->setControllerVendorName('Baumer');
        $request->setControllerExtensionName($this->extensionName);
        $request->setPluginName($this->pluginName);
        $request->setControllerName($this->controllerName);
        $request->setControllerActionName($this->actionName);
        $request->setArguments($this->arguments);

        return $request;
    }


    /**
     * Prepare the call arguments
     *
     * @return AjaxDispatcher
     */
    public function initCallArguments()
    {
        $request = GeneralUtility::_GP('request');

        if ($request) {
            $this->setRequestArgumentsFromJSON($request);
        } else {
            $this->setRequestArgumentsFromGetPost();
        }

        $this->extensionName = $this->requestArguments['extension'] ? $this->requestArguments['extension'] : 'Baumer';
        $this->pluginName = $this->requestArguments['plugin'];
        $this->controllerName = $this->requestArguments['controller'];
        $this->actionName = $this->requestArguments['action'];
        $this->arguments = $this->requestArguments['arguments'];
        if (!is_array($this->arguments)) {
            $this->arguments = [];
        }

        return $this;
    }


    /**
     * Set the request array from JSON
     *
     * @param string $request
     */
    protected function setRequestArgumentsFromJSON($request)
    {
        $requestArray = json_decode($request, true);
        if (is_array($requestArray)) {
            ArrayUtility::mergeRecursiveWithOverrule($this->requestArguments, $requestArray);
        }
    }


    /**
     * Set the request array from the getPost array
     */
    protected function setRequestArgumentsFromGetPost()
    {
        $validArguments = ['extension', 'plugin', 'controller', 'action', 'arguments'];
        foreach ($validArguments as $argument) {
            if (GeneralUtility::_GP($argument)) {
                $this->requestArguments[$argument] = GeneralUtility::_GP($argument);
            }
        }
    }


    /**
     * @param $extensionName
     * @return AjaxDispatcher
     * @throws \Exception
     */
    public function setExtensionName($extensionName)
    {
        if (!$extensionName) {
            throw new \Exception('No extension name set for extbase request.', 1327583056);
        }

        $this->extensionName = $extensionName;
        return $this;
    }


    /**
     * @param $pluginName
     * @return AjaxDispatcher
     */
    public function setPluginName($pluginName)
    {
        $this->pluginName = $pluginName;
        return $this;
    }


    /**
     * @param $controllerName
     * @return AjaxDispatcher
     */
    public function setControllerName($controllerName)
    {
        $this->controllerName = $controllerName;
        return $this;
    }


    /**
     * @param $actionName
     * @return AjaxDispatcher
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;
        return $this;
    }
}
