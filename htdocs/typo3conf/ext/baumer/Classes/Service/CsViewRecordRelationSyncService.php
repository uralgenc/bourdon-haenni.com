<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Service;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class CsViewRecordRelationSyncService
{
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * @var DatabaseConnection
     */
    protected $db;

    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->db = &$GLOBALS['TYPO3_DB'];
    }

    /**
     * @param array $record
     * @param \TYPO3\CMS\Backend\Form\Element\UserElement $csview
     */
    public function syncRecords($record, $csview)
    {
        $contentElementUid = $record['row']['uid'];
        $csView = $record['row']['pi_flexform']['data']['options']['lDEF']['csview']['vDEF'][0];

        // If csview changes, it won't find entries. We don't want to delete existing entire's with right csview record.
        $rows = $this->db->exec_SELECTcountRows('*', 'tx_baumer_domain_model_csviewrecordrelation as vrr LEFT JOIN tx_baumer_domain_model_csviewrecord as vr ON vrr.cs_view_record = vr.uid',
            'vrr.tt_content = '.$contentElementUid . ' AND vr.cs_view = "'. $csView .'"');

        if ($rows == 0) {
            // Delete all entries for this tt_content element
            $this->db->exec_DELETEquery('tx_baumer_domain_model_csviewrecordrelation',
                'tt_content = '.$contentElementUid);

            $count = $this->createRelation($csView, $contentElementUid);

            // Avoid an endless loop in empty csview
            if ($count > 0) {
                // Changing entries oin fly will throw an error, reload needed
                echo '<script>window.location.href = window.location.href;</script>';
                exit;
            }
        }
    }

    /**
     * @param integer $csView
     * @param integer $contentElementUid
     * @return int
     */
    protected function createRelation($csView, $contentElementUid)
    {
        // Get all records for the selected csview
        $list = $this->db->exec_SELECTgetRows('*', 'tx_baumer_domain_model_csviewrecord',
            'cs_view = "'. $csView .'"');

        foreach ($list as $row) {
            // Insert relation between content element and csviewrecord
            $this->db->exec_INSERTquery('tx_baumer_domain_model_csviewrecordrelation', [
                'pid' => $row['pid'],
                'cs_view_record' => $row['uid'],
                // 'sorting' => rand(0,40000),
                'tstamp' => time(),
                'crdate' => time(),
                'tt_content' => $contentElementUid
            ]);
        }

        return count($list);
    }
}
