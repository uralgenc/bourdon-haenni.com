<?php
namespace Baumer\Baumer\Overrides;

/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

class PageRepository extends \TYPO3\CMS\Frontend\Page\PageRepository
{

    /**
     * Will find the page carrying the domain record matching the input domain.
     * Might exit after sending a redirect-header IF a found domain record
     * instructs to do so.
     *
     * @param string $domain Domain name to search for. Eg. "www.typo3.com". Typical the HTTP_HOST value.
     * @param string $path Path for the current script in domain. Eg. "/somedir/subdir". Typ. supplied by \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('SCRIPT_NAME')
     * @param string $request_uri Request URI: Used to get parameters from if they should be appended. Typ. supplied by \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('REQUEST_URI')
     *
     * @return mixed If found, returns integer with page UID where found. Otherwise blank. Might exit if location-header is sent, see description.
     * @see \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController::findDomainRecord()
     */
    public function getDomainStartPage($domain, $path = '', $request_uri = '')
    {
        $domain = explode(':', $domain);
        $domain = strtolower(preg_replace('/\\.$/', '', $domain[0]));
        // Removing extra trailing slashes
        $path = trim(preg_replace('/\\/[^\\/]*$/', '', $path));
        // Appending to domain string
        $domain .= $path;
        $domain = preg_replace('/\\/*$/', '', $domain);
        /**
         * This is the additional condition to have domain mapping without domain records
         */
        if (isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['domainMapping']) && $domain !== exec('hostname -f')) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['SYS']['domainMapping'] as $subdomain => $pageUid) {
                if ($subdomain . '.' . exec('hostname -f') === $domain) {
                    return $pageUid;
                }
            }
            throw new \RuntimeException('No Mapping for domain ' . $domain . ' defined', 1423505730);
        } else {
            $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('pages.uid,sys_domain.redirectTo,sys_domain.redirectHttpStatusCode,sys_domain.prepend_params', 'pages,sys_domain', 'pages.uid=sys_domain.pid
						AND sys_domain.hidden=0
						AND (sys_domain.domainName=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($domain, 'sys_domain') . ' OR sys_domain.domainName=' . $GLOBALS['TYPO3_DB']->fullQuoteStr(($domain . '/'), 'sys_domain') . ') ' . $this->where_hid_del . $this->where_groupAccess, '', '', 1);
            $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
            $GLOBALS['TYPO3_DB']->sql_free_result($res);
            if ($row) {
                if ($row['redirectTo']) {
                    $redirectUrl = $row['redirectTo'];
                    if ($row['prepend_params']) {
                        $redirectUrl = rtrim($redirectUrl, '/');
                        $prependStr  = ltrim(substr($request_uri, strlen($path)), '/');
                        $redirectUrl .= '/' . $prependStr;
                    }
                    $statusCode = (int) $row['redirectHttpStatusCode'];
                    if ($statusCode && defined('TYPO3\\CMS\\Core\\Utility\\HttpUtility::HTTP_STATUS_' . $statusCode)) {
                        \TYPO3\CMS\Core\Utility\HttpUtility::redirect($redirectUrl, constant('TYPO3\\CMS\\Core\\Utility\\HttpUtility::HTTP_STATUS_' . $statusCode));
                    } else {
                        \TYPO3\CMS\Core\Utility\HttpUtility::redirect($redirectUrl, \TYPO3\CMS\Core\Utility\HttpUtility::HTTP_STATUS_301);
                    }
                    die;
                } else {
                    return $row['uid'];
                }
            }
        }
        return null;
    }
}
