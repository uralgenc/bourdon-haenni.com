<?php
namespace Baumer\Baumer\Utility;

/**
 * Class ContentservAttributes
 *
 * @package Baumer\Baumer\Utility
 */
class ContentservAttributes
{

    /**
     * @param array $attributes
     * @param string $name
     * @param string $field
     * @return string|bool
     */
    public static function extractAttributeFieldValueByName($attributes, $name, $field = 'Value')
    {
        if (is_array($attributes) && !empty($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute['Name'] == $name) {
                    return $attribute[$field];
                    break;
                }
            }
        }
        return false;
    }

    /**
     * @param array $attributes
     * @param int $attributeId
     * @param string $field
     * @return string|bool
     */
    public static function extractAttributeFieldValueByAttributeId($attributes, $attributeId, $field = 'Value')
    {
        if (is_array($attributes) && !empty($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute['ID'] == $attributeId) {
                    return $attribute[$field];
                    break;
                }
            }
        }
        return false;
    }
}
