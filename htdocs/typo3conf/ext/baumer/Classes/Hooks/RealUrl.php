<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\Hooks;

class RealUrl
{

    /**
     * @param array $params
     */
    public function user_decodeSpURL_preProc(&$params)
    {
        //TODO decode URLs
    }


    /**
     * @param array $params
     */
    public function user_encodeSpURL_postProc(&$params)
    {
        //TODO encode URLs
    }


    /**
     * Search a domain for the page to avoid a exception in cronjobs
     * @param array $params
     * @param string $ref
     * @return string
     */
    public function getHost(&$params, &$ref)
    {
        if (!empty($ref) === true && $GLOBALS['TSFE']->id > 0) {
            /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
            $db = &$GLOBALS['TYPO3_DB'];
            $result = $db->exec_SELECTgetSingleRow('domainName', 'sys_domain', 'redirectTo=\'\' AND hidden=0 AND pid = ' .  $GLOBALS['TSFE']->id);
            return $result['domainName'];
        }
    }
}
