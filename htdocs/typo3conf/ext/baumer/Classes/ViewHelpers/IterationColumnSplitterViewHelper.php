<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace Baumer\Baumer\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class IterationColumnSplitterViewHelper extends AbstractViewHelper
{

    /**
     * @param mixed $iterator
     * @param int $chunks
     * @param string $columnSplitter
     *
     * @return string
     */
    public function render($iterator, $chunks = 2, $columnSplitter = '</div><div class="col-md-6">')
    {
        $breakEvery = intval($iterator['total'] / $chunks);

        if ($iterator) {
            if (intval($iterator['cycle'] / $breakEvery) > intval(($iterator['cycle']-1) / $breakEvery)) {
                return $columnSplitter;
            }
        }
        return '';
    }
}
