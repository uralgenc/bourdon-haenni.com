<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <jhoechtl@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Baumer\Baumer\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class MamFileAttributesViewHelper extends AbstractViewHelper
{

    /**
     * @param string $as
     * @param string $mamFileId
     */
    public function render($as, $mamFileId)
    {
        /** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        /** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe */
        $tsfe = $GLOBALS['TSFE'];

        $record = $db->exec_SELECTgetSingleRow(
            'sfm.data',
            'sys_file sf, sys_file_metadata sfm',
            'sf.uid = sfm.file AND sfm.sys_language_uid = ' . $tsfe->sys_language_uid . ' AND sf.mam_file_id = "' . $mamFileId . '"'
        );
        if (!empty($record['data'])) {
            $decodedData = json_decode($record['data'], true);
            if (isset($decodedData['Attributes'])) {
                $attributes = [];
                foreach ($decodedData['Attributes'] as $attribute) {
                    $attributes[$attribute['ID']] = $attribute;
                }
                $decodedData['Attributes'] = $attributes;
            }

            if ($this->templateVariableContainer->exists($as)) {
                $this->templateVariableContainer->remove($as);
            }
            $this->templateVariableContainer->add($as, $decodedData);
        }
    }
}
