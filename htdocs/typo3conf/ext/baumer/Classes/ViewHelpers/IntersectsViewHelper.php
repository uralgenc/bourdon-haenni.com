<?php
namespace Baumer\Baumer\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Martin R. Krause <martin.r.krause@gmx.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

/**
 * Intersects ViewHelper
 *
 * @package Baumer
 */
class IntersectsViewHelper extends AbstractConditionViewHelper
{

    /**
     * @var mixed
     */
    protected $evaluation = false;

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('a', 'mixed', 'Array or Traversble of objects', true);
        $this->registerArgument('b', 'mixed', 'Array or Traversble of objects', true);
    }

    /**
     * Render method
     *
     * @return string
     */
    public function render()
    {
        $a = $this->arguments['a'];
        $b = $this->arguments['b'];

        $result = count(array_intersect(
            $this->mysteriousHeapToArray($a),
            $this->mysteriousHeapToArray($b)
        )) > 0;

        if ($result) {
            return $this->renderThenChild();
        } else {
            return $this->renderElseChild();
        }
    }

    /**
     * Transform an unknown heap into an array
     * Because arrays are nice and fluffy
     *
     * @param $heap
     * @return mixed
     */
    private function mysteriousHeapToArray($heap)
    {
        $asArray = [];
        if (true === is_array($heap)) {
            $asArray = $heap;
        } elseif (true === $heap instanceof ObjectStorage) {
            /** @var $heap ObjectStorage */
            $asArray = $heap->toArray();
        } elseif (true === $heap instanceof LazyObjectStorage) {
            /** @var $heap LazyObjectStorage */
            $asArray = $heap->toArray();
        } elseif (true === $heap instanceof QueryResult) {
            /** @var $heap QueryResult */
            $asArray = $heap->toArray();
        } elseif (true === is_string($heap)) {
            $asArray = str_split($heap);
        } elseif (true === $heap instanceof DomainObjectInterface) {
            // I'm alone!
            $asArray = [$heap];
        }

        return $asArray;
    }
}
