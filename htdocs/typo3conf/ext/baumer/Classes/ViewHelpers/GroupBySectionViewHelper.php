<?php
namespace Baumer\Baumer\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices to the license
 *  from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class GroupBySectionViewHelper
 *
 * @package Baumer\Baumer\ViewHelpers
 */
class GroupBySectionViewHelper extends AbstractViewHelper
{

    /**
     * @param array $attributes
     * @param string $as
     */
    public function render($attributes, $as)
    {
        if (!is_array($attributes)) {
            return;
        }

        $sections = [];

        foreach ($attributes as $attribute) {
            if (isset($attribute['SectionTitle'])) {
                if (!isset($sections[$attribute['SectionTitle']])) {
                    $sections[$attribute['SectionTitle']] = [
                        'SectionTitle' => $attribute['SectionTitle'],
                        'TranslatedSectionTitle' => $attribute['TranslatedSectionTitle'],
                        'SectionTitleSortOrder' => isset($attribute['SectionTitleSortOrder']) ? (int)$attribute['SectionTitleSortOrder'] : 0,
                        'Attributes' => []
                    ];
                }
                $sections[$attribute['SectionTitle']]['Attributes'][] = $attribute;
            }
        }
        // TODO sort sections and attributes in sections by sort value

        if ($this->templateVariableContainer->exists($as)) {
            return;
        }
        $this->templateVariableContainer->add($as, $sections);
    }
}
