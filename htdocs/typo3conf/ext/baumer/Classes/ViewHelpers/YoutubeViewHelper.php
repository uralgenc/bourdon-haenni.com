<?php
namespace Baumer\Baumer\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyObjectStorage;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

/**
 * Youtube ViewHelper
 *
 * @package Baumer
 */
class YoutubeViewHelper extends \FluidTYPO3\Vhs\ViewHelpers\Media\YoutubeViewHelper
{

    /**
     * Render method
     *
     * @return string
     */
    public function render()
    {
        $videoId = $this->arguments['videoId'];
        $width = $this->arguments['width'];
        $height = $this->arguments['height'];

        $this->tag->addAttribute('width', $width);
        $this->tag->addAttribute('height', $height);

        $src = $this->getSourceUrl($videoId);

        if (false === (boolean) $this->arguments['legacyCode']) {
            $this->tag->addAttribute('src', $src);
            $this->tag->addAttribute('frameborder', 0);
            $this->tag->addAttribute('allowFullScreen', 'allowFullScreen');
            $this->tag->forceClosingTag(true);
        } else {
            $this->tag->setTagName('object');

            $tagContent = '';

            $paramAttributes = [
                'movie' => $src,
                'allowFullScreen' => 'true',
                'scriptAccess' => 'always',
            ];
            foreach ($paramAttributes as $name => $value) {
                $tagContent .= $this->renderChildTag('param', [$name => $value], true);
            }

            $embedAttributes = [
                'src' => $src,
                'type' => 'application/x-shockwave-flash',
                'width' => $width,
                'height' => $height,
                'allowFullScreen' => 'true',
                'scriptAccess' => 'always',
            ];
            $tagContent .= $this->renderChildTag('embed', $embedAttributes, true);

            $this->tag->setContent($tagContent);
        }

        return $this->tag->render();
    }

    /**
     * Returns video source url according to provided arguments
     *
     * @param string $videoId
     *
     * @return string
     */
    private function getSourceUrl($videoId)
    {
        $src = (boolean) true === $this->arguments['extendedPrivacy'] ? self::YOUTUBE_PRIVACY_BASEURL : self::YOUTUBE_BASEURL;
        if (false === $this->arguments['legacyCode']) {
            $src .= '/embed/' . $videoId;
            if (false === (boolean) $this->arguments['showRelated'] || true === (boolean) $this->arguments['autoplay']) {
                $src .= '?';
            }
            if (false === (boolean) $this->arguments['showRelated']) {
                $src .= 'rel=0&';
            }
            if (true === (boolean) $this->arguments['autoplay']) {
                $src .= 'autoplay=1';
            }
        } else {
            $src .= '/v/' . $this->arguments['videoId'] . '?version=3';
            if (false === (boolean) $this->arguments['showRelated']) {
                $src .= '&rel=0';
            }
            if (true === (boolean) $this->arguments['autoplay']) {
                $src .= '&autoplay=1';
            }
        }

        $src .= '&wmode=transparent';

        return $src;
    }

    /**
     * Renders the provided tag and its attributes
     *
     * @param string $tagName
     * @param array $attributes
     * @param boolean $forceClosingTag
     *
     * @return string
     */
    private function renderChildTag($tagName, $attributes = [], $forceClosingTag = false)
    {
        $tagBuilder = clone($this->tag);
        $tagBuilder->reset();
        $tagBuilder->setTagName($tagName);
        $tagBuilder->addAttributes($attributes);
        $tagBuilder->forceClosingTag($forceClosingTag);
        $childTag = $tagBuilder->render();
        unset($tagBuilder);

        return $childTag;
    }
}
