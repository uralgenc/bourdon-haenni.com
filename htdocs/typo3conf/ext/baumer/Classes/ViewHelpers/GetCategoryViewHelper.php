<?php
namespace Baumer\Baumer\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Andreas Winter <awinter@1drop.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Youtube ViewHelper
 *
 * @package Baumer
 */
class GetCategoryViewHelper extends AbstractViewHelper
{

    /**
     * Render method
     *
     * @param TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories
     * @param int $parentId
     * @return array
     */
    public function render($categories, $parentId)
    {
        foreach ($categories as $category) {
            if ($category && $category->getParent() && $category->getParent()->getUid() == $parentId) {
                $selectedCats[$category->getUid()] = $category->getTitle();
            }
        }

        return $selectedCats;
    }
}
