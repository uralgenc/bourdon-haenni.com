<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace Baumer\Baumer\Command;

use Baumer\Baumer\Domain\Model\YoutubeVideo;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\Category;

/**
 * Class GoogleCommandController
 *
 * @package Baumer\Baumer\Command
 * @route off
 */
class GoogleCommandController extends AbstractCommandController
{

    /**
     * @var \Google_Client
     */
    protected $client;
    /**
     * @var \Baumer\Baumer\Domain\Repository\YoutubeVideoRepository
     * @inject
     */
    protected $youtubeVideoRepository;
    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;


    protected function initializeCommand()
    {
        parent::initializeCommand();
        $this->client = new \Google_Client();
        $this->client->setApplicationName('Baumer-Site');
        $this->client->setDeveloperKey($this->settings['youtube']['developerKey']);
    }

    public function importYoutubeFeedCommand()
    {
        $youtubeVideos = $this->youtubeVideoRepository->findAll();
        $playlistCategoryMapping = $this->settings['youtube']['playlistCategoryMapping'];


        $youtubeService = new \Google_Service_YouTube($this->client);
        $playlists = $youtubeService->playlists->listPlaylists('snippet', ['channelId' => $this->settings['youtube']['channelId']]);
        // Grab all playlists from baumer channel
        foreach ($playlists->getItems() as $playlist) {
            /** @var \Google_Service_YouTube_Playlist $playlist */
            $playlistId = $playlist->getId();

            $playlistItems = $youtubeService->playlistItems->listPlaylistItems('contentDetails', ['playlistId' => $playlistId, 'maxResults' => 50]);
            $videoIds = [];
            // Grab all videoIds in current playlist to reduce amount of API calls
            foreach ($playlistItems->getItems() as $playlistItem) {
                /** @var \Google_Service_YouTube_PlaylistItem $playlistItem */
                /** @var \Google_Service_YouTube_PlaylistItemContentDetails $videoDetails */
                $videoDetails = $playlistItem->getContentDetails();
                $videoIds[] = $videoDetails->getVideoId();
            }
            // Grab actual Video information
            /** @var \Google_Service_YouTube_Video[] $videos */
            $videos = $youtubeService->videos->listVideos('snippet,contentDetails,statistics', ['id' => implode(',', $videoIds)])->getItems();
            foreach ($videos as $video) {
                // Grab details about video
                /** @var \Google_Service_YouTube_VideoSnippet $videoSnippet */
                $videoSnippet = $video->getSnippet();
                /** @var \Google_Service_YouTube_VideoContentDetails $videoDetails */
                $videoDetails = $video->getContentDetails();
                /** @var \Google_Service_YouTube_VideoStatistics $videoStatistics */
                $videoStatistics = $video->getStatistics();
                /*
                 * Get thumbnail with highest available resolution or as fallback construct url
                 */
                /** @var \Google_Service_YouTube_ThumbnailDetails $thumbnails */
                $thumbnails = $videoSnippet->getThumbnails();
                /** @var \Google_Service_YouTube_Thumbnail $thumbnail */
                if (($thumbnail = $thumbnails->getMaxres()) == null) {
                    if (($thumbnail = $thumbnails->getHigh()) == null) {
                        if (($thumbnail = $thumbnails->getStandard()) == null) {
                            $thumbnail = $thumbnails->getDefault();
                        }
                    }
                }
                if (!empty($thumbnail)) {
                    $thumbnailUrl = $thumbnail->getUrl();
                } else {
                    $thumbnailUrl = 'http://img.youtube.com/vi/' . $video->getId() . '/0.jpg';
                }

                // Iterate over all youtube videos and update record if available else create it
                $videoExists = false;
                /** @var YoutubeVideo[] $youtubeVideos */
                foreach ($youtubeVideos as $youtubeVideo) {
                    if ($youtubeVideo->getVideoId() == $video->getId() && $youtubeVideo->getPlaylistId() == $playlistId) {
                        $videoExists = true;
                        $youtubeVideo->setViewCount($videoStatistics->getViewCount());
                    }
                }
                if (!$videoExists) {
                    $titleParts = GeneralUtility::trimExplode('|', $videoSnippet->getTitle());
                    $videoLanguage = $titleParts[0];
                    $videoTitle = $titleParts[2];
                    /** @var YoutubeVideo $youtubeVideo */
                    $youtubeVideo = $this->objectManager->get(YoutubeVideo::class);
                    $youtubeVideo->setTitle($videoTitle);
                    $youtubeVideo->setDescription($videoSnippet->getDescription());
                    $youtubeVideo->setVideoId($video->getId());
                    $youtubeVideo->setLanguage($videoLanguage);
                    $youtubeVideo->setPid($this->settings['youtube']['storagePid']);
                    $youtubeVideo->setPlaylistId($playlistId);
                    $youtubeVideo->setDuration($videoDetails->getDuration());
                    $youtubeVideo->setPublishedAt(new \DateTime($videoSnippet->getPublishedAt()));
                    $youtubeVideo->setThumbnailUrl($thumbnailUrl);
                    $youtubeVideo->setViewCount($videoStatistics->getViewCount());
                    $videoCategory = $this->categoryRepository->findByUid($playlistCategoryMapping[$playlistId]);
                    if ($videoCategory instanceof Category) {
                        $youtubeVideo->addCategory($videoCategory);
                    }

                    $this->youtubeVideoRepository->add($youtubeVideo);
                }
                //@TODO manage youtube language overlay as soon as they are available
            }
        }
    }
}
