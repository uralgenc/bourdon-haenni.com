<?php
namespace Baumer\Baumer\Command;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Anja Leichsenring <maddy@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Baumer\Baumer\Service\Contentserv\CsConnection;
use Baumer\Baumer\Service\FileImportService;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Baumer\Baumer\Exception\InvalidArgumentException;

/**
 * Uses ContentServ connection to synchronize files from MAM to fileadmin
 */
class MamCommandController extends AbstractCommandController
{

    const SYNC_MODE_TREE = 1;
    const SYNC_MODE_CHANGES = 2;

    protected static $syncModes = [
        self::SYNC_MODE_CHANGES,
        self::SYNC_MODE_TREE
    ];

    /**
     * @var \Baumer\Baumer\Service\Contentserv\CsConnection
     * @inject
     */
    protected $contentServConnection;

    /**
     * @var \Baumer\Baumer\Service\FileImportService
     */
    protected $fileImportService;
    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Indexer\File
     * @inject
     */
    protected $fileIndexer;

    protected $authParameter;

    /**
     * initial method, called from scheduler
     *
     * @param string $sourceFolders Comma-separated list of folder UIDs, given by MAM
     * @param integer $syncMode 1: full import, 2: incremental changes (frequent use)
     * @param integer $fileStorage TYPO3 "File Storage" uid
     *
     * @throws InvalidArgumentException
     * @throws \Baumer\Baumer\Exception\AuthenticationFailedException
     * @throws \Baumer\Baumer\Exception\UnsupportedCommandException
     * @return boolean
     */
    public function mamImportCommand($sourceFolders, $syncMode = self::SYNC_MODE_CHANGES, $fileStorage = 1)
    {
        $startTime = time();
        $syncMode = (int)$syncMode;
        $sourceFolders = GeneralUtility::intExplode(',', $sourceFolders, true);
        if (empty($sourceFolders) || (count($sourceFolders) == 1 && $sourceFolders[0] == 0)) {
            throw new InvalidArgumentException(
                'Invalid source folder definition given. Needs to be a comma-separated string made from integer values.', 1409899446);
        }
        if (!in_array($syncMode, self::$syncModes)) {
            throw new InvalidArgumentException('The requested syncMode could not be resolved.', 1409900106);
        }

        // Authenticate against Contentserv API
        $this->authParameter = $this->contentServConnection->authenticate('MAM');

        /** @var FileImportService $fileImportService */
        $this->fileImportService = $this->getFileImportService($this->authParameter, $fileStorage);

        if ($syncMode == self::SYNC_MODE_TREE) {
            $this->syncFull($sourceFolders);
        } elseif ($syncMode == self::SYNC_MODE_CHANGES) {
            $this->syncChanges($sourceFolders);
        }

        $this->fileIndexer->indexFiles('', $startTime);

        return true;
    }

    /**
     * Iterate over all files in storage and extract content of file into
     * field tika_content
     *
     * @param int $fileStorage
     * @throws \Baumer\Baumer\Exception\AuthenticationFailedException
     * @throws \Baumer\Baumer\Exception\UnsupportedCommandException
     */
    public function updateTikaContentCommand($fileStorage = 1)
    {
        $storage = ResourceFactory::getInstance()->getStorageObject($fileStorage);
        $this->authParameter = $this->contentServConnection->authenticate('MAM');
        /** @var FileImportService $fileImportService */
        $this->fileImportService = $this->getFileImportService($this->authParameter, $fileStorage);
        /** @var \TYPO3\CMS\Core\Resource\File $file */
        foreach ($storage->getDefaultFolder()->getFiles(0, 0, Folder::FILTER_MODE_USE_OWN_AND_STORAGE_FILTERS, true) as $file) {
            $this->fileImportService->updateFileTikaContent($file);
        }
    }

    /**
     * Retrieve the service (this function is needed for testing)
     *
     * @param string $authParameter
     * @param $fileStorage
     *
     * @return FileImportService
     */
    protected function getFileImportService($authParameter, $fileStorage)
    {
        /** @var FileImportService $instance */
        $instance = $this->objectManager->get(FileImportService::class);
        $instance->setAuthParameter($authParameter);
        $instance->setStorageUid($fileStorage);
        $instance->initialStorage();
        $instance->setSettings($this->settings);
        return $instance;
    }

    protected function syncFull($sourceFolders)
    {
        $parameter = [];
        $parameter['depth'] = 0;
        $parameter['limit'] = 1000;
        $parameter['filter'] = 'State=' . $this->settings['mamImport']['allowedState'];
        $parameter['expand'] = 'Attributes.' . $this->settings['mamImport']['languageAttributeId'] . ',Attributes.'.implode(',Attributes.', array_keys($this->settings['mamImport']['mamCategoriesMapping']));

        foreach ($sourceFolders as $sourceFolderUid) {
            if ($sourceFolderUid > 0) {
                $parameter['folderId'] = $sourceFolderUid;
                foreach ($this->settings['mamImport']['languageMapping'] as $mamLanguage => $sysLanguageUid) {
                    $parameter['lang'] = $mamLanguage;
                    $parameter['page'] = 0;
                    do {
                        $result = $this->contentServConnection->runCommand('admin/rest/' . CsConnection::MAM_ENDPOINT . '/search/*/', $this->authParameter, $parameter);
                        if (is_array($result)) {
                            if (isset($result['File'])) {
                                $files = $result['File'];
                            } else {
                                $files = (isset($result['Files']) ? $result['Files'] : []);
                            }
                            if ($files) {
                                // Code needs a folder and a label to run the foreach
                                $result['IsFolder'] = '1';
                                $result['Label'] = 'Temp';
                                $this->fileImportService->importFolder($result, $sysLanguageUid);
                            }
                        }
                        $parameter['page']++;
                    } while ($this->commandHasResult($result));
                }
            }
        }
    }

    protected function commandHasResult($result)
    {
        if (is_array($result)) {
            if ($result['Meta']['Status'] == 'error') {
                return false;
            }

            return true;
        }

        return false;
    }

    protected function syncChanges($sourceFolders)
    {
        $parameter = [];
        $parameter['limit'] = 5000;
        $parameter['begin'] = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 7);
        $parameter['expand'] = 'Attributes.' . $this->settings['mamImport']['languageAttributeId'] . ',Attributes.'.implode(',Attributes.', array_keys($this->settings['mamImport']['mamCategoriesMapping']));

        foreach ($sourceFolders as $sourceFolderUid) {
            if ($sourceFolderUid > 0) {
                foreach ($this->settings['mamImport']['languageMapping'] as $mamLanguage => $sysLanguageUid) {
                    $parameter['lang'] = $mamLanguage;
                    $result = $this->contentServConnection->runCommand('admin/rest/' . CsConnection::MAM_ENDPOINT . '/changes/' . $sourceFolderUid, $this->authParameter, $parameter);
                    if (is_array($result)) {
                        if (isset($result['File'])) {
                            $files = $result['File'];
                        } else {
                            $files = (isset($result['Files']) ? $result['Files'] : []);
                        }
                        if ($files) {
                            // Code needs a folder and a label to run the foreach
                            $result['IsFolder'] = '1';
                            $result['Label'] = 'Temp';
                            $this->fileImportService->importFolder($result, $sysLanguageUid);
                        }
                    }
                }
            }
        }
    }
}
