<?php

namespace Baumer\Baumer\Command;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Martin Heigermoser <martin.heigermoser@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use FluidTYPO3\Flux\Configuration\BackendConfigurationManager;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Messaging\FlashMessageQueue;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

/**
 * Class AbstractCommandController
 *
 * @author Martin Heigermoser <martin.heigermoser@typovision.de>
 *
 * @package Baumer\Baumer\Command
 */
abstract class AbstractCommandController extends CommandController
{

    const BOURDON_CS_ONLINE_VIEW = 146;
    const BAUMER_CS_ONLINE_VIEW = 158;
    const BOURDON_ROOT_PID = 410;

    /**
     * @var BackendConfigurationManager
     */
    protected $configurationManager;

    /**
     * @var array
     */
    protected $settings;
    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $db;
    /**
     * @var \TYPO3\CMS\Core\Log\Logger
     */
    protected $logger;
    /**
     * @var FlashMessageQueue
     */
    protected $flashMessageQueue;
    /**
     * @var string
     */
    protected $_EXTKEY = 'baumer';
    /**
     * @var int
     */
    protected $_TS_PID = 1;

    /**
     * @var array
     */
    protected $foreground_colors = [
        'black' => '0;30',
        'dark_gray' => '1;30',
        'blue' => '0;34',
        'light_blue' => '1;34',
        'green' => '0;32',
        'light_green' => '1;32',
        'cyan' => '0;36',
        'light_cyan' => '1;36',
        'red' => '0;31',
        'light_red' => '1;31',
        'purple' => '0;35',
        'light_purple' => '1;35',
        'brown' => '0;33',
        'yellow' => '1;33',
        'light_gray' => '0;37',
        'white' => '1;37'
    ];

    /**
     * @var array
     */
    protected $background_colors = [
        'black' => '40',
        'red' => '41',
        'green' => '42',
        'yellow' => '43',
        'blue' => '44',
        'magenta' => '45',
        'cyan' => '46',
        'light_gray' => '47'
    ];

    /**
     * We add the functionality to have magic initialize methods in front of a command
     */
    protected function mapRequestArgumentsToControllerArguments()
    {
        $this->initializeCommand();
        $actionInitializationMethodName = 'initialize' . ucfirst($this->commandMethodName);
        if (method_exists($this, $actionInitializationMethodName)) {
            call_user_func([$this, $actionInitializationMethodName]);
        }
        parent::mapRequestArgumentsToControllerArguments();
    }


    /**
     * initialize controller settings
     */
    protected function initializeCommand()
    {
        $this->configurationManager = $this->objectManager->get(BackendConfigurationManager::class);
        $this->configurationManager->setConfiguration();
        $this->configurationManager->setCurrentPageId($this->_TS_PID);
        $this->settings = [];
        $tsSettings = $this->configurationManager->getConfiguration($this->_EXTKEY);
        if (isset($tsSettings['settings'])) {
            $this->settings = $tsSettings['settings'];
        }
        $this->settings['_rootPid'] = $this->_TS_PID;
        $this->db = &$GLOBALS['TYPO3_DB'];
        $this->logger = $this->objectManager->get(LogManager::class)->getLogger(__CLASS__);
        /** @var FlashMessageService $flashMessageService */
        $flashMessageService = $this->objectManager->get(FlashMessageService::class);
        $this->flashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
    }

    /**
     * Return a colored string
     *
     * @param string $string
     * @param string $foreground_color
     * @param string $background_color
     *
     * @return string
     */
    protected function getColoredString($string, $foreground_color = null, $background_color = null)
    {
        $colored_string = "";

        // Check if given foreground color found
        if (isset($this->foreground_colors[$foreground_color])) {
            $colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
        }
        // Check if given background color found
        if (isset($this->background_colors[$background_color])) {
            $colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
        }

        // Add string and end coloring
        $colored_string .= $string . "\033[0m";

        return $colored_string;
    }
}
