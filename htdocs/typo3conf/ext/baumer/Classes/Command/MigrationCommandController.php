<?php

namespace Baumer\Baumer\Command;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Martin Heigermoser <martin.heigermoser@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

/**
 * Class MigrationCommandController
 *
 * @author Martin Heigermoser <martin.heigermoser@typovision.de>
 *
 * @package Baumer\Baumer\Command
 */
class MigrationCommandController extends AbstractCommandController
{

    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $db;

    /**
     * @var string
     */
    protected $originalDatabase;

    /**
     * These fields will be ignored while importing the old data into the new database.
     *
     * If you need any of those fields, remove them and process them as you wish.
     *
     * @var array
     */
    protected $ignoreFeUsersFields = [
        'static_info_country',
        'zone',
        'language',
        'gender',
        'cnum',
        'status',
        'date_of_birth',
        'comments',
        'by_invitation',
        'module_sys_dmail_html',
        'terms_acknowledged',
        'token',
        'tx_srfeuserregister_password',
    ];

    /**
     * @var string
     */
    protected $defaultTargetPid = '218';

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->db = & $GLOBALS['TYPO3_DB'];
        $this->originalDatabase = $GLOBALS['TYPO3_CONF_VARS']['DB']['database'];
    }

    /**
     * Check if the provided database can be selected
     *
     * @throws \RuntimeException
     *
     * @param string $database
     */
    protected function checkRequirement($database)
    {
        $availableDBs = $this->db->admin_get_dbs();
        if (array_search($database, $availableDBs) === false) {
            throw new \RuntimeException('The database ' . $database . ' has to be available to the current DB User', 1408545331);
        }
    }

    /**
     * @param string $database
     */
    protected function selectOldDatabase($database)
    {
        $this->checkRequirement($database);
        $this->db->setDatabaseName($database);
        $this->db->sql_select_db();
    }

    /**
     * @return boolean
     */
    protected function restoreOriginalDatabase()
    {
        return $this->db->sql_select_db($this->originalDatabase);
    }

    /**
     * Migrate the fe_users into the new structure
     *
     * Uses 'baumer' as the source database as default.
     * You can specify the database / targetPid to use as CLI argument, e.g.:
     *
     *   ./typo3/cli_dispatch.phpsh extbase migration:frontenduser --oldDatabase=my_old_databasename --targetPid=123
     *
     * @throws \RuntimeException
     *
     * @param string $oldDatabase
     * @param integer $targetPid
     */
    public function frontendUserCommand($oldDatabase = 'baumer', $targetPid = null)
    {
        $warning = $this->getColoredString('this command will ', 'yellow') .
            $this->getColoredString('truncate ', 'red') .
            $this->getColoredString('the tables fe_users and fe_groups in the ' .
                $this->originalDatabase .
                " database. \n\nPress STRG-C to cancel (you have 10 seconds)", 'yellow') . "\n";

        echo $warning;

        sleep(10);

        echo $this->getColoredString("\nStarting the migration", 'green') . "\n";

        if (is_null($targetPid)) {
            $targetPid = $this->defaultTargetPid;
        }
        $this->selectOldDatabase($oldDatabase);
        $oldFeUserFields = $this->db->admin_get_fields('fe_users');
        $users = $this->db->exec_SELECTgetRows('*', 'fe_users', 'deleted = 0 AND disable = 0');
        $groups = $this->db->exec_SELECTgetRows('*', 'fe_groups', 'hidden = 0 AND deleted = 0');
        $newUsers = [];
        if ($this->restoreOriginalDatabase()) {
            $feUserFields = $this->db->admin_get_fields('fe_users');
            $missingFields = array_diff(array_keys($oldFeUserFields), array_keys($feUserFields));
            foreach ($users as $user) {
                $newUsers[] = array_diff_key($user, array_flip($missingFields));
            }

            $this->db->exec_TRUNCATEquery('fe_users');
            $this->db->exec_TRUNCATEquery('fe_groups');
            $insertedUsers = $this->db->exec_INSERTmultipleRows('fe_users', array_keys($newUsers[0]), $newUsers);
            $insertedGroups = $this->db->exec_INSERTmultipleRows('fe_groups', array_keys($groups['0']), $groups);

            if ($insertedUsers === true && $insertedGroups === true) {
                $updatedUsers = $this->db->exec_UPDATEquery('fe_users', '1 = 1', ['pid' => $targetPid]);
                $updatedGroups = $this->db->exec_UPDATEquery('fe_groups', '1 = 1', ['pid' => $targetPid]);

                if ($updatedUsers === true && $updatedGroups === true) {
                    $this->outputLine($this->getColoredString('Successfully migrated the users / groups', 'green'));
                } else {
                    throw new \RuntimeException('Could not update the records', 1408607240);
                }
            }
        } else {
            throw new \RuntimeException('Could not reselect the original database', 1408569057);
        }
    }

    /**
     * Write Evalanche country IDs to static_countries
     */
    public function evalancheCountryMappingCommand()
    {
        $evalancheMapping = [
            [62, 'Afghanistan'],
            [63, 'Albania'],
            [64, 'Algeria'],
            [128, 'American Samoa'],
            [65, 'Andorra'],
            [66, 'Angola'],
            [130, 'Anguilla'],
            [131, 'Antarctica'],
            [132, 'Antigua and Barbuda'],
            [134, 'Argentina'],
            [67, 'Armenia'],
            [135, 'Aruba'],
            [115, 'Australia'],
            [2, 'Austria'],
            [68, 'Azerbaijan'],
            [136, 'Bahamas'],
            [69, 'Bahrain'],
            [70, 'Bangladesh'],
            [137, 'Barbados'],
            [71, 'Belarus'],
            [15, 'Belgium'],
            [138, 'Belize'],
            [72, 'Benin'],
            [262, 'Bermuda'],
            [139, 'Bhutan'],
            [140, 'Bolivia'],
            [29, 'Bosnia'],
            [73, 'Botswana'],
            [142, 'Bouvet Island'],
            [124, 'Brazil'],
            [144, 'British Indian Ocean Territory'],
            [143, 'British Virgin Islands'],
            [145, 'Brunei'],
            [30, 'Bulgaria'],
            [74, 'Burkina Faso'],
            [146, 'Burundi'],
            [181, 'Cambodia'],
            [75, 'Cameroon'],
            [31, 'Canada'],
            [182, 'Canary Islands'],
            [183, 'Cape Verde'],
            [76, 'Central African Republic'],
            [147, 'Ceuta, Melilla'],
            [250, 'Chad'],
            [125, 'Chile'],
            [32, 'China'],
            [260, 'Christmas Island'],
            [185, 'Cocos (Keeling) Islands'],
            [186, 'Colombia'],
            [187, 'Comoros'],
            [77, 'Congo'],
            [148, 'Cook Islands'],
            [149, 'Costa Rica'],
            [40, 'Croatia'],
            [189, 'Cuba'],
            [59, 'Cyprus'],
            [53, 'Czech Republic'],
            [150, 'Côte d’Ivoire'],
            [188, 'Democratic Republic of the Congo'],
            [16, 'Denmark'],
            [151, 'Dominica'],
            [152, 'Dominican Republic'],
            [153, 'Djibouti'],
            [215, 'Timor-Leste'],
            [154, 'Ecuador'],
            [28, 'Egypt'],
            [155, 'El Salvador'],
            [133, 'Equatorial Guinea'],
            [156, 'Eritrea'],
            [34, 'Estonia'],
            [78, 'Ethiopia'],
            [157, 'Falkland Islands'],
            [158, 'Faroes'],
            [159, 'FiJi'],
            [17, 'Finland'],
            [18, 'France'],
            [160, 'French Guayana'],
            [161, 'French Polynesia'],
            [162, 'French Southern Territories'],
            [163, 'Gabon'],
            [79, 'Gambia'],
            [80, 'Georgia'],
            [1, 'Germany'],
            [81, 'Ghana'],
            [164, 'Gibraltar'],
            [19, 'Greece'],
            [166, 'Greenland'],
            [165, 'Grenada'],
            [167, 'Guadeloupe'],
            [168, 'Guam'],
            [169, 'Guatemala'],
            [170, 'Guernsey'],
            [171, 'Guinea'],
            [172, 'Guinea-Bissau'],
            [173, 'French Guiana'],
            [174, 'Haiti'],
            [175, 'Heard Island and McDonald Islands'],
            [176, 'Honduras'],
            [116, 'Hong Kong'],
            [56, 'Hungary'],
            [35, 'Iceland'],
            [82, 'India'],
            [83, 'Indonesia'],
            [84, 'Iran'],
            [85, 'Iraq'],
            [21, 'Ireland'],
            [177, 'Isle of Man'],
            [36, 'Israel'],
            [22, 'Italy'],
            [178, 'Jamaica'],
            [37, 'Japan'],
            [179, 'Jersey'],
            [38, 'Jordan'],
            [180, 'Cayman Islands'],
            [86, 'Kazakhstan'],
            [87, 'Kenya'],
            [184, 'Kiribati'],
            [263, 'Kosovo'],
            [88, 'Kuwait'],
            [89, 'Kyrgyzstan'],
            [190, 'Laos'],
            [41, 'Latvia'],
            [42, 'Lebanon'],
            [191, 'Lesotho'],
            [192, 'Liberia'],
            [90, 'Libya'],
            [91, 'Liechtenstein'],
            [43, 'Lithuania'],
            [23, 'Luxembourg'],
            [193, 'Macao'],
            [44, 'Macedonia'],
            [194, 'Madagascar'],
            [92, 'Malawi'],
            [121, 'Malaysia'],
            [195, 'Maldives'],
            [196, 'Mali'],
            [45, 'Malta'],
            [197, 'Marshall Islands'],
            [198, 'Martinique'],
            [199, 'Mauritania'],
            [200, 'Mauritius'],
            [201, 'Mayotte'],
            [122, 'Mexico'],
            [202, 'Micronesia'],
            [93, 'Moldova'],
            [94, 'Monaco'],
            [203, 'Mongolia'],
            [204, 'Montenegro'],
            [205, 'Montserrat'],
            [95, 'Morocco'],
            [96, 'Mozambique'],
            [206, 'Myanmar'],
            [207, 'Namibia'],
            [208, 'Nauru'],
            [97, 'Nepal'],
            [24, 'Netherlands'],
            [211, 'Netherlands Antilles'],
            [209, 'New Caledonia'],
            [117, 'New Zealand'],
            [210, 'Nicaragua'],
            [98, 'Niger'],
            [99, 'Nigeria'],
            [212, 'Niue'],
            [214, 'Norfolk Island'],
            [120, 'North Korea'],
            [213, 'Northern Marianas'],
            [61, 'Norway'],
            [46, 'Oman'],
            [100, 'Pakistan'],
            [216, 'Palau'],
            [101, 'Palestine'],
            [217, 'Panama'],
            [218, 'Papua New Guinea '],
            [219, 'Paraguay'],
            [220, 'Peru'],
            [221, 'Philippines'],
            [222, 'Pitcairn Islands'],
            [47, 'Poland'],
            [25, 'Portugal'],
            [223, 'Puerto Rico'],
            [102, 'Qatar'],
            [48, 'Romania'],
            [225, 'Rwanda'],
            [49, 'Russia'],
            [224, 'Réunion'],
            [227, 'Saint Barthélemy'],
            [228, 'Saint Martin'],
            [229, 'Samoa'],
            [230, 'San Marino'],
            [50, 'Saudi Arabia'],
            [232, 'Senegal'],
            [39, 'Serbia'],
            [233, 'Seychelles'],
            [234, 'Sierra Leone'],
            [114, 'Singapore'],
            [51, 'Slovakia'],
            [52, 'Slovenia'],
            [226, 'Solomon Islands'],
            [235, 'Somalia'],
            [126, 'South Africa'],
            [241, 'South Georgia and the South Sandwich Islands'],
            [118, 'South Korea'],
            [27, 'Spain'],
            [103, 'Sri Lanka'],
            [236, 'Saint Helena'],
            [237, 'Saint Kitts and Nevis'],
            [238, 'Saint Lucia'],
            [239, 'Saint Pierre and Miquelon'],
            [240, 'Saint Vincent and the Grenadines'],
            [104, 'Sudan'],
            [242, 'Suriname'],
            [243, 'Svalbard'],
            [105, 'Swaziland'],
            [26, 'Sweden'],
            [3, 'Switzerland'],
            [106, 'Syria'],
            [231, 'São Tomé e Príncipe'],
            [119, 'Taiwan'],
            [244, 'Tajikistan'],
            [107, 'Tanzania'],
            [245, 'Thailand'],
            [246, 'Togo'],
            [247, 'Tokelau'],
            [248, 'Tonga'],
            [249, 'Trinidad and Tobago'],
            [55, 'Tunisia'],
            [54, 'Turkey'],
            [251, 'Turkmenistan'],
            [252, 'Turks and Caicos Islands'],
            [253, 'Tuvalu'],
            [129, 'US Virgin Islands'],
            [108, 'Uganda'],
            [109, 'Ukraine'],
            [58, 'United Arab Emirates'],
            [20, 'United Kingdom'],
            [57, 'United States'],
            [254, 'United States Minor Outlying Islands'],
            [255, 'Uruguay'],
            [110, 'Uzbekistan'],
            [256, 'Vanuatu'],
            [257, 'Vatican City'],
            [258, 'Venezuela'],
            [123, 'Vietnam'],
            [259, 'Wallis and Futuna'],
            [261, 'Western Sahara'],
            [111, 'Yemen'],
            [112, 'Zambia'],
            [113, 'Zimbabwe'],
            [127, 'Åland']
        ];

        foreach ($evalancheMapping as $country) {
            $this->db->exec_UPDATEquery(
                'static_countries',
                'cn_short_en LIKE \'%' . $country[1]. '%\'',
                [
                    'tx_baumer_evalanche_country_id' => $country[0],
                ]
            );
        }

        // 60 is for other
        $this->db->exec_UPDATEquery(
            'static_countries',
            'tx_baumer_evalanche_country_id = 0',
            [
                'tx_baumer_evalanche_country_id' => 60,
            ]
        );
    }
}
