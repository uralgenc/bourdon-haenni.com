<?php
namespace Baumer\Baumer\Command;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions GmbH & Co. KG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use Baumer\Baumer\Controller\CsEndpointController;
use Baumer\Baumer\Domain\Model\Category;
use Baumer\Baumer\Domain\Model\CsView;
use Baumer\Baumer\Domain\Model\CsViewRecord;
use Baumer\Baumer\Exception\InvalidArgumentException;
use Baumer\Baumer\Utility\TsfeUtil;
use Exception;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\TimeTracker\NullTimeTracker;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Utility\EidUtility;

/**
 * Pim import command
 *
 * @package Baumer
 * @subpackage Controller
 * @route off
 */
class PimCommandController extends AbstractCommandController
{

    /**
     * @var \Baumer\Baumer\Service\Contentserv\CsConnection
     * @inject
     */
    protected $csConnection;

    /**
     * @var \Baumer\Baumer\Service\Contentserv\ViewService
     * @inject
     */
    protected $csViewService;

    /**
     * @var \Baumer\Baumer\Service\Contentserv\ProductService
     * @inject
     */
    protected $csProductService;

    /**
     * @var \Baumer\Baumer\Service\Contentserv\CoreService
     * @inject
     */
    protected $csCoreService;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRepository
     * @inject
     */
    protected $csViewRepository;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRecordRepository
     * @inject
     */
    protected $csViewRecordRepository;

    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Indexer\ContentservView
     * @inject
     */
    protected $csIndexer;

    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Indexer\File
     * @inject
     */
    protected $fileIndexer;

    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Indexer\Page
     * @inject
     */
    protected $pageIndexer;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var \TYPO3\CMS\Core\DataHandling\DataHandler
     */
    protected $tce;

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceFactory
     * @inject
     */
    protected $fileResourceFactory = null;

    /**
     * @var array
     */
    protected $attributeCache = [];

    /**
     * @var array
     */
    protected $bourdonSettings;

    /**
     * Imports categories and value lists from contentServ
     *
     * ./typo3/cli_dispatch.phpsh extbase pim:importcategories
     */
    public function importCategoriesCommand()
    {
        // initialize an instance of DataHandler in order to handle localisation
        $this->startTce();
        $languages = $this->settings['pimImport']['overlays'];
        $valueListIds = [];
        $categoryView = $this->csViewService->getView($this->settings['pimImport']['categoryView']);
        $tmpView = new CsView();
        $tmpView->fromJson($categoryView);
        $attributeIdsHoldingCategoryValueLists = $tmpView->getAllReferencedAttributeIds();
        foreach ($attributeIdsHoldingCategoryValueLists as $attributeIdHoldingCategoryValueLists) {
            $attributeForCategory = $this->csViewService->getAttribute($attributeIdHoldingCategoryValueLists, 1, '');
            if (!empty($attributeForCategory['ParamA'])) {
                $valueListIds[] = $attributeForCategory['ParamA'];
            }
        }


        foreach ($valueListIds as $valueListId) {
            $valueRange = $this->csCoreService->getValueRange($valueListId);

            // Ensure the category exists in default language
            $category = $this->categoryRepository->findOneByContentServId($valueListId);
            if (is_null($category)) {
                $category = new Category();
                $category->setPid(0);
                $category->setContentServId($valueListId);
                $category->setTitle($valueRange['Label']);
                $this->forcePersistObject($category, $this->categoryRepository);
            } else {
                $category->setTitle($valueRange['Label']);
            }
            // Localize Main categories
            foreach ($languages as $languageKey => $languageValue) {
                $translatedValueRange = $this->csCoreService->getValueRange($valueListId, $languageValue, '');
                if (isset($translatedValueRange['TranslatedLabel']) &&
                    strlen($translatedValueRange['TranslatedLabel']) > 0
                ) {
                    $updateValues = [
                        'title' => $translatedValueRange['TranslatedLabel'],
                    ];
                    $this->localize($languageKey, $category->getUid(), 'sys_category', $updateValues);
                }
            }

            // Ensure category subitems exist
            foreach ($valueRange['ValuerangeValues'] as $valueRangeValue) {
                $subCategory = $this->categoryRepository->findOneByContentServId($valueRangeValue['ID']);
                if (is_null($subCategory)) {
                    $subCategory = new Category();
                    $subCategory->setParent($category);
                    $subCategory->setContentServId($valueRangeValue['ID']);
                    $subCategory->setPid(0);
                    $subCategory->setTitle($valueRangeValue['Value']);
                    $this->forcePersistObject($subCategory, $this->categoryRepository);
                } else {
                    $subCategory->setTitle($valueRangeValue['Value']);
                }
                // Localize sub categories
                foreach ($languages as $languageKey => $languageValue) {
                    if (isset($valueRangeValue['Values'][$languageKey]) &&
                        strlen($valueRangeValue['Values'][$languageKey]) > 0
                    ) {
                        $updateValues = [
                            'title' => $valueRangeValue['Values'][$languageKey],
                        ];
                        $this->localize($languageKey, $subCategory->getUid(), 'sys_category', $updateValues);
                    }
                }
            }
        }
        // reset admin access for scheduler task user
        /** @noinspection PhpInternalEntityUsedInspection */
        $this->tce->BE_USER->user['admin'] = 0;
        return true;
    }

    /**
     * Process the oldest item in the CS worker queue
     */
    public function processCsWorkerQueueCommand()
    {
        $res = $this->db->exec_SELECTgetRows(
            '*',
            'tx_baumer_cs_worker_queue',
            'NOT in_progress AND NOT failure',
            '',
            'crdate ASC',
            '1'
        );
        if (is_array($res) && $res[0]['uid'] > 0) {
            $task = $res[0];
            $this->db->exec_UPDATEquery(
                'tx_baumer_cs_worker_queue',
                'uid = ' . $task['uid'],
                [
                    'in_progress' => 1,
                    'tstamp' => time(),
                ]
            );
            $parameters = unserialize($task['parameters']);

            try {
                switch ($task['action']) {
                    case CsEndpointController::UPDATE_PRODUCT_ACTION:
                        $result = $this->updateProductAction($parameters['contentServId']);
                        break;
                    case CsEndpointController::DELETE_PRODUCT_ACTION:
                        $result = $this->deleteProductAction($parameters['contentServId']);
                        break;
                    case CsEndpointController::UPDATE_VIEW_ACTION:
                        $result = $this->updateViewAction($parameters['contentServId']);
                        break;
                    case CsEndpointController::DELETE_VIEW_ACTION:
                        $result = $this->deleteViewAction($parameters['contentServId']);
                        break;
                    default:
                        $result = ['status' => true, 'message' => 'Action does not exist'];
                }
            } catch (Exception $e) {
                $result = ['status' => false, 'message' => $e->getMessage() . ' (' . $e->getCode() . ')'];
                $this->db->exec_UPDATEquery(
                    'tx_baumer_cs_worker_queue',
                    'uid = ' . $task['uid'],
                    ['in_progress' => 0, 'failure' => 1, 'message' => $result['message']]
                );
            } finally {
                if ($result['status']) {
                    $this->db->exec_DELETEquery('tx_baumer_cs_worker_queue', 'uid = ' . $task['uid']);
                } else {
                    $this->db->exec_UPDATEquery(
                        'tx_baumer_cs_worker_queue',
                        'uid = ' . $task['uid'],
                        ['in_progress' => 0, 'failure' => 1, 'message' => $result['message']]
                    );
                }
            }

            return true;
        } else {
            // no open non running tasks found
            return true;
        }
    }

    /**
     * Load CsView models and CsViewRecord models via Contentserv REST-API
     *
     * @param int $csViewFolderId
     */
    public function fullImportViewsCommand($csViewFolderId = 0)
    {
        $this->logger->info('Started full-import for ContentServ views', ['CS-Folder-ID' => $csViewFolderId]);
        if ($csViewFolderId === 0) {
            $csViewFolderId = (int)$this->settings['pimImport']['CsViewsFolder'];
        }

        /*
         * Step 1: Deaktivate CsEndpoint Worker Queue to avoid concurrence in elastic search index
         */
        $this->db->exec_UPDATEquery(
            'tx_scheduler_task',
            'serialized_task_object LIKE "%baumer:pim:processcsworkerqueue%"',
            ['disable' => 1]
        );

        /*
         * Step 2: Load and insert/update all views of the given tree
         */
        $csViews = [];
        $contentservViews = $this->csViewService->getLeafViews($csViewFolderId, 1, '');
        foreach ($contentservViews as $contentservView) {
            $csView = $this->importView($contentservView['ID']);
            if ($csView instanceof CsView) {
                $csViews[] = $csView;
            }
        }

        /*
         * Step 3: Localize all views
         */
        $this->localizeViews($csViews);
        $this->localizeVirtualViews();

        /*
         * Step 4: Load all products and insert/update them
         */
        $alreadyImportedProductIds = [];
        /** @var CsView $csView */
        foreach ($csViews as $csView) {
            foreach ($csView->getProductIds() as $productId) {
                if (in_array($productId, $alreadyImportedProductIds)) {
                    $csViewRecord = $this->csViewRecordRepository->findOneByContentServId($productId);
                    if ($csViewRecord instanceof CsViewRecord) {
                        $csViewRecord->groupAttributesForView($csView, $this->getValidAttributeGroupIds());
                        $this->csViewRecordRepository->update($csViewRecord);
                        $csViewRecord->addView($csView);
                    }
                } else {
                    $csViewRecord = $this->importViewRecord($productId);
                    if ($csViewRecord instanceof CsViewRecord) {
                        $csViewRecord->addView($csView);
                        $csViewRecord->setPid($this->getStoragePidAndInitTsfe($csView));
                        $csViewRecord->groupAttributesForView($csView, $this->getValidAttributeGroupIds());
                        $csViewRecord->setSite($this->determineSite($csViewRecord));
                        $this->csViewRecordRepository->update($csViewRecord);
                        $alreadyImportedProductIds[] = $csViewRecord->getContentServId();
                    }
                }
            }
            $this->persistenceManager->persistAll();
        }

        /*
         * Step 5: Localize all CsViewRecords and index them in one go
         */
        $indexSuffix = '_' . time();
        $this->csIndexer->ensureIndexAndTypeMappingsAllLanguages($indexSuffix);
        /** @var CsViewRecord $csViewRecord */
        foreach ($this->csViewRecordRepository->findByContentServIds($alreadyImportedProductIds) as $csViewRecord) {
            if ($csViewRecord instanceof CsViewRecord) {
                if ($csViewRecord->getSite() == CsViewRecord::BOURDON_SITE_KEY) {
                    $this->csIndexer->injectSettings($this->bourdonSettings);
                } else {
                    $this->csIndexer->injectSettings($this->settings);
                }
                $this->localizeViewRecord($csViewRecord);
                $this->csIndexer->indexViewRecord($csViewRecord, $indexSuffix);
            }
        }

        /*
         * Step 6: Index files and pages. Because we create new indices, we have to reindex everything
         */
        $this->fileIndexer->indexFiles($indexSuffix);
        // $this->pageIndexer->indexPages($this->settings['rootPids']['int'], $indexSuffix);
        $this->pageIndexer->indexPages($this->bourdonSettings['rootPids']['int'], $indexSuffix);
        $this->csIndexer->ensureAliasForAllIndizesWithSuffix($indexSuffix);

        /*
         * Step 6: Delete orphans (records that were not imported and therefore should be deleted)
         */
        $this->csViewRepository->deleteOrphans($csViews);
        $this->csViewRecordRepository->deleteOrphansComparingContentServIds($alreadyImportedProductIds);

        /*
         * Step 7: Reactivate Scheduler task
         */
        $this->db->exec_UPDATEquery(
            'tx_scheduler_task',
            'uid = ' . (int)$this->settings['pimImport']['csEndpointWorkerQueueSchedulerTask'],
            ['disable' => 0],
            true
        );

        // reset admin access for scheduler task user
        /** @noinspection PhpInternalEntityUsedInspection */
        $this->tce->BE_USER->user['admin'] = 0;
        $this->logger->info('Finished full-import for ContentServ views', ['CS-Folder-ID' => $csViewFolderId]);
    }

    /*
     * CsEndpoint worker queue actions
     */

    /**
     * Request a given ContentServ ID and the product (CsViewRecord) with all attributes in the DB
     *
     * @param string $contentServId
     * @return string JSON response
     */
    protected function updateProductAction($contentServId)
    {
        /*
         * Step 1: Load product from ContentServ and Update/Insert Product in TYPO3 DB
         */
        $csViewRecord = $this->importViewRecord($contentServId);
        if ($csViewRecord instanceof CsViewRecord) {
            $this->persistenceManager->persistAll();

            /*
             * Step 2: Find views containing the given product and update them
             */
            $views = $this->loadViewsForProductId($csViewRecord->getContentServId());

            /*
             * Step 3: Load and insert/updates views
             */
            $storagePid = false;
            $csViews = [];
            $newCsViews = [];
            foreach ($views as $view) {
                $csView = $this->csViewRepository->findOneByContentServId($view['ID']);
                if (!$csView instanceof CsView) {
                    $csView = $this->importView($view['ID']);
                    if ($csView instanceof CsView) {
                        $newCsViews[] = $csView;
                    }
                }
                if ($csView instanceof CsView) {
                    $csViews[] = $csView;
                    if (!$storagePid) {
                        $storagePid = $this->getStoragePidAndInitTsfe($csView);
                    }
                    $csViewRecord->addView($csView);
                    $csViewRecord->groupAttributesForView($csView, $this->getValidAttributeGroupIds());
                }
            }
            if (!$storagePid) {
                // no view from the Online View Trees could be found for product
                return [
                    'status' => false,
                    'message' => 'No view from the Online View Trees could be found for product with ID ' .
                                 $contentServId .
                                 ' (1458063773)',
                ];
            }
            $csViewRecord->setPid($storagePid);
            $csViewRecord->setSite($this->determineSite($csViewRecord));

            // We have changed the csViewRecord (set groupAttributes), so we have to save it - again!
            $this->csViewRecordRepository->update($csViewRecord);
            $this->persistenceManager->persistAll();

            /*
             * Step 4: Localize CsProduct & news CsViews
             */
            $this->localizeViews($newCsViews);
            $this->localizeViewRecord($csViewRecord);

            /*
             * Step 5: Update Elasticsearch
             */
            if ($csViewRecord->getSite() == CsViewRecord::BOURDON_SITE_KEY) {
                $this->csIndexer->injectSettings($this->bourdonSettings);
            } else {
                $this->csIndexer->injectSettings($this->settings);
            }
            $this->csIndexer->indexViewRecord($csViewRecord);

            /*
             * Step 6: Update workflow status for product in ContentServ, but only in staging server
             */
            if (GeneralUtility::getApplicationContext()->isProduction()
                && GeneralUtility::getApplicationContext()->__toString() === 'Production/Staging'
            ) {
                if (!$this->csProductService->updateWorkflow(
                    $contentServId,
                    (int)$this->settings['pimImport']['workflowActions']['product']['availableOnStage']
                )
                ) {
                    // status update failed
                    return [
                        'status' => false,
                        'message' => 'Product for ContentServ ID' .
                                     $contentServId .
                                     ' was inserted/updated but workflow action failed. Therefor TYPO3 has correct data but status in CS wrong. (1458146420)',
                    ];
                };
            }
        } else {
            // no product was returned by ContentServ
            return [
                'status' => false,
                'message' => 'No product was return by ContentServ for ID' .
                             $contentServId .
                             ' or has no valid workflow state (1458063671)',
            ];
        }

        return ['status' => true, 'message' => ''];
    }

    /**
     * Deletes a given CsViewRecord
     *
     * @param $contentServId
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    protected function deleteProductAction($contentServId)
    {
        $csViewRecord = $this->csViewRecordRepository->findOneByContentServId($contentServId);
        if ($csViewRecord instanceof CsViewRecord) {
            $this->csIndexer->deleteViewRecord($csViewRecord);
            $this->csViewRecordRepository->remove($csViewRecord);
            return ['status' => true, 'message' => $csViewRecord->getLabel() . ' was set to deleted from TYPO3 DB'];
        } else {
            return [
                'status' => false,
                'message' => 'CsViewRecord with ContentServ ID ' . $contentServId . ' could not be found (1458316526)',
            ];
        }
    }

    /**
     * Update/Insert a CsView and its changed CsViewRecord Relations
     *
     * @param $contentServId
     * @return array
     * @throws InvalidArgumentException
     */
    protected function updateViewAction($contentServId)
    {
        /*
         * Step 1: Load and insert/update CsView
         */
        $csView = $this->importView($contentServId);
        if ($csView instanceof CsView) {
            $newProductContentServIds = array_diff(
                $csView->getProductIds(),
                $csView->getCsViewRecordContentservIds()
            );
            $orphanProductsContentServId = array_diff(
                $csView->getCsViewRecordContentservIds(),
                $csView->getProductIds()
            );
            if (count($newProductContentServIds) > 0) {
                /*
                 * Step 1.1 Add new products
                 */
                foreach ($newProductContentServIds as $newProductContentServId) {
                    $csViewRecord = $this->csViewRecordRepository->findOneByContentServId($newProductContentServId);
                    if ($csViewRecord instanceof CsViewRecord) {
                        $csView->addRecord($csViewRecord);
                    } else {
                        $csViewRecord = $this->importViewRecord($newProductContentServId);
                        $csViewRecord->setPid($csView->getPid());
                        $this->persistenceManager->persistAll();

                        if ($csViewRecord instanceof CsViewRecord) {
                            $csView->addRecord($csViewRecord);
                        }
                    }
                }
                /*
                 * Step 1.2 Remove products not in view any more
                 */
                foreach ($orphanProductsContentServId as $poorOrphanProductContentServId) {
                    // we only detach the orphan form this view, it might still be valid for other views
                    // If it becomes a full orphan, the full import task will end its misery.
                    $csView->removeCsViewRecordByContentservId($poorOrphanProductContentServId);
                }
            }

            /*
             * Step 2: Update grouped attributes of CsViewRecords
             */
            $this->persistenceManager->persistAll();
            $this->localizeViews([$csView]);
            /** @var CsViewRecord $csViewRecord */
            foreach ($csView->getRecords() as $csViewRecord) {
                $csViewRecord->groupAttributesForView($csView, $this->getValidAttributeGroupIds());
                $this->csViewRecordRepository->update($csViewRecord);
            }
            $this->persistenceManager->persistAll();

            /*
             * Step 3: Localize and index to elasticsearch
             */
            foreach ($csView->getRecords() as $csViewRecord) {
                $this->localizeViewRecord($csViewRecord);
                if ($csViewRecord->getSite() == CsViewRecord::BOURDON_SITE_KEY) {
                    $this->csIndexer->injectSettings($this->bourdonSettings);
                } else {
                    $this->csIndexer->injectSettings($this->settings);
                }
                $this->csIndexer->indexViewRecord($csViewRecord);
            }

            /*
             * Step 4: Update workflow status for product in ContentServ, but only on staging server
             */
            if (GeneralUtility::getApplicationContext()->isProduction()
                && GeneralUtility::getApplicationContext()->__toString() === 'Production/Staging'
            ) {
                if (!$this->csViewService->updateWorkflow(
                    $contentServId,
                    (int)$this->settings['pimImport']['workflowActions']['view']['availableOnStage']
                )
                ) {
                    // status update failed
                    return [
                        'status' => false,
                        'message' => 'Product for ContentServ ID' .
                                     $contentServId .
                                     ' was inserted/updated but workflow action failed. Therefor TYPO3 has correct data but status in CS wrong. (1458146420)',
                    ];
                };
            }
        } else {
            return [
                'status' => false,
                'message' => 'CsView for ContentServ ID ' .
                             $contentServId .
                             ' could not be found or has no valid workflow state (1458326619)',
            ];
        }
        return ['status' => true, 'message' => ''];
    }

    /**
     * Deletes a given CsView
     *
     * @param $contentServId
     * @return array
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    protected function deleteViewAction($contentServId)
    {
        $csView = $this->csViewRepository->findOneByContentServId($contentServId);
        if ($csView instanceof CsView) {
            $this->csViewRepository->remove($csView);
            return ['status' => true, 'message' => $csView->getLabel() . ' was set to deleted from TYPO3 DB'];
        } else {
            return [
                'status' => false,
                'message' => 'CsView with ContentServ ID ' . $contentServId . ' could not be found (1458316582)',
            ];
        }
    }

    /*
     * Private methods below
     */

    /**
     * Start the DataHandler and the CommandHandler of the TYPO3 core
     * in order to do CmdMap and DataMap commands
     */
    protected function startTce()
    {
        // initialize an instance of DataHandler in order to handle localisation
        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tce */
        $this->tce = $this->objectManager->get('TYPO3\\CMS\\Core\\DataHandling\\DataHandler');
        $this->tce->bypassAccessCheckForRecords = true;
        $this->tce->updateModeL10NdiffData = false;
        $this->tce->enableLogging = false;
        $this->tce->BE_USER = $GLOBALS['BE_USER'];
        // user needs permission to write records for tables
        /** @noinspection PhpInternalEntityUsedInspection */
        $this->tce->BE_USER->user['admin'] = 1;
        $this->tce->stripslashes_values = 0;
    }

    /**
     * @param AbstractEntity $object
     * @param Repository $repository
     *
     * @return AbstractEntity
     */
    protected function forcePersistObject(AbstractEntity $object, Repository $repository)
    {
        $repository->add($object);
        $this->persistenceManager->persistAll();

        return $object;
    }

    /**
     * Localize given record into provided language,
     * afterwards overlay new records with values from updateValues array
     *
     * @param int $languageKey target language uid
     * @param int $originalUid uid of record in default language
     * @param string $table table of record insert
     * @param array $updateValues fields to update (fieldName => value)
     *
     * @throws Exception
     */
    protected function localize($languageKey, $originalUid, $table, $updateValues = [])
    {
        $res = $this->db->exec_SELECTgetRows(
            'uid',
            $table,
            'sys_language_uid = ' . (int)$languageKey . ' AND l10n_parent = ' . (int)$originalUid . ' AND NOT deleted'
        );
        if ((int)$res[0]['uid'] > 0) {
            $dbResult = $this->db->exec_UPDATEquery(
                $table,
                'uid = ' . (int)$res[0]['uid'],
                array_merge($updateValues, ['hidden' => 0, 'tstamp' => time()])
            );
        } else {
            $dbResult = $this->db->exec_INSERTquery(
                $table,
                array_merge(
                    $updateValues,
                    [
                        'hidden' => 0,
                        'l10n_parent' => (int)$originalUid,
                        'sys_language_uid' => (int)$languageKey,
                        'tstamp' => time(),
                        'crdate' => time(),
                    ]
                )
            );
        }
        if (!$dbResult) {
            throw new Exception(
                'Localization DB operation failed for table ' .
                $table .
                ', originalUid ' .
                $originalUid .
                ', language ' .
                $languageKey .
                '. result: ' .
                var_dump($res), 1459675690
            );
        }
    }

    /**
     * Import a CsView by it's ContentservId using the REST API.
     * The Records related to this view will also be imported
     *
     * @param string $contentservViewId
     * @return CsView|null
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    protected function importView($contentservViewId)
    {
        $contentservViewDetails = $this->csViewService->getView($contentservViewId);
        if (false !== $contentservViewDetails && in_array(
                $contentservViewDetails['State']['ID'],
                $this->getValidWorkflowStates('view')
            )
        ) {
            $csView = $this->csViewRepository->findOneByContentServId($contentservViewId);
            if (!$csView instanceof CsView) {
                $csView = new CsView();
            }
            $csView->fromJson($contentservViewDetails);
            if ($csView->_isNew()) {
                $this->csViewRepository->add($csView);
            } else {
                $this->csViewRepository->update($csView);
            }
            if ($storagePid = $this->getStoragePidAndInitTsfe($csView)) {
                $csView->setPid($storagePid);
                $this->persistenceManager->persistAll();
                return $csView;
            } else {
                // no storage pid means we have a view outside of the baumer or bourdon online views tree
                // or a malformed view.
                return null;
            }
        } else {
            return null;
        }
    }

    /*
     * Import a single Cs Product in default language
     *
     * @param $contentServId
     * @return CsViewRecord
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    protected function importViewRecord($contentServId)
    {
        $product = $this->csProductService->getProduct($contentServId);
        if (false !== $product && in_array(
                $product['State']['ID'],
                $this->getValidWorkflowStates('product')
            )
        ) {
            $attributeIds = [];
            foreach ($product['Attributes'] as $attributeIdx => $attribute) {
                $product['Attributes'][$attributeIdx] = $this->unsetUnusedAndReformatAttributeFields($attribute);
                if (is_null($attribute['TranslatedName'])) {
                    $product['Attributes'][$attributeIdx]['TranslatedName'] = '';
                }
                if (is_null($attribute['FormattedValue'])) {
                    $product['Attributes'][$attributeIdx]['FormattedValue'] = '';
                }
                $attributeIds[] = $attribute['ID'];
            }
            $this->cacheAttributes($attributeIds);
            $product = $this->enrichProductsAttributes([$product])[0];

            $csViewRecord = $this->csViewRecordRepository->findOneByContentServId($product['ID']);
            if (!$csViewRecord instanceof CsViewRecord) {
                $csViewRecord = new CsViewRecord();
            }
            $csViewRecord->fromJson($product);
            if ($csViewRecord->_isNew()) {
                $this->csViewRecordRepository->add($csViewRecord);
            } else {
                $this->csViewRecordRepository->update($csViewRecord);
            }
            $this->persistenceManager->persistAll();
            return $csViewRecord;
        } else {
            return null;
        }
    }

    /**
     * Removes unused attribute Fields and reformated/adds other
     *
     * @param $attribute
     * @return mixed
     */
    protected function unsetUnusedAndReformatAttributeFields($attribute)
    {
        $removeFields = [
            'TranslatedLabels',
            'TranslatedLabel',
            'ExternalKey',
            'Self',
            'Category',
            'BaseType',
            'CSType',
            'TranslatedPaneTitles',
            'TranslatedSectionTitles',
            'DefaultValue',
            'IsEditable',
            'IsRequired',
            'IsSearchable',
            'ExportSeparator',
            'IsPreviewImage',
            'PaneTitlevalueRange',
            'SectionTitlevalueRange',
            'ParamB',
            'ParamC',
            'ParamD',
            'ParamE',
            'ParamF',
            'ParamG',
            'ParamH',
            'ParamI',
            'ParamJ',
        ];
        $integerFields = ['Sortorder'];
        foreach ($removeFields as $removeField) {
            unset($attribute[$removeField]);
        }
        if ($attribute['Type'] == 'number' && is_numeric($attribute['Value'])) {
            $attribute['NumberValue'] = (float)$attribute['Value'];
        } else {
            $attribute['NumberValue'] = 0;
        }
        foreach ($integerFields as $integerField) {
            $attribute[$integerField] = (int)$attribute[$integerField];
        }
        return $attribute;
    }

    /**
     * We keep all attributes with their languages in memory,
     * because they are reused by every product.
     *
     * @param array $attributeIds
     */
    protected function cacheAttributes($attributeIds)
    {
        $languages = $this->settings['pimImport']['overlays'];
        if (!isset($languages[0])) {
            $languages[0] = 'en';
        }
        foreach ($languages as $languageKey => $languageValue) {
            $languageKey = (int)$languageKey;
            if (!isset($this->attributeCache[$languageKey])) {
                $this->attributeCache[$languageKey] = [];
            }
            foreach ($attributeIds as $attrId) {
                if (!isset($this->attributeCache[$languageKey][$attrId]) ||
                    empty($this->attributeCache[$languageKey][$attrId])
                ) {
                    $attribute = $this->csViewService->getAttribute($attrId, $languageValue);
                    $attribute = $this->unsetUnusedAndReformatAttributeFields($attribute);
                    $this->attributeCache[$languageKey][$attrId] = $attribute;
                }
            }
        }
    }

    /**
     * This should only be used after all attributeIds used by the product
     * have been cached.
     *
     * @param array $products
     * @param integer $languageKey
     * @return array
     */
    protected function enrichProductsAttributes($products, $languageKey = 0)
    {
        if (!is_array($products)) {
            return $products;
        }
        foreach ($products as $productIdx => $product) {
            if (isset($product['Attributes']) && is_array($product['Attributes'])) {
                foreach ($product['Attributes'] as $attrIdx => $attribute) {
                    if (isset($this->attributeCache[$languageKey][$attribute['ID']])) {
                        $attribute = array_merge($attribute, $this->attributeCache[$languageKey][$attribute['ID']]);
                        $attribute = $this->unsetUnusedAndReformatAttributeFields($attribute);
                        $products[$productIdx]['Attributes'][$attrIdx] = $attribute;
                    }
                }
            }
        }
        return $products;
    }

    /**
     * Load and Find View that contain the product.
     *
     * @param $contentServId
     * @return array
     */
    protected function loadViewsForProductId($contentServId)
    {
        $views = [];
        $viewCandidates = array_merge(
            $this->csViewService->getChildren(self::BAUMER_CS_ONLINE_VIEW, 1, 'Selection'),
            $this->csViewService->getChildren(self::BOURDON_CS_ONLINE_VIEW, 1, 'Selection')
        );
        foreach ($viewCandidates as $viewCandidate) {
            if (is_array($viewCandidate['Selection'])) {
                foreach ($viewCandidate['Selection'] as $productInViewCandidate) {
                    if ($productInViewCandidate['ID'] == $contentServId) {
                        $views[] = $viewCandidate;
                        break;
                    }
                }
            }
        }
        return $views;
    }

    /**
     * Determines the storage pid based on the cs view path and call the initTsfe method with proper root page id
     *
     * @param $csView
     * @return int
     * @throws Exception
     * @internal param $csObject
     */
    protected function getStoragePidAndInitTsfe($csView)
    {
        $storagePid = false;
        if ($csView instanceof CsView) {
            if (is_array($csView->getData()['Path']['IDs']) &&
                in_array(self::BOURDON_CS_ONLINE_VIEW, $csView->getData()['Path']['IDs'])
            ) {
                $storagePid = (int)$this->bourdonSettings['pimImport']['storages']['productStorage'];
                if (!(isset($GLOBALS['TSFE']) && $GLOBALS['TSFE'] instanceof TypoScriptFrontendController) ||
                    (int)$GLOBALS['TSFE']->id !== (int)$this->bourdonSettings['rootPids']['int']
                ) {
                    TsfeUtil::initializeTsfe((int)$this->bourdonSettings['rootPids']['int'], 0, false);
                }
            } elseif (is_array($csView->getData()['Path']['IDs']) &&
                      in_array(self::BAUMER_CS_ONLINE_VIEW, $csView->getData()['Path']['IDs'])
            ) {
                $storagePid = (int)$this->settings['pimImport']['storages']['productStorage'];
                if (!(isset($GLOBALS['TSFE']) && $GLOBALS['TSFE'] instanceof TypoScriptFrontendController) ||
                    (int)$GLOBALS['TSFE']->id !== 1
                ) {
                    TsfeUtil::initializeTsfe(1, 0, false);
                }
            }
        }
        return $storagePid;
    }

    /**
     * Get all attributes ID that are configured as grouped attributes
     *
     * @return array
     */
    protected function getValidAttributeGroupIds()
    {
        return array_merge(
            array_values($this->settings['productfinder']['attributes']),
            array_values($this->settings['configurator']['attributes']),
            array_values($this->bourdonSettings['productfinder']['attributes']),
            array_values($this->bourdonSettings['configurator']['attributes'])
        );
    }

    /**
     * Determine and return the site (baumer or bourdon) based in storage pid (in lieu of a better criteria)
     *
     * @param CsViewRecord $csViewRecord
     * @return string
     */
    protected function determineSite(CsViewRecord $csViewRecord)
    {
        if ($csViewRecord->getPid() == (int)$this->bourdonSettings['pimImport']['storages']['productStorage']) {
            return CsViewRecord::BOURDON_SITE_KEY;
        } else {
            if ($csViewRecord->getPid() == (int)$this->settings['pimImport']['storages']['productStorage']) {
                return CsViewRecord::BAUMER_SITE_KEY;
            } else {
                return '';
            }
        }
    }

    /**
     * Create translation overlays for the !already persisted! $csViews
     *
     * @param CsView[] $csViews
     * @throws InvalidArgumentException
     */
    protected function localizeViews($csViews)
    {
        $languages = $this->settings['pimImport']['overlays'];
        // Create language overlays for values from typoscript configuration
        /** @var CsView $csView */
        foreach ($csViews as $csView) {
            if (empty($csView->getUid())) {
                throw new InvalidArgumentException('Cannot localize CsView that is not persisted', 1445941098);
            }
            foreach ($languages as $languageKey => $languageValue) {
                if ($languageKey == 0) {
                    continue;
                } else {
                    $translatedLeaf = $this->csViewService->getView($csView->getContentServId(), $languageValue, '*');
                    if (is_array($translatedLeaf)) {
                        $this->localize(
                            $languageKey,
                            $csView->getUid(),
                            'tx_baumer_domain_model_csview',
                            [
                                'pid' => $csView->getPid(),
                                'label' => $translatedLeaf['Label'],
                                'data' => json_encode($translatedLeaf),
                                'content_serv_id' => $csView->getContentServId(),
                                'class' => $csView->getClass(),
                            ]
                        );
                    }
                }
            }
        }
    }

    /**
     * Given the !already persisted! $csViewRecord,
     * we create the language overlays for the CsViewRecord.
     *
     * @param CsViewRecord $csViewRecord
     * @throws InvalidArgumentException
     */
    protected function localizeViewRecord(CsViewRecord $csViewRecord)
    {
        if (empty($csViewRecord->getUid())) {
            throw new InvalidArgumentException('Cannot localize CsViewRecord that is not persisted', 1445941099);
        }
        $languages = $this->settings['pimImport']['overlays'];
        foreach ($languages as $languageKey => $languageValue) {
            if ($languageKey == 0) {
                continue;
            } else {

                // get translation from CS
                $translatedProduct = $this->csProductService->getProduct(
                    $csViewRecord->getContentServId(),
                    $languageValue
                );
                $translatedProduct = $this->enrichProductsAttributes([$translatedProduct], $languageKey)[0];
                // avoid null values
                foreach ($translatedProduct['Attributes'] as $attributeIdx => $attribute) {
                    if (is_null($attribute['TranslatedName'])) {
                        $translatedProduct['Attributes'][$attributeIdx]['TranslatedName'] = '';
                    }
                    if (is_null($attribute['FormattedValue'])) {
                        $translatedProduct['Attributes'][$attributeIdx]['FormattedValue'] = '';
                    }
                }
                // group attributes
                $translatedProduct['GroupedAttributes'] = [];
                /** @var CsView $csView */
                foreach ($csViewRecord->getCsViews() as $csView) {
                    if (!empty($attributeGroupIds = $csView->getAttributeIdGroups())) {
                        foreach ($attributeGroupIds as $groupId => $attributeIds) {
                            if (in_array($groupId, $this->getValidAttributeGroupIds())) {
                                $translatedProduct['GroupedAttributes'][$groupId] = [];
                                foreach ($attributeIds as $attributeId) {
                                    $attribute = null;
                                    foreach ($translatedProduct['Attributes'] as $attributeCandidate) {
                                        if (is_numeric($attributeCandidate['ID']) &&
                                            (int)$attributeCandidate['ID'] === (int)$attributeId
                                        ) {
                                            $attribute = $attributeCandidate;
                                            break;
                                        }
                                    }
                                    if ($attribute) {
                                        $translatedProduct['GroupedAttributes'][$groupId][] = $attribute;
                                    }
                                }
                            }
                        }
                    }
                }
                // do localization in database
                if (is_array($translatedProduct)) {
                    $this->localize(
                        $languageKey,
                        $csViewRecord->getUid(),
                        'tx_baumer_domain_model_csviewrecord',
                        [
                            'label' => $translatedProduct['Label'],
                            'pid' => $csViewRecord->getPid(),
                            'sorting' => 0,
                            'data' => json_encode($translatedProduct),
                            'sap_id' => $csViewRecord->getSapId(),
                            'content_serv_id' => $csViewRecord->getContentServId(),
                        ]
                    );
                }
            }
        }
    }


    /**
     * The given !already persisted! $csViews are not actual Contentserv Views, but
     * are generated as CsView because they are folders containing products.
     * Therefore the translations are not selected from the View REST endpoint,
     * but the Product REST endpoint.
     *
     * @throws InvalidArgumentException
     */
    protected function localizeVirtualViews()
    {
        $csViews = $this->csViewRepository->findAllSubselectors();
        $languages = $this->settings['pimImport']['overlays'];
        // Create language overlays for values from typoscript configuration
        /** @var CsView $csView */
        foreach ($csViews as $csView) {
            $usedAttributeIds = $csView->getAllAttributeIds();
            $attributeIdGroups = $csView->getAttributeIdGroups();
            $this->cacheAttributes($usedAttributeIds);

            foreach ($languages as $languageKey => $languageValue) {
                $translatedVirtualViews = $this->csProductService->search(
                    [$csView->getContentServId()],
                    $usedAttributeIds,
                    $languageValue,
                    $attributeIdGroups
                );
                if (is_array($translatedVirtualViews) && !empty($translatedVirtualViews)) {
                    $translatedVirtualView = array_pop($translatedVirtualViews);
                    $translatedVirtualView['isVirtualView'] = true;
                    $originalData = $csView->getData();
                    if (isset($originalData['Selection'])) {
                        $translatedVirtualView['Selection'] = $originalData['Selection'];
                    }
                    $this->localize(
                        $languageKey,
                        $csView->getUid(),
                        'tx_baumer_domain_model_csview',
                        [
                            'label' => $translatedVirtualView['Label'],
                            'data' => json_encode($translatedVirtualView),
                            'content_serv_id' => $csView->getContentServId(),
                            'class' => $csView->getClass(),
                        ]
                    );
                }
            }
        }
    }

    /**
     * initialize controller settings
     */
    protected function initializeCommand()
    {
        parent::initializeCommand();
        $this->configurationManager->setConfiguration();
        $this->configurationManager->setCurrentPageId(self::BOURDON_ROOT_PID);
        $this->bourdonSettings = [];
        $tsBourdonSettings = $this->configurationManager->getConfiguration('bourdon');
        if (isset($tsBourdonSettings['settings'])) {
            $this->bourdonSettings = $tsBourdonSettings['settings'];
        }
    }

    /**
     * The given $product is a Contentserv-Folder an therefore contains Sub-Products.
     * E.g. MEX => MEX2, MEX5, MEX7
     * This will be treated as a CsView with CsViewRecords, as it contains the same logic.
     *
     * @param array $product
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    protected function importProductFolderAsSubselector($product)
    {
        $virtualView = $this->csViewRepository->findOneByContentServId($product['ID']);
        if (!$virtualView instanceof CsView) {
            $virtualView = new CsView();
        }
        $virtualView->setPid($this->settings['pimImport']['storages']['productStorage']);
        $product['isVirtualView'] = true;
        $virtualView->fromJson($product);
        $virtualView->setClass(CsView::CLASS_SELECTOR);
        $virtualView->setContentServId($product['ID']);

        $allUsedAttributeIds = $virtualView->getAllAttributeIds();
        $this->cacheAttributes($allUsedAttributeIds);
        $virtualProducts = $this->csProductService->searchChildren(
            $product['ID'],
            $virtualView->getAllAttributeIds(),
            1,
            $virtualView->getAttributeIdGroups()
        );
        $virtualProducts = $this->enrichProductsAttributes($virtualProducts, $virtualView->getData());
        $productContentservIds = [];
        foreach ($virtualProducts as $virtualProduct) {
            $productContentservIds[] = (int)$virtualProduct['ID'];
            if (!($virtualViewRecord = $virtualView->getCsViewRecordByContentservId($virtualProduct['ID']))
                 instanceof
                 CsViewRecord
            ) {
                $virtualViewRecord = new CsViewRecord();
            }
            $virtualViewRecord->setPid($this->settings['pimImport']['storages']['productStorage']);
            $virtualViewRecord->fromJson($virtualProduct);
            $virtualView->addRecord($virtualViewRecord);
        }
        $product['Selection'] = $virtualProducts;
        $virtualView->setData(json_encode($product));

        if ($virtualView->_isNew()) {
            $this->csViewRepository->add($virtualView);
        } else {
            $this->csViewRepository->update($virtualView);
        }
    }

    /**
     * @param array $attributes
     * @param int $attributeId
     * @return array|bool
     */
    protected function getSortingValuesById($attributes, $attributeId)
    {
        foreach ($attributes as $attribute) {
            if ($attribute['ID'] == $attributeId) {
                return explode(',', $attribute['Value']);
            }
        }

        return false;
    }

    /**
     * Only certain workflow states in CS are valid for publication. They differe from staging to live and their
     * mapping is configures in $this->settings['pimImport']['validWorkflowStates']
     *
     * @param string $type
     * @return mixed
     */
    protected function getValidWorkflowStates($type = 'product')
    {
        if (GeneralUtility::getApplicationContext()->isProduction()
            && GeneralUtility::getApplicationContext()->__toString() === 'Production/Live'
        ) {
            return $this->settings['pimImport']['validWorkflowStates']['live'][$type];
        } else {
            return $this->settings['pimImport']['validWorkflowStates']['staging'][$type];
        }
    }
}
