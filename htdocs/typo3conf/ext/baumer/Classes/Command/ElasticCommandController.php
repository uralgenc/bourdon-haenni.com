<?php
/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Martin Heigermoser <martin.heigermoser@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

namespace Baumer\Baumer\Command;

use Baumer\Baumer\Domain\Model\CsView;
use Baumer\Baumer\Domain\Model\CsViewRecord;

/**
 * Class ElasticCommandController
 *
 * @package Baumer\Baumer\Command
 */
class ElasticCommandController extends AbstractCommandController
{

    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Indexer\File
     * @inject
     */
    protected $fileIndexer;
    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Indexer\ContentservView
     * @inject
     */
    protected $csIndexer;
    /**
     * @var \Baumer\Baumer\Service\Elasticsearch\Indexer\Page
     * @inject
     */
    protected $pageIndexer;

    /**
     * @var \Baumer\Baumer\Domain\Repository\CsViewRecordRepository
     * @inject
     */
    protected $csViewRecordRepository;

    /**
     * @var array
     */
    protected $bourdonSettings;


    /**
     * Creates completely new elasticsearch indizes and switches
     * the alias after they are finished.
     */
    public function indexAllCommand()
    {
        $indexSuffix = '_' . time();
        $this->csIndexer->ensureIndexAndTypeMappingsAllLanguages($indexSuffix);
        $this->bourdonSettings = [];
        $this->configurationManager->setConfiguration();
        $this->configurationManager->setCurrentPageId(self::BOURDON_ROOT_PID);
        $tsBourdonSettings = $this->configurationManager->getConfiguration('bourdon');
        if (isset($tsBourdonSettings['settings'])) {
            $this->bourdonSettings = $tsBourdonSettings['settings'];
        }
        foreach ($this->csViewRecordRepository->findAll() as $csViewRecord) {
            if ($csViewRecord instanceof CsViewRecord) {
                if ($csViewRecord->getSite() == CsViewRecord::BOURDON_SITE_KEY) {
                    $this->csIndexer->injectSettings($this->bourdonSettings);
                } else {
                    $this->csIndexer->injectSettings($this->settings);
                }
                $this->csIndexer->indexViewRecord($csViewRecord, $indexSuffix);
            }
        }
        $this->fileIndexer->indexFiles($indexSuffix);
        $this->pageIndexer->indexPages($this->settings['rootPids']['int'], $indexSuffix);

        $this->csIndexer->ensureAliasForAllIndizesWithSuffix($indexSuffix);
    }

    /**
     * Index CsViewRecords that belong to the Contentserv Selectors.
     * This method just updates the existing index. Nothing will be deleted.
     */
    public function indexSelectorsCommand()
    {
        $this->csIndexer->indexRecordsByViewClass(CsView::CLASS_SELECTOR, [$this->settings['pimImport']['storages']['productStorage']]);
    }

    /**
     * Index CsViewRecords that belong to the Contentserv Configurator.
     * This method just updates the existing index. Nothing will be deleted.
     */
    public function indexProductsCommand()
    {
        $this->csIndexer->indexRecordsByViewClass(CsView::CLASS_CONFIGURATOR, [$this->settings['pimImport']['storages']['productStorage']]);
    }

    /**
     * Index files to Elasticsearch
     * This method just updates the existing index. Nothing will be deleted.
     */
    public function indexDownloadsCommand()
    {
        $this->fileIndexer->indexFiles();
    }

    public function indexPagesCommand()
    {
        $this->pageIndexer->indexPages($this->settings['rootPids']['int']);
    }

    /**
     * Delete deprecated elasticsearch indizes
     */
    public function cleanUpOldIndizesCommand()
    {
        foreach ($this->settings['elasticsearch']['connection']['languageIndizes'] as $indexName) {
            $this->csIndexer->cleanOldIndizesForAlias($indexName);
        }
    }
}
