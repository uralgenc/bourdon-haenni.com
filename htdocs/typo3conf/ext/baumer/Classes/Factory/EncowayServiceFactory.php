<?php
namespace Baumer\Baumer\Factory;

/***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>, Typovision GmbH
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/
use Baumer\Baumer\Service\EncowayService;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * EncowayServiceFactory
 * @author Hans Höchtl <extern.hans.hoechtl@typovision.de>
 */
class EncowayServiceFactory implements SingletonInterface
{

    /**
     * Get the service for the current articleName
     *
     * @param string $articleName
     * @param array $settings
     *
     * @return EncowayService
     * @throws \Exception
     */
    public function getService($articleName, $settings)
    {
        $encowayService = null;
        /** @var \TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication $feUser */
        $feUser = $GLOBALS['TSFE']->fe_user;
        $feUser->fetchSessionData();
        if (isset($feUser->sesData['encoway']) &&
            isset($feUser->sesData['encoway'][$articleName])) {
            $encowayService = GeneralUtility::makeInstance(EncowayService::class, $settings);
            $encowayService->setConfigurationId($feUser->sesData['encoway'][$articleName]);
        } else {
            /** @var EncowayService $encowayService */
            $encowayService = GeneralUtility::makeInstance(EncowayService::class, $settings);
            $encowayService->initializeConfiguration($articleName);
            $feUser->sesData['encoway'][$articleName] = $encowayService->getConfigurationId();
            $feUser->sesData_change = true;
            $feUser->storeSessionData();
        }
        return $encowayService;
    }

    /**
     * Reset the service stored in session for the given articleName
     *
     * @param string $articleName
     * @param array $settings
     *
     * @return EncowayService
     * @throws \Exception
     */
    public function resetService($articleName, $settings)
    {
        $encowayService = null;
        /** @var \TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication $feUser */
        $feUser = $GLOBALS['TSFE']->fe_user;
        $feUser->fetchSessionData();
        $encowayService = GeneralUtility::makeInstance(EncowayService::class, $settings);
        $encowayService->initializeConfiguration($articleName);
        $feUser->sesData['encoway'][$articleName] = $encowayService->getConfigurationId();
        $feUser->sesData_change = true;
        $feUser->storeSessionData();
        return $encowayService;
    }
}
