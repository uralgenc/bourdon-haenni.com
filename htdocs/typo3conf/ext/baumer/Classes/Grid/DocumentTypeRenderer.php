<?php
namespace Baumer\Baumer\Grid;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Anja Leichsenring <anja.leichsenring@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use Fab\Vidi\Grid\ColumnRendererAbstract;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class rendering documenttype list of an asset in the grid.
 */
class DocumentTypeRenderer extends ColumnRendererAbstract
{

    /**
     * Renders category list of an asset in the grid.
     *
     * @return string
     */
    public function render()
    {
        $objectManager =  new ObjectManager();
        $categoryRepository = $objectManager->get('TYPO3\\CMS\\Extbase\\Domain\\Repository\\CategoryRepository');
        /** @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
        $settings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'baumer', 'tx_baumer');

        $result = '';

        $categories = $this->object['metadata']['documenttype'];
        if (!empty($categories)) {
            $parentCategory = $categoryRepository->findByUid($settings['documenttype_parentcategory']);

            /** @var $category \TYPO3\CMS\Extbase\Domain\Model\Category */
            foreach ($categories as $category) {
                if ($category->getParent()->getUid() == $parentCategory->getUid()) {
                    $result .= sprintf('<li>%s</li>', $category['title']);
                }
            }
            $result = sprintf('<ul class="documenttype-list">%s</ul>', $result);
        }
        return $result;
    }
}
