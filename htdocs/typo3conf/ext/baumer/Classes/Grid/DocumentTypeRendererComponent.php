<?php
namespace Baumer\Baumer\Grid;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Anja Leichsenring <anja.leichsenring@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use Fab\Vidi\Grid\GenericColumn;

/**
 * Class for configuring a "Category" Grid Renderer in the Grid TCA.
 */
class DocumentTypeRendererComponent extends GenericColumn
{

    /**
     * Constructor for a "Category" Grid Renderer Component.
     */
    public function __construct()
    {
        $className = 'Baumer\Baumer\Grid\DocumentTypeRenderer';
        parent::__construct($className, []);
    }
}
