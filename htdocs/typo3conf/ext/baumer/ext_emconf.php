<?php
/***************************************************************
 * Extension Manager/Repository config file for ext: "baumer"
 *
 * Auto generated by Extension Builder 2014-07-23
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Baumer',
    'description' => 'Provides bootstrap content elements using fluidcontent',
    'category' => 'misc',
    'author' => 'Martin Heigermoser, Hans Höchtl, Anja Leichsenring, Oliver Schulz, Lars Trebing',
    'author_email' => 'martin.heigermoser@typovision.de, hans.hoechtl@typovision.de, anja.leichsenring@typovision.de, oliver.schulz@typovision.de, lars.trebing@typovision.de',
    'author_company' => 'typovision GmbH',
    'dependencies' => 'cms,flux',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '1.1.5',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-7.9.99',
            'flux' => '',
            'vhs' => '',
            'fluidcontent' => '',
//			'tt_address' => '2.3',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
