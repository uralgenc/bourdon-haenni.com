#
# Table structure for table 'tx_baumerserial_domain_model_shipment'
#
CREATE TABLE tx_baumerserial_domain_model_shipment (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	commission_number varchar(255) DEFAULT '' NOT NULL,
	part_number varchar(255) DEFAULT '' NOT NULL,
	serial_number varchar(255) DEFAULT '' NOT NULL,
	invoice_number varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,
	invoice_date int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_baumerserial_import_lastfile'
#
CREATE TABLE tx_baumerserial_import_lastfile (
	file varchar(255) DEFAULT '' NOT NULL,
	md5 varchar(32) DEFAULT '' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL
);