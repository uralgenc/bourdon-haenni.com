<?php
namespace Baumer\BaumerSerial\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Lars Trebing <lars.trebing@typovision.de>, typovision
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * @package BaumerSerial
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Shipment extends AbstractEntity {

	/**
	 * Commission number
	 *
	 * @var string
	 */
	protected $commissionNumber;

	/**
	 * Part number
	 *
	 * @var string
	 */
	protected $partNumber;

	/**
	 * Serial number
	 *
	 * @var string
	 */
	protected $serialNumber;

	/**
	 * Invoice number
	 *
	 * @var string
	 */
	protected $invoiceNumber;

	/**
	 * Description
	 *
	 * @var string
	 */
	protected $description;

	/**
	 * Invoice date
	 *
	 * @var \DateTime
	 */
	protected $invoiceDate;

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 *
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Appends a new line of text to the description
	 *
	 * @param string $descriptionLine
	 *
	 * @return void
	 */
	public function appendLineToDescription($descriptionLine) {
		$this->description .= "\n" . $descriptionLine;
	}

	/**
	 * Returns the invoiceDate
	 *
	 * @return \DateTime $invoiceDate
	 */
	public function getInvoiceDate() {
		return $this->invoiceDate;
	}

	/**
	 * Sets the invoiceDate
	 *
	 * @param \DateTime $invoiceDate
	 *
	 * @return void
	 */
	public function setInvoiceDate($invoiceDate) {
		$this->invoiceDate = $invoiceDate;
	}

	/**
	 * Returns the partNumber
	 *
	 * @return string partNumber
	 */
	public function getPartNumber() {
		return $this->partNumber;
	}

	/**
	 * Sets the partNumber
	 *
	 * @param string $partNumber
	 *
	 * @return string partNumber
	 */
	public function setPartNumber($partNumber) {
		$this->partNumber = $partNumber;
	}

	/**
	 * Returns the serialNumber
	 *
	 * @return string serialNumber
	 */
	public function getSerialNumber() {
		return $this->serialNumber;
	}

	/**
	 * Sets the serialNumber
	 *
	 * @param string $serialNumber
	 *
	 * @return string serialNumber
	 */
	public function setSerialNumber($serialNumber) {
		$this->serialNumber = $serialNumber;
	}

	/**
	 * Returns the commissionNumber
	 *
	 * @return string commissionNumber
	 */
	public function getCommissionNumber() {
		return $this->commissionNumber;
	}

	/**
	 * Sets the commissionNumber
	 *
	 * @param string $commissionNumber
	 *
	 * @return string commissionNumber
	 */
	public function setCommissionNumber($commissionNumber) {
		$this->commissionNumber = $commissionNumber;
	}

	/**
	 * Returns the invoiceNumber
	 *
	 * @return string invoiceNumber
	 */
	public function getInvoiceNumber() {
		return $this->invoiceNumber;
	}

	/**
	 * Sets the invoiceNumber
	 *
	 * @param string $invoiceNumber
	 *
	 * @return string invoiceNumber
	 */
	public function setInvoiceNumber($invoiceNumber) {
		$this->invoiceNumber = $invoiceNumber;
	}
}
