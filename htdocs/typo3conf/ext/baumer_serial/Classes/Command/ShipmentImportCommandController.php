<?php
namespace Baumer\BaumerSerial\Command;

/*****************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Lars Trebing <lars.trebing@typovision.de>
 *      2014 Oliver Schulz <oliver.schulz@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 *****************************************************************/

use \Baumer\Baumer\Command\AbstractCommandController;
use \TYPO3\CMS\Core\Messaging\FlashMessageQueue;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;

/**
 * Class MigrationCommandController
 *
 * @author Oliver Schulz <oliver.schulz@typovision.de>
 *
 * @package BaumerSerial
 * @subpackage Command
 */
class ShipmentImportCommandController extends AbstractCommandController
{

	/**
	 * Import serial numbers from the newest file in the given directory
	 *
	 * ./typo3/cli_dispatch.phpsh extbase shipmentimport:import /your/absolute/directory/
	 *
	 * @param string $directory Path containing the serial number files
	 *
	 * @return TRUE if successful
	 * @throws \RuntimeException if not successful
	 */
	public function importCommand($directory)
	{

		$fileMD5 = '';

		if (($handle = @opendir($directory)) === false) {
			/** @var FlashMessage $flashMessage */
			$flashMessage = GeneralUtility::makeInstance(FlashMessage::class, 'Error reading directory',
				'Import failed', FlashMessage::ERROR);
			$this->flashMessageQueue->addMessage($flashMessage);
			throw new \RuntimeException('Error reading directory', 1410388009);
		}

		$filectime = 0;
		while (false !== ($fileName = readdir($handle))) {
			$file = $directory . '/' . $fileName;
			// Get newest file
			if (mime_content_type($file) === 'text/plain' && filectime($file) > $filectime) {
				$filectime = filectime($file);
				$fileMD5 = md5_file($file);
				$fileToImport = $file;
			}
		}

		if (!isset($fileToImport) || !isset($fileMD5)) {
			/** @var FlashMessage $flashMessage */
			$flashMessage = GeneralUtility::makeInstance(FlashMessage::class, 'No file to import found',
				'Import failed', FlashMessage::ERROR);
			$this->flashMessageQueue->addMessage($flashMessage);
			throw new \RuntimeException('No file to import found', 1410390402);
		}

		// Check if file was already imported
		$lastImportedFile = $this->db->exec_SELECTgetSingleRow('*', 'tx_baumerserial_import_lastfile',
			'md5 = ' . $this->db->fullQuoteStr($fileMD5, 'tx_baumerserial_import_lastfile'));

		if ($lastImportedFile !== null && $lastImportedFile !== false) {
			/** @var FlashMessage $flashMessage */
			$flashMessage = GeneralUtility::makeInstance(FlashMessage::class, 'File already imported', 'Import failed',
				FlashMessage::ERROR);
			$this->flashMessageQueue->addMessage($flashMessage);
			throw new \RuntimeException('File already imported', 1410390508);
		}

		$this->db->exec_INSERTquery('tx_baumerserial_import_lastfile', array(
				'file' => $fileToImport,
				'md5' => $fileMD5,
				'tstamp' => time()
			));

		$fileHandle = @fopen($fileToImport, 'r');
		if ($fileHandle === false) {
			/** @var FlashMessage $flashMessage */
			$flashMessage = GeneralUtility::makeInstance(FlashMessage::class, 'Error reading file', 'Import failed',
				FlashMessage::ERROR);
			$this->flashMessageQueue->addMessage($flashMessage);
			throw new \RuntimeException('Error reading file', 1410390563);
		}

		$count = 0;
		$currentRecordFingerprint = '';
		$imported = false;

		while (($line = fgets($fileHandle)) !== false) {
			$line = trim($line);
			if (preg_match('/^(\d{10})\|(\d{18})\|(\d{18})\|(\d{10})\|(.{132})\|(\d{7})   \|[ ]{10}\|[ ]{10}\|\d{6}\|$/',
				$line, $match)) {
				$recordFingerprint = $match[1] . $match[2] . $match[3] . $match[4] . $match[6];
				if (isset($shipment) && $recordFingerprint === $currentRecordFingerprint) {
					$shipment['description'] .= "\n" . trim(iconv('ISO-8859-1', 'UTF-8', $match[5]));
				} else {
					if (isset($shipment)) {
						// save
						$imported = true;
						$this->db->exec_INSERTquery('tx_baumerserial_domain_model_shipment', $shipment);
						unset($shipment);
					}

					$shipment = array(
						'pid' => 0,
						'crdate' => time(),
						'tstamp' => time(),
						'commission_number' => ltrim($match[1], '0'),
						'part_number' => ltrim($match[2], '0'),
						'serial_number' => ltrim($match[3], '0'),
						'invoice_number' => ltrim($match[4], '0'),
						'description' => trim(iconv('ISO-8859-1', 'UTF-8', $match[5])),
						'invoice_date' => $this->parseDate($match[6])
					);

					$currentRecordFingerprint = $recordFingerprint;
				}
			}
			$count++;
		}
		if (isset($shipment)) {
			// save
			$imported = true;
			$this->db->exec_INSERTquery('tx_baumerserial_domain_model_shipment', $shipment);
			unset($shipment);
		}

		if ($imported === false) {
			/** @var FlashMessage $flashMessage */
			$flashMessage = GeneralUtility::makeInstance(FlashMessage::class, 'No matching lines in file found',
				'Import failed', FlashMessage::ERROR);
			$this->flashMessageQueue->addMessage($flashMessage);
			throw new \RuntimeException('No matching lines in file found', 1410390682);
		}

		return true;
	}

	/**
	 * Parse a YYYMMDD date string (where YYY is the number of years since 1900,
	 * e.g. 114 for 2014) to a Unix timestamp
	 *
	 * @param $dateString
	 *
	 * @return integer $dateString
	 */
	protected function parseDate($dateString)
	{
		$year = 1900 + (int)substr($dateString, 0, 3);
		$month = (int)substr($dateString, 3, 2);
		$day = (int)substr($dateString, 5, 2);

		return mktime(12, 0, 0, $month, $day, $year);
	}
}
