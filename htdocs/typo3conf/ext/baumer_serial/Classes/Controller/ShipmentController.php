<?php
namespace Baumer\BaumerSerial\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Lars Trebing <lars.trebing@typovision.de>, typovision
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Baumer\BaumerSerial\Domain\Model\Shipment;

/**
 * @package BaumerSerial
 * @subpackage Controller
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ShipmentController extends ActionController {

	/**
	 * shipmentRepository
	 *
	 * @var \Baumer\BaumerSerial\Domain\Repository\ShipmentRepository
	 * @inject
	 */
	protected $shipmentRepository;

	/**
	 * action show
	 *
	 * @param \Baumer\BaumerSerial\Domain\Model\Shipment $shipment
	 * @return void
	 */
	public function showAction( Shipment $shipment) {
		$this->view->assign('shipment', $shipment);
	}

	/**
	 * action search
	 *
	 * @param string $serialNumber
	 * @return void
	 */
	public function searchAction($serialNumber = '') {
		$serialNumber = ltrim(trim($serialNumber), '0');
		if(strlen($serialNumber) > 0) {
			$shipments = $this->shipmentRepository->findBySerialNumber($serialNumber);
		} else {
			$shipments = array();
		}
		$this->view->assignMultiple(array(
			'requestedSerialNumber' => $serialNumber,
			'shipments' =>  $shipments
		));
	}

}
