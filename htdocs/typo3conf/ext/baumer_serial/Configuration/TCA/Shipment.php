<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_baumerserial_domain_model_shipment'] = [
	'ctrl' => $TCA['tx_baumerserial_domain_model_shipment']['ctrl'],
	'interface' => [
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, commission_number, part_number, serial_number, invoice_number, description, invoice_date',
	],
	'types' => [
		'1' => ['showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, commission_number, part_number, serial_number, invoice_number, description, invoice_date,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'],
	],
	'palettes' => [
		'1' => ['showitem' => ''],
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
			'config' => [
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => [
					['LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1],
					['LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0]
				],
			],
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
				'items' => [
					['', 0],
				],
				'foreign_table' => 'tx_baumerserial_domain_model_shipment',
				'foreign_table_where' => 'AND tx_baumerserial_domain_model_shipment.pid=###CURRENT_PID### AND tx_baumerserial_domain_model_shipment.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			],
		],
		't3ver_label' => [
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.versionLabel',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			]
		],
		'hidden' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => [
				'type' => 'check',
			],
		],
		'starttime' => [
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.starttime',
			'config' => [
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => [
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				],
			],
		],
		'endtime' => [
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.endtime',
			'config' => [
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => [
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				],
			],
		],
		'commission_number' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:baumer_serial/Resources/Private/Language/locallang_db.xml:tx_baumerserial_domain_model_shipment.commission_number',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'part_number' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:baumer_serial/Resources/Private/Language/locallang_db.xml:tx_baumerserial_domain_model_shipment.part_number',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'serial_number' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:baumer_serial/Resources/Private/Language/locallang_db.xml:tx_baumerserial_domain_model_shipment.serial_number',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'invoice_number' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:baumer_serial/Resources/Private/Language/locallang_db.xml:tx_baumerserial_domain_model_shipment.invoice_number',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'description' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:baumer_serial/Resources/Private/Language/locallang_db.xml:tx_baumerserial_domain_model_shipment.description',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			],
		],
		'invoice_date' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:baumer_serial/Resources/Private/Language/locallang_db.xml:tx_baumerserial_domain_model_shipment.invoice_date',
			'config' => [
				'type' => 'input',
				'size' => 7,
				'eval' => 'date',
				'checkbox' => 1,
				'default' => time()
			],
		],
	],
];
