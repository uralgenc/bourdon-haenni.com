<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Baumer.' . $_EXTKEY,
	'Itemquery',
	'Serial number query'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Serial number database');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_baumerserial_domain_model_shipment', 'EXT:baumer_serial/Resources/Private/Language/locallang_csh_tx_baumerserial_domain_model_shipment.xml');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_baumerserial_domain_model_shipment');
$TCA['tx_baumerserial_domain_model_shipment'] = [
	'ctrl' => [
		'title' => 'LLL:EXT:baumer_serial/Resources/Private/Language/locallang_db.xml:tx_baumerserial_domain_model_shipment',
		'label' => 'commission_number',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		],
		'searchFields' => 'commission_number,part_number,serial_number,invoice_number,description,invoice_date,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Shipment.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_baumerserial_domain_model_shipment.gif'
	],
];
