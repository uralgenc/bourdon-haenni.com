<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Baumer.' . $_EXTKEY,
	'Itemquery',
	[
		'Shipment' => 'search, show',
	],
	// non-cacheable actions
	[
		'Shipment' => 'search, show',
	]
);

/**
 * register the serial import Command Controller
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \Baumer\BaumerSerial\Command\ShipmentImportCommandController::class;
