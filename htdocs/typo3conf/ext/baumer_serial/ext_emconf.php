<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'Serial number database',
	'description' => '',
	'category' => 'typovision',
	'author' => 'Lars Trebing',
	'author_email' => 'lars.trebing@typovision.de',
	'author_company' => 'typovision',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '0.0.1',
	'constraints' => [
		'depends' => [
			'extbase' => '1.3',
			'fluid' => '1.3',
			'typo3' => '4.5',
		],
		'conflicts' => [],
		'suggests' => [],
	],
];
