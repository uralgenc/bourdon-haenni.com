<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace ODS\OdsSolr\Indexer;

/**
 * Class FallbackLanguageIndexer
 *
 * @package ODS\OdsSolr\Indexer
 */
class FallbackLanguageIndexer extends \Tx_Solr_IndexQueue_Indexer {

	/**
	 * Converts an item array (record) to a Solr document by mapping the
	 * record's fields onto Solr document fields as configured in TypoScript.
	 *
	 * @param \Tx_Solr_IndexQueue_Item $item An index queue item
	 * @param integer $language Language Id
	 * @return \Apache_Solr_Document The Solr document converted from the record
	 */
	protected function itemToDocument( \Tx_Solr_IndexQueue_Item $item, $language = 0 ) {
		$solrConfiguration = \Tx_Solr_Util::getSolrConfigurationFromPageId($item->getRootPageUid(), TRUE, $language);
		$indexingConfiguration = $solrConfiguration['index.']['queue.'][$item->getIndexingConfigurationName() . '.'];
		if (isset($indexingConfiguration['indexer.']) &&
			isset($indexingConfiguration['indexer.']['doLanguageFallback']) &&
			$indexingConfiguration['indexer.']['doLanguageFallback'] == TRUE) {

			$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['settingLanguage_preProcess']['overrideLanguageFallback'] = function(&$params, &$ref){
				$ref->config['config']['sys_language_overlay'] = 1;
			};
		}

		return parent::itemToDocument($item, $language);
	}
}
