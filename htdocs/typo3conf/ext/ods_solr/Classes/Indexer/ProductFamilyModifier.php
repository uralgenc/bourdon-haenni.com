<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace ODS\OdsSolr\Indexer;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class ProductFamilyModifier implements \Tx_Solr_IndexQueuePageIndexerDocumentsModifier {

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 */
	protected $configurationManager;
	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;
	/**
	 * @var \Baumer\Baumer\Domain\Repository\ProductFamilyRepository
	 */
	protected $productFamilyRepository;
	/**
	 * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
	 */
	protected $uriBuilder;
	/**
	 * @var array
	 */
	protected $settings;

	function __construct() {
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		$this->productFamilyRepository = $this->objectManager->get('Baumer\\Baumer\\Domain\\Repository\\ProductFamilyRepository');
		$this->configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
		$this->configurationManager->setContentObject(GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer'));
		$this->uriBuilder = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Mvc\\Web\\Routing\\UriBuilder');
		$this->settings = $this->configurationManager->getConfiguration( ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'baumer');
	}

	/**
	 * Modifies the given documents
	 *
	 * @param $item \Tx_Solr_IndexQueue_Item    The currently being indexed item.
	 * @param $language integer    The language uid of the documents
	 * @param $documents array    An array of documents to be indexed
	 *
	 * @return    array    An array of modified documents
	 */
	public function modifyDocuments( \Tx_Solr_IndexQueue_Item $item, $language, array $documents ) {
		if ($item->getType() === 'tx_baumer_domain_model_productfamily') {
			/** @var \Baumer\Baumer\Domain\Model\ProductFamily $productFamily */
			$productFamily = $this->productFamilyRepository->findByUid($item->getRecordUid());
			$productAttributes = $productFamily->toArray();

			$filterAttributes = array();

			if (isset($productAttributes['available_filters']) && is_array($productAttributes['available_filters'])) {
				foreach ($productAttributes['available_filters'] as $avFilter) {
					if (array_key_exists($avFilter['id'], $productAttributes['attributes']) &&
						array_key_exists('values', $productAttributes['attributes'][$avFilter['id']]) &&
						!empty($productAttributes['attributes'][$avFilter['id']]['values'])) {
						$filterAttributes[$avFilter['id']] = $productAttributes['attributes'][$avFilter['id']]['values'];
					}
				}
			}

			$tableAttributes = array();
			if (isset($productAttributes['available_tables']) && is_array($productAttributes['available_tables'])) {
				foreach ($productAttributes['available_tables'] as $tableAttribute) {
					if (isset($productAttributes['attributes'][$tableAttribute['id']]) &&
						!empty($productAttributes['attributes'][$tableAttribute['id']]['values'])
					) {
						$tableAttributes[] = $productAttributes['attributes'][$tableAttribute['id']];
					}
				}
			}

			$mainGroupUid = $productAttributes['mainGroupUid'];

			$productLink = $this->buildUri($this->settings['encoway']['configuratorPid'], $productFamily->getUid(), $mainGroupUid);

			$newDocuments = array();

			/** @var \Apache_Solr_Document[] $documents */
			foreach ($documents as $document) {

				$document->setField('url', $productLink);
				$document->setField('mainGroupUid_intS', $mainGroupUid);

				if (!empty($tableAttributes)) {
					$document->setField('tableAttributesSerialized_stringS', json_encode($tableAttributes));
				}

				foreach ($filterAttributes as $attributeId => $filterValues) {
					foreach ($filterValues as $filterValue) {
						$document->addField('attribute_' . $attributeId . '_stringM', $filterValue);
					}
				}
				$newDocuments[] = $document;
			}
			return $newDocuments;

		} else {
			return $documents;
		}
	}

	protected function buildUri($uid, $productFamily, $mainGroup = NULL, $language = 0) {
		if (!class_exists('Tx_Solr_Util')) {
			throw new \RuntimeException('EXT:solr is needed for TSFE faking', 1415988860);
		}
		\Tx_Solr_Util::initializeTsfe('1', $language);

		$this->uriBuilder->setTargetPageUid($uid)->uriFor('configurator', array('productFamily' => $productFamily, 'mainGroup' => $mainGroup),'Encoway', 'Baumer', 'Configurator');
		return $this->uriBuilder->buildFrontendUri();
	}
}
