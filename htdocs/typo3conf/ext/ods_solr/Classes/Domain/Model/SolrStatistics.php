<?php
namespace ODS\OdsSolr\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Hans Höchtl <hhoechtl@1drop.de>, Onedrop Solutions GmbH & Co. KG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * SolrStatistics
 */
class SolrStatistics extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * rootPid
	 *
	 * @var integer
	 */
	protected $rootPid = 0;

	/**
	 * language
	 *
	 * @var integer
	 */
	protected $language = 0;

	/**
	 * numFound
	 *
	 * @var integer
	 */
	protected $numFound = 0;

	/**
	 * suggestionsShown
	 *
	 * @var boolean
	 */
	protected $suggestionsShown = FALSE;

	/**
	 * timeTotal
	 *
	 * @var integer
	 */
	protected $timeTotal = 0;

	/**
	 * timePreparation
	 *
	 * @var integer
	 */
	protected $timePreparation = 0;

	/**
	 * timeProcessing
	 *
	 * @var integer
	 */
	protected $timeProcessing = 0;

	/**
	 * feuserId
	 *
	 * @var integer
	 */
	protected $feuserId = 0;

	/**
	 * cookie
	 *
	 * @var string
	 */
	protected $cookie = '';

	/**
	 * ip
	 *
	 * @var string
	 */
	protected $ip = '';

	/**
	 * keywords
	 *
	 * @var string
	 */
	protected $keywords = '';

	/**
	 * page
	 *
	 * @var integer
	 */
	protected $page = 0;

	/**
	 * filters
	 *
	 * @var string
	 */
	protected $filters = '';

	/**
	 * parameters
	 *
	 * @var string
	 */
	protected $parameters = '';

	/**
	 * Returns the rootPid
	 *
	 * @return integer $rootPid
	 */
	public function getRootPid() {
		return $this->rootPid;
	}

	/**
	 * Sets the rootPid
	 *
	 * @param integer $rootPid
	 * @return void
	 */
	public function setRootPid($rootPid) {
		$this->rootPid = $rootPid;
	}

	/**
	 * Returns the language
	 *
	 * @return integer $language
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * Sets the language
	 *
	 * @param integer $language
	 * @return void
	 */
	public function setLanguage($language) {
		$this->language = $language;
	}

	/**
	 * Returns the numFound
	 *
	 * @return integer $numFound
	 */
	public function getNumFound() {
		return $this->numFound;
	}

	/**
	 * Sets the numFound
	 *
	 * @param integer $numFound
	 * @return void
	 */
	public function setNumFound($numFound) {
		$this->numFound = $numFound;
	}

	/**
	 * Returns the suggestionsShown
	 *
	 * @return boolean $suggestionsShown
	 */
	public function getSuggestionsShown() {
		return $this->suggestionsShown;
	}

	/**
	 * Sets the suggestionsShown
	 *
	 * @param boolean $suggestionsShown
	 * @return void
	 */
	public function setSuggestionsShown($suggestionsShown) {
		$this->suggestionsShown = $suggestionsShown;
	}

	/**
	 * Returns the boolean state of suggestionsShown
	 *
	 * @return boolean
	 */
	public function isSuggestionsShown() {
		return $this->suggestionsShown;
	}

	/**
	 * Returns the timeTotal
	 *
	 * @return integer $timeTotal
	 */
	public function getTimeTotal() {
		return $this->timeTotal;
	}

	/**
	 * Sets the timeTotal
	 *
	 * @param integer $timeTotal
	 * @return void
	 */
	public function setTimeTotal(integer $timeTotal) {
		$this->timeTotal = $timeTotal;
	}

	/**
	 * Returns the timePreparation
	 *
	 * @return integer $timePreparation
	 */
	public function getTimePreparation() {
		return $this->timePreparation;
	}

	/**
	 * Sets the timePreparation
	 *
	 * @param integer $timePreparation
	 * @return void
	 */
	public function setTimePreparation(integer $timePreparation) {
		$this->timePreparation = $timePreparation;
	}

	/**
	 * Returns the timeProcessing
	 *
	 * @return integer $timeProcessing
	 */
	public function getTimeProcessing() {
		return $this->timeProcessing;
	}

	/**
	 * Sets the timeProcessing
	 *
	 * @param integer $timeProcessing
	 * @return void
	 */
	public function setTimeProcessing(integer $timeProcessing) {
		$this->timeProcessing = $timeProcessing;
	}

	/**
	 * Returns the feuserId
	 *
	 * @return integer $feuserId
	 */
	public function getFeuserId() {
		return $this->feuserId;
	}

	/**
	 * Sets the feuserId
	 *
	 * @param integer $feuserId
	 * @return void
	 */
	public function setFeuserId($feuserId) {
		$this->feuserId = $feuserId;
	}

	/**
	 * Returns the cookie
	 *
	 * @return string $cookie
	 */
	public function getCookie() {
		return $this->cookie;
	}

	/**
	 * Sets the cookie
	 *
	 * @param string $cookie
	 * @return void
	 */
	public function setCookie($cookie) {
		$this->cookie = $cookie;
	}

	/**
	 * Returns the ip
	 *
	 * @return string $ip
	 */
	public function getIp() {
		return $this->ip;
	}

	/**
	 * Sets the ip
	 *
	 * @param string $ip
	 * @return void
	 */
	public function setIp($ip) {
		$this->ip = $ip;
	}

	/**
	 * Returns the keywords
	 *
	 * @return string $keywords
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * Sets the keywords
	 *
	 * @param string $keywords
	 * @return void
	 */
	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	/**
	 * Returns the page
	 *
	 * @return integer $page
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 * Sets the page
	 *
	 * @param integer $page
	 * @return void
	 */
	public function setPage($page) {
		$this->page = $page;
	}

	/**
	 * Returns the filters
	 *
	 * @return string $filters
	 */
	public function getFilters() {
		return $this->filters;
	}

	/**
	 * Sets the filters
	 *
	 * @param string $filters
	 * @return void
	 */
	public function setFilters($filters) {
		$this->filters = $filters;
	}

	/**
	 * Returns the parameters
	 *
	 * @return string $parameters
	 */
	public function getParameters() {
		return $this->parameters;
	}

	/**
	 * Sets the parameters
	 *
	 * @param string $parameters
	 * @return void
	 */
	public function setParameters($parameters) {
		$this->parameters = $parameters;
	}

}