<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace ODS\OdsSolr\QueryModifier;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;

/**
 * Class TabModifier
 *
 * @package ODS\OdsSolr\QueryModifier
 */
class TabModifier implements \Tx_Solr_QueryModifier {

	/**
	 * @var \Tx_Solr_SolrService
	 */
	protected $solr;
	/**
	 * @var array
	 */
	protected $tabPriority = [ ];

	function __construct() {
		$this->solr = GeneralUtility::makeInstance( 'Tx_Solr_ConnectionManager' )->getConnectionByPageId(
			$GLOBALS['TSFE']->id,
			$GLOBALS['TSFE']->sys_language_uid
		);
		if ( $GLOBALS['TSFE']->tmpl
			->setup['plugin.']['tx_solr.']['search.']['faceting.']['facets.']['datatype_stringS.']['manualSortOrder'] ) {
			$this->tabPriority = GeneralUtility::trimExplode( ',',
				$GLOBALS['TSFE']->tmpl
					->setup['plugin.']['tx_solr.']['search.']['faceting.']['facets.']['datatype_stringS.']['manualSortOrder']
			);
		}
	}

	/**
	 * Modifies the given query and returns the modified query as result
	 *
	 * @param $query \Tx_Solr_Query The query to modify
	 *
	 * @return \Tx_Solr_Query The modified query
	 */
	public function modifyQuery( \Tx_Solr_Query $query ) {
		if ( ! empty( $this->tabPriority ) ) {

			// Extract current tab if one is set
			$filterValue = '';
			$filters     = $query->getFilters();
			foreach ( $filters as $key => $filterString ) {
				if ( GeneralUtility::isFirstPartOfStr( $filterString, '{!tag=datatype_stringS}(datatype_stringS:') ) {
					$filterValue = substr(str_replace('{!tag=datatype_stringS}(datatype_stringS:"', '', $filters[ $key ]),0, -2);
				}
			}

			// Create grouped query without tab filter to check for a possible tab
			$newQuery = clone $query;
			$newQuery->removeFilter( '{!tag=datatype_stringS}(datatype_stringS' );
			$newQuery->removeFilter( 'datatype_stringS' );
			$newQuery->setFaceting( FALSE );
			$newQuery->setSpellchecking(FALSE);
			$newQuery->setHighlighting( FALSE );
			$newQuery->setGrouping( TRUE );
			$newQuery->addGroupField( 'datatype_stringS' );
			$newQuery->setNumberOfResultsPerGroup(0);
			// Execute grouped query
			$response = $this->solr->search( $newQuery->getQueryString(), 0, 10, $newQuery->getQueryParameters() );

			$doTabFallback = FALSE;

			// If no tab is selected automatically choose the best tab according to priority defined by facets sort order
			// If a tab is selected, check if the results would be empty and fallback to next possible tab
			if (empty($filterValue)) {
				$doTabFallback = TRUE;
			} elseif (!$this->checkGroupsForFiltervalue($response->grouped->datatype_stringS->groups, $filterValue)) {
				$doTabFallback = TRUE;
			}

			if ($doTabFallback) {
				$foundPossibleTab = FALSE;
				foreach ($this->tabPriority as $possibleTab) {
					if (!$foundPossibleTab &&
					    $this->checkGroupsForFiltervalue($response->grouped->datatype_stringS->groups, $possibleTab)) {
						$foundPossibleTab = $possibleTab;
					}
				}
				if($foundPossibleTab) {
					// Redirect to page with correct tab
					/** @var \Tx_Solr_Query_LinkBuilder $queryLinkBuilder */
					$queryLinkBuilder = GeneralUtility::makeInstance('Tx_Solr_Query_LinkBuilder', $query);
					$newUrl = htmlspecialchars_decode($queryLinkBuilder->getQueryUrl(['filter' => ['datatype_stringS:' . $foundPossibleTab]]));
					HttpUtility::redirect($newUrl, HttpUtility::HTTP_STATUS_303);
				}
			}

		}

		return $query;
	}

	/**
	 * @param $groups array
	 * @param $filterValue string
	 *
	 * @return bool
	 */
	private function checkGroupsForFiltervalue( $groups, $filterValue ) {
		$found = FALSE;
		foreach ( $groups as $group ) {
			if ( $group->groupValue === $filterValue && $group->doclist->numFound > 0 ) {
				$found = TRUE;
			}
		}

		return $found;
	}
}
