<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace ODS\OdsSolr\Overrides;

/**
 * Class FacetingComponent
 *
 * @package ODS\OdsSolr\Overrides
 */
class FacetingComponent extends \Tx_Solr_Search_FacetingComponent {

	public function initializeSearchComponent() {
		if ($this->searchConfiguration['faceting']) {

			$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['modifySearchQuery']['faceting'] = 'Tx_Solr_Query_Modifier_Faceting';
			$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['modifySearchQuery']['tabs'] = 'ODS\\OdsSolr\\QueryModifier\\TabModifier';
		}
	}
}
