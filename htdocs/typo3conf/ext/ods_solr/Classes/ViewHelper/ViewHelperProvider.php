<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
namespace ODS\OdsSolr\ViewHelper;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ViewHelperProvider
 *
 * @package ODS\OdsSolr\ViewHelper
 */
class ViewHelperProvider implements \Tx_Solr_ViewHelperProvider {

	/**
	 * provides additional viewhelper objects to be added to the templating engine
	 *
	 * @param    array    array with a structure of view helper name => Tx_Solr_ViewHelper objects
	 * @return array
	 */
	public function getViewHelpers() {
		$fluidResult = GeneralUtility::makeInstance('ODS\\OdsSolr\\ViewHelper\\FluidResult');
		$htmlSpecialCharsDecode = GeneralUtility::makeInstance('ODS\\OdsSolr\\ViewHelper\\HtmlSpecialCharsDecode');

		return array(
			'FLUIDRESULT' => $fluidResult,
			'HTMLSPECIALCHARSDECODE' => $htmlSpecialCharsDecode
		);
	}
}
