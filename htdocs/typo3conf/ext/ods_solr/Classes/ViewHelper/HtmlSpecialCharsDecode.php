<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

namespace ODS\OdsSolr\ViewHelper;


class HtmlSpecialCharsDecode implements \Tx_Solr_ViewHelper {

	/**
	 * constructor, takes an optional array of arguments for initialisation
	 *
	 * @param $arguments array optional initialisation arguments
	 */
	public function __construct( array $arguments = array() ) {
	}

	/**
	 * execute method
	 *
	 * @param  $arguments  array  array of arguments
	 *
	 * @return  string  The rendered output.
	 */
	public function execute( array $arguments = array() ) {
		return htmlspecialchars_decode($arguments[0]);
	}
}
