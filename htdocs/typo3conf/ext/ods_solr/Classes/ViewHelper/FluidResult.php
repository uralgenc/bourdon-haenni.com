<?php
/**
 * Copyright notice
 *
 * (c) Onedrop Solutions GmbH & Co. KG, www.1drop.de
 *
 * @author Hans Höchtl <hhoechtl@1drop.de>
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
namespace ODS\OdsSolr\ViewHelper;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FluidResult
 *
 * @package ODS\OdsSolr\ViewHelper
 */
class FluidResult extends \Tx_Solr_ViewHelper_AbstractSubpartViewHelper {

	/**
	 * constructor, takes an optional array of arguments for initialisation
	 *
	 * @param $arguments array optional initialisation arguments
	 */
	public function __construct( array $arguments = array() ) {
	}

	/**
	 * execute method
	 *
	 * @param    array    array of arguments
	 *
	 * @return    string    The rendered output.
	 */
	public function execute( array $arguments = array() ) {
		/** @var \TYPO3\CMS\Fluid\View\StandaloneView $view */
		$view = GeneralUtility::makeInstance('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
		$resourcePath = GeneralUtility::getFileAbsFileName('EXT:baumer/Resources/Private');
		$view->setPartialRootPath($resourcePath . '/Partials');

		$templateLoops = $this->getTemplate()->getLoops();
		$templateVars['results'] = $templateLoops['result_documents']['data'];

		$currentTab = str_replace('datatype_stringS%3A', '', $_GET['tx_solr']['filter'][0]);
		$currentTab = str_replace('datatype_stringS:', '', $currentTab);

		switch ($currentTab) {
			case 'product':
				$view->setTemplatePathAndFilename($resourcePath . '/Templates/Solr/Results/Product.html');
				break;
			case 'news':
				$view->setTemplatePathAndFilename($resourcePath . '/Templates/Solr/Results/News.html');
				break;
			case 'download':
				$view->setTemplatePathAndFilename($resourcePath . '/Templates/Solr/Results/Downloads.html');
				break;
			default:
				$view->setTemplatePathAndFilename($resourcePath . '/Templates/Solr/Results/List.html');
		}

		$view->assignMultiple($templateVars);
		$content = $view->render();
		return $content;
	}

	/**
	 * @return \ODS\OdsSolr\Overrides\SolrTemplate
	 */
	public function getTemplate() {
		return parent::getTemplate();
	}
}
