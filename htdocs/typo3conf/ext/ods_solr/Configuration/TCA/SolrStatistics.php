<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_solr_statistics'] = array(
	'ctrl' => $TCA['tx_solr_statistics']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'uid,pid,root_pid,tstamp,language,num_found,suggestions_shown,time_total,time_preparation,time_processing,feuser_id,cookie,ip,keywords,page,filters,parameters',
	),
	'types' => array(
		'1' => array('showitem' => 'uid,pid,root_pid,tstamp,language,num_found,suggestions_shown,time_total,time_preparation,time_processing,feuser_id,cookie,ip,keywords,page,filters,parameters'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'uid' => array(
			'exclude' => 0,
			'label' => 'uid',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'pid' => array(
			'exclude' => 0,
			'label' => 'pid',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'tstamp' => array(
			'exclude' => 0,
			'label' => 'tstamp',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'root_pid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.root_pid',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'language' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.language',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'num_found' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.num_found',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'suggestions_shown' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.suggestions_shown',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'time_total' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.time_total',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'time',
				'checkbox' => 1,
				'default' => time()
			)
		),
		'time_preparation' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.time_preparation',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'time',
				'checkbox' => 1,
				'default' => time()
			)
		),
		'time_processing' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.time_processing',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'time',
				'checkbox' => 1,
				'default' => time()
			)
		),
		'feuser_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.feuser_id',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'cookie' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.cookie',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'ip' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.ip',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'keywords' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.keywords',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'page' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.page',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'filters' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.filters',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'parameters' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ods_solr/Resources/Private/Language/locallang_db.xlf:tx_odssolr_domain_model_solrstatistics.parameters',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
	),
);
