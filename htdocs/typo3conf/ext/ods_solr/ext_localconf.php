<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/*
 * Modify indexing documents for ProductFamily
 */
if (!is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['IndexQueueIndexer']['preAddModifyDocuments'])) {
	$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['IndexQueueIndexer']['preAddModifyDocuments'] = array();
}
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['IndexQueueIndexer']['preAddModifyDocuments'][] = 'ODS\\OdsSolr\\Indexer\\ProductFamilyModifier';

/*
 * Custom viewhelpers for advanced templating
 */
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['PiResults']['addViewHelpers'][] = 'ODS\\OdsSolr\\ViewHelper\\ViewHelperProvider';

/*
 * Overrides
 */
// Tx_Solr_Template, because we need some protected variables to inject a fluid StandaloneView
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['Tx_Solr_Template']['className'] = 'ODS\\OdsSolr\\Overrides\\SolrTemplate';
// Tx_Solr_Search_FacetingComponent because we need to ensure that this component is loaded before TabModifier
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['Tx_Solr_Search_FacetingComponent']['className'] = 'ODS\\OdsSolr\\Overrides\\FacetingComponent';


/*
 * DEACTIVATE UNUSED SOLR FUNCTIONS => WE USE ELASTICSEARCH
 */
unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['ApacheSolrForTypo3\Solr\Task\ReIndexTask']);
unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['ApacheSolrForTypo3\Solr\Task\IndexQueueWorkerTask']);
unset($TYPO3_CONF_VARS['FE']['eID_include']['tx_solr_suggest']);
unset($TYPO3_CONF_VARS['FE']['eID_include']['tx_solr_api']);
