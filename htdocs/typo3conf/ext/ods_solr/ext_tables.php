<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Baumer Solr');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_solr_statistics', 'Solr Statistic Item');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_solr_statistics');
$TCA['tx_solr_statistics'] = array(
	'ctrl' => array(
		'title'	=> 'tx_solr_statistics',
		'label' => 'keywords',
		'label_alt' => 'tstamp, keywords',
		'label_alt_force' => 1,
		'tstamp' => 'tstamp',
		'crdate' => 'tstamp',
		'sortby' => 'tstamp',
		'default_sortby' => 'ORDER BY tstamp DESC',
		'enablecolumns' => array(),
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/SolrStatistics.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/statistics.png'
	),
);

/*
 * DEACTIVATE UNUSED SOLR FUNCTIONS => WE USE ELASTICSEARCH
 */

if (TYPO3_MODE == 'BE') {
	unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['reports']['tx_reports']['status']['providers']['solr']);
	unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['additionalBackendItems']['cacheActions']['clearSolrConnectionCache']);

	// Unset garbage collection hooks
	if(($key = array_search('&ApacheSolrForTypo3\\Solr\\GarbageCollector', $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'])) !== false) {
		unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][$key]);
	}
	if(($key = array_search('&ApacheSolrForTypo3\\Solr\\GarbageCollector', $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'])) !== false) {
		unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$key]);
	}

	// Unset record monitor hooks
	if(($key = array_search('ApacheSolrForTypo3\\Solr\\IndexQueue\\RecordMonitor', $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'])) !== false) {
		unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][$key]);
	}
	if(($key = array_search('ApacheSolrForTypo3\\Solr\\IndexQueue\\RecordMonitor', $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'])) !== false) {
		unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$key]);
	}

	$toolsModules = explode(',',$GLOBALS['TBE_MODULES']['tools']);
	if(($key = array_search('SolrAdministration', $toolsModules)) !== false) {
		unset($toolsModules[$key]);
		$GLOBALS['TBE_MODULES']['tools'] = implode(',', $toolsModules);
	}
}
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('
[adminUser = 1]
options.contextMenu.table.pages.items.850 >
options.contextMenu.table.pages.items.851 >
[global]
');
