#
# Table structure for table 'tx_umantis_domain_model_job'
#
CREATE TABLE tx_umantis_domain_model_job (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	internal_code varchar(255) DEFAULT '' NOT NULL,
	umantis_id int(11) DEFAULT '0' NOT NULL,
	position varchar(255) DEFAULT '' NOT NULL,
	umantis_position_id int(11) DEFAULT '0' NOT NULL,
	company varchar(255) DEFAULT '' NOT NULL,
	department varchar(255) DEFAULT '' NOT NULL,
	location varchar(255) DEFAULT '' NOT NULL,
	fixed_time tinyint(1) unsigned DEFAULT '0' NOT NULL,
	form_of_employment int(11) DEFAULT '0' NOT NULL,
	has_priority tinyint(1) unsigned DEFAULT '0' NOT NULL,
	state int(11) DEFAULT '0' NOT NULL,
	slogan text NOT NULL,
	company_description text NOT NULL,
	introduction text NOT NULL,
	job_description text NOT NULL,
	challenge varchar(255) DEFAULT '' NOT NULL,
	offer text NOT NULL,
	profile_title varchar(255) DEFAULT '' NOT NULL,
	profile_text text NOT NULL,
	video_url varchar(255) DEFAULT '' NOT NULL,
	finishing_text text NOT NULL,
	contact_person_text text NOT NULL,
	application_url varchar(255) DEFAULT '' NOT NULL,
	categories int(11) unsigned DEFAULT '0' NOT NULL,
	country int(11) unsigned DEFAULT '0',
	layout_image int(11) unsigned DEFAULT '0',
	company_layout int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_umantis_domain_model_joblayoutimages'
#
CREATE TABLE tx_umantis_domain_model_joblayoutimages (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	layout_id varchar(255) DEFAULT '' NOT NULL,
	layout_image int(11) unsigned NOT NULL default '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_umantis_domain_model_jobcompanylayout'
#
CREATE TABLE tx_umantis_domain_model_jobcompanylayout (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	company_code varchar(255) DEFAULT '' NOT NULL,
	base_color varchar(255) DEFAULT '' NOT NULL,
	border_color varchar(255) DEFAULT '' NOT NULL,
	footer_color varchar(255) DEFAULT '' NOT NULL,
	list_image int(11) unsigned NOT NULL default '0',
	footer_image int(11) unsigned NOT NULL default '0',
	show_apply_button tinyint(1) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'sys_category'
#
CREATE TABLE sys_category (

	job  int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_umantis_domain_model_job'
#
CREATE TABLE tx_umantis_domain_model_job (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);
