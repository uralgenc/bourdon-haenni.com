baumerApp.controller('JobListController', ['$scope', '$timeout', function($scope, $timeout) {
	$scope.jobs = _.map(jobs, function(job){
		job['url'] = jobLinks[job.internalCode];
		return job;
	});
	$scope.filtersExpanded = false;


	$scope.filterCountries = [];
	$scope.availableCountries = _.compact(_.unique(_.pluck(jobs, 'country')));

	$scope.filterDepartments = [];
	$scope.availableDepartments = _.compact(_.unique(_.pluck(jobs, 'department')));

	$scope.filterCompanies = [];
	$scope.availableCompanies = _.compact(_.unique(_.pluck(jobs, 'company')));

	$scope.toggleFilterExpansion = function() {
		$scope.filtersExpanded = !$scope.filtersExpanded;
		if ($scope.filtersExpanded) {
			$timeout(function(){
				$('.filterCatFlyout').equalHeights();
			});
		} else {
			$timeout(function(){
				$('.filterCatFlyout').css('height', '');
			});
		}
	};

	$scope.resetFilters = function() {
		$scope.filterDepartments = [];
		$scope.filterCompanies = [];
		$scope.filterCountries = [];
	};


	$scope.updateCountryFilter = function(country) {
		if(_.contains($scope.filterCountries, country)) {
			$scope.filterCountries = _.filter($scope.filterCountries, function(con){ return con != country; });
		} else {
			$scope.filterCountries.push(country);
		}
	};

	$scope.updateDepartmentFilter = function(department) {
		if(_.contains($scope.filterDepartments, department)) {
			$scope.filterDepartments = _.filter($scope.filterDepartments, function(dep){ return dep != department; });
		} else {
			$scope.filterDepartments.push(department);
		}
	};

	$scope.updateCompanyFilter = function(company) {
		if(_.contains($scope.filterCompanies, company)) {
			$scope.filterCompanies = _.filter($scope.filterCompanies, function(co){ return co != company; });
		} else {
			$scope.filterCompanies.push(company);
		}
	};

	$scope.isFilteredByCountry = function (country) {
		return _.contains($scope.filterCountries, country);
	};
	$scope.isFilteredByDepartment = function (department) {
		return _.contains($scope.filterDepartments, department);
	};
	$scope.isFilteredByCompany = function (company) {
		return _.contains($scope.filterCompanies, company);
	};


	$scope.filterJobs = function(job) {
		var departmentMatches = _.contains($scope.filterDepartments, job.department);
		if($scope.filterDepartments.length == 0){
			departmentMatches = true;
		}
		var companyMatches = _.contains($scope.filterCompanies, job.company);
		if($scope.filterCompanies.length == 0){
			companyMatches = true;
		}
		var countryMatches = _.contains($scope.filterCountries, job.country);
		if($scope.filterCountries.length == 0){
			countryMatches = true;
		}
		return countryMatches && departmentMatches && companyMatches;
	};


}]);
