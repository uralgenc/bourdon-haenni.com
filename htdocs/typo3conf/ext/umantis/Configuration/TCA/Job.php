<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_umantis_domain_model_job'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_umantis_domain_model_job']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, internal_code, umantis_id, position, umantis_position_id, company, department, location, fixed_time, form_of_employment, has_priority, state, slogan, company_description, introduction, job_description, challenge, offer, profile_title, profile_text, video_url, finishing_text, contact_person_text, application_url, categories, country, layout_image, company_layout',
	),
	'types' => array(
		'1' => array('showitem' =>
			'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, internal_code, state, has_priority,
			--div--;LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.tca.job_info, position, company, department, location, fixed_time, form_of_employment,
			--div--;LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.tca.umantis, umantis_id, umantis_position_id,
			--div--;LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.tca.text, slogan;;;richtext:rte_transform[mode=ts_links], company_description;;;richtext:rte_transform[mode=ts_links],
			introduction;;;richtext:rte_transform[mode=ts_links], job_description;;;richtext:rte_transform[mode=ts_links],
			challenge, offer;;;richtext:rte_transform[mode=ts_links], profile_title, profile_text;;;richtext:rte_transform[mode=ts_links],
			video_url, finishing_text;;;richtext:rte_transform[mode=ts_links], contact_person_text;;;richtext:rte_transform[mode=ts_links],
			--div--;LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.tca.relation, country, layout_image, company_layout, application_url, categories,
			--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_umantis_domain_model_job',
				'foreign_table_where' => 'AND tx_umantis_domain_model_job.pid=###CURRENT_PID### AND tx_umantis_domain_model_job.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'internal_code' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.internal_code',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'umantis_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.umantis_id',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'position' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.position',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'umantis_position_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.umantis_position_id',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'company' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.company',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'department' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.department',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'location' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.location',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'fixed_time' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.fixed_time',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'form_of_employment' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.form_of_employment',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array(
						'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.form_of_employment.full_time',
						\Baumer\Umantis\Domain\Model\Job::FORM_OF_EMPLOYMENT_FULL_TIME
					),
					array(
						'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.form_of_employment.part_time',
						\Baumer\Umantis\Domain\Model\Job::FORM_OF_EMPLOYMENT_PART_TIME
					),
					array(
						'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.form_of_employment.trainee',
						\Baumer\Umantis\Domain\Model\Job::FORM_OF_EMPLOYMENT_TRAINEE
					),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'has_priority' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.has_priority',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'state' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.state',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array(
						'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.state.unpublished',
						\Baumer\Umantis\Domain\Model\Job::STATE_UNPUBLISHED
					),
					array(
						'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.state.published',
						\Baumer\Umantis\Domain\Model\Job::STATE_PUBLISHED
					),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'slogan' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.slogan',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'company_description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.company_description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'introduction' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.introduction',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'job_description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.job_description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'challenge' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.challenge',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'offer' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.offer',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'profile_title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.profile_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'profile_text' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.profile_text',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'video_url' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.video_url',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'finishing_text' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.finishing_text',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'contact_person_text' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.contact_person_text',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'script' => 'wizard_rte.php',
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'application_url' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.application_url',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'categories' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.categories',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'sys_category',
				'foreign_field' => 'job',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.country',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'static_countries',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'layout_image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.layout_image',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_umantis_domain_model_joblayoutimages',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		'company_layout' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.company_layout',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_umantis_domain_model_jobcompanylayout',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
	),
);
