<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_umantis_domain_model_jobcompanylayout'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_umantis_domain_model_jobcompanylayout']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'company_code, base_color, border_color, footer_color, list_image, footer_image, show_apply_button',
	),
	'types' => array(
		'1' => array('showitem' => '1, company_code, base_color, border_color, footer_color, list_image, footer_image, show_apply_button, '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'company_code' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout.company_code',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required,unique'
			),
		),
		'base_color' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout.base_color',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'border_color' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout.border_color',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'footer_color' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout.footer_color',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'list_image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout.list_image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'listImage',
				array('maxitems' => 1),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'footer_image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout.footer_image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'footerImage',
				array('maxitems' => 1),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'show_apply_button' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout.show_apply_button',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		
	),
);
