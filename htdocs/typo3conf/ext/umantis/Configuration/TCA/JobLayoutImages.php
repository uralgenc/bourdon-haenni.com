<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_umantis_domain_model_joblayoutimages'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_umantis_domain_model_joblayoutimages']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'layout_id, layout_image',
	),
	'types' => array(
		'1' => array('showitem' => '1, layout_id, layout_image, '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'layout_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_joblayoutimages.layout_id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required,unique'
			),
		),
		'layout_image' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_joblayoutimages.layout_image',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'layoutImage',
				array('maxitems' => 1),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		
	),
);
