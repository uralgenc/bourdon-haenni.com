
plugin.tx_umantis {
	view {
		# cat=plugin.tx_umantis/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:umantis/Resources/Private/Templates/
		# cat=plugin.tx_umantis/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:umantis/Resources/Private/Partials/
		# cat=plugin.tx_umantis/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:umantis/Resources/Private/Layouts/
		pdfTypenum = 504446
	}
	persistence {
		# cat=plugin.tx_umantis//a; type=string; label=Default storage PID
		storagePid = 278
	}
	settings {
		# cat=plugin.tx_umantis//a; type=string; label=Default company code
		defaultJobCompanyLayout = DEFAULT
		# cat=plugin.tx_umantis//a; type=string; label=Default layout image
		defaultJobLayoutImage = 5664
		# cat=plugin.tx_umantis//a; type=boolean; label=PDF is forced to download, otherwise browser inline
		forcePdfDownload = 0
	}
}
