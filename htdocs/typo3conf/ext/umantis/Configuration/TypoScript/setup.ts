
plugin.tx_umantis {
	view {
		templateRootPath = {$plugin.tx_umantis.view.templateRootPath}
		partialRootPath = {$plugin.tx_umantis.view.partialRootPath}
		layoutRootPath = {$plugin.tx_umantis.view.layoutRootPath}
		formatToPageTypeMapping {
			pdf = {$plugin.tx_umantis.view.pdfTypenum}
		}
	}
	persistence {
		storagePid = {$plugin.tx_umantis.persistence.storagePid}
	}
	features {
		rewrittenPropertyMapper = 1
	}
	settings {
		defaultJobCompanyLayout = {$plugin.tx_umantis.settings.defaultJobCompanyLayout}
		defaultJobLayoutImage = {$plugin.tx_umantis.settings.defaultJobLayoutImage}
		storagePid = {$plugin.tx_umantis.persistence.storagePid}
		pdfTypenum = {$plugin.tx_umantis.view.pdfTypenum}
		forcePdfDownload = {$plugin.tx_umantis.settings.forcePdfDownload}
	}
}

module.tx_umantis < plugin.tx_umantis

# =================================================
#	PDF Generation typenum
# =================================================

tx_umantis_pdf = PAGE
tx_umantis_pdf {
	typeNum = {$plugin.tx_umantis.view.pdfTypenum}
	config {
		xhtml_cleaning = 0
		admPanel = 0
		no_cache = 1
	}
	includeCSS {
		file1 = {$filepaths.css}standalonePrint.css
	}

	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = Umantis
		pluginName = Jobs
		vendorName = Baumer
		controller = Job
		action = showPdf
		switchableControllerActions {
			Job {
				1 = showPdf
			}
		}
		view < plugin.tx_umantis.view
		persistence < plugin.tx_umantis.persistence
		settings < plugin.tx_umantis.settings
	}
}

# =================================================
#	Solr index queue
# =================================================

plugin.tx_solr.index.queue.jobs = 1
plugin.tx_solr.index.queue.jobs {
	table = tx_umantis_domain_model_job

	additionalPageIds = {$plugin.tx_umantis.persistence.storagePid}

	fields {
		thumbnail_stringS = TEXT
		thumbnail_stringS.value = http://placehold.it/180x120

		abstract = SOLR_CONTENT
		abstract.field = job_description

		content = SOLR_CONTENT
		content.field = job_description

		title = title
		titleshort_stringS = title
		keywords = internal_code

		company_stringS = company
		department_stringS = department
		location_stringS = location
		position_stringS = position

		fixedTime_intS = fixed_time

		formOfEmployment_stringS = CASE
		formOfEmployment_stringS {
			key.field = form_of_employment
			0 = TEXT
			0.data = LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.form_of_employment.full_time
			1 = TEXT
			1.data = LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.form_of_employment.part_time
			2 = TEXT
			2.data = LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job.form_of_employment.trainee
		}
		state_intS = state

		category_stringM = SOLR_RELATION
		category_stringM {
			localField = categories
			multiValue = 1
		}

		country_stringS = SOLR_RELATION
		country_stringS {
			localField = country
		}

		url = TEXT
		url {
			typolink {
				parameter = {$plugin.tx_baumer.settings.jobPageUid}
				additionalParams = &tx_umantis_jobs[controller]=Job&tx_umantis_jobs[action]=show&tx_umantis_jobs[job]={field:uid}
				additionalParams.insertData = 1
				useCacheHash = 1
				returnLast = url
			}
		}

	}
}
