<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Baumer.' . $_EXTKEY,
	'Jobs',
	'Jobs'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Umantis Integration');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_umantis_domain_model_job', 'EXT:umantis/Resources/Private/Language/locallang_csh_tx_umantis_domain_model_job.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_umantis_domain_model_job');
$GLOBALS['TCA']['tx_umantis_domain_model_job'] = [
	'ctrl' => [
		'title'	=> 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_job',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		],
		'searchFields' => 'title,internal_code,umantis_id,position,umantis_position_id,company,department,location,fixed_time,form_of_employment,has_priority,state,slogan,company_description,introduction,job_description,challenge,offer,profile_title,profile_text,video_url,finishing_text,contact_person_text,application_url,categories,country,layout_image,company_layout,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Job.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/user_suit.png'
	],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_umantis_domain_model_joblayoutimages', 'EXT:umantis/Resources/Private/Language/locallang_csh_tx_umantis_domain_model_joblayoutimages.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_umantis_domain_model_joblayoutimages');
$GLOBALS['TCA']['tx_umantis_domain_model_joblayoutimages'] = [
	'ctrl' => [
		'title'	=> 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_joblayoutimages',
		'label' => 'layout_id',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'adminOnly' => TRUE,

		'languageField' => '-1',
		'delete' => 'deleted',
		'enablecolumns' => [

		],
		'searchFields' => 'layout_id,layout_image,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/JobLayoutImages.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/document_image.png'
	],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_umantis_domain_model_jobcompanylayout', 'EXT:umantis/Resources/Private/Language/locallang_csh_tx_umantis_domain_model_jobcompanylayout.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_umantis_domain_model_jobcompanylayout');
$GLOBALS['TCA']['tx_umantis_domain_model_jobcompanylayout'] = [
	'ctrl' => [
		'title'	=> 'LLL:EXT:umantis/Resources/Private/Language/locallang_db.xlf:tx_umantis_domain_model_jobcompanylayout',
		'label' => 'company_code',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'adminOnly' => TRUE,

		'languageField' => '-1',
		'delete' => 'deleted',
		'enablecolumns' => [

		],
		'searchFields' => 'company_code,base_color,border_color,footer_color,list_image,footer_image,show_apply_button,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/JobCompanyLayout.php',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/client_account_template.png'
	],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
	$_EXTKEY,
	'tx_umantis_domain_model_job'
);
