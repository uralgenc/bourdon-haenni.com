<?php
namespace Baumer\Umantis\Command;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Baumer\Baumer\Command\AbstractCommandController;
use SJBR\StaticInfoTables\Domain\Model\Country;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use Baumer\Umantis\Domain\Model\Job;

/**
 * Class UmantisCommandController
 *
 * @author Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 * @package Baumer\Umantis\Command
 */
class UmantisCommandController extends AbstractCommandController {

	/**
	 * @var \Baumer\Umantis\Domain\Repository\JobRepository
	 * @inject
	 */
	protected $jobRepository;

	/**
	 * @var \Baumer\Umantis\Domain\Repository\JobCompanyLayoutRepository
	 * @inject
	 */
	protected $jobCompanyLayoutRepository;

	/**
	 * @var \Baumer\Umantis\Domain\Repository\JobLayoutImagesRepository
	 * @inject
	 */
	protected $jobLayoutImagesRepository;

	/**
	 * @var \SJBR\StaticInfoTables\Domain\Repository\CountryRepository
	 * @inject
	 */
	protected $countryRepository;

	/**
	 * Searches in an array of Jobs for a job having the specified property and value
	 *
	 * @param array $queryResult
	 * @param $property
	 * @param $value
	 *
	 * @return Job|boolean
	 */
	private function searchQueryResultForPropertyValue(array $queryResult, $property, $value) {
		$getterName = 'get' . ucfirst($property);
		foreach ($queryResult as $record) {
			if (method_exists($record, $getterName) &&
				$record->{$getterName}() === $value) {
				return $record;
			}
		}
		return FALSE;
	}

	/**
	 * Get an array of all UmantisIds inside the queryResult
	 *
	 * @param array $queryResult
	 *
	 * @return array
	 */
	private function getAllUmantisIds(array $queryResult) {
		$umantisIds = array();
		foreach ($queryResult as $record) {
			if (method_exists($record, 'getUmantisId') &&
				$record->getUmantisId() > 0) {
				$umantisIds[] = $record->getUmantisId();
			}
		}
		return $umantisIds;
	}

	/**
	 * Process the xml and update records
	 *
	 * @param string $content
	 * @return void
	 */
	private function updateUmantisJobs($content) {
		/** @var PersistenceManager $persistenceManager */
		$persistenceManager = $this->objectManager->get(PersistenceManager::class);
		$settings = $this->configurationManager->getConfiguration('umantis', 'job');

		// Read persisted jobs from db for delta compare
		$existingJobs = $this->jobRepository->findAllLanguageParents()->toArray();
		$umantisIds = array_flip($this->getAllUmantisIds($existingJobs));

		$xml = new \SimpleXMLElement($content, LIBXML_NOCDATA);
		if (property_exists($xml, 'Job-Items')) {
			/** @var \SimpleXMLElement $jobNode */
			foreach ($xml->{'Job-Items'}->children() as $jobNode) {

				/** @noinspection PhpUndefinedFieldInspection */
				$umantisId = (integer) $jobNode->attributes()->id;
				unset($umantisIds[$umantisId]);
				// Check if there's already a job with this umantisID stored
				$defaultJob = $this->searchQueryResultForPropertyValue($existingJobs, 'umantisId', $umantisId);
				if ($defaultJob === FALSE) {
					$defaultJob = new Job();
				} else {
					$this->db->exec_DELETEquery('tx_umantis_domain_model_job', 'l10n_parent = ' . $defaultJob->getUid());
				}
				$englishJob = new Job();
				$germanJob = new Job();

				// Update values of umantis records
				$defaultJob->updateLocalizedJob($jobNode, '_ger', 'German');
				$englishJob->updateLocalizedJob($jobNode, '_eng', 'English');
				$germanJob->updateLocalizedJob($jobNode, '_ger', 'German');
				$defaultJob->updateLocalizedJobRelations($jobNode,
					$this->countryRepository,
					$this->jobLayoutImagesRepository,
					$this->jobCompanyLayoutRepository
				);
				$englishJob->updateLocalizedJobRelations($jobNode,
					$this->countryRepository,
					$this->jobLayoutImagesRepository,
					$this->jobCompanyLayoutRepository
				);
				$germanJob->updateLocalizedJobRelations($jobNode,
					$this->countryRepository,
					$this->jobLayoutImagesRepository,
					$this->jobCompanyLayoutRepository
				);

				// Persist jobs to get stored uid for overlay records
				if ($defaultJob->_isNew()) {
					$defaultJob->setPid($settings['storagePid']);
					$defaultJob->setLanguageUid(0);
					$this->jobRepository->add($defaultJob);
					$persistenceManager->persistAll();
				} else {
					$this->jobRepository->update($defaultJob);
				}

				// Assign language params to overlay records
				$germanJob->setPid($settings['storagePid']);
				$germanJob->setLanguageUid(1);
				$germanJob->setL10nParent($defaultJob->getUid());
				$englishJob->setPid($settings['storagePid']);
				$englishJob->setLanguageUid(2);
				$englishJob->setL10nParent($defaultJob->getUid());

				// Persist overlay records
				$this->jobRepository->add($germanJob);
				$this->jobRepository->add($englishJob);
			}
			// Delete all records, which were added by umantis but are not longer present
			if (count($umantisIds) > 0) {
				$umantisIds = array_flip($umantisIds);
				$this->db->exec_DELETEquery('tx_umantis_domain_model_job', 'umantis_id > 0 AND umantis_id IN (' . implode(',', $umantisIds) . ')');
			}
		} else {
			GeneralUtility::sysLog(
				'Umantis XML doesn\'t contain section Job-Items as expected',
				'umantis',
				GeneralUtility::SYSLOG_SEVERITY_ERROR
			);
		}
	}

	/**
	 * Read XML from specified URL and imports job records
	 *
	 * @param string $umantisUrl Full URL to XML export from Umantis
	 * @return void
	 */
	public function importXmlCommand ($umantisUrl = '') {
		// Read XML content from provided url
		$content = GeneralUtility::getUrl($umantisUrl, 0, array('Content-Type: text/xml; charset=utf-8'));
		if ($content !== FALSE) {
			$this->updateUmantisJobs($content);
		} else {
			GeneralUtility::sysLog(
				'Umantis XML could not be read. Received empty reply from url',
				'umantis',
				GeneralUtility::SYSLOG_SEVERITY_ERROR
			);
		}
	}
}
