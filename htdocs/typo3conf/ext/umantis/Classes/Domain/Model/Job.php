<?php
namespace Baumer\Umantis\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use SJBR\StaticInfoTables\Domain\Model\Country;
use SJBR\StaticInfoTables\Domain\Repository\CountryRepository;
use Baumer\Umantis\Domain\Repository\JobCompanyLayoutRepository;
use Baumer\Umantis\Domain\Repository\JobLayoutImagesRepository;

/**
 * Job
 */
class Job extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	use \Baumer\Baumer\Domain\Model\Behavior\ToArrayTrait;

	const STATE_UNPUBLISHED = 0;
	const STATE_PUBLISHED = 1;

	const FORM_OF_EMPLOYMENT_FULL_TIME = 0;
	const FORM_OF_EMPLOYMENT_PART_TIME = 1;
	const FORM_OF_EMPLOYMENT_TRAINEE = 2;

	/**
	 * Title of the job
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * Internal code usually provided by external API
	 *
	 * @var string
	 */
	protected $internalCode = '';

	/**
	 * ID of the job from umantis
	 *
	 * @var integer
	 */
	protected $umantisId = 0;

	/**
	 * Job position (e.g. Employee)
	 *
	 * @var string
	 */
	protected $position = '';

	/**
	 * ID of the position type from umantis
	 *
	 * @var integer
	 */
	protected $umantisPositionId = 0;

	/**
	 * The company which offers the job
	 *
	 * @var string
	 */
	protected $company = '';

	/**
	 * The department inside the company
	 *
	 * @var string
	 */
	protected $department = '';

	/**
	 * The location of the company
	 *
	 * @var string
	 */
	protected $location = '';

	/**
	 * Is the job limited in time (e.g. contract for 2 years)
	 *
	 * @var boolean
	 */
	protected $fixedTime = FALSE;

	/**
	 * Is the employment full-time, part-time, trainee
	 *
	 * @var integer
	 */
	protected $formOfEmployment = 0;

	/**
	 * Sort this job on top
	 *
	 * @var boolean
	 */
	protected $hasPriority = FALSE;

	/**
	 * The state of the job. Is it published?
	 *
	 * @var integer
	 */
	protected $state = 0;

	/**
	 * Slogan of the job (teasertext)
	 *
	 * @var string
	 */
	protected $slogan = '';

	/**
	 * Description of the company
	 *
	 * @var string
	 */
	protected $companyDescription = '';

	/**
	 * Introduction text
	 *
	 * @var string
	 */
	protected $introduction = '';

	/**
	 * Description of the job
	 *
	 * @var string
	 */
	protected $jobDescription = '';

	/**
	 * challenge
	 *
	 * @var string
	 */
	protected $challenge = '';

	/**
	 * List of functions this job offers
	 *
	 * @var string
	 */
	protected $offer = '';

	/**
	 * Prefix-text for profile
	 *
	 * @var string
	 */
	protected $profileTitle = '';

	/**
	 * List of attributes a job candidate should have
	 *
	 * @var string
	 */
	protected $profileText = '';

	/**
	 * URL to a video presenting the job
	 *
	 * @var string
	 */
	protected $videoUrl = '';

	/**
	 * The finishing sentence
	 *
	 * @var string
	 */
	protected $finishingText = '';

	/**
	 * Contactperson text and way of contact
	 *
	 * @var string
	 */
	protected $contactPersonText = '';

	/**
	 * URL which contains application form
	 *
	 * @var string
	 */
	protected $applicationUrl = '';

	/**
	 * l10nParent
	 *
	 * @var \int
	 */
	protected $l10nParent;

	/**
	 * @param int $languageUid
	 * @return void
	 */
	public function setLanguageUid($languageUid) {
		$this->_languageUid = $languageUid;
	}

	/**
	 * @return int
	 */
	public function getLanguageUid() {
		return $this->_languageUid;
	}

	/**
	 * @param int $l10nParent
	 * @return void
	 */
	public function setL10nParent($l10nParent) {
		$this->l10nParent = $l10nParent;
	}

	/**
	 * @return int
	 */
	public function getL10nParent() {
		return $this->l10nParent;
	}

	/**
	 * categories
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
	 * @cascade remove
	 */
	protected $categories = NULL;

	/**
	 * country
	 *
	 * @var \SJBR\StaticInfoTables\Domain\Model\Country
	 */
	protected $country = NULL;

	/**
	 * Defines the image for the job to be output. Fallback is definded in TS
	 *
	 * @var \Baumer\Umantis\Domain\Model\JobLayoutImages
	 */
	protected $layoutImage = NULL;

	/**
	 * Company Layout that should be used for detail view
	 *
	 * @var \Baumer\Umantis\Domain\Model\JobCompanyLayout
	 */
	protected $companyLayout = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the internalCode
	 *
	 * @return string $internalCode
	 */
	public function getInternalCode() {
		return $this->internalCode;
	}

	/**
	 * Sets the internalCode
	 *
	 * @param string $internalCode
	 * @return void
	 */
	public function setInternalCode($internalCode) {
		$this->internalCode = $internalCode;
	}

	/**
	 * Returns the umantisId
	 *
	 * @return integer $umantisId
	 */
	public function getUmantisId() {
		return $this->umantisId;
	}

	/**
	 * Sets the umantisId
	 *
	 * @param integer $umantisId
	 * @return void
	 */
	public function setUmantisId($umantisId) {
		$this->umantisId = $umantisId;
	}

	/**
	 * Returns the position
	 *
	 * @return string $position
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * Sets the position
	 *
	 * @param string $position
	 * @return void
	 */
	public function setPosition($position) {
		$this->position = $position;
	}

	/**
	 * Returns the umantisPositionId
	 *
	 * @return integer $umantisPositionId
	 */
	public function getUmantisPositionId() {
		return $this->umantisPositionId;
	}

	/**
	 * Sets the umantisPositionId
	 *
	 * @param integer $umantisPositionId
	 * @return void
	 */
	public function setUmantisPositionId($umantisPositionId) {
		$this->umantisPositionId = $umantisPositionId;
	}

	/**
	 * Returns the company
	 *
	 * @return string $company
	 */
	public function getCompany() {
		return $this->company;
	}

	/**
	 * Sets the company
	 *
	 * @param string $company
	 * @return void
	 */
	public function setCompany($company) {
		$this->company = $company;
	}

	/**
	 * Returns the department
	 *
	 * @return string $department
	 */
	public function getDepartment() {
		return $this->department;
	}

	/**
	 * Sets the department
	 *
	 * @param string $department
	 * @return void
	 */
	public function setDepartment($department) {
		$this->department = $department;
	}

	/**
	 * Returns the location
	 *
	 * @return string $location
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * Sets the location
	 *
	 * @param string $location
	 * @return void
	 */
	public function setLocation($location) {
		$this->location = $location;
	}

	/**
	 * Returns the fixedTime
	 *
	 * @return boolean $fixedTime
	 */
	public function getFixedTime() {
		return $this->fixedTime;
	}

	/**
	 * Sets the fixedTime
	 *
	 * @param boolean $fixedTime
	 * @return void
	 */
	public function setFixedTime($fixedTime) {
		$this->fixedTime = $fixedTime;
	}

	/**
	 * Returns the boolean state of fixedTime
	 *
	 * @return boolean
	 */
	public function isFixedTime() {
		return $this->fixedTime;
	}

	/**
	 * Returns the formOfEmployment
	 *
	 * @return integer $formOfEmployment
	 */
	public function getFormOfEmployment() {
		return $this->formOfEmployment;
	}

	/**
	 * Sets the formOfEmployment
	 *
	 * @param integer $formOfEmployment
	 * @return void
	 */
	public function setFormOfEmployment($formOfEmployment) {
		$this->formOfEmployment = $formOfEmployment;
	}

	/**
	 * Returns the hasPriority
	 *
	 * @return boolean $hasPriority
	 */
	public function getHasPriority() {
		return $this->hasPriority;
	}

	/**
	 * Sets the hasPriority
	 *
	 * @param boolean $hasPriority
	 * @return void
	 */
	public function setHasPriority($hasPriority) {
		$this->hasPriority = $hasPriority;
	}

	/**
	 * Returns the boolean state of hasPriority
	 *
	 * @return boolean
	 */
	public function isHasPriority() {
		return $this->hasPriority;
	}

	/**
	 * Returns the state
	 *
	 * @return integer $state
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * Sets the state
	 *
	 * @param integer $state
	 * @return void
	 */
	public function setState($state) {
		$this->state = $state;
	}

	/**
	 * Returns the slogan
	 *
	 * @return string $slogan
	 */
	public function getSlogan() {
		return $this->slogan;
	}

	/**
	 * Sets the slogan
	 *
	 * @param string $slogan
	 * @return void
	 */
	public function setSlogan($slogan) {
		$this->slogan = $slogan;
	}

	/**
	 * Returns the companyDescription
	 *
	 * @return string $companyDescription
	 */
	public function getCompanyDescription() {
		return $this->companyDescription;
	}

	/**
	 * Sets the companyDescription
	 *
	 * @param string $companyDescription
	 * @return void
	 */
	public function setCompanyDescription($companyDescription) {
		$this->companyDescription = $companyDescription;
	}

	/**
	 * Returns the introduction
	 *
	 * @return string $introduction
	 */
	public function getIntroduction() {
		return $this->introduction;
	}

	/**
	 * Sets the introduction
	 *
	 * @param string $introduction
	 * @return void
	 */
	public function setIntroduction($introduction) {
		$this->introduction = $introduction;
	}

	/**
	 * Returns the jobDescription
	 *
	 * @return string $jobDescription
	 */
	public function getJobDescription() {
		return $this->jobDescription;
	}

	/**
	 * Sets the jobDescription
	 *
	 * @param string $jobDescription
	 * @return void
	 */
	public function setJobDescription($jobDescription) {
		$this->jobDescription = $jobDescription;
	}

	/**
	 * Returns the challenge
	 *
	 * @return string $challenge
	 */
	public function getChallenge() {
		return $this->challenge;
	}

	/**
	 * Sets the challenge
	 *
	 * @param string $challenge
	 * @return void
	 */
	public function setChallenge($challenge) {
		$this->challenge = $challenge;
	}

	/**
	 * Returns the offer
	 *
	 * @return string $offer
	 */
	public function getOffer() {
		return $this->offer;
	}

	/**
	 * Sets the offer
	 *
	 * @param string $offer
	 * @return void
	 */
	public function setOffer($offer) {
		$this->offer = $offer;
	}

	/**
	 * Returns the profileTitle
	 *
	 * @return string $profileTitle
	 */
	public function getProfileTitle() {
		return $this->profileTitle;
	}

	/**
	 * Sets the profileTitle
	 *
	 * @param string $profileTitle
	 * @return void
	 */
	public function setProfileTitle($profileTitle) {
		$this->profileTitle = $profileTitle;
	}

	/**
	 * Returns the profileText
	 *
	 * @return string $profileText
	 */
	public function getProfileText() {
		return $this->profileText;
	}

	/**
	 * Sets the profileText
	 *
	 * @param string $profileText
	 * @return void
	 */
	public function setProfileText($profileText) {
		$this->profileText = $profileText;
	}

	/**
	 * Returns the videoUrl
	 *
	 * @return string $videoUrl
	 */
	public function getVideoUrl() {
		return $this->videoUrl;
	}

	/**
	 * Sets the videoUrl
	 *
	 * @param string $videoUrl
	 * @return void
	 */
	public function setVideoUrl($videoUrl) {
		$this->videoUrl = $videoUrl;
	}

	/**
	 * Returns the finishingText
	 *
	 * @return string $finishingText
	 */
	public function getFinishingText() {
		return $this->finishingText;
	}

	/**
	 * Sets the finishingText
	 *
	 * @param string $finishingText
	 * @return void
	 */
	public function setFinishingText($finishingText) {
		$this->finishingText = $finishingText;
	}

	/**
	 * Returns the contactPersonText
	 *
	 * @return string $contactPersonText
	 */
	public function getContactPersonText() {
		return $this->contactPersonText;
	}

	/**
	 * Sets the contactPersonText
	 *
	 * @param string $contactPersonText
	 * @return void
	 */
	public function setContactPersonText($contactPersonText) {
		$this->contactPersonText = $contactPersonText;
	}

	/**
	 * Returns the applicationUrl
	 *
	 * @return string $applicationUrl
	 */
	public function getApplicationUrl() {
		return $this->applicationUrl;
	}

	/**
	 * Sets the applicationUrl
	 *
	 * @param string $applicationUrl
	 * @return void
	 */
	public function setApplicationUrl($applicationUrl) {
		$this->applicationUrl = $applicationUrl;
	}

	/**
	 * Adds a Category
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $category) {
		$this->categories->attach($category);
	}

	/**
	 * Removes a Category
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove The Category to be removed
	 * @return void
	 */
	public function removeCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove) {
		$this->categories->detach($categoryToRemove);
	}

	/**
	 * Returns the categories
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category> $categories
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Sets the categories
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category> $categories
	 * @return void
	 */
	public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories) {
		$this->categories = $categories;
	}

	/**
	 * Returns the country
	 *
	 * @return \SJBR\StaticInfoTables\Domain\Model\Country $country
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * Sets the country
	 *
	 * @param \SJBR\StaticInfoTables\Domain\Model\Country $country
	 * @return void
	 */
	public function setCountry(\SJBR\StaticInfoTables\Domain\Model\Country $country) {
		$this->country = $country;
	}

	/**
	 * Returns the layoutImage
	 *
	 * @return \Baumer\Umantis\Domain\Model\JobLayoutImages $layoutImage
	 */
	public function getLayoutImage() {
		return $this->layoutImage;
	}

	/**
	 * Sets the layoutImage
	 *
	 * @param \Baumer\Umantis\Domain\Model\JobLayoutImages $layoutImage
	 * @return void
	 */
	public function setLayoutImage(\Baumer\Umantis\Domain\Model\JobLayoutImages $layoutImage) {
		$this->layoutImage = $layoutImage;
	}

	/**
	 * Returns the companyLayout
	 *
	 * @return \Baumer\Umantis\Domain\Model\JobCompanyLayout $companyLayout
	 */
	public function getCompanyLayout() {
		return $this->companyLayout;
	}

	/**
	 * Sets the companyLayout
	 *
	 * @param \Baumer\Umantis\Domain\Model\JobCompanyLayout $companyLayout
	 * @return void
	 */
	public function setCompanyLayout(\Baumer\Umantis\Domain\Model\JobCompanyLayout $companyLayout) {
		$this->companyLayout = $companyLayout;
	}

	/**
	 * Convert XML Job-Node to localized Job model
	 *
	 * @param \SimpleXMLElement $jobNode
	 * @param string $localizationSuffix e.g. _ger
	 * @param string $publicationSuffix e.g. German
	 *
	 * @return void
	 */
	public function updateLocalizedJob(\SimpleXMLElement $jobNode, $localizationSuffix = '', $publicationSuffix = '') {
		$this->setInternalCode((string)$jobNode->attributes()->JobID);
		$this->setUmantisId((integer)$jobNode->attributes()->id);
		$this->setTitle((string)$jobNode->{'Stellentitel' . $localizationSuffix});
		$this->setPosition((string)$jobNode->{'Position' . $localizationSuffix});
		$this->setUmantisPositionId((int)$jobNode->Position_umantisID);
		$this->setFixedTime(TRUE);
		$fixedTime = (string)$jobNode->Befristung;
		if ($fixedTime === 'nein') {
			$this->setFixedTime(FALSE);
		}
		$this->setFormOfEmployment(self::FORM_OF_EMPLOYMENT_FULL_TIME);
		$formOfEmplyoment = (string)$jobNode->Beschaeftigungsart_eng;
		if ($formOfEmplyoment === 'Part time') {
			$this->setFormOfEmployment(self::FORM_OF_EMPLOYMENT_PART_TIME);
		}
		$this->setDepartment((string)$jobNode->{'Unternehmensbereich' . $localizationSuffix});
		$this->setCompany((string)$jobNode->{'Unternehmen' . $localizationSuffix});
		$this->setLocation((string)$jobNode->{'Arbeitsort' . $localizationSuffix});
		$this->setHasPriority(FALSE);
		$priority = (string)$jobNode->Priority;
		if ($priority !== 'nein') {
			$this->setHasPriority(TRUE);
		}

		$pub = $jobNode->Stellenmarkt->{'Publication' . $publicationSuffix};
		$this->setState(self::STATE_UNPUBLISHED);
		$status = (string)$pub->attributes()->status;
		if (!empty($status)) {
			$this->setState(self::STATE_PUBLISHED);
		}
		$this->setSlogan((string)$pub->Slogan);
		$this->setCompanyDescription((string)$pub->Firmenbeschreibung);
		$this->setIntroduction((string)$pub->Einleitung);
		$this->setJobDescription((string)$pub->Aufgabenbeschreibung);
		$this->setChallenge((string)$pub->Hauptaufgaben);
		$this->setOffer((string)$pub->Angebot);
		$this->setProfileTitle((string)$pub->TitelProfil);
		$this->setProfileText((string)$pub->TextProfil);
		$this->setVideoUrl((string)$pub->VideoLink);
		$this->setFinishingText((string)$pub->Abschlusstext);
		$this->setContactPersonText((string)$pub->Kontaktperson);
		$this->setApplicationUrl((string)$pub->ApplyLink);
	}

	/**
	 * @param \SimpleXMLElement $jobNode
	 * @param CountryRepository $countryRepository
	 * @param JobLayoutImagesRepository $jobLayoutImagesRepository
	 * @param JobCompanyLayoutRepository $jobCompanyLayoutRepository
	 * @return void
	 */
	public function updateLocalizedJobRelations(
			\SimpleXMLElement $jobNode,
			CountryRepository $countryRepository,
			JobLayoutImagesRepository $jobLayoutImagesRepository,
			JobCompanyLayoutRepository $jobCompanyLayoutRepository
		) {
		$countryName = (string)$jobNode->Land_eng;
		/** @var Country $country */
		$country = $countryRepository->findOneByShortNameEn($countryName);
		// The countryName can be either shortcode or official name (e.g. USA)
		if (is_null($country)) {
			$country = $countryRepository->findOneByOfficialNameEn($countryName);
		}
		$this->setCountry($country);

		$insertionLayout = (string)$jobNode->Stellenmarkt->PublicationGerman->InserateLayout;
		$insertionLayoutCompanyCode = substr($insertionLayout, 4);

		$layoutImage = $jobLayoutImagesRepository->findOneByLayoutId($insertionLayout);
		$this->setLayoutImage($layoutImage);

		if ($insertionLayoutCompanyCode == FALSE) {
			$insertionLayoutCompanyCode = 'DEFAULT';
		}
		$companyLayout = $jobCompanyLayoutRepository->findOneByCompanyCode($insertionLayoutCompanyCode);
		$this->setCompanyLayout($companyLayout);
	}

	protected function addToAttributesArray(&$array) {
		$array['country'] = $this->getCountry()->getShortNameEn();

		// Filter only necessary attributes (security + performance)
		$allowedFrontendAttributes = ['internalCode', 'country', 'company', 'department', 'title', 'location'];
		foreach($array as $k => $v){
			if(!in_array($k, $allowedFrontendAttributes)){
				unset($array[$k]);
			}
		}
	}

}
