<?php
namespace Baumer\Umantis\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * JobCompanyLayout
 */
class JobCompanyLayout extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * companyCode
	 *
	 * @var string
	 * @validate NotEmpty
	 */
	protected $companyCode = '';

	/**
	 * baseColor
	 *
	 * @var string
	 */
	protected $baseColor = '';

	/**
	 * borderColor
	 *
	 * @var string
	 */
	protected $borderColor = '';

	/**
	 * footerColor
	 *
	 * @var string
	 */
	protected $footerColor = '';

	/**
	 * listImage
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $listImage = NULL;

	/**
	 * footerImage
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $footerImage = NULL;


	/**
	 * showApplyButton
	 *
	 * @var boolean
	 */
	protected $showApplyButton = FALSE;

	/**
	 * Returns the companyCode
	 *
	 * @return string $companyCode
	 */
	public function getCompanyCode() {
		return $this->companyCode;
	}

	/**
	 * Sets the companyCode
	 *
	 * @param string $companyCode
	 * @return void
	 */
	public function setCompanyCode($companyCode) {
		$this->companyCode = $companyCode;
	}

	/**
	 * Returns the baseColor
	 *
	 * @return string $baseColor
	 */
	public function getBaseColor() {
		return $this->baseColor;
	}

	/**
	 * Sets the baseColor
	 *
	 * @param string $baseColor
	 * @return void
	 */
	public function setBaseColor($baseColor) {
		$this->baseColor = $baseColor;
	}

	/**
	 * Returns the borderColor
	 *
	 * @return string $borderColor
	 */
	public function getBorderColor() {
		return $this->borderColor;
	}

	/**
	 * Sets the borderColor
	 *
	 * @param string $borderColor
	 * @return void
	 */
	public function setBorderColor($borderColor) {
		$this->borderColor = $borderColor;
	}

	/**
	 * Returns the footerColor
	 *
	 * @return string $footerColor
	 */
	public function getFooterColor() {
		return $this->footerColor;
	}

	/**
	 * Sets the footerColor
	 *
	 * @param string $footerColor
	 * @return void
	 */
	public function setFooterColor($footerColor) {
		$this->footerColor = $footerColor;
	}

	/**
	 * Returns the listImage
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $listImage
	 */
	public function getListImage() {
		return $this->listImage;
	}

	/**
	 * Sets the listImage
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $listImage
	 * @return void
	 */
	public function setListImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $listImage) {
		$this->listImage = $listImage;
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	public function getFooterImage() {
		return $this->footerImage;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $footerImage
	 * @return void
	 */
	public function setFooterImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $footerImage) {
		$this->footerImage = $footerImage;
	}

	/**
	 * Returns the showApplyButton
	 *
	 * @return boolean $showApplyButton
	 */
	public function getShowApplyButton() {
		return $this->showApplyButton;
	}

	/**
	 * Sets the showApplyButton
	 *
	 * @param boolean $showApplyButton
	 * @return void
	 */
	public function setShowApplyButton($showApplyButton) {
		$this->showApplyButton = $showApplyButton;
	}

	/**
	 * Returns the boolean state of showApplyButton
	 *
	 * @return boolean
	 */
	public function isShowApplyButton() {
		return $this->showApplyButton;
	}

}
