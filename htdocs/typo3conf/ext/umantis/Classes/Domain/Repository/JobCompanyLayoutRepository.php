<?php
namespace Baumer\Umantis\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use Baumer\Umantis\Domain\Model\JobCompanyLayout;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for JobCompanyLayouts
 */
class JobCompanyLayoutRepository extends Repository {

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @inject
	 */
	protected $configurationManager;

	/**
	 * Set default query settings
	 */
	public function initializeObject() {
		/** @var $defaultQuerySettings Typo3QuerySettings */
		$defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
		// add the pid constraint
		$defaultQuerySettings->setRespectStoragePage(FALSE);
		$defaultQuerySettings->setRespectSysLanguage(FALSE);
		$this->setDefaultQuerySettings($defaultQuerySettings);
	}

	/**
	 * Returns the CompanyLayout object by its company_code.
	 * If the company_code is not found the configured default fallback
	 * will be returned.
	 *
	 * @param string $companyCode
	 *
	 * @return JobCompanyLayout
	 */
	public function findOneByCompanyCode($companyCode) {
		$query = $this->createQuery();
		$result = $query->matching($query->equals('company_code', $companyCode))->setLimit(1)->execute();
		if ($result->count() == 0) {
			$settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'umantis', 'job');
			$companyCode = $settings['defaultJobCompanyLayout'];
			$result = $query->matching($query->equals('company_code', $companyCode))->setLimit(1)->execute();
		}
		return $result->getFirst();
	}
}
