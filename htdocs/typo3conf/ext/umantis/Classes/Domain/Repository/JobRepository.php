<?php
namespace Baumer\Umantis\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for Jobs
 */
class JobRepository extends Repository {

	protected $defaultOrderings = array(
		'has_priority' => QueryInterface::ORDER_DESCENDING,
		'company' => QueryInterface::ORDER_ASCENDING,
		'internal_code' => QueryInterface::ORDER_ASCENDING
	);

	/**
	 * Get all jobs in default sys_language
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findAllLanguageParents() {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setLanguageUid(0);
		return $result = $query->matching(
			$query->logicalAnd([
					$query->equals('sys_language_uid', 0),
					$query->equals('l10n_parent', 0)
				]
			)
		)->execute();
	}
}
