<?php
namespace Baumer\Umantis\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Hans Höchtl <extern.hans.hoechtl@typovision.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use mikehaertl\wkhtmlto\Pdf;

/**
 * JobController
 */
class JobController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * jobRepository
	 *
	 * @var \Baumer\Umantis\Domain\Repository\JobRepository
	 * @inject
	 */
	protected $jobRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$jobs = $this->jobRepository->findAll();
		$jobsJson = [];
		foreach($jobs as $job){
			$jobsJson[] = $job->toArray();
		}
		$this->view->assign('jobs', json_encode($jobsJson));
		$this->view->assign('jobModels', $jobs);
	}

	/**
	 * action show
	 *
	 * @param \Baumer\Umantis\Domain\Model\Job $job
	 * @return void
	 */
	public function showAction(\Baumer\Umantis\Domain\Model\Job $job) {
		$this->view->assign('job', $job);
	}

	/**
	 * action showPdf is used internally to generate view which is used
	 * for rendering a PDF using wkhtmltopdf
	 * @see pdfAction
	 *
	 * @param \Baumer\Umantis\Domain\Model\Job $job
	 * @return void
	 */
	public function showPdfAction(\Baumer\Umantis\Domain\Model\Job $job) {
		$requestUri = $this->uriBuilder->getRequest()->getRequestUri();
		parse_str(urldecode(parse_url($requestUri, PHP_URL_QUERY)), $uriParams);
		// Restrict this action to pdfTypenum otherwise redirect to show method
		if (isset($uriParams['type']) && $uriParams['type'] === $this->settings['pdfTypenum']) {
			$this->view->assign('job', $job);
		} else {
			$this->redirect('show', NULL, NULL, array('job' => $job));
		}
	}

	/**
	 * action pdfAction generates the URI for $job using the @see showPdfAction
	 * and passes this URI to wkhtmltopdf binary (installed via composer).
	 * The generated PDF is then sent to the browser as download.
	 *
	 * @param \Baumer\Umantis\Domain\Model\Job $job
	 * @return void
	 */
	public function pdfAction(\Baumer\Umantis\Domain\Model\Job $job) {
		// Generate URI for showPdfAction with typeNum for PDF
		$requestArgs = $this->uriBuilder->getRequest()->getArguments();
		$requestArgs['action'] = 'showPdf';
		$this->uriBuilder->setTargetPageType($this->settings['pdfTypenum']);
		$this->uriBuilder->setCreateAbsoluteUri(TRUE);
		$pdfUri = $this->uriBuilder->uriFor('showPdf', $requestArgs);
		// Pass URI to wkhtmltopdf
		$pdf = new Pdf($pdfUri);
		$pdf->binary = PATH_site . '../vendor/bin/wkhtmltopdf-amd64';
		$pdf->setOptions(array(
			'ignoreWarnings'	=> TRUE,
			'orientation'		=> 'landscape'
		));
		if ($this->settings['forcePdfDownload']) {
			$pdf->send($job->getInternalCode() . '.pdf');
		} else {
			$pdf->send();
		}
		exit();
	}
}
