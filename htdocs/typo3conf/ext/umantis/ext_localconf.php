<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Baumer.' . $_EXTKEY,
	'Jobs',
	[
		'Job' => 'list, show, showPdf, pdf',
	],
	// non-cacheable actions
	[
		'Job' => 'showPdf, pdf',
	]
);

/**
 * register the migration Command Controller
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \Baumer\Umantis\Command\UmantisCommandController::class;
