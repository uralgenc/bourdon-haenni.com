<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "static_info_tables_fr".
 *
 * Auto generated 04-08-2014 13:16
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Static Info Tables (fr)',
	'description' => 'French (fr) language pack for the Static Info Tables providing localized names for countries, currencies and so on.',
	'category' => 'misc',
	'version' => '6.0.2',
	'state' => 'stable',
	'uploadfolder' => true,
	'createDirs' => '',
	'clearcacheonload' => true,
	'author' => 'Francois Suter',
	'author_email' => 'typo3@cobweb.ch',
	'author_company' => 'Cobweb',
	'constraints' => 
	array (
		'depends' => 
		array (
			'static_info_tables' => '6.0.2-',
			'typo3' => '6.0.6-6.2.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

