<?php
// todo: add correct development domaains
$devDomains = [
    'baumer' => [
        'global' => 'dev.baumer.com',
    ],
    'bourdon' => [
        'global' => 'bourdondev.baumer.com',
        'de' => 'de.bourdondev.baumer.com',
        'fr' => 'fr.bourdondev.baumer.com'
    ]

];
$stagingDomains = [
    'baumer' => [
        'global' => 'staging.baumer.com'
    ],
    'bourdon' => [
        'global' => 'staging.bourdon.baumer.com',
        'de'     => 'de.staging.bourdon.baumer.com',
        'fr'     => 'fr.staging.bourdon.baumer.com'
    ]
];
$liveDomains = [
    'baumer' => [
        'global' => 'www.baumer.com'
    ],
    'bourdon' => [
        'global' => 'bourdon.baumer.com',
        'de'     => 'de.bourdon.baumer.com',
        'fr'     => 'fr.bourdon.baumer.com'
    ],
];
/**
 * !!! IMPORTANT !!!
 *
 * To enable your local domains add
 * the file LocalRealUrlDomains.php with following content:
 *
 * <?php
 * $localDevDomains = [
 *        'baumer' => 'myname.4pweb.vm.1drop.de',
 *        'bourdon' => 'bourdon.myname.4pweb.vm.1drop.de',
 * ]
 * ?>
 */
$localDevDomains = null;
$localRealUrlDomainsFile = TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('typo3conf/LocalRealUrlDomains.php');
if (file_exists($localRealUrlDomainsFile)) require_once(TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('typo3conf/LocalRealUrlDomains.php'));


// set domains based on request domains (environment)
if (preg_match('/dev.baumer.com/i', $_SERVER['HTTP_HOST'])) {
    $domains = $devDomains;
} elseif (preg_match('/4pweb.vm.1drop.de/i', $_SERVER['HTTP_HOST']) && is_array($localDevDomains)) {
    $domains = $localDevDomains;
} elseif (preg_match('/staging/i', $_SERVER['HTTP_HOST']) && is_array($localDevDomains)) {
    $domains = $stagingDomains;
} else {
    $domains = $liveDomains;
}

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT'] = [
    'init' => [
        'appendMissingSlash' => 'ifNotFile',
        'enableCHashCache' => true,
        'enableUrlDecodeCache' => false,
        'enableUrlEncodeCache' => false,
        'respectSimulateStaticURLs' => true,
        'reapplyAbsRefPrefix' => true,
    ],
    'redirects' => [],
    'preVars' => [
        '1' => [
            'GETvar' => 'no_cache',
            'valueMap' => [
                'nc' => '1',
            ],
            'noMatch' => 'bypass'
        ],
        '2' => [
            'GETvar' => 'type',
            'valueMap' => [
                'CsEndpoint' => '1346',
                'Configurator' => '1444292478'
            ],
            'noMatch' => 'bypass'
        ],

        // config for CsEndpoint plugin (type = 1346)
        '3' => [
            'cond' => [
                'prevValueInList' => '1346'
            ],
            'GETvar' => 'tx_baumer_csendpoint[action]'
        ],
        '4' => [
            'cond' => [
                'prevValueInList' => 'updateProduct,deleteProduct,updateView,deleteView'
            ],
            'GETvar' => 'tx_baumer_csendpoint[contentServId]'
        ],
        '5' => [
            'GETvar' => 'L',
            'valueMap' => [
                'cn' => '4',
                'en' => '0',
                'fr' => '3',
                'de' => '1',
                'it' => '5',
                'pt' => '6',
            ],
            'valueDefault' => 'en'
        ],
    ],
    'pagePath' => [
        'type' => 'user',
        'userFunc' => 'Tx\\Realurl\\UriGeneratorAndResolver->main',
        'spaceCharacter' => '-',
        'languageGetVar' => 'L',
        'autoUpdatePathCache' => true,
        'expireDays' => 3,
        'firstHitPathCache' => 1,
        'rootpage_id' => 1
    ],
    'fixedPostVars' => [
        '449' => 'MyProducts',
        '451' => 'configurator',
        'MyProducts' => [
            [
                'GETvar' => 'tx_baumer_productlistcheckout[controller]'
            ], [
                'GETvar' => 'tx_baumer_productlistcheckout[action]'
            ], [
                'GETvar' => 'tx_baumer_productlistcheckout[code]'
            ],
        ],
        'configurator' => [
            [
                'GETvar' => 'tx_baumer_csview[controller]',
            ], [
                'GETvar' => 'tx_baumer_csview[action]'
            ], [
                'GETvar' => 'tx_baumer_csview[csViewRecord]',
                'lookUpTable' => [
                    'table' => 'tx_baumer_domain_model_csviewrecord',
                    'id_field' => 'uid',
                    'alias_field' => 'label',
                    'useUniqueCache' => FALSE,
                    'enable404forInvalidAlias' => TRUE,
                    'autoUpdate' => TRUE,
                    'addWhereClause' => ' AND NOT deleted AND NOT hidden'
                ]
            ], [
                'GETvar' => 'tx_baumer_csview[shortCode]'
            ]
        ]

    ],
    'postVarSets' => [],
    'fileName' => [
        'defaultToHTMuffixOnPrev' => false,
        'acceptHTMLsuffix' => false,
        'index' => [
            'feed.rss' => [
                'keyValues' => [
                    'type' => 9818,
                ],
            ],
            'sitemap.xml' => [
                'keyValues' => [
                    'type' => 776,
                ],
            ],
        ],
    ],
];

/*
 * setting for baumer domains
 */
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['baumer']['global']] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT'];

/*
 * settings for bourdon domains
 */
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['global']] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT'];
// bourdon.baumer.com (global)
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['global']]['pagePath']['rootpage_id'] = 410;
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['global']]['preVars']['5']['valueMap'] = [
    'en' => '0',
    'de' => '1',
    'fr' => '3'
];
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['global']]['preVars']['5']['valueDefault'] = 'en';
// de.boudon.baumer.com
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['de']] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['global']];
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['de']]['pagePath']['rootpage_id'] = 467;
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['de']]['preVars']['5']['valueMap'] = [
    'en' => '0',
    'de' => '1',
];
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['de']]['preVars']['5']['valueDefault'] = 'de';
// fr.boudon.baumer.com
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['fr']] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['global']];
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['fr']]['pagePath']['rootpage_id'] = 466;
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['fr']]['preVars']['5']['valueMap'] = [
    'en' => '0',
    'fr' => '3'
];
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'][$domains['bourdon']['fr']]['preVars']['5']['valueDefault'] = 'fr';