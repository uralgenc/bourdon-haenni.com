<?php

use TYPO3\CMS\Core\Utility\GeneralUtility;

if (file_exists(GeneralUtility::getFileAbsFileName('typo3conf/SHARED_DB'))) {
	$GLOBALS['TYPO3_CONF_VARS']['DB']['host'] = '10.50.101.4';
	$GLOBALS['TYPO3_CONF_VARS']['DB']['database'] = 'baumer-group';
	$GLOBALS['TYPO3_CONF_VARS']['DB']['username'] = 'baumer-group';
	$GLOBALS['TYPO3_CONF_VARS']['DB']['password'] = 'baumer-group';
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] .= ' (on SHARED_DB)';
}

$tikaConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tika']);
if (!file_exists(GeneralUtility::getFileAbsFileName('tika-app.jar'))) {
	$tikaConfiguration['extractor'] = 'solr';
} else {
	$tikaConfiguration['extractor'] = 'jar';
	$tikaConfiguration['tikaPath'] = GeneralUtility::getFileAbsFileName('tika-app.jar');
}
$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tika'] = serialize($tikaConfiguration);


if (\TYPO3\CMS\Core\Core\Bootstrap::getInstance()->getApplicationContext()->isDevelopment()) {
	/**
	 * This override adds the possibility to use multitree subdomains without domain records (for SHARED_DB, vagrant, etc.)
	 * You can override the mappings or undo the override by creating a config file inside conf.d
	 *
	 * The override PageRepository checks for $subdomain . exec('hostname -f')
	 * e.g. in vagrant flastname.4pweb.vm.1drop.de is FQDN hostname, so you will get de.flastname.4pweb.vm.1drop.de etc.
	 */
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['domainMapping'] = [
		'de' => 98,
		'ch' => 99,
		'cn' => 129,
		'us' => 91,
		'fr' => 134,
		'in' => 161,
		'it' => 140,
		'uk' => 201,
		'vn' => 207,
		'br' => 116,
		'pl' => 155,
		'kr' => 194,
		'es' => 200,
		'ca' => 122,
		'bene' => 110,
		'asean' => 188,
		'sca' => 182,
		'bourdon' => 410,
		'mena' => 146,
		'de.bourdon' => 467,
		'fr.bourdon' => 466,
		'be.bourdon' => 516,
		'ca.bourdon' => 517,
		'in.bourdon' => 469,
		'mena.bourdon' => 470,
		'asean.bourdon' => 471,
		'ch.bourdon' => 465,
		'us.bourdon' => 468,
		'vn.bourdon' => 472,
		'cn.bourdon' => 540,
		'dk.bourdon' => 541,
		'es.bourdon' => 542,
		'kr.bourdon' => 543,
		'uk.bourdon' => 544
	];
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Frontend\\Page\\PageRepository']['className'] = 'Baumer\\Baumer\\Overrides\\PageRepository';
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['cookieDomain'] = '.' . exec('hostname -f');
	$GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = TRUE;
	$GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = TRUE;
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 'file';
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = 1;
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['clearCacheSystem'] = TRUE;
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 1;
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = 0;
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['l10n']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\NullBackend';
}

/**
 * !!! IMPORTANT !!!
 *
 * To enable your local configuration add
 * one or more files in conf.d as they are
 * loaded automatically.
 */
$configurationFiles = GeneralUtility::getFilesInDir(GeneralUtility::getFileAbsFileName('typo3conf/conf.d/'), 'php', TRUE);
foreach ($configurationFiles as $file) {
	require_once($file);
}

//TODO: language and subdomain handling
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('plugin.tx_solr.index.queue.pages.indexer.frontendDataHelper.host = ' . exec('hostname -f'));
