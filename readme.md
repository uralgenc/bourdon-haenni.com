# Baumer Group Development Setup

## Installation

As bundler, and librarian-puppet are having trouble with Ruby 2.2 it's highly recommended to install
[RVM](https://rvm.io/) and use a custom ruby environment. The project comes with a ```.ruby-version``` file
that defines the compatible version of ruby.

First install all needed dependencies (bundler installs gems, the librarian gem installs puppet modules):

    bundle install
    cd puppet
    librarian-puppet install

If you need any other gems, add them to the Gemfile and run ```bundle install```.
This will update the Gemfile.lock file and install the dependency.
After this please both the Gemfile and Gemfile.lock have to be commited to the repository.

Copy the puppet/hieradata/local.yaml.example file to puppet/hieradata/local.yaml and change the username accordingly.

Then a ```vagrant up``` should install all needed modules and servers.

## Composer

TYPO3 and non-modified extensions are installed using composer.

When modifying composer.json, remember to generate an updated version of composer.lock (using ```composer update```) and commit both files to Git.

Also, remember to add every Composer-installed extension to ```htdocs/typo3conf/ext/.gitignore```.

## Baumer Group Template

This template based on bootstrap3 is built using compass and scss

### Asset Management

Required frontend dependencies are also maintained using composer. [Components installer](https://github.com/RobLoach/component-installer) is used to install dependencies into the provider extension.

### Usage

During development, use Compass to convert the SCSS files to CSS.

	compass watch

## ANT

There's also a `build.xml` containing serveral tasks that should make your life easier.

    ant -p

Will show you which tasks are available



## Docker to test puppet

    docker build -t debian-puppet:jessie .
    docker run -ti --name 4pweb -v .:/vagrant debian-puppet bash
    cd /vagrant/puppet
    puppet apply -vv --manifestdir `pwd` --modulepath modules --hiera_config hiera.yaml --environment development site.pp
