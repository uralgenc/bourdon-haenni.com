# == Class: php::apt
#
# Configure dotdeb package repository
#
# === Parameters
#
# No parameters
#
# === Variables
#
# No variables
#
# === Examples
#
#  include php::apt
#
# === Authors
#
# Christian "Jippi" Winther <jippignu@gmail.com>
#
# === Copyright
#
# Copyright 2012-2015 Christian "Jippi" Winther, unless otherwise noted.
#
class php::apt(
  $location     = 'https://packages.sury.org/php/',
  $release      = 'jessie',
  $repos        = 'main',
  $include_src  = false,
  $sury       = true
) {

  if ($sury) {
    # wheezy-php55 requires both repositories to work correctly
    # See: http://www.dotdeb.org/instructions/
    if $release == 'jessie' {
      apt::source { 'sury':
        location    => $location,
        release     => 'jessie',
        repos       => $repos,
        include_src => $include_src
      }
    }

    exec { 'add_sury_key':
      command =>
      'curl -L --silent "https://packages.sury.org/php/apt.gpg" | apt-key add -',
      unless  => 'apt-key list | grep -q sury',
      path    => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ];
    }

  }

}
