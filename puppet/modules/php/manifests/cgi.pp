# == Class: php::cgi
#
# Install and configure php CLI
#
# === Parameters
#
# [*ensure*]
#   The PHP ensure of PHP CLI to install
#
# [*package*]
#   The package name for PHP CLI
#   For debian it's php5-cgi
#
# [*provider*]
#   The provider used to install php5-cgi
#   Could be "pecl", "apt" or any other OS package provider
#
# [*inifile*]
#   The path to the ini php5-cgi ini file
#
# [*settings*]
#   Hash with 'set' nested hash of key => value
#   set changes to agues when applied to *inifile*
#
# === Variables
#
# No variables
#
# === Examples
#
#  include php::cgi
#
# === Authors
#
# Christian "Jippi" Winther <jippignu@gmail.com>
#
# === Copyright
#
# Copyright 2012-2015 Christian "Jippi" Winther, unless otherwise noted.
#
class php::cgi(
  $ensure   = $php::cgi::params::ensure,
  $package  = $php::cgi::params::package,
  $provider = $php::cgi::params::provider,
  $inifile  = $php::cgi::params::inifile,
  $settings = $php::cgi::params::settings
) inherits php::cgi::params {

  php::contrib::base_package { 'cgi':
    ensure   => $ensure,
    provider => $provider;
  } ->

  package { $package:
    ensure   => $ensure,
    provider => $provider;
  } ->

  php::cgi::config { 'php-cgi':
    file   => $inifile,
    config => $settings
  }

}
