# == Class: php::params
#
# PHP params class
#
# Configuration class for php module
#
# === Parameters
#
# No parameters
#
# === Variables
#
# [*ensure*]
#   The PHP ensure of PHP to install
#
# === Examples
#
#  include php::dev
#
# === Authors
#
# Christian "Jippi" Winther <jippignu@gmail.com>
#
# === Copyright
#
# Copyright 2012-2015 Christian "Jippi" Winther, unless otherwise noted.
#
class php::params {

  $php_version_maj = '5'

  $ensure = 'installed'

  $config_root = '/etc/php5'

  if $::php_version == '' or versioncmp($::php_version, '5.4') >= 0 {
    $config_root_ini = "${::php::params::config_root}/mods-available"
  } else {
    $config_root_ini = "${::php::params::config_root}/conf.d"
  }

  $augeas_contrib_dir = '/usr/share/augeas/lenses/contrib'
  $settings = [
    'set "PHP/short_open_tag" "Off"',
    'set "PHP/asp_tags" "Off"',
    'set "PHP/expose_php" "Off"',
    'set "PHP/memory_limit" "1G"',
    'set "PHP/display_errors" "Off"',
    'set "PHP/log_errors" "On"',
    'set "PHP/post_max_size" "500M"',
    'set "PHP/upload_max_filesize" "500M"',
    'set "PHP/max_execution_time" "600"',
    'set "PHP/max_input_vars" "15000"',
    'set "PHP/allow_url_include" "Off"',
    'set "PHP/output_buffering" "4096"',
    'set "PHP/output_handler" "Off"',
    'set "Date/date.timezone" "Europe/Berlin"',
    'clear "PHP/user_ini.filename"',
  ]
}
