class baumer::base (
    $location     = 'https://packages.sury.org/php/',
    $release      = $::lsbdistcodename,
    $repos        = 'main',
    $include_src  = false,
) {

    package { ['apt-transport-https', 'lsb-release']:
        ensure => 'installed',
        before => Apt::Source['sury']
    }



    apt::source { 'sury':
        location => $location,
        release  => $release,
        repos    => $repos,
        include  => {
            'src' => $include_src,
        },
        notify   => [Exec['apt_update']],
        key      => {
            id     => 'DF3D585DB8F0EB658690A554AC0E47584A7A714D',
            source => 'https://packages.sury.org/php/apt.gpg',
        },
    }

    apt::pin { 'testing':
        packages => 'libpcre3',
        priority => 995,
        release => 'testing',
        require => Apt::Source['debian_testing']
    }

    apt::source { 'debian_testing':
        location => 'http://ftp2.de.debian.org/debian/',
        release  => 'testing',
        repos    => 'main',
        pin      => '-10',
        include  => {
            'src' => true,
            'deb' => true
        },
        require  => [Apt::Source['sury']]
    }-> Exec['apt_update']

    $default_packages = hiera_array('default-packages')
    ensure_packages(
        $default_packages,
        {
            ensure  => installed,
            require => [Apt::Source['sury'], Exec['apt_update']],
        }
    )
}
