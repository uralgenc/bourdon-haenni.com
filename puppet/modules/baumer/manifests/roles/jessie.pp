# == Class: Baumer::Roles::Jessie
# Jessie VM with:
# - Web (Nginx, PHP-FPM)
# - Elasticsearch
class baumer::roles::jessie (
) {
  include baumer::roles::base
  include baumer::elasticsearch
  include baumer::web
}
Class['baumer::roles::base'] -> Class['baumer::roles::jessie'] -> Class['baumer::web']
