
class baumer::roles::base{
	include apt
	include augeas
	include baumer::base
}

Class['apt'] -> Class['augeas'] -> Class['baumer::base'] -> Class['baumer::roles::base']
