class baumer::roles::dev(
) {
    include baumer::roles::base
    include baumer::roles::database
    include baumer::roles::jessie
    # include baumer::roles::elasticsearch
    include baumer::dev
}
Class['baumer::roles::base'] -> Class['baumer::roles::dev']
