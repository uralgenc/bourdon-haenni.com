# == Class: Baumer::Web
class baumer::web (
    $projectroot,
    $clone_project,
    $basedomain,
    $php_packages = [],
    $typo3_context = 'Production',
    $elasticsearch_backend = 'http://localhost:9200',
    $auth_basic = undef,
    $auth_basic_user_file = undef,
    $server_aliases = [],
    $source = 'git@gitlab.com:baumer/4pweb.baumer.com.git',
    $revision = 'master',
    $content_serv = undef,
    $fpm_socket = '/var/run/php7.0-fpm.sock',
    $project_user = 'www-data',
    $force_chown = true,
    $use_composer = false
) {

    validate_bool($clone_project)
    validate_array($content_serv)
    validate_array($server_aliases)
    validate_absolute_path($fpm_socket)

    require baumer::base

    include nginx
    include php
    include php::cli
    include php::fpm
    include php::extension::gd
    include php::extension::curl
    include php::extension::mcrypt
    include php::extension::intl
    include php::extension::mysql
    include php::extension::redis
    include php::extension::tidy
    include php::composer
    include php::composer::auto_update
    # contain bundler

    package {'libpcre3/testing':
        before => [Class['php::fpm::package']],
        require => Apt::Source['debian_testing'],
    }


    Class['baumer::base'] -> Class['php']
    Class['baumer::base'] -> Class['php::extension::gd']
    Class['baumer::base'] -> Class['php::extension::curl']
    Class['baumer::base'] -> Class['php::extension::mcrypt']
    Class['baumer::base'] -> Class['php::extension::intl']
    Class['baumer::base'] -> Class['php::extension::mysql']
    Class['baumer::base'] -> Class['php::extension::redis']
    Class['baumer::base'] -> Class['php::extension::tidy']
    Class['baumer::base'] -> Class['php::composer']
    Class['baumer::base'] -> Class['php::cli']
    Class['php::cli'] -> Class['php::fpm::package'] -> Class['php::fpm']

    php::fpm::pool { 'www':
        ensure       => 'present',
        listen       => $fpm_socket,
        listen_owner => 'www-data',
        listen_group => 'www-data',
        listen_mode  => '0660',
        require      => Class['baumer::base']
    }

    package { $php_packages :
        ensure  => installed,
        require      => Class['baumer::base']
    }

    package { 'nginx-module-geoip':
        ensure => installed,
        require => Class['baumer::base']
    }

    file { '/etc/nginx/fastcgi_params':
        ensure  => present,
        source  => 'puppet:///modules/baumer/nginx/fastcgi_params',
        require => Package['nginx'],
    }
    file { '/etc/nginx/sites-available/baumer.conf':
        ensure  => present,
        content => template('baumer/nginx/sites-available/baumer.conf.erb'),
        require => [Package['nginx'], File['/etc/nginx/fastcgi_params']],
        notify  => [Service['nginx']],
    }
    file { '/etc/nginx/sites-enabled/baumer.conf':
        ensure  => link,
        target  => '/etc/nginx/sites-available/baumer.conf',
        require => File['/etc/nginx/sites-available/baumer.conf'],
        notify  => [Service['nginx']],
    }
    file { '/var/www':
        ensure => 'directory',
    }->
    file { '/var/www/.ssh':
        ensure => 'directory',
        mode   => '0700',
        owner  => $project_user,
        group  => $project_user,
    }->
    file { '/var/www/.ssh/config':
        ensure  => 'present',
        mode    => '0600',
        owner   => $project_user,
        group   => $project_user,
        content => "Host gitlab.com\n\tStrictHostKeyChecking no\n",
    }

    if $projectroot {

        if $clone_project {
            vcsrepo { $projectroot:
                ensure   => present,
                provider => git,
                source   => $source,
                revision => $revision,
                require  => [
                    File['/var/www/.ssh/config'],
                ],
            }
            file { "${projectroot}/puppet":
                ensure  => absent,
                force   => true,
                require => Vcsrepo[$projectroot],
            }
            File[$projectroot] {
                require => [Vcsrepo[$projectroot]],
            }
        }
        file { $projectroot:
            ensure  => 'directory',
        }
        if $force_chown {
            File[$projectroot] {
                owner   => $project_user,
                group   => $project_user,
                recurse => true,
            }
        }

        Exec {
            path => ['/bin/', '/usr/bin', '/usr/local/bin'],
        }

        exec { 'composer install':
          command     => 'composer install --no-dev --ignore-platform-reqs',
          cwd         => $projectroot,
          environment => "HOME=${projectroot}",
          require     => Class['php::composer'],
          refreshonly => true,
        }
        if $use_composer and $clone_project {
            file { "${projectroot}/.ssh":
                ensure => 'directory',
                mode   => '0700',
                owner  => $project_user,
                group  => $project_user,
            }->
            file { "${projectroot}/.ssh/config":
                ensure  => 'present',
                mode    => '0600',
                owner   => $project_user,
                group   => $project_user,
                content => "Host gitlab.com\n\tStrictHostKeyChecking no\n",
            }
            File["${projectroot}/.ssh/config"]->Exec['composer install']
            Vcsrepo[$projectroot] ~> Exec['composer install']
        }
    }
}
