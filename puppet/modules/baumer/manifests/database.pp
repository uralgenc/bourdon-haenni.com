class baumer::database(
  $databases = {},
){
  create_resources ('mysql::db', $databases)
}
Class['baumer::base'] -> Class['baumer::database']
