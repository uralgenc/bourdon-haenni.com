class baumer::elasticsearch(
  $instances = {},
  $templates = {},
  $es_clustername = 'baumer',
  $disk_threshold_active = true,
  $es_ip = '127.0.0.1',
  $package_url = 'https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.3.0/elasticsearch-2.3.0.deb',
) {
  class { '::elasticsearch':
    status      => 'running',
    autoupgrade => true,
    package_url => $package_url,
    java_install => true
  }
  if $::lsbdistcodename == 'jessie' {
    file { "/etc/systemd/system/elasticsearch-${es_clustername}.service.d":
      ensure => 'directory',
    }->
    file { "/etc/systemd/system/elasticsearch-${es_clustername}.service.d/override.conf":
      ensure => 'file',
      source => 'puppet:///modules/baumer/systemd_override.conf',
      notify => [Exec["systemd_reload_${es_clustername}"]],
    }
  }
  elasticsearch::instance{$es_clustername:
    config => {
      'cluster' => {
        'name'                     => $es_clustername,
        'index.number_of_shards'   => 1,
        'index.number_of_replicas' => 0,
        'routing'                  => {
          'allocation' => {
            'node_initial_primaries_recoveries' => 18,
            'disk'                              => {
              'threshold_enabled' => $disk_threshold_active,
            },
          },
        },
      },
      'network' => {
        'host' => $es_ip,
      },
      'path.repo' => ['/usr/share/elasticsearch/backup']
    },
  }
  elasticsearch::plugin{'lmenezes/elasticsearch-kopf':
    instances  => $es_clustername,
  }

  create_resources('::elasticsearch::template', $templates)
}
Class['baumer::base'] -> Class['baumer::elasticsearch']
