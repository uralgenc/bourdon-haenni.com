class baumer::dev(
  $additional_webroot = '/var/www',
) {
  # include mailcatcher
  # include php::dev
  # include php::pear
  include timezone

  user { 'vagrant':
    ensure   => present,
    home     => '/home/vagrant',
    password => 'vagrant',
    shell    => '/bin/bash',
  }

  #	class { 'xhprof':
  #		require => File[$additional_webroot],
  #		service => 'nginx',
  #	}
  $docroot_xhprof = "${additional_webroot}/xhprof/xhprof_html/"

  $packages = [
    'graphviz',
  ]
  package { $packages:
    ensure  => 'present'
  }

  #
  # link /etc/hiera.yaml to ours to test hiera queries
  # inside the vm
  #
  file { '/etc/hiera.yaml':
    ensure => link,
    target => '/vagrant/puppet/hiera.yaml',
  }

  file { '/usr/bin/php-debug':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => "#!/bin/sh\nXDEBUG_CONFIG=\"idekey=PHPSTORM\" /usr/bin/php -dxdebug.remote_enable=1 -dxdebug.remote_host=1.1.1.1 \$@",
  }

  # class { 'php::extension::xdebug' :
  #   package  => 'xdebug',
  #   require  => [Class['php::fpm'], Class['php::pear']],
  #   provider => 'pecl',
  #   inifile  => '/etc/php5/fpm/conf.d/80-xdebug.ini',
  #   notify  => [Class['::php::fpm']],
  #   settings => [
  #     "set .anon/zend_extension '/usr/lib/php5/20131226/xdebug.so'",
  #     "set .anon/xdebug.max_nesting_level '1000'",
  #     "set .anon/xdebug.default_enable '1'",
  #     "set .anon/xdebug.remote_enable '1'",
  #     "set .anon/xdebug.remote_handler 'dbgp'",
  #     "set .anon/xdebug.remote_connect_back '1'",
  #     "set .anon/xdebug.remote_port '9000'",
  #     "set .anon/xdebug.remote_autostart '0'",
  #     "set .anon/html_errors 'On'",
  #     "set .anon/display_errors 'On'",
  #   ],
  # }

  file { '/etc/nginx/sites-available/xhprof.conf':
    ensure  => present,
    content => template('baumer/nginx/sites-available/xhprof.conf.erb'),
    require => Package['nginx'],
    notify  => [Service['nginx']],
  }
  file { '/etc/nginx/sites-enabled/xhprof.conf':
    ensure  => link,
    target  => '/etc/nginx/sites-available/xhprof.conf',
    require => File['/etc/nginx/sites-available/xhprof.conf'],
    notify  => [Service['nginx']],
  }

}
Class['baumer::base'] -> Class['baumer::dev']
