FROM debian:jessie

RUN apt-get update && apt-get install -y puppet git
RUN apt-get install -y ruby-dev
RUN gem install hiera hiera-puppet
