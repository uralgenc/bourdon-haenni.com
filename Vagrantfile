# -*- mode: ruby -*-
# vi: set ft=ruby :

require "yaml"

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

local = YAML.load_file(File.join(File.dirname(__FILE__), 'puppet/hieradata/local.yaml'))
common = YAML.load_file(File.join(File.dirname(__FILE__), 'puppet/hieradata/environment/development.yaml'))

$username = local['config']['username']
$domain = $username + '.' + common['baumer::web::basedomain']

subdomains = %w(profile prod ve gb ch es kr se-asia scan pl mid-east it in de fr cn ca br benelux us global bourdon en.bourdon de.bourdon fr.bourdon)
subdomains.map! { |subdomain|
  subdomain + "\." + $domain
}

subdomains.push($domain)


# Check for missing plugins
required_plugins = %w(vagrant-hostsupdater)
plugin_installed = false
required_plugins.each do |plugin|
	unless Vagrant.has_plugin?(plugin)
		system "vagrant plugin install #{plugin}"
		plugin_installed = true
	end
end

# If new plugins installed, restart Vagrant process
if plugin_installed === true
	exec "vagrant #{ARGV.join' '}"
end

Vagrant.require_version ">= 1.5"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "debian/jessie64"
  #config.vm.box_url = "file:///home/user/vagrant/baumer/debian-jessie64.box

  config.vm.hostname = $domain

  config.hostsupdater.aliases = subdomains

  # prevent "stdin is not a tty" warning on debian
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  config.ssh.forward_agent = true

  config.vm.network "private_network", ip: "1.1.1.99"
  config.vm.network "public_network"

  config.vm.provider 'virtualbox' do |vb|
    vb.gui = false
    vb.customize ['modifyvm', :id, '--memory', '2048']
    vb.customize ['modifyvm', :id, '--cpus', '2']
    vb.customize ['modifyvm', :id, '--ioapic', 'on']
  end

  config.vm.provision :shell do |shell|
    shell.path = "utils/setup.sh"
  end

  config.vm.provision "puppet" do |puppet|
    puppet.manifests_path = "puppet"
    puppet.manifest_file  = "site.pp"
    puppet.module_path = ['puppet/modules']
    puppet.options = "--hiera_config /vagrant/puppet/hiera.yaml --environment development"
  end
  config.vm.synced_folder ".", "/vagrant", type: "nfs", mount_options: ['rw', 'vers=3' 'udp', 'noatime']
  config.vm.provision 'shell', path: 'utils/afterStart.sh', :privileged => false, :run => 'always'
end
